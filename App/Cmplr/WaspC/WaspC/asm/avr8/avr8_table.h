// File: avr8_table.h
#ifndef _avr8_table_h_
#define	_avr8_table_h_

typedef struct _avr8_param_store
{
	byte	Addr;
	byte	Size;
}avr8_param_store;

typedef struct _avr8_param
{
	int		Begin;
	int		End;
	avr8_param_store	Store[2];
}avr8_param;

typedef struct _avr8_opcode
{
	char*		Name;
	char		Type;
	byte		Size;
	uint32		Value;
	avr8_param	Param1;
	avr8_param	Param2;
}avr8_opcode;

#define	opcode2(value)	\
	'o', 16, value

#define	opcode4(value)	\
	'o', 32, value

#define	param2(begin, end, addr1, bits1, addr2, bits2)	\
	{begin, end, {{addr1, bits1}, {addr2, bits2}}}

#define	param1(begin, end, addr1, bits1)	\
	param2(begin, end, addr1, bits1, 0, 0)

#define	noparam	\
	param1(0, -1, 0, 0)

avr8_opcode avr8_table[] =
{
	/* Name			Opcode						Param1							Param2					*/
	{"adc r,r",		opcode2(0x1C00),		param1(0, 31, 4, 5),			param2(0, 31, 0, 4, 9, 1)},
	{"add r,r",		opcode2(0x0C00),		param1(0, 31, 4, 5),			param2(0, 31, 0, 4, 9, 1)},
	{"adiw R,K",	opcode2(0x9600),		param1(12, 15, 4, 2),			param2(0, 63, 0, 4, 6, 2)},
	{"and r,r",		opcode2(0x2000),		param1(0, 31, 4, 5),			param2(0, 31, 0, 4, 9, 1)},
	{"andi r,K",	opcode2(0x7000),		param1(16, 31, 4, 4),			param2(0, 255, 0, 4, 8, 4)},
	{"asr r",		opcode2(0x9405),		param1(0, 31, 4, 5),			noparam},
	{"bclr s",		opcode2(0x9488),		param1(0, 7, 4, 3),				noparam},
	{"bld r,b",		opcode2(0xF800),		param1(0, 31, 4, 5),			param1(0, 7, 0, 3)},
	{"brbc s,k",	opcode2(0xF400),		param1(0, 7, 0, 3),				param1(-64, 63, 3, 7)},
	{"brbs s,k",	opcode2(0xF000),		param1(0, 7, 0, 3),				param1(-64, 63, 3, 7)},
	{"brcc k",		opcode2(0xF400),		param1(-64, 63, 3, 7),			noparam},
	{"brcs k",		opcode2(0xF000),		param1(-64, 63, 3, 7),			noparam},
	{"break",		opcode2(0x9598),		noparam,						noparam},
	{"breq k",		opcode2(0xF001),		param1(-64, 63, 3, 7),			noparam},
	{"brge k",		opcode2(0xF404),		param1(-64, 63, 3, 7),			noparam},
	{"brhc k",		opcode2(0xF405),		param1(-64, 63, 3, 7),			noparam},
	{"brhs k",		opcode2(0xF005),		param1(-64, 63, 3, 7),			noparam},
	{"brid k",		opcode2(0xF407),		param1(-64, 63, 3, 7),			noparam},
	{"brie k",		opcode2(0xF007),		param1(-64, 63, 3, 7),			noparam},
	{"brlo k",		opcode2(0xF000),		param1(-64, 63, 3, 7),			noparam},
	{"brlt k",		opcode2(0xF004),		param1(-64, 63, 3, 7),			noparam},
	{"brmi k",		opcode2(0xF002),		param1(-64, 63, 3, 7),			noparam},
	{"brne k",		opcode2(0xF401),		param1(-64, 63, 3, 7),			noparam},
	{"brpl k",		opcode2(0xF402),		param1(-64, 63, 3, 7),			noparam},
	{"brsh k",		opcode2(0xF400),		param1(-64, 63, 3, 7),			noparam},
	{"brtc k",		opcode2(0xF406),		param1(-64, 63, 3, 7),			noparam},
	{"brts k",		opcode2(0xF006),		param1(-64, 63, 3, 7),			noparam},
	{"brvc k",		opcode2(0xF403),		param1(-64, 63, 3, 7),			noparam},
	{"brvs k",		opcode2(0xF003),		param1(-64, 63, 3, 7),			noparam},
	{"bset s",		opcode2(0x9408),		param1(0, 7, 4, 3),				noparam},
	{"bst r,b",		opcode2(0xFA00),		param1(0, 31, 4, 5),			param1(0, 7, 0, 3)},
	{"call k",		opcode4(0x940E0000),	param2(0, 4194303, 0, 17, 20, 5),	noparam},
	{"cbi A,b",		opcode2(0x9800),		param1(0, 31, 3, 5),			param1(0, 7, 0, 3)},
	// cbr r,K = andi r,~K
	{"clc",			opcode2(0x9488),		noparam,						noparam},
	{"clh",			opcode2(0x94D8),		noparam,						noparam},
	{"cli",			opcode2(0x94F8),		noparam,						noparam},
	{"cln",			opcode2(0x94A8),		noparam,						noparam},
	// clr r = eor r,r
	{"cls",			opcode2(0x94C8),		noparam,						noparam},
	{"cls",			opcode2(0x94C8),		noparam,						noparam},
};

#endif
