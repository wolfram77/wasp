1. There must only be classes, not structures. User gets to decide whether it be as value or reference.
2. What about constructors as namespaces.
3. Examples:

1. a xor b
Functions may be more readable if Fn(params) is not the only way of calling a function.
:. Functions should be allowed to specify one (or many) methods of calling them. (call specification)

2. /// doc. comment
It could be great if the language supported specific format of description of comments (for intellisense).
:. Each function should have an information speciffication (like reference citations)

3. struct vs class?
Structures and classes represent equivalent concepts except that structures do not have any associated functions.
:. Use only classes or structures.

4. -> vs .
There is a confusion between using by value and by reference.
:. This should be left out to the user, though it maybe enforced in some way.

5. str.Replace()
There is a problem when replace leaves out the original string unchanged when that is desired.
The appropriate behaviour may be chosen with flags such as var <= str.Replace() (copy).

Functions like a xor be should be grouped inside classes of type a or type b.

All classes / function should be written as templates so as to enable wide reuse.

Primitive datatypes can also behave as classes. (minus is an operation on the primitive datatype (which is by value))

A particular function can be embedded into many classes from one place.

Multiple inheritance needs to be supported, but should aallow selectivity to avoid collision.

Single function inheritance can be supported through import.

Visibility specifiers should be more accurate.

Interface can be supported by use of function numbers???

Functions can return a set of data (not just one)

data set can be represented by {a, b, c, d}

It should be allowed to set additional properties to an object.

The thing about namespace and their specification

Need to specify properties of a class, a function, an object, a value.

In order to make a language to language converter, i should be able to detect
syntax of the new language and generate syntax for the old language from there.


basic format
import xxx

namespace x
{
	class/type a
	{
		properties
		functions
	}
}

Operators
=+

String representation

(1.{12}...)*10^11


Specifying properties to something

{propA, propB, propC=value, propD=value}: "string"

{propA, propB, propC=value, propD=value}: {"string" "" ""}

More flexible macros for defining a set of properties by one word.




Sample program
..............
import "stdlib.h":all;

namespace Data.Collection
{
	class Stack<type>
	{
		private:
		{
		packed var<type>	Data[capacity];
		packed int			Top, Capacity;
		}

		public inline:
		{
		Stack(int capacity [1], int dummy = 0)
		{
			[1] Maximum capacity of the stack
		}
		{
			Top = -1;
			Capacity = capacity;
		}
		~Stack()
		{
			free(Data);
		}

		// Peek item (pointer) at top of stack
		void* Peek()
		{
			if(Top == -1) return null;
			return Data[Top];
		}

		// Push item (pointer)
		void Push(void* item)
		{
			if(Top == Capacity)
			{
				Capacity <<= 1;
				Data = (void**) realloc(Data, Capacity);
			}
			Data[++Top] = item;
		}

		// Pop item (pointer)
		void* Pop()
		{
			if(Top == -1) return null;
			return Data[Top--];
		}
		}
	};
}

Resulting program
.................
<nothing>


