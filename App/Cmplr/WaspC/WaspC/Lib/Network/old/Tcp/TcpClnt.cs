﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Wasp.Data.Collection;

namespace Wasp.Network
{
	public class TcpClnt : NetClient
	{
		protected NetBuffer buff;
		public TcpHostInfo HostInfo;
		public TcpHost Host
		{
			get
			{ return (HostInfo != null) ? HostInfo.Host : null; }
		}
		public bool Connected
		{
			get
			{ return (sckt != null) && sckt.Connected; }
		}
		public bool FullyConnected
		{
			get
			{
				bool cnctd = (sckt != null) && sckt.Connected;
				if ( cnctd )
				{
					try { send(new NetPacket(new byte[0])); }
					catch ( Exception ) { cnctd = false; }
				}
				return cnctd;
			}
		}
		protected const int defBufferSize = 16384;
		protected const int defBufferTotalFree = defBufferSize / 2;


		// Create a new Client from Endpoint, or Socket
		public TcpClnt(IPEndPoint addr)
			: base(addr)
		{
		}
		public TcpClnt(IPAddress addr, int port)
			: base(addr, port)
		{
		}
		public TcpClnt(Socket sckt)
			: base(sckt)
		{
			addr = sckt.RemoteEndPoint;
		}

		// Connect to the Endpoint, if not yet connected
		protected void connect()
		{
			if ( !FullyConnected )
			{
				if ( sckt != null ) disconnect(true);
				sckt = new Socket(addr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
				sckt.Connect(addr);
				if ( EventFn.ProcessReceive != null ) buff = new NetBuffer(defBufferSize);
				ThreadPool.QueueUserWorkItem(new WaitCallback(thread_Receive));
			}
			if ( HostInfo != null )
			{
				HostInfo.Client.Remove(this);
				HostInfo.Client.Add(this);
			}
		}
		protected void task_Connect(object obj)
		{
			try
			{
				connect();
				if ( EventFn.Connected != null )
					EventFn.Connected(this, new NetEventArgs(NetEvent.Connected, this));
			}
			catch ( Exception err )
			{
				if ( EventFn.ConnectionFailed != null )
					EventFn.ConnectionFailed(this, new NetEventArgs(NetEvent.ConnectionFailed, this, err));
			}
		}
		public void Connect(bool async = false)
		{
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_Connect));
			else connect();
		}

		// Receive data
		protected NetPacket receiveStd()
		{
			sckt.Receive(new byte[0]);
			byte[] data = new byte[sckt.Available];
			sckt.Receive(data);
			return new NetPacket(this, data);
		}
		protected NetPacket receivePkt()
		{
			NetPacket pkt = null;
			// if some unprocessed data in buffer, process it
			if ( buff.Used > 0 )
			{
				if ( EventFn.ProcessReceive == null )
				{
					buff.BlockLength = buff.Used;
					buff.BlockFound = true;
				}
				else if ( !buff.BlockStarted )
				{ EventFn.ProcessReceive(this, new NetEventArgs(NetEvent.ProcessRecieve, buff)); }
			}
			// if block not yet found after processing, get fresh data from client
			if ( !buff.BlockFound )
			{
				// make sure there is sufficient free space
				sckt.Receive(new byte[0]);
				int avail = sckt.Available;
				if ( avail > buff.Free )
				{
					if ( avail <= buff.TotalFree ) buff.Reorder();
					else buff.TotalFree = avail + defBufferTotalFree;
				}
				// load data, and process it
				avail = sckt.Receive(buff.Data, buff.End, avail, SocketFlags.None);
				if ( avail > 0 )
				{
					buff.End += avail;
					if ( EventFn.ProcessReceive == null )
					{
						buff.BlockLength = buff.Used;
						buff.BlockFound = true;
					}
					else
					{ EventFn.ProcessReceive(this, new NetEventArgs(NetEvent.ProcessRecieve, buff)); }
				}
			}
			// get packet, if block found
			pkt = (buff.BlockFound) ? new NetPacket(this, buff.GetBlock()) : null;
			return pkt;
		}
		protected void thread_Receive(object obj)
		{
			try
			{
				while ( true )
				{
					NetPacket pkt;
					if ( EventFn.ProcessReceive != null ) pkt = receivePkt();
					else pkt = receiveStd();
					if ( pkt == null ) continue;
					if ( HostInfo != null )
					{
						if ( !HostInfo.ClientAccess ) HostInfo.Rcvd.Add(pkt);
						else rcvd.Add(pkt);
						lock ( HostInfo.PktLock )
						{ HostInfo.RcvPkts++; }
					}
					else rcvd.Add(pkt);
					rcvPkts++;
				}
			}
			catch ( Exception ) { }
		}

		// Send data
		protected void send(NetPacket pkt)
		{
			int sent, totalsent = 0;
			do
			{
				sent = sckt.Send(pkt.Data, totalsent, pkt.Size - totalsent, SocketFlags.None);
				totalsent += sent;
			} while ( sent > 0 );
			if ( totalsent != pkt.Size )
				throw new SocketException((int) SocketError.MessageSize);
			sndPkts++;
			if ( HostInfo != null )
			{
				lock ( HostInfo.PktLock )
				{ HostInfo.SndPkts++; }
			}
		}
		protected void task_Send(object obj)
		{
			try
			{
				send((NetPacket) obj);
				if ( EventFn.Sent != null )
					EventFn.Sent(this, new NetEventArgs(NetEvent.Sent, obj));
			}
			catch ( Exception err )
			{
				if ( EventFn.SendFailed != null )
					EventFn.SendFailed(this, new NetEventArgs(NetEvent.SendFailed, obj, err));
			}
		}
		public override void Send(NetPacket pkt, bool async = false)
		{
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_Send), pkt);
			else send(pkt);
		}

		// Disconnect client
		protected new void disconnect(bool fast = false)
		{
			base.disconnect(fast);
			buff.Close();
			buff = null;
			if ( HostInfo != null && HostInfo.AutoRemove )
				HostInfo.Client.Remove(this);
		}
		protected void task_Disconnect(object obj)
		{
			disconnect((bool) obj);
			if ( EventFn.Disconnected != null )
				EventFn.Disconnected(this, new NetEventArgs(NetEvent.Disconnected, this));
		}
		public void Disconnect(bool fast = false, bool async = false)
		{
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_Disconnect), fast);
			else disconnect(fast);
		}

		// Close client
		protected new void close()
		{
			base.close();
			if ( buff != null )
			{
				buff.Close();
				buff = null;
			}
			if ( HostInfo != null ) HostInfo.Client.Remove(this);
			HostInfo = null;
		}
		protected void task_Close(object obj)
		{
			close();
			if ( EventFn.Closed != null )
				EventFn.Closed(this, new NetEventArgs(NetEvent.Closed, this));
		}
		public override void Close(bool async = false)
		{
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_Close));
			else close();
		}
	}
}
