﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Wasp.Data.Collection;

namespace Wasp.Network
{
	public abstract class NetHost : NetClient
	{
		public static string Name
		{
			get
			{ return Dns.GetHostName(); }
		}
		public static IPAddress[] IpAddrs
		{
			get
			{ return Dns.GetHostAddresses(Name); }
		}
		public static IPAddress IPv4Addr
		{
			get
			{ return (IPAddress) IpAddrs.First(ip => (ip.AddressFamily == AddressFamily.InterNetwork)); }
		}
		public static IPAddress IPv6Addr
		{
			get
			{ return (IPAddress) IpAddrs.First(ip => (ip.AddressFamily == AddressFamily.InterNetworkV6)); }
		}
		protected static int hosts = 0;
		public static int Hosts
		{
			get
			{ return hosts; }
		}

		// Create a new Network Host
		public NetHost(IPEndPoint addr)
			: base(addr)
		{
			hosts++;
		}
		public NetHost(IPAddress addr, int port)
			: this(new IPEndPoint(addr, port))
		{
		}
		public NetHost(Socket sckt)
			: base(sckt)
		{
			this.addr = sckt.LocalEndPoint;
			hosts++;
		}

		// Send
		public abstract override void Send(NetPacket pkt, bool async = false);
		public abstract void Send(object addr, NetPacket pkt, bool async = false);

		// Close
		protected new void close()
		{
			base.close();
			hosts--;
		}
		public abstract override void Close(bool async = false);
	}
}
