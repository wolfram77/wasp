﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wasp.Network
{
	public class NetPacket
	{
		public object Addr;
		public byte[] Data;
		public int Size
		{
			get
			{ return (Data != null) ? Data.Length : 0; }
		}

		// Create Packet
		public NetPacket(object addr, byte[] data = null)
		{
			Addr = addr;
			Data = data;
		}
		public NetPacket(byte[] data = null)
		{
			Data = data;
		}

		// Close Packet
		public void Close()
		{
			Addr = null;
			Data = null;
		}
	}
}
