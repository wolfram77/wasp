﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Wasp.Data.Collection;

namespace Wasp.Network
{
	public abstract class NetClient
	{
		protected Socket sckt;
		protected EndPoint addr;
		protected int rcvPkts, sndPkts;
		protected ParList<NetPacket> rcvd;
		public object Type;
		public NetEventFnGroup EventFn;
		public Socket Sckt
		{
			get
			{ return sckt; }
		}
		public IPEndPoint Addr
		{
			get
			{ return (IPEndPoint)addr; }
		}
		public int RcvPkts
		{
			get
			{ return rcvPkts; }
		}
		public int SndPkts
		{
			get
			{ return sndPkts; }
		}
		public int PendingRcvPkts
		{
			get
			{ return rcvd.Count; }
		}


		// Create a new Network Client
		protected NetClient()
		{
			rcvd = new ParList<NetPacket>();
			EventFn = new NetEventFnGroup();
		}
		public NetClient(IPEndPoint addr)
			: this()
		{
			this.addr = addr;
		}
		public NetClient(IPAddress addr, int port)
			: this(new IPEndPoint(addr, port))
		{
		}
		public NetClient(Socket sckt)
		{
			this.sckt = sckt;
		}

		// Receive data
		public NetPacket Receive()
		{
			return rcvd.Pop();
		}

		// Send data
		public abstract void Send(NetPacket pkt, bool async = false);

		// Disconnect client
		protected void disconnect(bool fast = false)
		{
			if ( sckt == null ) return;
			if ( !fast )
			{
				try
				{ sckt.Shutdown(SocketShutdown.Both); }
				catch ( Exception ) { }
				try
				{ sckt.Disconnect(false); }
				catch ( Exception ) { }
			}
			sckt.Close();
			sckt = null;
		}

		// Close client
		protected void close()
		{
			disconnect(true);
			addr = null;
			rcvPkts = 0;
			sndPkts = 0;
			rcvd.Close();
			rcvd = null;
			Type = null;
			EventFn = null;
		}
		public abstract void Close(bool async = false);
	}
}
