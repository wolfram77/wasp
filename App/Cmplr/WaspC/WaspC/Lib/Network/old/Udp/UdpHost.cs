﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Wasp.Data.Collection;

namespace Wasp.Network
{
	public class UdpHost : NetHost
	{
		protected NetBuffer buff;
		protected const int defBufferSize = 16384;


		// Creates a UDP Host
		public UdpHost(IPEndPoint addr)
			: base(addr)
		{
			if ( addr == null ) throw new SocketException((int) SocketError.AddressNotAvailable);
			sckt = new Socket(addr.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
			sckt.ExclusiveAddressUse = true;
			sckt.DontFragment = true;
			sckt.Bind(addr);
			buff = new NetBuffer(defBufferSize);
			ThreadPool.QueueUserWorkItem(new WaitCallback(thread_Receive));
		}
		public UdpHost(IPAddress addr, int port)
			: this(new IPEndPoint(addr, port))
		{
		}

		// Thread: Recieve
		protected void thread_Receive(object obj)
		{
			try
			{
				while ( true )
				{
					EndPoint clnt = new IPEndPoint(Addr.Address, Addr.Port);
					int len = sckt.ReceiveFrom(buff.Data, ref clnt);
					if ( len <= 0 ) continue;
					byte[] data = new byte[len];
					Array.Copy(buff.Data, data, len);
					NetPacket pkt = new NetPacket(clnt, data);
					rcvd.Add(pkt);
					rcvPkts++;
					if ( EventFn.Received != null )
						EventFn.Received(this, new NetEventArgs(NetEvent.Received, pkt));
				}
			}
			catch ( Exception ) { }
		}

		// Send Data
		protected void send(object obj)
		{
			NetPacket pkt = (NetPacket) obj;
			int sent = sckt.SendTo(pkt.Data, (EndPoint)pkt.Addr);
			if ( sent != pkt.Size )
				throw new SocketException((int) SocketError.MessageSize);
			sndPkts++;
		}
		protected void task_Send(object obj)
		{
			try
			{
				send(obj);
				if ( EventFn.Sent != null )
					EventFn.Sent(this, new NetEventArgs(NetEvent.Sent, obj));
			}
			catch ( Exception err )
			{
				if ( EventFn.SendFailed != null )
					EventFn.SendFailed(this, new NetEventArgs(NetEvent.SendFailed, obj, err));
			}
		}
		public override void Send(object addr, NetPacket pkt, bool async = false)
		{
			if ( addr != null ) pkt.Addr = addr;
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_Send), pkt);
			else send(pkt);
		}
		public override void Send(NetPacket pkt, bool async = false)
		{
			Send(null, pkt, async);
		}

		// Close the Host
		protected new void close()
		{
			base.close();
			if ( buff != null )
			{
				buff.Close();
				buff = null;
			}
		}
		protected void task_Close(object obj)
		{
			NetEventFn closed = EventFn.Closed;
			close();
			if ( closed != null )
				closed(this, new NetEventArgs(NetEvent.Closed, this));
		}
		public override void Close(bool async = false)
		{
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_Close));
			else close();
		}
	}
}
