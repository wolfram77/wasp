﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wasp.Network
{
	public enum NetEvent
	{
		Closed,
		NewClient,
		ConnectionLost,
		Connected,
		ConnectionFailed,
		Disconnected,
		ReceiveChecked,
		ReceiveCheckFailed,
		Received,
		ReceiveFailed,
		Sent,
		SendFailed,
		ProcessRecieve,
		ProcessSend
	}

	public class NetEventArgs : EventArgs
	{
		public NetEvent Event;
		public object Data;
		public Exception Error;

		// Create a Network Event Argument
		public NetEventArgs(NetEvent evnt, object data = null, Exception error = null)
		{
			Event = evnt;
			Data = data;
			Error = error;
		}
	}

	public delegate void NetEventFn(object sender, NetEventArgs e);

	public class NetEventFnGroup
	{
		public NetEventFn Closed, NewClient, ConnectionLost;
		public NetEventFn Connected, ConnectionFailed, Disconnected, ReceiveChecked;
		public NetEventFn ReceiveCheckFailed, Received, ReceiveFailed, Sent, SendFailed;
		public NetEventFn ProcessReceive, ProcessSend;

		// Create a Network Event Function Group
		public NetEventFnGroup()
		{
		}
	}
}
