#ifndef	_Data_Primitive_Concat_h_
#define	_Data_Primitive_Concat_h_


// primitive_Concat
#define primitive_Concat2(v1, v0, sft)	\
	(((v1) << (sft)) | (v0))

#define primitive_Concat4(v3, v2, v1, v0, sft)	\
	primitive_Concat2(primitive_Concat2(primitive_Concat2(v3, v2, sft), v1, sft), v0, sft)

#define primitive_Concat8(v7, v6, v5, v4, v3, v2, v1, v0, sft)	\
	primitive_Concat2(primitive_Concat4(primitive_Concat4(v7, v6, v5, v4, sft), v3, v2, v1, sft), v0, sft)

#define	primitive_Concat(...)	\
	macro_Fn(macro_Fn9(__VA_ARGS__, primitive_Concat8, _8, _7, _6, primitive_Concat4, _4, primitive_Concat2)(__VA_ARGS__))


#endif
