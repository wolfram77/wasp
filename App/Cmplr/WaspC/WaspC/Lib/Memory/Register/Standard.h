#ifndef	_Memory_Register_Standard_h_
#define	_Memory_Register_Standard_h_


// R0-R31 Registers
#ifndef R0
#define R0		reg_Define8(0x00)
#define R1		reg_Define8(0x01)
#define R2		reg_Define8(0x02)
#define R3		reg_Define8(0x03)
#define R4		reg_Define8(0x04)
#define R5		reg_Define8(0x05)
#define R6		reg_Define8(0x06)
#define R7		reg_Define8(0x07)
#define R8		reg_Define8(0x08)
#define R9		reg_Define8(0x09)
#define R10		reg_Define8(0x0A)
#define R11		reg_Define8(0x0B)
#define R12		reg_Define8(0x0C)
#define R13		reg_Define8(0x0D)
#define R14		reg_Define8(0x0E)
#define R15		reg_Define8(0x0F)
#define R16		reg_Define8(0x10)
#define R17		reg_Define8(0x11)
#define R18		reg_Define8(0x12)
#define R19		reg_Define8(0x13)
#define R20		reg_Define8(0x14)
#define R21		reg_Define8(0x15)
#define R22		reg_Define8(0x16)
#define R23		reg_Define8(0x17)
#define R24		reg_Define8(0x18)
#define R25		reg_Define8(0x19)
#define R26		reg_Define8(0x1A)
#define R27		reg_Define8(0x1B)
#define R28		reg_Define8(0x1C)
#define R29		reg_Define8(0x1D)
#define R30		reg_Define8(0x1E)
#define R31		reg_Define8(0x1F)
#endif


// X, Y, Z registers
#ifndef X
#define X		reg_Define16(0x1A)
#define Y		reg_Define16(0x1C)
#define Z		reg_Define16(0x1E)
#endif


#endif
