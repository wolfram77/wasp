#ifndef	_Memory_Register_Basic_h_
#define	_Memory_Register_Basic_h_


// Register Define
#define	reg_Define8(addr)	\
	(*((volatile uint8_t*)(addr)))

#define	reg_Define16(addr)	\
	(*((volatile uint16_t*)(addr)))


// IO Define
#define	reg_DefineIO8(addr)	\
	reg_Define8((addr) + 0x20)

#define	reg_DefineIO16(addr)	\
	reg_Define16((addr) + 0x20)


#endif
