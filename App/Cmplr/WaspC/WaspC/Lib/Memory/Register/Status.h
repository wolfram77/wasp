#ifndef	_Memory_Register_Status_h_
#define	_Memory_Register_Status_h_


// Status Register
#ifndef SREG
#define SREG		reg_DefineIO8(0x3F)
#endif


// Flags
#ifndef SREG_I
#define SREG_I		7
#define SREG_T		6
#define SREG_H		5
#define SREG_S		4
#define SREG_V		3
#define SREG_N		2
#define SREG_Z		1
#define SREG_C		0
#endif


#endif
