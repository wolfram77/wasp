#ifndef	_Memory_EEPROM_Basic_h_
#define	_Memory_EEPROM_Basic_h_


// Register
#ifndef SP
#define SP		reg_DefineIO16(0x3D)
#endif
#ifndef SPL
#define SPL		reg_DefineIO8(0x3D)
#define SPH		reg_DefineIO8(0x3E)
#endif

// call by OS when EEPROM used, and is off
// Enable
inline void eeprom_Enable()
{
	// power_EEPROM_On();
	bit_Set(EECR, EERIE);
}


// call by OS when EEPROM unused, and is on
// Disable
inline void eeprom_Disable()
{
	bit_Clear(EECR, EERIE);
	// power_EEPROM_Off();
}


// Read
uword eeprom_Read(Task* tsk)
{
	task_WaitWhile(bit_Test(EECR, EEPE));
	
}
#endif
