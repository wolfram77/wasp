/*	Copyright (c) 2013, National Institute of Technology, Rourkela
 *	Copyright (c) 2013, Subhajit Sahu
 *	All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of the copyright holders nor the names of
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 **/
 
/*	Support Sample: Sample Description
 *	Path: Support / Sample
 *	File: Sample.h
 *
 *	Sample Purpose and Usage.
 *	
 *	Author:
 *	Subhajit Sahu <wolfram77@gmail.com>
 *	
 *	This file is a part of WASP OS.
 **/


#ifndef	_Support_Sample_h_
#define	_Support_Sample_h_


// Sample Block
// ------------
#define sample_Function1(a, b)	\
	((a) | (b))

#define sample_Function2(a, b, c)	\
	((a) | (b) | (c))

/*	Sample description.
 *	
 *	out = sample_Function(a, b)
 *	out = sample_Function(a, b, c)
 *	
 *	a:		value 1
 *	b:		value 2
 *	c:		value 3
 *	out:	return value
 **/
#define	sample_Function(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, sample_Function2, sample_Function1)(__VA_ARGS__))


// register unsigned char counter asm("r3");
// r2-r7 free, r8-r15 if params not used


#endif
