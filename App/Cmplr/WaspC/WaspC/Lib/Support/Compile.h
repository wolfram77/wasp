#ifndef	_Support_Compile_h_
#define	_Support_Compile_h_


// Platform
#define platform_Name_Win32		0x0100
#define platform_Name_AVR		0x0101


// Compiler
#define compiler_Name_GCC		0x0200
#define compiler_Name_VisualC	0x0201


#endif
