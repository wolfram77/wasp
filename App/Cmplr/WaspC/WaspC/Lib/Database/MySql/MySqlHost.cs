﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Wasp.Data.Collection;

namespace Wasp.Database
{
	public class MySqlHost
	{
		private ParList<MySqlClnt> client;
		public MySqlHostInfo Info;
		public DbEventFnGroup EventFn;
		public MySqlClnt this[int indx]
		{
			get
			{ return client.ElementAt(indx); }
		}
		public int Clients
		{
			get { return client.Count; }
		}
		public int Operations
		{
			get
			{
				lock (Info.OperationLock)
				{ return Info.Operations; }
			}
		}
		private static int hosts = 0;
		public static int Hosts
		{
			get
			{ return hosts; }
		}


		// Creates an SQL Host
		public MySqlHost()
		{
			client = new ParList<MySqlClnt>(true);
			EventFn = new DbEventFnGroup();
			// initialize Host information
			Info = new MySqlHostInfo();
			Info.Host = this;
			Info.Client = client;
			Info.OperationLock = new object();
			Info.AutoRemove = true;
			hosts++;
		}

		// Get Client
		public MySqlClnt GetClient(int indx)
		{
			return client.ElementAt(indx);
		}

		// Add a new Client
		public MySqlClnt AddClient(MySqlClnt clnt, bool async = false)
		{
			if (clnt.Addr == null) return null;
			clnt.HostInfo = Info;
			clnt.EventFn = EventFn;
			if (!clnt.Connected) clnt.Connect(async);
			return clnt;
		}

		// Remove a Client
		public void RemoveClient(MySqlClnt clnt, bool async = false)
		{
			clnt.Disconnect(async);
			client.Remove(clnt);
		}

		// Execute NonQuery
		public int ExecuteNonQuery(MySqlClnt clnt, MySqlCommand cmd,  bool async = false)
		{
			return clnt.ExecuteNonQuery(cmd, async);
		}
		
		// Execute NonQuery on a type of clients
		public List<int> ExecuteNonQueryOnType(object type, MySqlCommand cmd, bool async = false)
		{
			List<int> rows = new List<int>();
			foreach (MySqlClnt clnt in client)
			{
				if (type == null || clnt.Type.Equals(type))
				{ rows.Add(clnt.ExecuteNonQuery(cmd, async)); }
			}
			return rows;
		}

		// Execute Scalar
		public object ExecuteScalar(MySqlClnt clnt, MySqlCommand cmd, bool async = false)
		{
			return clnt.ExecuteScalar(cmd, async);
		}

		// Execute Reader
		public MySqlDataReader ExecuteReader(MySqlClnt clnt, MySqlCommand cmd, bool async = false)
		{
			return clnt.ExecuteReader(cmd, async);
		}

		// Close the Host
		public void Close(bool async = false)
		{
			foreach ( MySqlClnt clnt in client )
				clnt.Close(async);
			client.Close();
			client = null;
			Info = null;
			EventFn = null;
		}
	}
}
