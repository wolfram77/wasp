﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Xml;
using Wasp.Data.Collection;

namespace Wasp.Database
{
	public class SQLiteHost
	{
		private ParList<SQLiteClnt> client;
		public SQLiteHostInfo Info;
		public DbEventFnGroup EventFn;
		public SQLiteClnt this[int indx]
		{
			get
			{ return client.ElementAt(indx); }
		}
		public int Clients
		{
			get { return client.Count; }
		}
		public int Operations
		{
			get
			{
				lock (Info.OperationLock)
				{ return Info.Operations; }
			}
		}
		private static int hosts = 0;
		public static int Hosts
		{
			get
			{ return hosts; }
		}


		// Creates an SQL Host
		public SQLiteHost()
		{
			client = new ParList<SQLiteClnt>(true);
			EventFn = new DbEventFnGroup();
			// initialize Host information
			Info = new SQLiteHostInfo();
			Info.Host = this;
			Info.Client = client;
			Info.OperationLock = new object();
			Info.AutoRemove = true;
			hosts++;
		}

		// Get Client
		public SQLiteClnt GetClient(int indx)
		{
			return client.ElementAt(indx);
		}

		// Add a new Client
		public SQLiteClnt AddClient(SQLiteClnt clnt, bool async = false)
		{
			if (clnt.Addr == null) return null;
			clnt.HostInfo = Info;
			clnt.EventFn = EventFn;
			if (!clnt.Connected) clnt.Connect(async);
			return clnt;
		}

		// Remove a Client
		public void RemoveClient(SQLiteClnt clnt, bool async = false)
		{
			clnt.Disconnect(async);
			client.Remove(clnt);
		}

		// Execute NonQuery
		public int ExecuteNonQuery(SQLiteClnt clnt, SQLiteCommand cmd, bool async = false)
		{
			return clnt.ExecuteNonQuery(cmd, async);
		}

		// Execute NonQuery on a type of clients
		public List<int> ExecuteNonQueryOnType(object type, SQLiteCommand cmd, bool async = false)
		{
			List<int> rows = new List<int>();
			foreach (SQLiteClnt clnt in client)
			{
				if (type == null || clnt.Type.Equals(type))
				{ rows.Add(clnt.ExecuteNonQuery(cmd, async)); }
			}
			return rows;
		}

		// Execute Scalar
		public object ExecuteScalar(SQLiteClnt clnt, SQLiteCommand cmd, bool async = false)
		{
			return clnt.ExecuteScalar(cmd, async);
		}

		// Execute Reader
		public SQLiteDataReader ExecuteReader(SQLiteClnt clnt, SQLiteCommand cmd, bool async = false)
		{
			return clnt.ExecuteReader(cmd, async);
		}

		// Close the Host
		public void Close(bool async = false)
		{
			foreach ( SQLiteClnt clnt in client )
				clnt.Close(async);
			client.Close();
			client = null;
			Info = null;
			EventFn = null;
		}
	}
}
