﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wasp.Data.Collection;

namespace Wasp.Database
{
	public class SQLiteHostInfo
	{
		public SQLiteHost Host;
		public ParList<SQLiteClnt> Client;
		public object OperationLock;
		public int Operations;
		public bool AutoRemove;

		public SQLiteHostInfo()
		{
		}
	}
}
