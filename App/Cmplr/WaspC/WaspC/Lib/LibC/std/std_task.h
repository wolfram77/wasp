/*
----------------------------------------------------------------------------------------
	task: Coroutine-based multi-tasking support module
	File: std_task.h

	Copyright � 2013 Subhajit Sahu. All rights reserved.

	This file is part of WASP. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

    WASP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WASP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WASP.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/


// Note:
// Please set Visual C use Debug Information Format to:
// Program Database (/Zi)
//
#ifndef	_std_task_h_
#define	_std_task_h_


#include "std_macro.h"
#include "std_type.h"
#include "std_list.h"


// Task Mold format
// 
// Each task needs to have an object of an individual task mold. It is used to store
// continuation line, task status, and state buffer. Depending on the size a state
// buffer required, an appropriate task mold needs to be chosen. State buffer is used
// to store state variables (non-global) which need to restored after the task has
// regained the CPU. The range is from 8 to 256 bytes (by default, provided in powers
// of 2). For any different size mold, MoldMake() can be used make a mold of desired
// size.
// 
#define	task_Make(sz)	\
typedef	struct _task##sz	\
{	\
	num		(*fn)(struct _task##sz*);	\
	int		line;	\
	num		stat;	\
	byte	repo[sz];	\
}task##sz

#define	task_Mk		task_Make

task_Mk(2);
task_Mk(4);
task_Mk(8);
task_Mk(16);
task_Mk(32);
task_Mk(64);
task_Mk(128);
task_Mk(256);

#ifndef	task_Def
#define	task_Def	task4
#endif

typedef task_Def	task;

typedef struct _task_header
{
	num		(*fn)(struct _task_header*);
	int		line;
	num		stat;
}task_header;

typedef task_header					task_hdr;
typedef num (*task_functionptr)(task*);
typedef task_functionptr			task_fnptr;
typedef num (*task_header_functionptr)(task_hdr*);
typedef task_header_functionptr		task_hdr_fnptr;
typedef snum						task_semaphore;
typedef	task_semaphore				task_sem;


// Task List Mold
// 
// An internal Task list is used to store pointers to task functions and pointers to
// their task object. This list is used to perform task switching. This list object is
// required while initializing emTask library. The list must be pre-iniltialized. To make
// a task list, first a task list mold of desired size can be made using TaskListMoldMake(),
// and then, an object needs to be created from the mold and initilized using emList library.
// 
typedef task*	task_ptr;

#define	list_TaskPtrMake(sz)	\
	list_Mk(task_ptr, sz)

#define	list_TaskPtrMk	\
	list_TaskPtrMake

list_TaskPtrMk(8);
list_TaskPtrMk(16);
list_TaskPtrMk(32);
list_TaskPtrMk(64);
list_TaskPtrMk(128);
list_TaskPtrMk(256);

#ifndef	list_TaskPtrDef
#define	list_TaskPtrDef		list_8task_ptr
#endif

typedef list_TaskPtrDef		list_task_ptr;

list_task_ptr	task_List;
num				task_Exec;	


// Exit Status constants
#define	task_terminated		0x00
#define	task_exited			0x01
#define	task_exited_error	0x02
#define	task_looped			0x10
#define	task_waiting		0x11
#define	task_switched		0x12
#define	task_switched_out	0x13


// Function:
// Setup()
// [Setup]
// 
// Sets up Task before use. This is required before
// anything else. 
// 
// Parameters:
// none
// 
// Returns:
// nothing
//
#define	task_Setup()	\
		list_Init(task_List)


// Function:
// Initialize(tsk, *tsk_fn)
// [Initialize / Init]
// 
// Initializes a task object (task) before use.
// 
// Parameters:
// tsk:		the task object to initialize
// *tsk_fn:	function pinter to the task
// 
// Returns:
// nothing
//
#define	task_Initialize(tsk, tsk_fn)	\
	macro_Begin	\
	(tsk).fn = (task_hdr_fnptr)(tsk_fn);	\
	(tsk).line = 0;	\
	(tsk).stat = 0;	\
	macro_End

#define	task_Init	task_Initialize


// Function:
// GetActive()
// [GetActive / GetActv]
// 
// Gives the number of active tasks (added to the running list).
// 
// Parameters:
// none
// 
// Returns:
// num_tasks:	number of active tasks
//
#define	task_GetActive()	\
	(task_List.count)

#define	task_GetActv	task_GetActive


// Function:
// GetFree()
// [GetFree]
// 
// Gives the number of free cells (space to add a task) available in task list.
// 
// Parameters:
// none
// 
// Returns:
// cells_free:	number of free cells in task list
//
#define	task_GetFree()	\
	(task_List.size - task_List.count)

// Function:
// Add(*tsk)
// [Add]
// 
// Adds a new task to the list of tasks to be executed. 
// 
// Parameters:
// *tsk:		the task object for the task
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failed to add
// 
num task_AddF(task* tsk)
{
	num ret;
	list_IndexOf(ret, task_List, tsk);
	if(ret == (num)-1) ret = list_Add(task_List, tsk);
	return ret;
}

#define	task_Add(tsk)	\
	task_AddF((task*)(tsk))


// Function:
// Remove(*tsk)
// [Remove / Rmv]
// 
// Removes a task from the list of tasks to be executed. 
// (terminates it)
// 
// Parameters:
// *tsk:	the task object for the task to be removed
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failed to remove
// 
num task_RemoveF(task* tsk)
{
	num ret;
	list_Remove(ret, task_List, tsk);
	if(ret == (num)0) tsk->stat = task_terminated;
	return ret;
}

#define	task_Remove(tsk)	\
	task_RemoveF((task*)(tsk))

#define	task_Rmv	task_Remove


// Function:
// RemoveAll()
// [RemoveAll / RmvAll]
// 
// Removes all running tasks, and thus the Run() returns
// (terminates all tasks)
// 
// Parameters:
// none
// 
// Returns:
// nothing
//
#define task_RemoveAll()	\
	list_Clear(task_List)

#define	task_RmvAll		task_RemoveAll


// Function:
// Run()
// [Run]
// 
// Executes all tasks and returns only when all tasks have been removed.
// 
// Parameters:
// none
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num task_Run()
{
	task* tsk;
	num count_old;
	if(!task_GetActive()) return (num)-1;
	task_Exec = 0;
	while(1)
	{
		count_old = task_GetActive();
		tsk = task_List.val[task_Exec];
		tsk->stat = (tsk->fn)(tsk);
		if((tsk->stat & 0xF0) == 0 || task_GetActive() != count_old)
		{
			if(!task_GetActive()) return 0;
			list_IndexOf(task_Exec, task_List, tsk);
			if(task_Exec == (num)-1) task_Exec = -1;
			else if((tsk->stat & 0xF0) == 0)
			{
				list_RemoveAt(task_List, task_Exec);
				task_Exec--;
			}
			if(!task_GetActive()) return 0;
		}
		task_Exec++;
		task_Exec = (task_Exec < task_GetActive())? task_Exec : 0;
	}
	return (num)0;
}


// Macro:
// Fn(name)
// [Fn]
// 
// A macro that is to be used to create a task function
// 
// Parameters:
// name:	name of the task function
// 
#define	task_Fn(name)	\
	byte name(task* task_obj)


// Macro:
// Begin
// [Begin / Bgn]
// 
// Used to begin a task. Must be used in the first line of a
// task function
// 
// Parameters:
// none
// 
// Returns:
// nothing
// 
#define	task_Begin	\
	switch(task_obj->line)	\
	{	\
		case 0:

#define	task_Bgn	task_Begin


// Macro:
// End
// [End]
// 
// Used to end a task. Must be used in the last line of a
// task function
// 
// Parameters:
// none
// 
// Returns:
// nothing
// 
#define	task_End	\
		}	\
	task_obj->line = 0;	\
	return task_looped;


// Function:
// SaveState(<local variables>)
// [SaveState / Save / Sv]
// 
// Save local variables to the state buffer of task object
// 
// Parameters:
// <local variables>:	a list of local variables (as typ1, var1, typ2, var2, ...) to store separated with commas
// 
// Returns:
// nothing
// 
#define	task_SaveState1(typ1, var1)	\
	(*((typ1*)task_obj->repo) = var1)

#define	task_SaveState2(typ1, var1, typ2, var2)	\
	macro_Begin	\
	*((typ1*)task_obj->repo) = var1;	\
	*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
	macro_End

#define	task_SaveState3(typ1, var1, typ2, var2, typ3, var3)	\
	macro_Begin	\
	*((typ1*)task_obj->repo) = var1;	\
	*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
	*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
	macro_End

#define	task_SaveState4(typ1, var1, typ2, var2, typ3, var3, typ4, var4)	\
	macro_Begin	\
	*((typ1*)task_obj->repo) = var1;	\
	*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
	*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
	*((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3))) = var4;	\
	macro_End

#define	task_SaveState5(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5)	\
	macro_Begin	\
	*((typ1*)task_obj->repo) = var1;	\
	*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
	*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
	*((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3))) = var4;	\
	*((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4))) = var5;	\
	macro_End

#define	task_SaveState6(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6)	\
	macro_Begin	\
	*((typ1*)task_obj->repo) = var1;	\
	*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
	*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
	*((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3))) = var4;	\
	*((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4))) = var5;	\
	*((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5))) = var6;	\
	macro_End

#define	task_SaveState7(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6, typ7, var7)	\
	macro_Begin	\
	*((typ1*)task_obj->repo) = var1;	\
	*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
	*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
	*((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3))) = var4;	\
	*((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4))) = var5;	\
	*((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5))) = var6;	\
	*((typ7*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6))) = var7;	\
	macro_End

#define	task_SaveState8(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6, typ7, var7, typ8, var8)	\
	macro_Begin	\
	*((typ1*)task_obj->repo) = var1;	\
	*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
	*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
	*((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3))) = var4;	\
	*((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4))) = var5;	\
	*((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5))) = var6;	\
	*((typ7*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6))) = var7;	\
	*((typ8*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6) + sizeof(typ7))) = var8;	\
	macro_End

#define	task_SaveState(...)	\
	macro_Fn(macro_Fn17(_0, __VA_ARGS__, task_SaveState8, _15, task_SaveState7, _13, task_SaveState6, _11, task_SaveState5, _9, task_SaveState4, _7, task_SaveState3, _5, task_SaveState2, _3, task_SaveState1, macro_FnE, macro_FnE)(__VA_ARGS__))

#define	task_Save	task_SaveState
#define	task_Sv		task_SaveState


// Function:
// LoadState(<local variables>)
// [LoadState / Load / Ld]
// 
// Load local variables from the state buffer of task object
// 
// Parameters:
// <local variables>:	a list of local variables (as typ1, var1, typ2, var2, ...) to load separated with commas
// 
// Returns:
// nothing
// 
#define	task_LoadState1(typ1, var1)	\
	(var1 = *((typ1*)task_obj->repo))

#define	task_LoadState2(typ1, var1, typ2, var2)	\
	macro_Begin	\
	var1 = *((typ1*)task_obj->repo);	\
	var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
	macro_End

#define	task_LoadState3(typ1, var1, typ2, var2, typ3, var3)	\
	macro_Begin	\
	var1 = *((typ1*)task_obj->repo);	\
	var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
	var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
	macro_End

#define	task_LoadState4(typ1, var1, typ2, var2, typ3, var3, typ4, var4)	\
	macro_Begin	\
	var1 = *((typ1*)task_obj->repo);	\
	var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
	var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
	var4 = *((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3)));	\
	macro_End

#define	task_LoadState5(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5)	\
	macro_Begin	\
	var1 = *((typ1*)task_obj->repo);	\
	var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
	var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
	var4 = *((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3)));	\
	var5 = *((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4)));	\
	macro_End

#define	task_LoadState6(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6)	\
	macro_Begin	\
	var1 = *((typ1*)task_obj->repo);	\
	var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
	var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
	var4 = *((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3)));	\
	var5 = *((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4)));	\
	var6 = *((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5)));	\
	macro_End

#define	task_LoadState7(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6, typ7, var7)	\
	macro_Begin	\
	var1 = *((typ1*)task_obj->repo);	\
	var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
	var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
	var4 = *((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3)));	\
	var5 = *((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4)));	\
	var6 = *((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5)));	\
	var7 = *((typ7*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6)));	\
	macro_End

#define	task_LoadState8(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6, typ7, var7, typ8, var8)	\
	macro_Begin	\
	var1 = *((typ1*)task_obj->repo);	\
	var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
	var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
	var4 = *((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3)));	\
	var5 = *((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4)));	\
	var6 = *((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5)));	\
	var7 = *((typ7*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6)));	\
	var8 = *((typ8*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6) + sizeof(typ7)));	\
	macro_End

#define	task_LoadState(...)	\
	macro_Fn(macro_Fn17(_0, __VA_ARGS__, task_LoadState8, _15, task_LoadState7, _13, task_LoadState6, _11, task_LoadState5, _9, task_LoadState4, _7, task_LoadState3, _5, task_LoadState2, _3, task_LoadState1, macro_FnE, macro_FnE)(__VA_ARGS__))

#define	task_Load	task_LoadState
#define	task_Ld		task_LoadState


// Function:
// SwitchOut()
// [SwitchOut]
// 
// Used to switch out a task. Next time, the task will start from begining.
// 
// Parameters:
// none
// 
// Returns:
// nothing
// 
#define	task_SwitchOut()	\
	macro_Begin	\
	task_obj->line = 0;	\
	return task_switched_out;	\
	macro_End


// Function:
// Switch(<local variables>)
// [Switch]
// 
// Used to switch from task. If no local variables are present in task, use Switch().
// The task will continue from here.
// 
// Parameters:
// <local variables>:	a list of local variables (as typ1, var1, typ2, var2, ...) to store separated with commas
// 
// Returns:
// nothing
// 
#define	task_Switch(...)	\
	macro_Begin	\
	task_obj->line = __LINE__;	\
	task_SaveState(__VA_ARGS__);	\
	return task_switched;	\
	case __LINE__:	\
	task_LoadState(__VA_ARGS__);	\
	macro_End

#if task_abbr >= 3
#define	tsk_Switch		task_Switch
#endif


// Function:
// WaitWhile(wait_cond, <local variables>)
// [WaitWhile]
// 
// Used to wait while a condition is satisfied.
// 
// Parameters:
// wait_cond:	wait condition (will wait as long as this condition is true)
// <local variables>:	a list of local variables (as typ1, var1, typ2, var2, ...) to store separated with commas
// 
// Returns:
// nothing
// 
#define	task_WaitWhile(wait_cond, ...)	\
	macro_Begin	\
	task_obj->line = __LINE__;	\
	task_SaveState(__VA_ARGS__);	\
	case __LINE__:	\
	if(wait_cond) return task_waiting;	\
	task_LoadState(__VA_ARGS__);	\
	macro_End


// Function:
// WaitUntil(wait_cond, <local variables>)
// [WaitUntil / WaitUntl]
// 
// Used to wait until a condition is satisfied.
// 
// Parameters:
// wait_cond:	wait condition (will wait until this condition is true)
// <local variables>:	a list of local variables (as typ1, var1, typ2, var2, ...) to store separated with commas
// 
// Returns:
// nothing
// 
#define	task_WaitUntil(wait_cond, ...)	\
	task_WaitWhile(!(wait_cond), __VA_ARGS__)

#define	task_WaitUntl	task_WaitUntil


// Function:
// Exit()
// [Exit]
// 
// Used to exit from task. The task is removed from execution.
// 
// Parameters:
// none
// 
// Returns:
// nothing
// 
#define	task_Exit()	\
	return task_exited


// Function:
// SemaphoreWait(sem, <local variables>)
// [SemaphoreWait / SemWait]
// 
// Used to wait for a semaphore. To be used before entering a
// critical section.
// 
// Parameters:
// sem:		semaphore used
// <local variables>:	a list of local variables (as typ1, var1, typ2, var2, ...) to store separated with commas
// 
// Returns:
// nothing
// 
#define	task_SemaphoreWait(sem, ...)	\
	macro_Begin	\
	task_WaitUntil((sem) > 0, __VA_ARGS__);	\
	(sem)--;	\
	macro_End

#define	task_SemWait	task_SemaphoreWait


// Function:
// SemaphoreSignal(sem)
// [SemaphoreSignal / SemSgnl]
// 
// Used to signal with a semaphore. Used while coming out of
// a critical section (manually enable interrupts after this).
// 
// Parameters:
// sem:		semaphore used
// 
// Returns:
// nothing
// 
#define	task_SemaphoreSignal(sem)	\
	((sem)++)


#endif
