/*
----------------------------------------------------------------------------------------
	type: Type definition and conversion module
	File: std_type.h

	Copyright � 2013 Subhajit Sahu. All rights reserved.

	This file is part of WASP. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WASP is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WASP is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WASP.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/


#ifndef	_std_type_h_
#define	_std_type_h_


#include <string.h>
#include "std_macro.h"


// Constants
#ifndef null
#define null	0
#endif
#ifndef	NULL
#define	NULL	0
#endif
#ifndef TRUE
#define TRUE	1
#define	FALSE	0
#endif
#ifndef true
#define	true	1
#define	false	0
#endif


// Data-types & min-max values
typedef	signed char			int8;
typedef	unsigned char		uint8;
typedef	short				int16;
typedef	unsigned short		uint16;
typedef	long				int32;
typedef	unsigned long		uint32;
typedef	long long			int64;
typedef	unsigned long long	uint64;
#ifndef byte
typedef	unsigned char		byte;
#endif
typedef signed char			sbyte;
typedef unsigned short		ushort;
typedef unsigned int		uint;
typedef unsigned long		ulong;
typedef char*				string;
#if lib_word_size >=16
typedef int		num;
typedef	int		snum;
typedef uint	unum;
#else
typedef byte	num;
typedef	sbyte	snum;
typedef	byte	unum;
#endif


// Data-type Min, Max value
#define int8_min	(-0x80)
#define int8_max	(0x7F)
#define int16_min	(-0x8000)
#define int16_max	(0x7FFF)
#define int32_min	(-0x80000000L)
#define int32_max	(0x7FFFFFFFL)
#define int64_min	(-0x8000000000000000LL)
#define int64_max	(0x7FFFFFFFFFFFFFFFLL)
#define uint8_min	(0U)
#define uint8_max	(0xFFU)
#define uint16_min	(0U)
#define uint16_max	(0xFFFFU)
#define uint32_min	(0UL)
#define uint32_max	(0xFFFFFFFFUL)
#define uint64_min	(0ULL)
#define uint64_max	(0xFFFFFFFFFFFFFFFFULL)
#define char_min	uint8_min
#define char_max	uint8_max
#define byte_min	uint8_min
#define byte_max	uint8_max
#define sbyte_min	int8_min
#define sbyte_max	int8_max
#define short_min	int16_min
#define short_max	int16_max
#define ushort_min	uint16_min
#define ushort_max	uint16_max
#define long_min	int32_min
#define long_max	int32_max
#define	ulong_min	uint32_min
#define ulong_max	uint32_max
#if lib_word_size >= 16
#define int_min		int32_min
#define int_max		int32_max
#define uint_min	uint32_min
#define uint_max	uint32_max
#define	num_min		int_min
#define	num_max		int_max
#define	snum_min	int_min
#define	snum_max	int_max
#define	unum_min	uint_min
#define	unum_max	uint_min
#else
#define int_min		int16_min
#define int_max		int16_max
#define uint_min	uint16_min
#define uint_max	uint16_max
#define	num_min		byte_min
#define	num_max		byte_max
#define	snum_min	sbyte_min
#define	snum_max	sbyte_max
#define	unum_min	byte_min
#define	unum_max	byte_min
#endif


// Functions
#if lib_compiler == lib_compiler_gcc
#define type_Of(expr)	\
	typeof(expr)
#elif lib_compiler == lib_compiler_visual_c
#define	type_Of(expr)	\
	decltype(expr)
#endif


// Type Buffer format
// [BufferMake / BuffMk]
// 
// Type buffers can be created with different sizes. type_buff16 has a size of 16 bytes,
// type_buff32 has 32 bytes size, and so on. The range is from 16 to 256 bytes (in powers
// of 2). Type buffers of other sizes can be made using BuffMake(size). The default Buffer
// has a size of 16 bytes.
// 
#define	type_BufferMake(sz)	\
typedef union _type_buffer##sz	\
{	\
	byte	Byte[sz];	\
	sbyte	Sbyte[1];	\
	char	Char[1];	\
	short	Short[1];	\
	ushort	Ushort[1];	\
	long	Long[1];	\
	ulong	Ulong[1];	\
	int8	Int8[1];	\
	uint8	Uint8[1];	\
	int16	Int16[1];	\
	uint16	Uint16[1];	\
	int32	Int32[1];	\
	uint32	Uint32[1];	\
	int64	Int64[1];	\
	uint64	Uint64[1];	\
	float	Float[1];	\
	double	Double[1];	\
	int		Int[1];		\
	uint	Uint[1];	\
}type_buffer##sz, type_buff##sz

#define	type_BuffMake	type_BufferMake
#define	type_BufferMk	type_BufferMake
#define	type_BuffMk		type_BufferMake


type_BufferMake(16);
type_BufferMake(32);
type_BufferMake(64);
type_BufferMake(128);
type_BufferMake(256);

#ifndef	type_BufferDef
#define	type_BufferDef	type_buffer16
#endif

typedef type_BufferDef	type_buffer;
typedef	type_BufferDef	type_buff;


// Function:
// Concat(<list_of_values>, sft)
// [Concat / Cat]
// 
// Concatenates smaller data types to a bigger data type.
// 
// Parameters:
// <list_of_values>:	list of bits / nibbles / bytes / shorts / longs, etc.
// sft:					bit shift amount per value
// 
// Returns:
// value:	concatenated value
// 
#define type_Concat2(v1, v0, sft)	\
	(((v1) << (sft)) | (v0))

#define type_Concat4(v3, v2, v1, v0, sft)	\
	type_Concat2(type_Concat2(type_Concat2(v3, v2, sft), v1, sft), v0, sft)

#define type_Concat8(v7, v6, v5, v4, v3, v2, v1, v0, sft)	\
	type_Concat2(type_Concat4(type_Concat4(v7, v6, v5, v4, sft), v3, v2, v1, sft), v0, sft)

#define	type_Concat(...)	\
	macro_Fn(macro_Fn9(__VA_ARGS__, type_Concat8, _8, _7, _6, type_Concat4, _4, type_Concat2)(__VA_ARGS__))

#define type_Cat	type_Concat



// Function:
// To<Type>(<smaller_data_types>)
// [To<Type>]
//
// Assembles smaller data types to a bigger data type. The
// assembling is done in little endian format, which means
// that the smaller data representing the least significant
// part should come first, and the smaller data representing
// the most significant part should come last.
//
// Parameters:
// smaller_data_types:	list of bytes, shorts, ints, etc.
//
// Returns:
// <type>_value:	the value of the (bigger) assembled data type
//
#define type_ToNibble(v3, v2, v1, v0)	\
	((byte)type_Cat(v3, v2, v1, v0, 1))

#define	type_ToNibl		type_ToNibble

#define type_ToByteNib(v1, v0)	\
	((byte)type_Cat(v1, v0, 4))

#define	type_ToByteBit(v7, v6, v5, v4, v3, v2, v1, v0)	\
	((byte)type_Cat(v7, v6, v5, v4, v3, v2, v1, v0, 1))

#define type_ToByte(...)	\
	macro_Fn(macro_Fn8(__VA_ARGS__, type_ToByteBit, _7, _6, _5, _4, _3, type_ToByteNib)(__VA_ARGS__))

#define	type_ToSbyte(...)	\
	macro_Fn((sbyte)type_ToByte(__VA_ARGS__))

#define	type_ToInt8		type_ToSbyte
#define	type_ToUint8	type_ToByte

#define	type_ToChar(...)	\
	macro_Fn((char)type_ToByte(__VA_ARGS__))

#define	type_ToShort(v1, v0)	\
	((short)type_Cat(v1, v0, 8))

#define	type_ToUshort(v1, v0)	\
	((ushort)type_Cat(v1, v0, 8))

#define	type_ToInt16	type_ToShort
#define	type_ToUint16	type_ToUshort

#define	type_ToInt32Srt(v1, v0)	\
	((int32)type_Cat(v1, v0, 16))

#define	type_ToInt32Byt(v3, v2, v1, v0)	\
	((int32)type_Cat(v3, v2, v1, v0, 8))

#define	type_ToInt32(...)	\
	macro_Fn(macro_Fn4(__VA_ARGS__, type_ToInt32Byt, _3, type_ToInt32Srt)(__VA_ARGS__))

#define	type_ToUint32(...)	\
	((uint32)type_ToInt32(__VA_ARGS__))

#define	type_ToLong		type_ToInt32
#define	type_ToUlong	type_ToUint32

#define	type_ToInt64Lng(v1, v0)	\
	((int64)type_Cat(v1, v0, 32))

#define	type_ToInt64Srt(v3, v2, v1, v0)	\
	((int64)type_Cat(v3, v2, v1, v0, 16))

#define	type_ToInt64Byt(v7, v6, v5, v4, v3, v2, v1, v0)	\
	((int64)type_Cat(v7, v6, v5, v4, v3, v2, v1, v0, 8))

#define	type_ToInt64(...)	\
	macro_Fn(macro_Fn8(__VA_ARGS__, type_ToInt64Byt, _7, _6, _5, type_ToInt64Srt, _3, type_ToInt64Lng)(__VA_ARGS__))

#define	type_ToUint64(...)	\
	((uint64)type_ToInt64(__VA_ARGS__))

#if lib_word_size >= 32
#define	type_ToInt		type_ToInt32
#define	type_ToUint		type_ToUint32
#else
#define	type_ToInt		type_ToInt16
#define	type_ToUint		type_ToUint16
#endif

inline float type_ToFloatSrt(ushort v1, ushort v0)
{
	float ret;
	*((ushort*)&ret) = v0;
	*(((ushort*)&ret) + 1) = v1;
	return ret;
}

inline float type_ToFloatByt(byte v3, byte v2, byte v1, byte v0)
{
	float ret;
	*((byte*)&ret) = v0;
	*(((byte*)&ret) + 1) = v1;
	*(((byte*)&ret) + 2) = v2;
	*(((byte*)&ret) + 3) = v3;
	return ret;
}

#define	type_ToFloat(...)	\
	macro_Fn(macro_Fn4(__VA_ARGS__, type_ToFloatByt, _3, type_ToFloatSrt)(__VA_ARGS__))

inline double type_ToDoubleLng(ulong v1, ulong v0)
{
	double ret;
	*((ulong*)&ret) = v0;
	*(((ulong*)&ret) + 1) = v1;
	return ret;
}

inline double type_ToDoubleSrt(ushort v3 ,ushort v2, ushort v1, ushort v0)
{
	double ret;
	*((ushort*)&ret) = v0;
	*(((ushort*)&ret) + 1) = v1;
	*(((ushort*)&ret) + 2) = v2;
	*(((ushort*)&ret) + 3) = v3;
	return ret;
}

inline double type_ToDoubleByt(byte v7, byte v6, byte v5, byte v4, byte v3, byte v2, byte v1, byte v0)
{
	double ret;
	*((byte*)&ret) = v0;
	*(((byte*)&ret) + 1) = v1;
	*(((byte*)&ret) + 2) = v2;
	*(((byte*)&ret) + 3) = v3;
	*(((byte*)&ret) + 4) = v4;
	*(((byte*)&ret) + 5) = v5;
	*(((byte*)&ret) + 6) = v6;
	*(((byte*)&ret) + 7) = v7;
	return ret;
}

#define	type_ToDouble(...)	\
	macro_Fn(macro_Fn8(__VA_ARGS__, type_ToDoubleByt, _7, _6, _5, type_ToDoubleSrt, _3, type_ToDoubleLng)(__VA_ARGS__))

#define	ToNibble		type_ToNibble
#define	ToNibl			type_ToNibble
#define	ToByte			type_ToByte
#define	ToSbyte			type_ToSbyte
#define	ToChar			type_ToChar
#define	ToShort			type_ToShort
#define	ToUshort		type_ToUshort
#define	ToInt			type_ToInt
#define	ToUint			type_ToUint
#define	ToLong			type_ToLong
#define	ToUlong			type_ToUlong
#define	ToInt8			type_ToInt8
#define	ToUint8			type_ToUint8
#define	ToInt16			type_ToInt16
#define	ToUint16		type_ToUint16
#define	ToInt32			type_ToInt32
#define	ToUint32		type_ToUint32
#define	ToInt64			type_ToInt64
#define	ToUint64		type_ToUint64
#define	ToFloat			type_ToFloat
#define	ToDouble		type_ToDouble


// Function:
// BinaryToHex(*dst, sz, *src, off, len, opt)
// [BinaryToHex / BinToHex]
// 
// Read hexadecimal string (dst) of maximum specified size (sz) of
// the soure binary data (src + off) of specified length (len). The
// options (opt) specify how the conversion is to be performed, and
// it takes as input a set of flags. If source base address is not
// specified, this library's internal buffer is assumed as the source
// base address.
// 
// Parameters:
// dst:	      the destination string where hex string will be stored
// sz:        the maximum possible size of the hex string (buffer size)
// src:	      the base address of source binary data
// off:	      offset to the binary data to be converted (src + off)
// len:	      length of data to be converted
// opt:	      conversion options (TYPE_add_space, TYPE_add_char, TYPE_big_endian)
// 
// Returns:
// nothing
// 
#define type_HexCharToBin(ch)		(((ch) <= '9')? (ch)-'0' : (ch)-'7')

#define type_BinToHexChar(bn)		(((bn) <= 9)? (bn)+'0' : (bn)+'7' )

#define	type_no_space			0

#define type_add_space			1

#define	type_has_space			1

#define	type_no_char			0

#define type_add_char			2

#define	type_has_char			2

#define	type_little_endian		0

#define type_big_endian			4

string type_BinaryToHexF(string dst, int sz, byte* src, int len, byte opt)
{
	string dend = dst + (sz - 1);
	src += (opt & type_big_endian)? 0 : (len - 1);
	int stp = (opt & type_big_endian)? 1 : -1;
	for(int i=0; i<len; i++, src+=stp)
	{
		*dst = type_BinToHexChar(*src >> 4); dst++; if(dst >= dend) break;
		*dst = type_BinToHexChar(*src & 0xF); dst++; if(dst >= dend) break;
		if(opt & type_add_char) { *dst = (*src < 32 || *src > 127)? '.' : *src; dst++; if(dst >= dend) break;}
		if(opt & type_add_space) { *dst = ' '; dst++; if(dst >= dend) break;}
	}
	*dst = '\0';
	return dst;
}

#define	type_BinaryToHex(dst, sz, src, off, len, opt)	\
	type_BinaryToHexF((string)(dst), (int)(sz), (byte*)(src) + (off), (int)(len), (byte)(opt))

#define	type_BinToHex	type_BinaryToHex


// Function:
// HexToBinary(*dst, off, len, *src, opt)
// [HexToBinary / HexToBin]
// 
// Writes binary data from the source hex string (src) to the destination
// address (dst + off) of specified length len. The options (opt) specify
// how the conversion is to be performed, and it takes as input a set of
// flags. If destination base address is not specified, this library's
// internal buffer is assumed as the destination base address.
// 
// Parameters:
// dst:	      the base address of destination
// off:	      the destination offset where the binary data will be stored (dst + off)
// len:       length of data at destination
// src:	      the hex string to be converted
// opt:	      conversion options (TYPE_has_space, TYPE_has_char, TYPE_big_endian)
//
// Returns:
// the converted data (dst)
// 
void type_HexToBinaryD(byte* dst, int len, string src, byte opt)
{
	char* psrc = src + strlen(src) - 1;
	dst += (opt & type_big_endian)? (len - 1) : 0;
	int i ,stp = (opt & type_big_endian)? -1 : 1;
	for(i=0; i<len; i++, dst+=stp)
	{
		if(opt & type_has_space) psrc--;
		if(opt & type_has_char) psrc--;
		*dst = (psrc < src)? 0 : type_HexCharToBin(*psrc); psrc--;
		*dst |= (psrc < src)? 0 : type_HexCharToBin(*psrc) << 4; psrc--;
	}
}

#define	type_HexToBinary(dst, off, len, src, opt)	\
	type_HexToBinaryD((byte*)(dst) + (off), (int)(len), (string)(src), (byte)(opt))

#define	type_HexToBin	type_HexToBinary


// Function:
// ParseBinary(*str, *val)
// [ParseBinary / ParseBin]
// 
// Parses a binary number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseBinaryF(string str, int64* val)
{
	int64 bin = 0;
	for(; *str; str++)
	{
		if((*str != '0') && (*str != '1')) return -1;
		bin = (bin << 1) | (*str - '0');
	}
	*val = bin;
	return 0;
}

#define	type_ParseBinaryM(str, val)	\
	type_ParseBinaryF((string)(str), (int64*)(val))

#define	type_ParseBinaryO(str, off, val)	\
	type_ParseBinaryM((byte*)(str) + (off), val)

#define	type_ParseBinary(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseBinaryO, type_ParseBinaryM)(__VA_ARGS__))

#define	type_ParseBin	type_ParseBinary


// Function:
// ParseOctal(*str, *val)
// [ParseOctal / ParseOct]
// 
// Parses an octal number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseOctalF(string str, int64* val)
{
	int64 oct = 0;
	for(; *str; str++)
	{
		if((*str < '0') || (*str >= '7')) return -1;
		oct = (oct << 3) | (*str - '0');
	}
	*val = oct;
	return 0;
}

#define	type_ParseOctalM(str, val)	\
	type_ParseOctalF((string)(str), (int64*)(val))

#define	type_ParseOctalO(str, off, val)	\
	type_ParseOctalM((byte*)(str) + (off), val)

#define	type_ParseOctal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseOctalO, type_ParseOctalM)(__VA_ARGS__))

#define	type_ParseOct	type_ParseOctal


// Function:
// ParseHexadecimal(*str, *val)
// [ParseHexadecimal / ParseHex]
// 
// Parses a hexadecimal number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseHexadecimalF(string str, int64* val)
{
	int64 hex = 0;
	for(; *str; str++)
	{
		if((*str >= '0') && (*str <= '9')) hex = (hex << 4) | (*str - '0');
		else if((*str >= 'A') && (*str <= 'F')) hex = (hex << 4) | (*str - 'A' + 10);
		else if((*str >= 'a') && (*str <= 'f')) hex = (hex << 4) | (*str - 'a' + 10);
		else return -1;
	}
	*val = hex;
	return 0;
}

#define	type_ParseHexadecimalM(str, val)	\
	type_ParseHexadecimalF((string)(str), (int64*)(val))

#define	type_ParseHexadecimalO(str, off, val)	\
	type_ParseHexadecimalM((byte*)(str) + (off), val)

#define	type_ParseHexadecimal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseHexadecimalO, type_ParseHexadecimalM)(__VA_ARGS__))

#define	type_ParseHex	type_ParseHexadecimal


// Function:
// ParseDecimal(*str, *val)
// [ParseDecimal / ParseDec]
// 
// Parses a decimal number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseDecimalF(string str, int64* val)
{
	int64 dec = 0;
	for(; *str; str++)
	{
		if((*str < '0') || (*str >= '7')) return -1;
		dec = (dec * 10) + (*str - '0');
	}
	*val = dec;
	return 0;
}

#define	type_ParseDecimalM(str, val)	\
	type_ParseDecimalF((string)(str), (int64*)(val))

#define	type_ParseDecimalO(str, off, val)	\
	type_ParseDecimalM((byte*)(str) + (off), val)

#define	type_ParseDecimal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseDecimalO, type_ParseDecimalM)(__VA_ARGS__))

#define	type_ParseDec	type_ParseDecimal


// Function:
// ParseFloat(*str, *val)
// [ParseFloat]
// 
// Parses a floating-point number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFloatF(string str, float* val)
{
	int ret = sscanf_s(str, "%f", val);
	return (ret == 1)? 0 : -1;
}

#define	type_ParseFloatM(str, val)	\
	type_ParseFloatF((string)(str), (float*)(val))

#define	type_ParseFloatO(str, off, val)	\
	type_ParseFloatM((byte*)(str) + (off), val)

#define	type_ParseFloat(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFloatO, type_ParseFloatM)(__VA_ARGS__))


// Function:
// ParseDouble(*str, *val)
// [ParseDouble]
// 
// Parses a double precision floating-point number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseDoubleF(string str, double* val)
{
	int ret = sscanf_s(str, "%lf", val);
	return (ret == 1)? 0 : -1;
}

#define	type_ParseDoubleM(str, val)	\
	type_ParseDoubleF((string)(str), (double*)(val))

#define	type_ParseDoubleO(str, off, val)	\
	type_ParseDoubleM((byte*)(str) + (off), val)

#define	type_ParseDouble(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseDoubleO, type_ParseDoubleM)(__VA_ARGS__))


// Function:
// ParseString(*str, *val)
// [ParseString / ParseStr]
// 
// Parses a string (as a number) in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseStringF(string str, int64* val)
{
	num i = 0;
	int64 strn = 0;
	for(; *str; str++, i++)
	{
		if(i >= 8) return -1;
		strn = (strn << 8) | *str;
	}
	*val = strn;
	return 0;
}

#define	type_ParseStringM(str, val)	\
	type_ParseStringF((string)(str), (int64*)(val))

#define	type_ParseStringO(str, off, val)	\
	type_ParseStringM((byte*)(str) + (off), val)

#define	type_ParseString(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseStringO, type_ParseStringM)(__VA_ARGS__))

#define	type_ParseStr	type_ParseString


// Function:
// ParseFullBinary(*str, *val)
// [ParseFullBinary / ParseFullBin]
// 
// Parses a full binary number in a string in format
// (0b????) or (????b)
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullBinaryF(string str, int64* val)
{
	num ret = -1;
	bool is_neg = false;
	if(*str == '-') is_neg = true;
	if(*str == '+' || *str == '-') str++;
	if((*str == '0') && ((str[1] == 'b') || (str[1] == 'B'))) ret = type_ParseBin(str, 2, val);
	else
	{
		int lsti = strlen(str) - 1;
		char lst = str[lsti];
		if((lst == 'b') || (lst == 'B'))
		{
			str[lsti] = '\0';
			ret = type_ParseBin(str, val);
			str[lsti] = lst;
		}
	}
	if(ret >= 0 && is_neg) *val = -(*val);
	return ret;
}

#define	type_ParseFullBinaryM(str, val)	\
	type_ParseFullBinaryF((string)(str), (int64*)(val))

#define	type_ParseFullBinaryO(str, off, val)	\
	type_ParseFullBinaryM((byte*)(str) + (off), val)

#define	type_ParseFullBinary(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullBinaryO, type_ParseFullBinaryM)(__VA_ARGS__))

#define	type_ParseFullBin	type_ParseFullBinary


// Function:
// ParseFullOctal(*str, *val)
// [ParseFullOctal / ParseFullOct]
// 
// Parses a full octal number in a string in format
// (0o????) or (????o)
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullOctalF(string str, int64* val)
{
	num ret = -1;
	bool is_neg = false;
	if(*str == '-') is_neg = true;
	if(*str == '+' || *str == '-') str++;
	if((*str == '0') && ((str[1] == 'o') || (str[1] == 'O'))) ret = type_ParseOct(str, 2, val);
	else
	{
		int lsti = strlen(str) - 1;
		char lst = str[lsti];
		if((lst == 'o') || (lst == 'O'))
		{
			str[lsti] = '\0';
			ret = type_ParseOct(str, val);
			str[lsti] = lst;
		}
	}
	if(ret >= 0 && is_neg) *val = -(*val);
	return ret;
}

#define	type_ParseFullOctalM(str, val)	\
	type_ParseFullOctalF((string)(str), (int64*)(val))

#define	type_ParseFullOctalO(str, off, val)	\
	type_ParseFullOctalM((byte*)(str) + (off), val)

#define	type_ParseFullOctal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullOctalO, type_ParseFullOctalM)(__VA_ARGS__))

#define	type_ParseFullOct	type_ParseFullOctal


// Function:
// ParseFullHexadecimal(*str, *val)
// [ParseFullHexadecimal / ParseFullHex]
// 
// Parses a full hexadecimal number in a string in format
// (0x????) or (????x) or (0h????) or (????h)
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullHexadecimalF(string str, int64* val)
{
	num ret = -1;
	bool is_neg = false;
	if(*str == '-') is_neg = true;
	if(*str == '+' || *str == '-') str++;
	if((*str == '0') && ((str[1] == 'x') || (str[1] == 'X') || (str[1] == 'h') || (str[1] == 'H'))) ret = type_ParseHex(str, 2, val);
	else
	{
		int lsti = strlen(str) - 1;
		char lst = str[lsti];
		if((lst == 'x') || (lst == 'X') || (lst == 'h') || (lst == 'H'))
		{
			str[lsti] = '\0';
			ret = type_ParseHex(str, val);
			str[lsti] = lst;
		}
	}
	if(ret >= 0 && is_neg) *val = -(*val);
	return ret;
}

#define	type_ParseFullHexadecimalM(str, val)	\
	type_ParseFullHexadecimalF((string)(str), (int64*)(val))

#define	type_ParseFullHexadecimalO(str, off, val)	\
	type_ParseFullHexadecimalM((byte*)(str) + (off), val)

#define	type_ParseFullHexadecimal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullHexadecimalO, type_ParseFullHexadecimalM)(__VA_ARGS__))

#define	type_ParseFullHex	type_ParseFullHexadecimal


// Function:
// ParseFullDecimal(*str, *val)
// [ParseFullDecimal / ParseFullDec]
// 
// Parses a full decimal number in a string in format
// (????) or (0d????) or (????d)
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullDecimalF(string str, int64* val)
{
	num ret = -1;
	bool is_neg = false;
	if(*str == '-') is_neg = true;
	if(*str == '+' || *str == '-') str++;
	if((*str == '0') && ((str[1] == 'd') || (str[1] == 'D'))) ret = type_ParseDec(str, 2, val);
	else
	{
		int lsti = strlen(str) - 1;
		char lst = str[lsti];
		if((lst == 'd') || (lst == 'D'))
		{
			str[lsti] = '\0';
			ret = type_ParseDec(str, val);
			str[lsti] = lst;
		}
		else ret = type_ParseDec(str, val);
	}
	if(ret >= 0 && is_neg) *val = -(*val);
	return ret;
}

#define	type_ParseFullDecimalM(str, val)	\
	type_ParseFullDecimalF((string)(str), (int64*)(val))

#define	type_ParseFullDecimalO(str, off, val)	\
	type_ParseFullDecimalM((byte*)(str) + (off), val)

#define	type_ParseFullDecimal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullDecimalO, type_ParseFullDecimalM)(__VA_ARGS__))

#define	type_ParseFullDec	type_ParseFullDecimal


// Function:
// ParseFullFloat(*str, *val)
// [ParseFullFloat]
// 
// Parses a full floating-point number
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullFloatF(string str, int64* val)
{
	num ret = -1;
	int lsti = strlen(str) - 1;
	char lst = str[lsti];
	if((lst == 'f') || (lst == 'F')) ret = type_ParseFloat(str, val);
	return ret;
}

#define	type_ParseFullFloatM(str, val)	\
	type_ParseFullFloatF((string)(str), (int64*)(val))

#define	type_ParseFullFloatO(str, off, val)	\
	type_ParseFullFloatM((byte*)(str) + (off), val)

#define	type_ParseFullFloat(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullFloatO, type_ParseFullFloatM)(__VA_ARGS__))


// Function:
// ParseFullDouble(*str, *val)
// [ParseFullDouble]
// 
// Parses a full double precision floating-point number
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullDoubleF(string str, int64* val)
{
	num ret = -1;
	int lsti = strlen(str) - 1;
	char lst = str[lsti];
	if((lst != 'f') && (lst != 'F')) ret = type_ParseDouble(str, val);
	return ret;
}

#define	type_ParseFullDoubleM(str, val)	\
	type_ParseFullDoubleF((string)(str), (int64*)(val))

#define	type_ParseFullDoubleO(str, off, val)	\
	type_ParseFullDoubleM((byte*)(str) + (off), val)

#define	type_ParseFullDouble(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullDoubleO, type_ParseFullDoubleM)(__VA_ARGS__))


// Function:
// ParseFullString(*str, *val)
// [ParseFullString / ParseFullStr]
// 
// Parses a full string number in a string in format
// ('????') or ("????")
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullStringF(string str, int64* val)
{
	num ret = -1;
	bool is_neg = false;
	if(*str == '-') is_neg = true;
	if(*str == '+' || *str == '-') str++;
	int lsti = strlen(str) - 1;
	char lst = str[lsti];
	if(((*str == '\'') && (lst == '\'')) || ((*str == '\"') && (lst == '\"')))
	{
		str[lsti] = '\0';
		ret = type_ParseStr(str, 1, val);
		str[lsti] = lst;
	}
	if(ret >= 0 && is_neg) *val = -(*val);
	return ret;
}

#define	type_ParseFullStringM(str, val)	\
	type_ParseFullStringF((string)(str), (int64*)(val))

#define	type_ParseFullStringO(str, off, val)	\
	type_ParseFullStringM((byte*)(str) + (off), val)

#define	type_ParseFullString(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullStringO, type_ParseFullStringM)(__VA_ARGS__))

#define	type_ParseFullStr	type_ParseFullString


// Function:
// ParseNumber(*str, *val)
// [ParseNumber / ParseNum]
// 
// Parses a number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// num_type:	the type of number parsed
// 
#define	type_unknown		-1
#define	type_binary			1
#define	type_octal			2
#define	type_decimal		3
#define	type_hexadecimal	4
#define	type_float			5
#define	type_double			6
#define	type_string			7
#define	type_bin	type_binary
#define	type_oct	type_octal
#define	type_hex	type_hexadecimal
#define	type_str	type_string

num type_ParseNumberF(string str, int64* val)
{
	num ret = type_ParseFullBin(str, val);
	if(ret < 0) ret = type_ParseFullOct(str, val);
	if(ret < 0) ret = type_ParseFullHex(str, val);
	if(ret < 0) ret = type_ParseFullDec(str, val);
	if(ret < 0) ret = type_ParseFullFloat(str, val);
	if(ret < 0) ret = type_ParseFullDouble(str, val);
	if(ret < 0) ret = type_ParseFullStr(str, val);
	return ret;
}

#define	type_ParseNumberM(str, val)	\
	type_ParseNumberF((string)(str), (int64*)(val))

#define	type_ParseNumberO(str, off, val)	\
	type_ParseNumberM((byte*)(str) + (off), val)

#define	type_ParseNumber(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseNumberO, type_ParseNumberM)(__VA_ARGS__))

#define	type_ParseNum			type_ParseNumber
#define	type_ParseFullNumber	type_ParseNumber
#define	type_ParseFullNum		type_ParseNumber


#endif
