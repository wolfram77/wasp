/*
----------------------------------------------------------------------------------------
	cc2500: CC2500 2.4GHz RF support module
	File: ext_cc2500.h

	This file is part of WASP. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WASP is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WASP is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WASP.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	cc2500 is a CC2500 2.4GHz RF support module for WASP. It has been developed for
	for Flood Prediction and Alert System project. The sensor is available at element14 > 
	Semiconductors - ICs > Sensors > Pressure > Max Operating Pressure = 700 kPa. For more
	information, please visit: https://github.com/wolfram77/WASP.
*/



#ifndef	_ext_cc2500_h_
#define	_ext_cc2500_h_



// Requisite headers
#include "std/std_macro.h"
#include "std/std_mem.h"
#include "std/std_reg.h"
#include "std/std_type.h"



// Select abbreviation level
// 
// The default abbreviation level is 7 i.e., members of this
// module can be accessed as cc2500<function_name> directly.
// The abbreviation level can be selected in the main header
// file of WASP library
#ifndef	cc2500_abbr
#define	cc2500_abbr		7
#endif



// pressure sensor object
typedef struct _cc2500
{
	int		pin;		// pin number connected
	float	mulf;		// multiplication factor
	float	addf;		// addition factor
}cc2500;

#if cc2500_abbr >= 1
typedef	cc2500	cc2500;
#endif



// Function:
// Init(*snsr)
// 
// Initializes a new cc2500 sensor object that can be used for
// reading from the sensor
// 
// Parameters:
// *snsr:	pointer to cc2500 sensor object
// pin:		pin to which the sensor is connected
// supply:	supply voltage to the sensor (in volts)
// offset:	fractional offset (of supply) of the sensor
// ref:		reference voltage used with ADC (in volts)
// bits:	number of bits of resolution of ADC
// max:		maximum pressure read by sensor
// 
// Returns:
// value:	the value recorded by sensor
// 
void cc2500_Init(cc2500* snsr, int pin, float supply, float offset, float ref, int bits, float max)
{
	snsr->pin = pin;
	snsr->mulf = ((ref / ((1 << bits) - 1)) / supply) * max;
	snsr->addf = (-offset) * max;
}

#if cc2500_abbr >= 1
#define	cc2500Init		cc2500_Init
#endif

#if	cc2500_abbr >= 2
#define	Init		cc2500_Init
#endif



// Function:
// Read(*snsr)
// 
// Reads pressure value using a sensor object
// 
// Parameters:
// *snsr:	sensor object to use for reading
// 
// Returns:
// value:	the value recorded by sensor
// 
#define	cc2500_Read(snsr)	\
	((analogRead((snsr)->pin) * (snsr)->mulf) + (snsr)->addf)

#if cc2500_abbr >= 1
#define	cc2500Read			cc2500_Read
#endif

#if	cc2500_abbr >= 2
#define	Read			cc2500_Read
#endif



#endif
