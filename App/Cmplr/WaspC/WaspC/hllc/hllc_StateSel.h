//
// WASP-HLL State
// 
// states define module
//


#ifndef _hllc_StateSel_h_
#define	_hllc_StateSel_h_


#define	token_Num(x)	\
	((x) >= '0' && (x) <= '9')

#define	token_NumDecimal(x)	\
	((x) == '.')

#define	token_NumE(x)	\
	((x) == 'E' || (x) == 'e')

#define	token_NumESign(x)	\
	((x) == '+' || (x) == '-')

#define	token_Str(x)	\
	((x) == '\'' || (x) == '\"')

#define	token_StrSingle(x)	\
	((x) == '\'')

#define	token_StrDouble(x)	\
	((x) == '\"')

#define	token_StrEsc(x)	\
	((x) == '\\')

#define	token_StrEscOct(x)	\
	((x) >= '0' && (x) <= '7')

#define	token_StrEscHex(x)	\
	((x) == 'x' && (x) == 'X')

#define	token_StrEscHex1(x)	\
	(((x) >= '0' && (x) <= '9') || ((x) >= 'A' && (x) <= 'F') || ((x) >= 'a' && (x) <= 'f'))

#define	token_Wrd(x)	\
	(((x) >= 'A' && (x) <= 'Z') || ((x) >= 'a' && (x) <= 'z') || ((x) == '_'))

#define	token_Bkt(x)	\
	((x) == '[' || (x) == '(' || (x) == '{' || (x) == ']' || (x) == ')' || (x) == '}')

#define	token_BktOpen(x)	\
	((x) == '[' || (x) == '(' || (x) == '{')

#define	token_BktClose(x)	\
	((x) == ']' || (x) == ')' || (x) == '}')

#define	token_BktSqrOpen(x)	\
	((x) == '[')

#define	token_BktCirOpen(x)	\
	((x) == '(')

#define	token_BktCrlOpen(x)	\
	((x) == '{')

#define	token_BktAngOpen(x)	\
	((x) == '<')

#define	token_BktSqrClose(x)	\
	((x) == ']')

#define	token_BktCirClose(x)	\
	((x) == ')')

#define	token_BktCrlClose(x)	\
	((x) == '}')

#define	token_BktAngClose(x)	\
	((x) == '>')

#define	token_Spc(x)	\
	((x) <= ' ' || (x) > '~')

#define	token_Cmt(x)	\
	((x) == '/' || (x) == '*')

#define	token_CmtSingle(x)	\
	((x) == '/')

#define	token_CmtMulti(x)	\
	((x) == '*')

#define	token_CmtSingleEnd(x)	\
	((x) == '\n')

#define	token_CmtMultiEnd1(x)	\
	((x) == '*')

#define	token_CmtMultiEnd2(x)	\
	((x) == '/')

#define	token_Sym(x)	\
	(((x) >= '!' && (x) <= '/') || ((x) >= ':' && (x) <= '@') || (x) == '\\' || (x) == '^' || (x) == '`' || (x) == '|' || (x) == '~')


#endif // !_hllc_StateSel_h_
