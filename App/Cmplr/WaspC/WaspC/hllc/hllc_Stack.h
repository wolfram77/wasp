//
// WASP-HLL Stack
// 
// stack module
//


#ifndef _hllc_Stack_h_
#define	_hllc_Stack_h_


#include <stdlib.h>

class Stack
{
public:
	void**	Data;
	int		Top, Capacity;

public:
	inline Stack(int capacity)
	{
		Capacity = capacity;
		Data = (void**) malloc(Capacity * sizeof(void*));
		Top = -1;
	}
	inline ~Stack()
	{
		free(Data);
	}

	// Peek item (pointer) at top of stack
	inline void* Peek()
	{
		if(Top == -1) return null;
		return Data[Top];
	}

	// Push item (pointer)
	inline void Push(void* item)
	{
		if(Top == Capacity)
		{
			Capacity <<= 1;
			Data = (void**) realloc(Data, Capacity);
		}
		Data[++Top] = item;
	}

	// Pop item (pointer)
	inline void* Pop()
	{
		if(Top == -1) return null;
		return Data[Top--];
	}
};


#endif // !_hllc_Stack_h_
