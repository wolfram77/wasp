//
// WASP-HLL TokenParser
// 
// parsing module
//


#ifndef _hllc_TokenParser_h_
#define	_hllc_TokenParser_h_


#include "hllc_Stack.h"
#include "hllc_State.h"
#include "hllc_StateSel.h"
#include "hllc_Token.h"

class TokenParser
{
private:
	char*	Data;
	int		Length;
	int		Capacity;
	int		BufferSize;
	int		Start, Ptr;
	int		State;
	Token*	TokenPtr;
	Stack*	TokenStack;
public:
	FILE*	File;
	Token*	Tokens;

private:
	// Read a line from file
	bool ReadLine()
	{
		int bgn = (Start == -1)? Ptr : Start;
		if(bgn > 0)
		{
			memcpy_s(Data, Capacity, Data + bgn, Length - bgn);
			Length -= bgn;
			Start = (Start == -1)? -1 : 0;
			Ptr -= bgn;
		}
		char* ret = fgets(Data + Length, Capacity - Length, File);
		if(ret == null) return false;
		Length += strlen(Data + Length);
		if(Length + BufferSize > Capacity)
		{
			Capacity += BufferSize;
			Data = (char*) realloc(Data, Capacity);
		}
		return true;
	}

	// Adds a token, if available (Start > 0)
	void AddToken(int start, int stop)
	{
		Token* token = new Token();
		token->Type = State;
		if(start < stop) token->SetValue(Data + start, stop - start);
		if(TokenPtr) TokenPtr->AddAfter(token);
		else if(TokenStack->Top >= 0) ((Token*)TokenStack->Peek())->Down = token;
		else Tokens = token;
		TokenPtr = token;
	}
	// Next Level
	void NextLevel()
	{
		TokenStack->Push(TokenPtr);
		TokenPtr = null;
	}
	// Previous Level
	void PrevLevel()
	{
		TokenPtr = (Token*) TokenStack->Pop();
	}

	// Process State
	void StateBkt(char val)
	{
		if(token_BktOpen(val))
		{
			if(token_BktSqrOpen(val)) State = token_bkt_sqr;
			else if(token_BktCirOpen(val)) State = token_bkt_cir;
			else if(token_BktCrlOpen(val)) State = token_bkt_crl;
			AddToken(0, 0);
			NextLevel();
		}
		else PrevLevel();
		State = token_dflt;
		Start = -1;
	}
	void StateDflt(char val)
	{
		if(Start >= 0) AddToken(Start, Ptr);
		Start = Ptr;
		if(token_Spc(val)) { State = token_dflt; Start = -1; }
		else if(token_Num(val)) State = token_num;
		else if(token_StrSingle(val)) State = token_str_single;
		else if(token_StrDouble(val)) State = token_str_double;
		else if(token_Wrd(val)) State = token_wrd;
		else if(token_Bkt(val)) StateBkt(val);
		else State = token_sym;
	}
	void StateNum(char val)
	{
		if(token_Num(val))
		{
			if(token_MatchState(State, token_num_e) && !token_MatchState(State, token_num_esign)) StateDflt(val);
			else State |= token_num;
		}
		else if(token_NumDecimal(val))
		{
			if(token_MatchState(State, token_num_decimal) || token_MatchState(State, token_num_e)) StateDflt(val);
			else State |= token_num_decimal;
		}
		else if(token_NumE(val))
		{
			if(token_MatchState(State, token_num_e)) StateDflt(val);
			else State |= token_num_e;
		}
		else if(token_NumESign(val))
		{
			if(token_MatchState(State, token_num_e)) State |= token_num_esign;
			else StateDflt(val);
		}
		else if(token_Wrd(val)) State |= token_num_char;
		else StateDflt(val); // Spc, Str, Bkt, Sym
	}
	void StateStr(char val)
	{
		if(token_MatchState(State, token_str_esc)) State = token_str;
		else if(token_StrEsc(val)) State = token_str_esc;
		else if(val == Data[Start])
		{
			AddToken(Start, Ptr + 1);
			State = token_dflt;
			Start = -1;
		}
		else State = token_str;
	}
	void StateWrd(char val)
	{
		if(token_Wrd(val)) State = token_wrd;
		else if(token_Num(val)) State = token_wrd;
		else StateDflt(val);
	}
	void StateSym(char val)
	{
		// token_Cmt
		if(token_Cmt(val) && Data[Start] == '/')
		{
			if(token_CmtSingle(val)) State = token_cmt_single;
			else State = token_cmt_multi;
		}
		else if(token_Sym(val))
		{
			if(Data[Start] == ';') StateDflt(val);
			else if(Data[Start] == '=' && val != '=') StateDflt(val);
			else State |= token_sym;
		}
		else StateDflt(val);
	}
	void StateCmt(char val)
	{
		if(token_MatchState(State, token_cmt_single))
		{
			if(token_CmtSingleEnd(val))
			{
				State = token_dflt;
				Start = -1;
			}
		}
		else
		{
			if(token_MatchState(State, token_cmt_multi_end1) && token_CmtMultiEnd2(val))
			{
				State = token_dflt;
				Start = -1;
			}
			else if(token_CmtMultiEnd1(val)) State |= token_cmt_multi_end1;
		}
	}

	// Parse a character
	bool ParseChar(char val)
	{
		// State Selection
		if(token_GroupState(State, token_dflt)) StateDflt(val);
		else if(token_GroupState(State, token_num)) StateNum(val);
		else if(token_GroupState(State, token_str)) StateStr(val);
		else if(token_GroupState(State, token_wrd)) StateWrd(val);
		else if(token_GroupState(State, token_sym)) StateSym(val);
		else if(token_GroupState(State, token_cmt)) StateCmt(val);
		return false;
	}

public:
	TokenParser(char* file, int bufferSize)
	{
		fopen_s(&File, file, "r");
		if(!File) return;
		Data = (char*) malloc(bufferSize);
		Length = 0;
		Capacity = bufferSize << 1;
		BufferSize = bufferSize;
		Start = -1; Ptr = 0;
		State = token_dflt;
		Tokens = null;
		TokenPtr = null;
		TokenStack = new Stack(256);
	}
	~TokenParser()
	{
		if(Data) free(Data);
		if(File) fclose(File);
		if(TokenStack) free(TokenStack);
		Data = null;
		File = null;
		TokenStack = null;
	}

	// Parse the file
	Token* Parse()
	{
		while(Ptr < Length || ReadLine())
		{
			ParseChar(Data[Ptr]);
			Ptr++;
		}
		ParseChar(' ');
		return Tokens;
	}
};


#endif // !_hllc_TokenParser_h_
