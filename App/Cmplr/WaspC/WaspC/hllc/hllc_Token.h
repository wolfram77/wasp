//
// WASP-HLL Token
// 
// token module
//


#ifndef _hllc_Token_h_
#define	_hllc_Token_h_


#include <stdlib.h>
#include "hllc_State.h"

// A Token
class Token
{
private:
	bool	alloc;
public:
	int		Type;
	void	*Value;
	Token	*Prev, *Next;
	Token	*Down;

public:
	Token()
	{
		alloc = false;
		Type = 0;
		Value = null;
		Prev = null;
		Next = null;
		Down = null;
	}
	~Token()
	{
		if(alloc) free(Value);
		if(Prev) Prev->Next = Next;
		if(Next) Next->Prev = Prev;
	}

	// Set value (direct)
	void SetValue(void* value)
	{
		if(alloc) free(Value);
		Value = value;
		alloc = false;
	}
	// Set value (alloc)
	void SetValue(void* value, int valueSize)
	{
		if(alloc) free(Value);
		Value = malloc(valueSize + 1);
		memcpy_s(Value, valueSize, value, valueSize);
		((char*)Value)[valueSize] = '\0';
		alloc = true;
	}

	// Add token after
	void AddAfter(Token* token)
	{
		token->Next = Next;
		token->Prev = this;
		if(Next) Next->Prev = token;
		Next = token;
	}
	// Add token before
	void AddBefore(Token* token)
	{
		token->Prev = Prev;
		token->Next = this;
		if(Prev) Prev->Next = token;
		Prev = token;
	}

	// Remove token
	void Remove()
	{
		if(Prev) Prev->Next = Next;
		if(Next) Next->Prev = Prev;
	}
};


#endif // !_hllc_Token_h_
