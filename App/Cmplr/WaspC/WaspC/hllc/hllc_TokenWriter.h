//
// WASP-HLL Token Writer
// 
// token to file writing module
//


#ifndef _hllc_Writer_h_
#define	_hllc_Writer_h_


#include "hllc_Stack.h"
#include "hllc_Token.h"

// Print all tokens
class _TokenWriter
{
private:
	// Write a Token Value
	void WriteToken(FILE* stream, Token* token, short mode)
	{
		if(token->Value)
		{
			if(token->Prev)
			{
				bool prevSym = token_GroupState(token->Prev->Type, token_sym);
				bool prevBkt = token_GroupState(token->Prev->Type, token_bkt);
				bool currSym = token_GroupState(token->Type, token_sym);
				bool currBkt = token_GroupState(token->Type, token_bkt);
				bool nospc = (prevSym && !currSym) || (!prevSym && currSym) || (prevBkt && !currBkt) || (!prevBkt && currBkt);
				if(!nospc) fprintf_s(stream, " ");
			}
			if(strcmp((char*) token->Value, ";") == 0) fprintf_s(stream, ";\n");
			else fprintf_s(stream, "%s", token->Value);
		}
		if(token_GroupState(token->Type, token_bkt))
		{
			char* bkt;
			if(mode == 1) bkt = (token->Type == token_bkt_sqr)? "[": ((token->Type == token_bkt_cir)? "(" : "{\n");
			else if(mode == 0) bkt = (token->Type == token_bkt_sqr)? "]": ((token->Type == token_bkt_cir)? ")" : "}\n");
			else bkt = (token->Type == token_bkt_sqr)? "[]": ((token->Type == token_bkt_cir)? "()" : "{}\n");
			fprintf_s(stream, "%s", bkt);
		}
	}
	
public:
	void Display(Token* token)
	{
		char c;
		bool godown = true;
		Stack* stack = new Stack(256);
		while(token)
		{
			if(token->Value) printf_s("%d\t- %s[%d]\n", stack->Top, token->Value, token->Type);
			else printf_s("%d\t- [%d]\n", stack->Top, token->Type);
			scanf_s("%c", &c);
			if(godown && token->Down)
			{
				stack->Push(token);
				token = token->Down;
			}
			else if(!(token->Next)) { token = (Token*) stack->Pop(); godown = false; }
			else { token = token->Next; godown = true; }
		}
		delete stack;
	}

	// Write all tokens to a file
	void WriteToFile(char* file, Token* token)
	{
		FILE* dst;
		bool goDown = true;
		Stack* stack = new Stack(256);
		fopen_s(&dst, file, "w");
		while(token)
		{
			if(token_GroupState(token->Type, token_bkt))
			{
				if(token->Down) WriteToken(dst, token, goDown);
				else WriteToken(dst, token, 2);
			}
			else WriteToken(dst, token, false);
			if(goDown && token->Down)
			{
				stack->Push(token);
				token = token->Down;
			}
			else if(!(token->Next))
			{
				token = (Token*) stack->Pop();
				goDown = false;
			}
			else
			{
				token = token->Next;
				goDown = true;
			}
		}
		delete stack;
	}
}TokenWriter;

#endif // !_hllc_Support_h_
