//
// WASP-HLL State
// 
// states define module
//


#ifndef _hllc_State_h_
#define	_hllc_State_h_


// Token State
#define	token_dflt				0x0000
#define	token_num				0x1000
#define	token_num_decimal		0x1001
#define	token_num_e				0x1002
#define	token_num_esign			0x1004
#define	token_num_char			0x1008
#define	token_str				0x2000
#define	token_str_single		0x2001
#define	token_str_double		0x2002
#define	token_str_esc			0x2004
#define	token_str_esc_oct1		0x2008
#define	token_str_esc_oct2		0x2010
#define	token_str_esc_hex		0x2020
#define	token_str_esc_hex1		0x2040
#define	token_str_esc_hex2		0x2080
#define	token_str_esc_hex3		0x2100
#define	token_wrd				0x3000
#define	token_wrd_key			0x3001
#define	token_wrd_id			0x3002
#define	token_wrd_id_nmspc		0x3004
#define	token_wrd_id_cls		0x3008
#define	token_wrd_id_var		0x3010
#define	token_wrd_id_fn			0x3020
#define	token_bkt				0x4000
#define	token_bkt_sqr			0x4001
#define	token_bkt_cir			0x4002
#define	token_bkt_crl			0x4004
#define	token_bkt_ang			0x4008
#define	token_sym				0x5000
#define	token_sym1				0x5000
#define	token_sym2				0x5001
#define	token_sym3				0x5002
#define	token_spc				0x6000
#define	token_cmt				0x7000
#define	token_cmt_single		0x7001
#define	token_cmt_multi			0x7002
#define	token_cmt_multi_end1	0x7004

#define	token_GetGroupState(x)	\
	((x) & 0xF000)

#define	token_GroupState(x, tkn)	\
	(((x) & 0xF000) == (tkn))

#define	token_MatchState(x, tkn)	\
	(((x) & (tkn)) == (tkn))


#endif // !_hllc_State_h_
