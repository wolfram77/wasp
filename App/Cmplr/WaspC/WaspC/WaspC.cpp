#include "stdafx.h"
#include <Windows.h>
#include "Lib/Lib.h"
#include "asm/avr8/avr8.h"
#include "hllc/hllc.h"

using namespace std;

void asm_test()
{
	printf("%s\t%d\n", avr8_table[2].Name, avr8_table[2].Param2.Store[1].Addr);
}

void hllc_test()
{
	char* file = "src.c";
	TokenParser src = TokenParser(file, 8192);
	src.Parse();
	TokenWriter.WriteToFile("dst.c", src.Tokens);
}

int _tmain(int argc, _TCHAR* argv[])
{
	hllc_test();
	return 0;
}
