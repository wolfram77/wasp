// Direct Send data to our GPRS gateway

int Led = 13;
void led_Delay(int time_ms, int rate)
{
  rate <<= 1;
  for(int i = 0; i < time_ms; i += rate)
  {
    digitalWrite(Led, HIGH);
    delay(rate);
    digitalWrite(Led, LOW);
    delay(rate);
  }
}

// Read out
void gsm_ReadOut()
{
  // read out response
  while(Serial.available())
    Serial.read();
}

// Write command to GSM
void gsm_Write(char* cmd, int waitTime, int rate)
{
  Serial.println(cmd);
  led_Delay(waitTime, rate);
  gsm_ReadOut();
}


boolean gsm_Ready = false;
// Send data to server
void gsm_SendData(char* data)
{
  if(!gsm_Ready)
  {
    // Wait to start
    led_Delay(15000, 2000);
    //Attach to the network
    gsm_Write("AT+CGATT=1", 2000, 1000);
    //Start task ans set the APN. Check your carrier APN
    gsm_Write("AT+CSTT=\"bsnllive\"", 2000, 500);
    //Bring up the wireless connection
    gsm_Write("AT+CIICR", 15000, 200);
    //Get the local IP address
    gsm_Write("AT+CIFSR", 1000, 200);
    // GSM module is ready
    gsm_Ready = true;
  }
  //Start a TCP connection to remote address. Port 80 is TCP.
  gsm_Write("AT+CIPSTART=\"TCP\",\"10.220.67.131\",\"8080\"", 4000, 100);
  // Send data to server
  gsm_Write("AT+CIPSEND", 1000, 50);
  gsm_Write(data, 1000, 25);
  Serial.write(26);
  led_Delay(4000, 15);
  gsm_ReadOut();
  gsm_Write("AT+CIPSTOP", 1000, 5);
  led_Delay(4000, 5);
  Serial.println("");
}


// Convert binary to hex
void typ_BinToHex(char* dst, byte* src, int len)
{
  char val;
  for(int i=0; i<len; i++, src++, dst+=2)
  {
    val = *src & 0xF;
    val = (val <= 9)? (val + '0') : (val - 0xA + 'A');
    dst[1] = val;
    val = *src >> 4;
    val = (val <= 9)? (val + '0') : (val - 0xA + 'A');
    dst[0] = val;
  }
}


// Packet format
typedef struct _PacketFormat
{
  long  Id;
  long  Time;
  float Value;
}PacketFormat;

PacketFormat PacketData;

// HTTP Protocol format - 12 bytes of hex data
char* Packet = "GET http://webgateway.apphb.com/ HTTP/1.1\r\n"
               "Host: webgateway.apphb.com\r\n"
               "User-Agent: wasp(000000000000000000000000)\r\n"
               "Connection: close\r\n"
               "\r\n\r\n";

void setup()
{
  Serial.begin(9600);
  pinMode(A0, INPUT);
  pinMode(Led, OUTPUT);
}

void loop()
{
  // Get pressure value
  int val = analogRead(A0);
  float pr = (val / 1024.0f);
  pr = (pr - 0.2f / 5.0f) * 700;
  // Put in packet format
  PacketData.Id = 1;
  PacketData.Time = millis();
  PacketData.Value = pr;
  typ_BinToHex(Packet+88, (byte*)&PacketData, sizeof(PacketData));
  gsm_SendData(Packet);
  delay(30000);
}


