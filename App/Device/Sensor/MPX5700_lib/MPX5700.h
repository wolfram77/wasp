/*
----------------------------------------------------------------------------------------
	mpx5700: MPX5700 Pressure Sensor support module
	File: mpx5700.h

	This file is part of WASP. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WASP is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WASP is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WASP.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	mpx5700 is an MPX5700 pressure sensor support module for WASP. It has been developed for
	for Flood Prediction and Alert System project. The sensor is available at element14 > 
	Semiconductors - ICs > Sensors > Pressure > Max Operating Pressure = 700 kPa. For more
	information, please visit: https://github.com/wolfram77/WASP.
*/



#ifndef	_ext_mpx5700_h_
#define	_ext_mpx5700_h_



// Requisite headers
#include "std/macro.h"
#include "std/memory.h"
#include "std/register.h"
#include "std/type.h"



// Select shorthand level
// 
// The default shorthand level is 1 i.e., members of this
// module can be accessed as mpx5700<function_name> directly.
// The shorthand level can be selected in the main header
// file of WASP library
#ifndef	mpx5700_abbr
#define	mpx5700_abbr	1
#endif



// pressure sensor object
typedef struct _mpx5700
{
	int		pin;		// pin number connected
	float	mulf;		// multiplication factor
	float	addf;		// addition factor
}mpx5700;

#if mpx5700_abbr >= 1
typedef	mpx5700	mpx5700;
#endif



// Function:
// Init(*snsr)
// 
// Initializes a new MPX5700 sensor object that can be used for
// reading from the sensor
// 
// Parameters:
// *snsr:	pointer to MPX5700 sensor object
// pin:		pin to which the sensor is connected
// supply:	supply voltage to the sensor (in volts)
// offset:	fractional offset (of supply) of the sensor
// ref:		reference voltage used with ADC (in volts)
// bits:	number of bits of resolution of ADC
// max:		maximum pressure read by sensor
// 
// Returns:
// value:	the value recorded by sensor
// 
void mpx5700_Init(mpx5700* snsr, int pin, float supply, float offset, float ref, int bits, float max)
{
	snsr->pin = pin;
	snsr->mulf = ((ref / ((1 << bits) - 1)) / supply) * max;
	snsr->addf = (-offset) * max;
}

#if mpx5700_abbr >= 1
#define	mpx5700Init		mpx5700_Init
#endif

#if	mpx5700_abbr >= 2
#define	Init		mpx5700_Init
#endif



// Function:
// Read(*snsr)
// 
// Reads pressure value using a sensor object
// 
// Parameters:
// *snsr:	sensor object to use for reading
// 
// Returns:
// value:	the value recorded by sensor
// 
#define	mpx5700_Read(snsr)	\
	((analogRead((snsr)->pin) * (snsr)->mulf) + (snsr)->addf)

#if mpx5700_abbr >= 1
#define	mpx5700Read			mpx5700_Read
#endif

#if	mpx5700_abbr >= 2
#define	Read			mpx5700_Read
#endif



#endif
