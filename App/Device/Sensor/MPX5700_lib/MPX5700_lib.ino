/*
----------------------------------------------------------------------------------------
	MPX5700_lib: MPX5700 Sensor test program
	File: MPX5700_lib.ino

	This file is part of WaspSnsr. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WaspSnsr is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WaspSnsr is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WaspSnsr.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	MPX5700_lib is a test code for MPX5700 pressure sensor for WaspSnsr. For more information,
	please visit: https://github.com/wolfram77/WaspSrvr.
*/

#include "MPX5700.h"

mpx5700 prsrSnsr;

void setup()
{
    Serial.begin(57600);
    pinMode(A0, INPUT);
    mpx5700Setup(&prsrSnsr, A0, 5.0f, 0.2f, 5.0f, 10, 700.0f);
}

void loop()
{
    float prsr = mpx5700Read(&prsrSnsr);
    Serial.print("Pressure: ");
    Serial.print(prsr);
    Serial.print(" kPa\n");
    delay(10);
}

