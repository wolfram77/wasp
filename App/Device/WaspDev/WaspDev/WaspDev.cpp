#include "Lib/Lib.h"

int maxi(int a, int b)
{
	return (a > b)? a : b;
}

void forceUse(void* a)
{
	if(*((byte*)a + 2)) *((byte*)a) = 0;
}

list_Define(10);

int main(void)
{
	while(true)
	{
		register byte a asm("r3") = maxi(1, 2);
		forceUse((void*)((int)a));
		int b = maxi(a, 2);
		thread_SaveContext(0);
		forceUse(&b);
	}
}
