#ifndef	_Data_Primitive_Types_h_
#define	_Data_Primitive_Types_h_


// Size-Specified Integers
typedef	signed char			int8;
typedef	unsigned char		uint8;
typedef	short				int16;
typedef	unsigned short		uint16;
typedef	long				int32;
typedef	unsigned long		uint32;
typedef	long long			int64;
typedef	unsigned long long	uint64;


// CPU Word Size Integer
#if cpu_WordSize == 8
typedef	int8	word;
typedef uint8	uword;
#elif cpu_WordSize == 16
typedef	int16	word;
typedef uint16	uword;
#elif cpu_WordSize == 64
typedef	int32	word;
typedef uint32	uword;
#elif cpu_WordSize == 64
typedef	int64	word;
typedef uint64	uword;
#endif


// Named Types
#ifndef byte
typedef	unsigned char	byte;
#endif
typedef	unsigned char	ubyte;
typedef signed char		sbyte;
typedef unsigned short	ushort;
typedef unsigned int	uint;
typedef unsigned long	ulong;


#endif
