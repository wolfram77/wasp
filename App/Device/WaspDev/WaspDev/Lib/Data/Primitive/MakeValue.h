#ifndef	_Data_Primitive_MakeValue_h_
#define	_Data_Primitive_MakeValue_h_


// Size-Specified Integers
#define uint8_MakeValueNib(v1, v0)	\
	((uint8) primitive_Concat(v1, v0, 4))

#define	uint8_MakeValueBit(v7, v6, v5, v4, v3, v2, v1, v0)	\
	((uint8) primitive_Concat(v7, v6, v5, v4, v3, v2, v1, v0, 1))

#define uint8_MakeValue(...)	\
	macro_Fn(macro_Fn8(__VA_ARGS__, uint8_MakeValueBit, _7, _6, _5, _4, _3, uint8_MakeValueBit)(__VA_ARGS__))

#define	int8_MakeValue(...)	\
	((int8) uint8_MakeValue(__VA_ARGS__))

#define	uint16_MakeValue(v1, v0)	\
	((uint16) primitive_Concat(v1, v0, 8))

#define	int16_MakeValue(v1, v0)	\
	((int16) uint16_MakeValue(v1, v0))

#define	uint32_MakeValueSrt(v1, v0)	\
	((uint32) primitive_Concat(v1, v0, 16))

#define	uint32_MakeValueByt(v3, v2, v1, v0)	\
	((uint32) primitive_Concat(v3, v2, v1, v0, 8))

#define	uint32_MakeValue(...)	\
	macro_Fn(macro_Fn4(__VA_ARGS__, uint32_MakeValueByt, _3, uint32_MakeValueSrt)(__VA_ARGS__))

#define	int32_MakeValue(...)	\
	((int32) uint32_MakeValue(__VA_ARGS__))

#define	uint64_MakeValueLng(v1, v0)	\
	((uint64) primitive_Concat(v1, v0, 32))

#define	uint64_MakeValueSrt(v3, v2, v1, v0)	\
	((uint64) primitive_Concat(v3, v2, v1, v0, 16))

#define	uint64_MakeValueByt(v7, v6, v5, v4, v3, v2, v1, v0)	\
	((uint64) primitive_Concat(v7, v6, v5, v4, v3, v2, v1, v0, 8))

#define	uint64_MakeValue(...)	\
	macro_Fn(macro_Fn8(__VA_ARGS__, int64_MakeValueByt, _7, _6, _5, int64_MakeValueSrt, _3, int64_MakeValueLng)(__VA_ARGS__))

#define	int64_MakeValue(...)	\
	((int64) uint64_MakeValue(__VA_ARGS__))


// CPU Word Size Integer
#if cpu_WordSize == 8
#define	uint_MakeValue		uint8_MakeValue
#define	int_MakeValue		int8_MakeValue
#elif cpu_WordSize == 16
#define	uint_MakeValue		uint16_MakeValue
#define	int_MakeValue		int16_MakeValue
#elif cpu_WordSize == 32
#define	uint_MakeValue		uint32_MakeValue
#define	int_MakeValue		int32_MakeValue
#elif cpu_WordSize == 64
#define	uint_MakeValue		uint64_MakeValue
#define	int_MakeValue		int64_MakeValue
#endif


// Named Types
#define	uinteger_MakeValue	uint_MakeValue
#define	integer_MakeValue	int_MakeValue
#define	byte_MakeValue		uint8_MakeValue
#define	ubyte_MakeValue		uint8_MakeValue
#define	sbyte_MakeValue		int8_MakeValue
#define	char_MakeValue		int8_MakeValue
#define	ushort_MakeValue	uint16_MakeValue
#define	short_MakeValue		int16_MakeValue
#define	ulong_MakeValue		uint32_MakeValue
#define	long_MakeValue		int32_MakeValue

inline float float_MakeValueSrt(ushort v1, ushort v0)
{
	float ret;
	*((ushort*) &ret) = v0;
	*(((ushort*) &ret) + 1) = v1;
	return ret;
}

inline float float_MakeValueByt(byte v3, byte v2, byte v1, byte v0)
{
	float ret;
	*((byte*) &ret) = v0;
	*(((byte*) &ret) + 1) = v1;
	*(((byte*) &ret) + 2) = v2;
	*(((byte*) &ret) + 3) = v3;
	return ret;
}

#define	float_MakeValue(...)	\
	macro_Fn(macro_Fn4(__VA_ARGS__, float_MakeValueByt, _3, float_MakeValueSrt)(__VA_ARGS__))


inline double double_MakeValueLng(ulong v1, ulong v0)
{
	double ret;
	*((ulong*) &ret) = v0;
	*(((ulong*) &ret) + 1) = v1;
	return ret;
}

inline double double_MakeValueSrt(ushort v3 ,ushort v2, ushort v1, ushort v0)
{
	double ret;
	*((ushort*) &ret) = v0;
	*(((ushort*) &ret) + 1) = v1;
	*(((ushort*) &ret) + 2) = v2;
	*(((ushort*) &ret) + 3) = v3;
	return ret;
}

inline double double_MakeValueByt(byte v7, byte v6, byte v5, byte v4, byte v3, byte v2, byte v1, byte v0)
{
	double ret;
	*((byte*) &ret) = v0;
	*(((byte*) &ret) + 1) = v1;
	*(((byte*) &ret) + 2) = v2;
	*(((byte*) &ret) + 3) = v3;
	*(((byte*) &ret) + 4) = v4;
	*(((byte*) &ret) + 5) = v5;
	*(((byte*) &ret) + 6) = v6;
	*(((byte*) &ret) + 7) = v7;
	return ret;
}

#define	double_MakeValue(...)	\
	macro_Fn(macro_Fn8(__VA_ARGS__, double_MakeValueByt, _7, _6, _5, double_MakeValueSrt, _3, double_MakeValueLng)(__VA_ARGS__))


#endif
