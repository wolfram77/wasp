#ifndef	_Data_Primitive_Constants_h_
#define	_Data_Primitive_Constants_h_


#ifndef	NULL
#define	NULL	(0)
#endif

#ifndef null
#define null	(0)
#endif

#ifndef TRUE
#define TRUE	(1)
#define	FALSE	(0)
#endif

#ifndef true
#define	true	(1)
#define	false	(0)
#endif


#endif
