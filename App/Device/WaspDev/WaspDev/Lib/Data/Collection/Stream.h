#ifndef	_Data_Collection_Stream_h_
#define	_Data_Collection_Stream_h_


// Define
typedef struct _stream_Block
{
byte*	Src;
	uint	Len;
	byte*	Busy;
}stream_Block;

#define	stream_Define(sz)	\
typedef struct _Stream##sz	\
{	\
	stream_Block*	Front;		\
	stream_Block*	Rear;		\
	stream_Block*	End;		\
	uword			Count;		\
	uword			Size;		\
	stream_Block	Block[sz];	\
}Stream##sz


// Header
typedef struct _stream_Header
{
	stream_Block*	Front;
	stream_Block*	Rear;
	stream_Block*	End;
	uword			Count;
	uword			Size;
}stream_Header;


// Default
stream_Define(2);
stream_Define(4);
stream_Define(8);
stream_Define(16);
stream_Define(32);
stream_Define(64);
stream_Define(128);
stream_Define(256);

#ifndef	stream_Default
#define	stream_Default		Stream4
#endif

typedef	stream_Default		Stream;


// Init
#define stream_Init(strm)	\
	macro_Begin	\
	(strm)->Front = (strm)->Block;	\
	(strm)->Rear = (strm)->Block;	\
	(strm)->End = (stream_Block*) mem(strm, sizeof(*(strm)))	\
	(strm)->Count = 0;	\
	(strm)->Size = (sizeof(*(strm)) - sizeof(stream_Header)) / sizeof((strm)->Block[0]);	\
	macro_End


// Clear
void stream_ClearF(Stream* strm)
{
	(strm)->Front = (strm)->Block;
	(strm)->Rear = (strm)->Block;
	(strm)->Count = 0;
}

#define stream_Clear(strm)	\
	stream_ClearF((Stream*)(strm))


// GetAvail
#define	stream_GetAvail(strm)	\
	((strm)->Count)


// GetFree
#define	stream_GetFree(strm)	\
	((strm)->Size - (strm)->Count)


// Write
uword stream_WriteF(Stream* strm, byte* src, uint len, byte* busy)
{
	stream_Block* blk;
	if(!stream_GetFree(strm)) return (uword)-1;
	if(busy) *busy = TRUE;
	blk = strm->Rear;
	blk->Src = src;
	blk->Len = len;
	blk->Busy = busy;
	strm->Count++;
	strm->Rear++;
	if(strm->Rear == strm->End) strm->Rear = strm->Block;
	return 0;
}

#define stream_Write(strm, src, len, busy)	\
	stream_WriteF((Stream*)(strm), (byte*)(src), (uint)(len), (byte*)(busy))


// Peek
#define stream_Peek(strm)	\
	((stream_GetAvail(strm))? (strm)->Front : null)


// Read (Block)
byte* stream_ReadF(Stream* strm, uint* plen, byte** pbusy)
{
	byte* src;
	stream_Block* blk;
	if(!stream_GetAvail(strm)) return null;
	blk = strm->Front;
	src = blk->Src;
	if(plen) *plen = blk->Len;
	if(pbusy) *pbusy = blk->Busy;
	strm->Count--;
	strm->Front++;
	if(strm->Front == strm->End) strm->Front = strm->Block;
	return src;
}

#define stream_Read(strm, plen, pbusy)	\
	stream_ReadF((Stream*)(strm), (uint*)(plen), (byte**)(pbusy))


// ReadByte
byte stream_ReadByteF(Stream* strm)
{
	byte val;
	stream_Block* blk;
	if(!stream_GetAvail(strm)) return (byte)-1;
	blk = strm->Front;
	val = *(blk->Src);
	blk->Src++;
	blk->Len--;
	if(blk->Len == 0)
	{
		if(blk->Busy) *(blk->Busy) = FALSE;
		strm->Count--;
		strm->Front++;
		if(strm->Front == strm->End) strm->Front = strm->Block;
	}
	return val;
}

#define stream_ReadByte(strm)	\
	stream_ReadByteF((Stream*)(strm))


#endif
