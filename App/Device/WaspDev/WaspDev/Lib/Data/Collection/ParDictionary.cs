﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Wasp.Data.Collection
{
	public class ParDictionary<K, V> : Dictionary<K, V>
	{
		public object Lock;
		public Semaphore Sem;
		public int BareCount
		{
			get
			{ return base.Count; }
		}
		public new int Count
		{
			get
			{
				lock (Lock)
				{ return base.Count; }
			}
		}
		public new V this[K key]
		{
			get
			{
				lock (Lock)
				{
					V val = default(V);
					base.TryGetValue(key, out val);
					return val;
				}
			}
			set
			{
				lock (Lock)
				{ base[key] = value; }
			}
		}


		// Create Parallel access list
		public ParDictionary(bool semUsed)
			: base()
		{
			Lock = new object();
			if (semUsed) Sem = new Semaphore(0, int.MaxValue);
		}
		public ParDictionary()
			: this(false)
		{
		}

		// Get enumerator
		public new Enumerator GetEnumerator()
		{
			return base.GetEnumerator();
		}

		// Get next element
		public KeyValuePair<K, V> BareNextElement(Enumerator enm)
		{
			KeyValuePair<K, V> kv;
			kv = (enm.MoveNext())? enm.Current : new KeyValuePair<K, V>();
			return kv;
		}
		public KeyValuePair<K, V> NextElement(Enumerator enm)
		{
			// wait until atleast one element is available
			if (Sem != null)
			{
				Sem.WaitOne();
				Sem.Release();
			}
			lock (Lock)
			{ return BareNextElement(enm); }
		}

		// Add element (Push)
		public void BareAdd(K key, V val)
		{
			base.Add(key, val);
		}
		public new void Add(K key, V val)
		{
			lock (Lock)
			{ base.Add(key, val); }
			if (Sem != null) Sem.Release();
		}

		// Remove element
		public bool BareRemove(K key)
		{
			return base.Remove(key);
		}
		public new bool Remove(K key)
		{
			bool success = false;
			lock (Lock)
			{ success = base.Remove(key); }
			if (success && Sem != null) Sem.WaitOne();
			return success;
		}

		// Clear dictionary
		public void BareClear()
		{
			base.Clear();
		}
		public new void Clear()
		{
			if (Sem != null) Sem.WaitOne(base.Count);
			lock (Lock)
			{ base.Clear(); }
		}

		// Close dictionary
		public void Close()
		{
			Clear();
			Lock = null;
			if (Sem != null)
			{
				Sem.Dispose();
				Sem = null;
			}
		}
	}
}
