#ifndef	_Data_Collection_List_h_
#define	_Data_Collection_List_h_


// Define
#define	list_Define(sz)	\
typedef struct _List##sz	\
{	\
	uword	Count;		\
	uword	Size;		\
	uint	Value[sz];	\
}List##sz


// Header
typedef struct _list_Header
{
	uword	Count;
	uword	Size;
}list_Header;


// Default List
list_Define(2);
list_Define(4);
list_Define(8);
list_Define(16);
list_Define(32);
list_Define(64);
list_Define(128);
list_Define(256);

#ifndef list_Default
#define	list_Default	List32
#endif

typedef list_Default	List;


// Init
#define	list_Init(lst)	\
	macro_Begin	\
		(lst)->Count = (uword)0;	\
		(lst)->Size = (uword)(sizeof(lst) - sizeof(list_Header)) / sizeof((lst).Value[(uword)0]);	\
	macro_End


// Clear
#define	list_Clear(lst)	\
		((lst)->Count = (uword)0)


// GetAvail
#define	list_GetAvail(lst)	\
	((lst)->Count)


// HasAvail
#define list_HasAvail(lst)	\
	list_GetAvail(lst)


// GetFree
#define	list_GetFree(lst)	\
	((lst)->Size - (lst)->Count)


#define list_HasFree(lst)	\
	list_GetFree(lst)


// IndexOf
uword list_IndexOfF(List* lst, uint elem)
{
	uword indx;
	for(indx = (uword)0; indx < lst->Count; indx++)
		if(lst->Value[indx] == elem) break;
	indx = (indx != lst->Count)? indx : (uword)-1;
	return indx;
}

#define	list_IndexOf(lst, elem)	\
	list_IndexOfF((List*)(lst), (uint)(elem))


// Add
uword list_AddF(List* lst, uint elem)
{
	if(!list_HasFree(lst)) return (uword)-1;
	lst->Value[lst->Count] = elem;
	lst->Count++;
	return (uword)0;
}

#define list_Add(lst, elem)	\
	list_AddF((List*)(lst), (uint)(elem))


// RemoveAt
uword list_RemoveAtF(List* lst, uint indx)
{
	if(indx >= list_GetAvail(lst)) return (uword)-1;
	lst->Count--;
	lst->Value[indx] = lst->Value[lst->Count];
	return (uword)0;
}

#define list_RemoveAt(lst, indx)	\
	list_RemoveAtF((List*)(lst), (uint)(indx))


// Remove
uword list_RemoveF(List* lst, uint elem)
{
	uword indx = list_IndexOf(lst, elem);
	if(indx != (uword)-1) indx = list_RemoveAt(lst, indx);
	return indx;
}

#define	list_Remove(lst, elem)	\
	list_RemoveF((List*)(lst), (uint)(elem))


#endif
