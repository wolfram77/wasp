#ifndef	_Data_Collection_Queue_h_
#define	_Data_Collection_Queue_h_


// Define
#define	queue_Define(sz)	\
typedef struct _Queue##sz	\
{	\
	uword	Rear;		\
	uword	Front;		\
	uword	Max;		\
	uint	Value[sz];	\
}Queue##sz


// Header
typedef struct _queue_Header
{
	uword	Rear;
	uword	Front;
	uword	Max;
}queue_Header;


// Default Queue
queue_Define(2);
queue_Define(4);
queue_Define(8);
queue_Define(16);
queue_Define(32);
queue_Define(64);
queue_Define(128);
queue_Define(256);

#ifndef queue_Default
#define	queue_Default	Queue32
#endif

typedef queue_Default	Queue;


// Init
#define	queue_Init(que)	\
	macro_Begin	\
	(que)->Rear = (uword)0;	\
	(que)->Front = (uword)0;	\
	(que)->Max = (uword)((sizeof(que) - sizeof(queue_Header)) / sizeof((que).Value[(uword)0])) - (uword)1;	\
	macro_End


// Clear
inline void queue_ClearF(Queue* que)
{
	que->Rear = (uword)0;
	que->Front = (uword)0;
}
#define queue_Clear(que)	\
	queue_ClearF((Queue*)(que))


// GetAvail
#define	queue_GetAvail(que)	\
	(((que)->Rear - (que)->Front) & (que)->Max)


// HasAvail
#define	queue_HasAvail(que)	\
	((que)->Rear != (que)->Front)


// GetFree
#define	queue_GetFree(que)	\
	((que)->Max - queue_GetAvail(que))


// HasFree
#define queue_HasFree(que)	\
	((que)->Rear != (((que)->Front - 1) & (que)->Max))


// Add
uword queue_AddF(Queue* que, uint elem)
{
	if(!queue_HasFree(que)) return (uword)-1;
	que->Value[que->Rear++] = elem;
	return (uword)0;
}

#define queue_Add(que, elem)	\
	queue_AddF((Queue*)(que), (uint)(elem))


// Peek
uint queue_PeekF(Queue* que)
{
	if(!queue_HasAvail(que)) return (uint)-1;
	return que->Value[que->Front];
}

#define queue_Peek(que)	\
	queue_PeekF((Queue*)(que))


// Remove
uint queue_RemoveF(Queue* que)
{
	if(!queue_HasAvail(que)) return (uint)-1;
	return que->Value[que->Front++];
}

#define	queue_Remove(que)	\
	queue_RemoveF((Queue*)(que))


#endif
