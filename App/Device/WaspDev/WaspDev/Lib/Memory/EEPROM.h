#ifndef	_Memory_Eeprom_h_
#define	_Memory_Eeprom_h_


// call by OS when EEPROM used, and is off
// Enable
inline void eeprom_Enable()
{
	// power_EEPROM_On();
	bit_Set(EECR, EERIE);
}


// call by OS when EEPROM unused, and is on
// Disable
inline void eeprom_Disable()
{
	bit_Clear(EECR, EERIE);
	// power_EEPROM_Off();
}
// store program memory enable
#define SPMEN 1

uword eeprom_Read(Task* tsk)
{
	while(bit_Test(SPMCSR, SPMEN)); // wait for flash operations
	while(bit_Test(EECR, EEPE)); // wait for prev. write
	EEAR = 0; // address
	bit_Set(EECR, EERE);
	return EEDR; // data
}

void eeprom_Write()
{
	while(bit_Test(SPMCSR, SPMEN)); // wait for flash operations
	while(bit_Test(EECR, EEPE)); // wait for prev. write
	EEAR = 0; // address
	EEDR = 0; // data
	bit_Set(EECR, EEMPE);
	bit_Set(EECR, EEPE);
}
#endif
