#ifndef	_Memory_Register_Stack_h_
#define	_Memory_Register_Stack_h_


// GPIO Registers (0x00 - 0x1F)
#ifndef GPIO0
#define GPIO0		reg_DefineIO8(0x00)
#define GPIO1		reg_DefineIO8(0x01)
#define GPIO2		reg_DefineIO8(0x02)
#define GPIO3		reg_DefineIO8(0x03)
#define GPIO4		reg_DefineIO8(0x04)
#define GPIO5		reg_DefineIO8(0x05)
#define GPIO6		reg_DefineIO8(0x06)
#define GPIO7		reg_DefineIO8(0x07)
#define GPIO8		reg_DefineIO8(0x08)
#define GPIO9		reg_DefineIO8(0x09)
#define GPIO10		reg_DefineIO8(0x0A)
#define GPIO11		reg_DefineIO8(0x0B)
#define GPIO12		reg_DefineIO8(0x0C)
#define GPIO13		reg_DefineIO8(0x0D)
#define GPIO14		reg_DefineIO8(0x0E)
#define GPIO15		reg_DefineIO8(0x0F)
#define GPIO16		reg_DefineIO8(0x10)
#define GPIO17		reg_DefineIO8(0x11)
#define GPIO18		reg_DefineIO8(0x12)
#define GPIO19		reg_DefineIO8(0x13)
#define GPIO20		reg_DefineIO8(0x14)
#define GPIO21		reg_DefineIO8(0x15)
#define GPIO22		reg_DefineIO8(0x16)
#define GPIO23		reg_DefineIO8(0x17)
#define GPIO24		reg_DefineIO8(0x18)
#define GPIO25		reg_DefineIO8(0x19)
#define GPIO26		reg_DefineIO8(0x1A)
#define GPIO27		reg_DefineIO8(0x1B)
#define GPIO28		reg_DefineIO8(0x1C)
#define GPIO29		reg_DefineIO8(0x1D)
#define GPIO30		reg_DefineIO8(0x1E)
#define GPIO31		reg_DefineIO8(0x1F)
#define GPOIR0		reg_DefineIO8(0x1E)
#define GPOIR1		reg_DefineIO8(0x2A)
#define GPOIR2		reg_DefineIO8(0x2B)
#endif


#endif
