#ifndef	_Task_Code_h_
#define	_Task_Code_h_


// Main
uword task_Main()
{
	uword ret;
	while(true)
	{
		task_LastExec = queue_Remove(task_Queue);
		// wait while tsk == -1
		ret = ((task_FnPtr)(task_LastExec->Addr))(task_LastExec));
		if((tsk->stat & 0xF0) == 0 || task_GetActive() != count_old)
		{
			if(!task_GetActive()) return 0;
			list_IndexOf(task_Exec, task_List, tsk);
			if(task_Exec == (uword)-1) task_Exec = -1;
			else if((tsk->stat & 0xF0) == 0)
			{
				list_RemoveAt(task_List, task_Exec);
				task_Exec--;
			}
			if(!task_GetActive()) return 0;
		}
		task_Exec++;
		task_Exec = (task_Exec < task_GetActive())? task_Exec : 0;
	}
	return (uword)0;
}


// Fn
#define	task_Fn(name, typ)	\
uword name(typ* task_Obj)


// Begin
#define	task_Begin(line_off)	\
switch(mem_ReadUint(task_Obj->State, line_off))	\
{	\
	case 0:

	#define	task_Bgn	task_Begin


	// End
	#define	task_End(line_off)	\
}	\
mem_WriteUint(task_Obj->State, line_off, 0);	\
return task_exited


// Parameter
#define	task_Parameter(struct_type, par_name)	\
(((struct_type*)task_Obj->State)->par_name)


// SaveState
#define	task_SaveState1(typ1, var1)	\
(*((typ1*)task_obj->repo) = var1)

#define	task_SaveState2(typ1, var1, typ2, var2)	\
macro_Begin	\
*((typ1*)task_obj->repo) = var1;	\
*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
macro_End

#define	task_SaveState3(typ1, var1, typ2, var2, typ3, var3)	\
macro_Begin	\
*((typ1*)task_obj->repo) = var1;	\
*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
macro_End

#define	task_SaveState4(typ1, var1, typ2, var2, typ3, var3, typ4, var4)	\
macro_Begin	\
*((typ1*)task_obj->repo) = var1;	\
*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
*((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3))) = var4;	\
macro_End

#define	task_SaveState5(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5)	\
macro_Begin	\
*((typ1*)task_obj->repo) = var1;	\
*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
*((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3))) = var4;	\
*((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4))) = var5;	\
macro_End

#define	task_SaveState6(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6)	\
macro_Begin	\
*((typ1*)task_obj->repo) = var1;	\
*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
*((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3))) = var4;	\
*((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4))) = var5;	\
*((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5))) = var6;	\
macro_End

#define	task_SaveState7(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6, typ7, var7)	\
macro_Begin	\
*((typ1*)task_obj->repo) = var1;	\
*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
*((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3))) = var4;	\
*((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4))) = var5;	\
*((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5))) = var6;	\
*((typ7*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6))) = var7;	\
macro_End

#define	task_SaveState8(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6, typ7, var7, typ8, var8)	\
macro_Begin	\
*((typ1*)task_obj->repo) = var1;	\
*((typ2*)(task_obj->repo + sizeof(typ1))) = var2;	\
*((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2))) = var3;	\
*((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3))) = var4;	\
*((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4))) = var5;	\
*((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5))) = var6;	\
*((typ7*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6))) = var7;	\
*((typ8*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6) + sizeof(typ7))) = var8;	\
macro_End

#define	task_SaveState(...)	\
macro_Fn(macro_Fn17(_0, __VA_ARGS__, task_SaveState8, _15, task_SaveState7, _13, task_SaveState6, _11, task_SaveState5, _9, task_SaveState4, _7, task_SaveState3, _5, task_SaveState2, _3, task_SaveState1, macro_FnE, macro_FnE)(__VA_ARGS__))

#define	task_Save	task_SaveState
#define	task_Sv		task_SaveState


// LoadState
#define	task_LoadState1(typ1, var1)	\
(var1 = *((typ1*)task_obj->repo))

#define	task_LoadState2(typ1, var1, typ2, var2)	\
macro_Begin	\
var1 = *((typ1*)task_obj->repo);	\
var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
macro_End

#define	task_LoadState3(typ1, var1, typ2, var2, typ3, var3)	\
macro_Begin	\
var1 = *((typ1*)task_obj->repo);	\
var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
macro_End

#define	task_LoadState4(typ1, var1, typ2, var2, typ3, var3, typ4, var4)	\
macro_Begin	\
var1 = *((typ1*)task_obj->repo);	\
var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
var4 = *((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3)));	\
macro_End

#define	task_LoadState5(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5)	\
macro_Begin	\
var1 = *((typ1*)task_obj->repo);	\
var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
var4 = *((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3)));	\
var5 = *((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4)));	\
macro_End

#define	task_LoadState6(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6)	\
macro_Begin	\
var1 = *((typ1*)task_obj->repo);	\
var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
var4 = *((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3)));	\
var5 = *((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4)));	\
var6 = *((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5)));	\
macro_End

#define	task_LoadState7(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6, typ7, var7)	\
macro_Begin	\
var1 = *((typ1*)task_obj->repo);	\
var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
var4 = *((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3)));	\
var5 = *((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4)));	\
var6 = *((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5)));	\
var7 = *((typ7*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6)));	\
macro_End

#define	task_LoadState8(typ1, var1, typ2, var2, typ3, var3, typ4, var4, typ5, var5, typ6, var6, typ7, var7, typ8, var8)	\
macro_Begin	\
var1 = *((typ1*)task_obj->repo);	\
var2 = *((typ2*)(task_obj->repo + sizeof(typ1)));	\
var3 = *((typ3*)(task_obj->repo + sizeof(typ1) + sizeof(typ2)));	\
var4 = *((typ4*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3)));	\
var5 = *((typ5*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4)));	\
var6 = *((typ6*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5)));	\
var7 = *((typ7*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6)));	\
var8 = *((typ8*)(task_obj->repo + sizeof(typ1) + sizeof(typ2) + sizeof(typ3) + sizeof(typ4) + sizeof(typ5) + sizeof(typ6) + sizeof(typ7)));	\
macro_End

#define	task_LoadState(...)	\
macro_Fn(macro_Fn17(_0, __VA_ARGS__, task_LoadState8, _15, task_LoadState7, _13, task_LoadState6, _11, task_LoadState5, _9, task_LoadState4, _7, task_LoadState3, _5, task_LoadState2, _3, task_LoadState1, macro_FnE, macro_FnE)(__VA_ARGS__))

#define	task_Load	task_LoadState
#define	task_Ld		task_LoadState


// Function:
// SwitchOut()
// [SwitchOut]
//
// Used to switch out a task. Next time, the task will start from begining.
//
// Parameters:
// none
//
// Returns:
// nothing
//
#define	task_SwitchOut()	\
macro_Begin	\
task_obj->line = 0;	\
return task_switched_out;	\
macro_End


// Function:
// Switch(<local variables>)
// [Switch]
//
// Used to switch from task. If no local variables are present in task, use Switch().
// The task will continue from here.
//
// Parameters:
// <local variables>:	a list of local variables (as typ1, var1, typ2, var2, ...) to store separated with commas
//
// Returns:
// nothing
//
#define	task_Switch(...)	\
macro_Begin	\
task_obj->line = __LINE__;	\
task_SaveState(__VA_ARGS__);	\
return task_switched;	\
case __LINE__:	\
task_LoadState(__VA_ARGS__);	\
macro_End

#if task_abbr >= 3
#define	tsk_Switch		task_Switch
#endif


// Function:
// WaitWhile(wait_cond, <local variables>)
// [WaitWhile]
//
// Used to wait while a condition is satisfied.
//
// Parameters:
// wait_cond:	wait condition (will wait as long as this condition is true)
// <local variables>:	a list of local variables (as typ1, var1, typ2, var2, ...) to store separated with commas
//
// Returns:
// nothing
//
#define	task_WaitWhile(wait_cond, ...)	\
macro_Begin	\
task_obj->line = __LINE__;	\
task_SaveState(__VA_ARGS__);	\
case __LINE__:	\
if(wait_cond) return task_waiting;	\
task_LoadState(__VA_ARGS__);	\
macro_End


// Function:
// WaitUntil(wait_cond, <local variables>)
// [WaitUntil / WaitUntl]
//
// Used to wait until a condition is satisfied.
//
// Parameters:
// wait_cond:	wait condition (will wait until this condition is true)
// <local variables>:	a list of local variables (as typ1, var1, typ2, var2, ...) to store separated with commas
//
// Returns:
// nothing
//
#define	task_WaitUntil(wait_cond, ...)	\
task_WaitWhile(!(wait_cond), __VA_ARGS__)

#define	task_WaitUntl	task_WaitUntil


// Function:
// Exit()
// [Exit]
//
// Used to exit from task. The task is removed from execution.
//
// Parameters:
// none
//
// Returns:
// nothing
//
#define	task_Exit()	\
return task_exited


// Function:
// SemaphoreWait(sem, <local variables>)
// [SemaphoreWait / SemWait]
//
// Used to wait for a semaphore. To be used before entering a
// critical section.
//
// Parameters:
// sem:		semaphore used
// <local variables>:	a list of local variables (as typ1, var1, typ2, var2, ...) to store separated with commas
//
// Returns:
// nothing
//
#define	task_SemaphoreWait(sem, ...)	\
macro_Begin	\
task_WaitUntil((sem) > 0, __VA_ARGS__);	\
(sem)--;	\
macro_End

#define	task_SemWait	task_SemaphoreWait


// Function:
// SemaphoreSignal(sem)
// [SemaphoreSignal / SemSgnl]
//
// Used to signal with a semaphore. Used while coming out of
// a critical section (manually enable interrupts after this).
//
// Parameters:
// sem:		semaphore used
//
// Returns:
// nothing
//
#define	task_SemaphoreSignal(sem)	\
((sem)++)


#endif
