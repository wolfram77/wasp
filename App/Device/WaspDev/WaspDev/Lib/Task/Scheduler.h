#ifndef	_Task_Scheduler_h_
#define	_Task_Scheduler_h_


// Ready, Suspended
list_Define(24);
Queue4 task_ReadyHigh;
Queue4 task_ReadyMed;
Queue16 task_ReadyLow;
List24 task_Suspended;


// Initialize task scheduler
void task_Init()
{
	queue_Init(task_ReadyHigh);
	queue_Init(task_ReadyMed);
	queue_Init(task_ReadyLow);
	list_Init(task_Suspended);
}


// Main
void task_Scheduler()
{
	while(true)
	{
		
	}
}


#endif
