#ifndef	_Task_Section_h_
#define	_Task_Section_h_


// section for OS services
#define section_OS	\
	Section(".os")

// section for application
#define section_App	\
	Section(".text")

// section for boot-able code
#define section_Boot	\
	Section(".boot")


#endif
