#ifndef	_thread_Thread_h_
#define	_thread_Thread_h_


// Thread states
#define thread_state_active			0
#define thread_state_ready			1
#define	thread_state_suspended		2
#define thread_state_stopped		3


// Definition
#define	thread_Define(sz)	\
typedef	struct _Thread##sz	\
{	\
	void*	Function;	\
	uword	State;		\
	uint	Data;		\
	byte	Stack[sz];	\
}Thread##sz


// Default Task
thread_Define(32);
thread_Define(64);
thread_Define(128);
thread_Define(256);

#ifndef	thread_Default
#define	thread_Default	Thread64
#endif

typedef thread_Default	Thread;


// Header
typedef struct _thread_Header
{
	void*	Function;
	uword	State;
	uint	Data;
}thread_Header;

typedef uword	(*thread_FnPtr)(Thread*);
typedef uword	(*thread_Header_FnPtr)(thread_Header*);


// Save State
void thread_SaveContext(uint stack_ptr)
{
	Asm(
	AsmLn("push r0")
	AsmLn("in r0, __SREG__")
	AsmLn("cli")
	AsmLn("push r0")
	AsmLn("push r1")
	AsmLn("push r2")
	AsmLn("push r3")
	AsmLn("push r4")
	AsmLn("push r5")
	AsmLn("push r6")
	AsmLn("push r7")
	AsmLn("push r8")
	AsmLn("push r9")
	AsmLn("push r10")
	AsmLn("push r11")
	AsmLn("push r12")
	AsmLn("push r13")
	AsmLn("push r14")
	AsmLn("push r15")
	AsmLn("push r16")
	AsmLn("push r17")
	AsmLn("push r18")
	AsmLn("push r19")
	AsmLn("push r20")
	AsmLn("push r21")
	AsmLn("push r22")
	AsmLn("push r23")
	AsmLn("push r24")
	AsmLn("push r25")
	AsmLn("push r26")
	AsmLn("push r27")
	AsmLn("push r28")
	AsmLn("push r29")
	AsmLn("push r30")
	AsmLn("push r31")
	);
	SP = stack_ptr;
}


// Initialize a thread
inline void thread_InitF(Thread* thread, void* function, uint data)
{
	thread->Function = function;
	thread->State = thread_state_stopped;
	thread->Data = data;
}

#define thread_Init(thread, function, data)	\
	thread_InitF((Thread*)(thread), (void*)(function), (uint)(data))


// Start a thread
void thread_Start(Thread* thread)
{
}
// thread_Yield
// thread_SaveState
// thread_Start
// thread_Sleep
// thread_SpinWait





#endif
