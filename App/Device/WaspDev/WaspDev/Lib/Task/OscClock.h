/*
 * OscClock.h
 *
 * Created: 19/1/2014 8:34:00 PM
 *  Author: wolfram77
 */ 


#ifndef OSCCLOCK_H_
#define OSCCLOCK_H_

// CLK freq = OSC(8MHz) / 2 ^ prescale
void task_SetClock(uword prescale)
{
	prescale &= 0xF;
	bit_Set(prescale, CLKPCE);
	bit_Set(CLKPR, CLKPCE);
	CLKPR = prescale;
}

void task_SetMode(uword mode)
{
	SMCR = mode | 1;
	// asm sleep
}

void task_PowerDown(uword device)
{
	bit_Set(PRR, device);
	// bit_Clear(PRR, device); to PowerUp
}

void wdt_Off()
{
	// cli
	// wdr
	bit_Clear(MCUCR, WDRF);
	bit_Set(WDTCSR, WDCE, WDE);
	WDTCSR = 0;
	// sti
}

void wdt_ChangePrescaler(uword prescale)
{
	// cli
	// wdr
	bit_Set(WDTCSR, WDCE, WDE);
	WDTCSR = prescale; // -|
	bit_Set(WDTCSR, WDE, WDP2, WDP0);
}

uword power_GetResetType()
{
	return MCUSR;
}

void setBootInterruptTable()
{
	bit_Set(MCUCR, IVCE);
	bit_Set(MCUCR, IVSEL);
}



#endif /* OSCCLOCK_H_ */