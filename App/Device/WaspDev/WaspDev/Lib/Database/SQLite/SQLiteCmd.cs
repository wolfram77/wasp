﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using Wasp.Data.Collection;

namespace Wasp.Database
{
	public static class SQLiteCmd
	{
		// Set Parameters
		public static void SetParameters(SQLiteCommand cmd, KeyVals prms)
		{
			KeyVals.Enumerator enm = prms.GetEnumerator();
			while (enm.MoveNext())
			{ cmd.Parameters.AddWithValue((string)enm.Current.Key, enm.Current.Value); }
			enm.Dispose();
		}
		public static void SetParameters(SQLiteCommand cmd, params object[] prms)
		{
			for (int i = 0; i < prms.Length; i += 2)
			{ cmd.Parameters.AddWithValue((string)prms[i], prms[i + 1]); }
		}
	}
}
