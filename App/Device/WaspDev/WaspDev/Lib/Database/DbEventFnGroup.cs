﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wasp.Database
{
	public enum DbEvent
	{
		Closed,
		NewClient,
		ConnectionLost,
		Connected,
		ConnectionFailed,
		Disconnected,
		NonQueryExecuted,
		ScalarExecuted,
		ReaderExecuted,
		XmlReaderExecuted,
		ExecutionFailed,
	}

	public class DbEventArgs : EventArgs
	{
		public DbEvent Event;
		public object Data;
		public Exception Error;

		// Create a Network Event Argument
		public DbEventArgs(DbEvent evnt, object data = null, Exception error = null)
		{
			Event = evnt;
			Data = data;
			Error = error;
		}
	}

	public delegate void DbEventFn(object sender, DbEventArgs e);

	public class DbEventFnGroup
	{
		public DbEventFn Closed, NewClient, ConnectionLost;
		public DbEventFn Connected, ConnectionFailed, Disconnected;
		public DbEventFn Executed, ExecutionFailed, NonQueryExecuted;
		public DbEventFn ScalarExecuted, ReaderExecuted, XmlReaderExecuted;

		// Create a Database Event Function Group
		public DbEventFnGroup()
		{
		}
	}
}
