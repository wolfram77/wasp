#ifndef	_Compile_Function_h_
#define	_Compile_Function_h_


// TypeOf
#if compiler_Name == compiler_Name_GCC
#define TypeOf(expr)	\
	typeof(expr)
#elif compiler_Name == compiler_Name_VisualC
#define	TypeOf(expr)	\
	decltype(expr)
#endif


// Asm
#define Asm	\
	__asm__ __volatile__

#define AsmLn(text)	\
	text "\n\t"


// Attrib
#define Attrib(type)	\
	__attribute__((type))

#define NoInline		noinline
#define AlwaysInline	always_inline
#define Const			const
#define Pure			pure
#define Interrupt		interrupt
#define Naked			naked
#define NoReturn		noreturn
#define NoThrow			nothrow
#define Section(name)	section(name)
#define Signal			signal
#define Unused			unused
#define Used			used
#define Packed			packed
#define Align(size)		align(size)
#define Deprecated		deprecated


#endif
