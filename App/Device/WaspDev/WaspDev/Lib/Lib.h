#ifndef	_Lib_h_
#define	_Lib_h_


// Compile Support
#include "Support/Sample.h"
#include "Support/Compile.h"
#include "Support/Function.h"
#include "Support/Macro.h"


// Compilation Options
#define platform_Name		platform_Name_AVR
#define compiler_Name		compiler_Name_GCC
#define cpu_WordSize		8
#define cpu_ClockFreq		16000000
#define ram_Size			4096


// Platform Files
#if platform_Name == platform_Name_AVR
#include <avr/io.h>
#include <avr/portpins.h>
#include <avr/interrupt.h>
#endif


// Include Files
#include "Data/Primitive/Constants.h"
#include "Data/Primitive/Types.h"
#include "Data/Primitive/Ranges.h"
#include "Data/Primitive/Char.h"
#include "Data/Primitive/Concat.h"
#include "Data/Primitive/MakeValue.h"
#include "Data/Primitive/Buffer.h"
#include "Memory/RAM/Basic.h"
#include "Memory/RAM/Block.h"
#include "Memory/RAM/Dynamic.h"
#include "Memory/Register/Basic.h"
#include "Memory/Register/Standard.h"
#include "Memory/Register/Status.h"
#include "Math/Basic.h"
#include "Math/Bit.h"
#include "Data/Compound/String.h"
#include "Data/Collection/List.h"
#include "Data/Collection/Stream.h"
#include "Data/Collection/Queue.h"
#include "Task/Thread.h"

#endif
