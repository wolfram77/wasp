﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wasp.Data.Collection;

namespace Wasp.Network
{
	public class TcpHostInfo
	{
		public TcpHost Host;
		public ParList<TcpClnt> Client;
		public ParList<NetPacket> Rcvd;
		public object PktLock;
		public int RcvPkts, SndPkts;
		public bool ClientAccess;
		public bool AutoRemove;
		public bool AutoClose;

		public TcpHostInfo()
		{
		}
	}
}
