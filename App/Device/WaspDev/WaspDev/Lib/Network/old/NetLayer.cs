﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;


namespace Wasp.Network
{
	// Net Layer Flag bits for Address
	public enum NetLayerAddressSize
	{
		Size16bit = 0x00,	// 16-bit address flag
		Size32bit = 0x01,	// 32-bit address flag
		Size64bit = 0x02,	// 64-bit address flag
		Size128bit = 0x03	// 128-bit (GUID) address flag
	}

	// Other Net Layer Flag bits
	public enum NetLayerFlag
	{
		Acknowledgement = 0x10,
		RequireAcknowledge = 0x20,
		Terminal = 0x40
	}

	// A Net Layer Packet
	public class NetLayerPacket
	{
		public byte Sync;			// Sync byte == 0xC3
		public byte Checksum;		// Checksum byte
		public byte Id;				// Packet ID
		public UInt16 Offset;		// Offset of App Layer Packet (otherwise 0xFFFF)
		public UInt16 Size;			// Packet Size
		public byte Flags;			// Option Flags
		public Guid Source;			// Source Address (2-16 bytes)
		public Guid Destination;	// Destination Address
		public byte[] Data;			// Packet Data
		public NetLayerAddressSize SourceSize
		{
			get
			{ return (NetLayerAddressSize) (Flags & 0x03); }
			set
			{ Flags = (byte) ((Flags & 0xFC) | (byte) value); }
		}
		public NetLayerAddressSize DestinationSize
		{
			get
			{ return (NetLayerAddressSize) ((Flags >> 2) & 0x03); }
			set
			{ Flags = (byte) ((Flags & 0xF3) | ((byte) value << 2)) ; }
		}
		public bool IsValid
		{
			get
			{ return Sync == SyncByte; }
			set
			{ Sync = (value) ? SyncByte : (byte) 0; }
		}
		public bool IsFragmented
		{
			get
			{ return Offset != 0xFFFF; }
			set
			{ if ( !value ) Offset = (UInt16) 0xFFFF; }
		}
		public bool IsAcknowledgement
		{
			get
			{ return GetFlag(NetLayerFlag.Acknowledgement); }
			set
			{ SetFlag(NetLayerFlag.Acknowledgement, value); }
		}
		public bool RequiresAcknowledge
		{
			get
			{ return GetFlag(NetLayerFlag.RequireAcknowledge); }
			set
			{ SetFlag(NetLayerFlag.RequireAcknowledge, value); }
		}
		public bool IsTerminal
		{
			get
			{ return GetFlag(NetLayerFlag.Terminal); }
			set
			{ SetFlag(NetLayerFlag.Terminal, value); }
		}
		public const byte SyncByte = 0xC3;

		// New Net Layer Packet
		public NetLayerPacket()
		{
		}
		
		// New Net Layer Packet (from raw data)
		public NetLayerPacket(byte[] rawdata, ref int start)
		{
			Sync = rawdata[start];
			if ( !IsValid ) return;
			Checksum = rawdata[start + 1];
			Id = rawdata[start + 2];
			Offset = BitConverter.ToUInt16(rawdata, start + 3);
			Size = BitConverter.ToUInt16(rawdata, start + 5);
			Flags = rawdata[start + 7];
			start += 8;
			Source = GetAddress(SourceSize, rawdata, ref start);
			Destination = GetAddress(DestinationSize, rawdata, ref start);
			Data = new byte[Size];
			Array.Copy(rawdata, start, Data, 0, Size);
		}
		
		// Get Raw Net Layer Packet
		public byte[] GetRaw()
		{
			if ( !IsValid ) return null;
			byte[] rawdata = new byte[1];
			rawdata[0] = Sync;
			return null;
		}

		// Get a particular Flag bit
		public bool GetFlag(NetLayerFlag flag)
		{
			return (Flags & (byte) flag) > 0;
		}

		// Set a particular Flag bit
		public void SetFlag(NetLayerFlag flag, bool value)
		{
			Flags = (byte) ((Flags & ~((byte) flag)) | (byte) flag);
		}
		
		// Get source / destination address (from raw data)
		private static Guid GetAddress(NetLayerAddressSize size, byte[] rawdata, ref int start)
		{
			Guid address;
			switch ( size )
			{
				case NetLayerAddressSize.Size16bit:
					address = new Guid(BitConverter.ToUInt16(rawdata, start), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
					start += 2;
					break;
				case NetLayerAddressSize.Size32bit:
					address = new Guid(BitConverter.ToUInt32(rawdata, start), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
					start += 4;
					break;
				case NetLayerAddressSize.Size64bit:
					address = new Guid(BitConverter.ToUInt32(rawdata, start), BitConverter.ToUInt16(rawdata, start + 4), BitConverter.ToUInt16(rawdata, start + 6), 0, 0, 0, 0, 0, 0, 0, 0);
					start += 8;
					break;
				case NetLayerAddressSize.Size128bit:
					byte[] guid_bytes = new byte[16];
					Array.Copy(rawdata, start, guid_bytes, 0, 16);
					address = new Guid(guid_bytes);
					start += 16;
					break;
				default:
					address = new Guid();
					break;
			}
			return address;
		}
	}

	public class NetLayerBuffer
	{
		public byte[] Data;
		public int Begin, End, BlockLength;
		public bool BlockStarted, BlockFound;
		public int Size
		{
			get
			{ return Data.Length; }
			set
			{
				if ( value < End - Begin ) return;
				byte[] buff = new byte[value];
				Array.Copy(Data, Begin, buff, 0, End - Begin);
				Data = buff;
				End -= Begin;
				Begin = 0;
			}
		}
		public int Used
		{
			get
			{ return End - Begin; }
		}
		public int Free
		{
			get
			{ return Data.Length - End; }
			set
			{ Size = Data.Length + Free - value; }
		}
		public int TotalFree
		{
			get
			{ return Data.Length - (End - Begin); }
			set
			{ Size = Data.Length + TotalFree - value; }
		}
		public byte this[int indx]
		{
			get
			{ return Data[indx]; }
			set
			{ Data[indx] = value; }
		}
		private const int defSize = 65536;

		// Create a new Network Buffer
		public NetBuffer(int size)
		{
			Data = new byte[size];
		}
		public NetBuffer()
			: this(defSize)
		{
		}

		// Index Of
		public int IndexOf(int begin, int end, byte val)
		{
			for ( int i = begin; i < end; i++ )
			{ if ( Data[i] == val ) return i; }
			return -1;
		}
		public int IndexOf(int begin, byte val)
		{
			return IndexOf(begin, End, val);
		}
		public int IndexOf(byte val)
		{
			return IndexOf(Begin, End, val);
		}

		// Reorder the data in buffer
		public void Reorder()
		{
			if ( !((Begin < End) && (Begin > 0)) )
			{
				Array.Copy(Data, Begin, Data, 0, End - Begin);
				End -= Begin;
				Begin = 0;
			}
		}

		// Get block
		public byte[] GetBlock()
		{
			byte[] blk = null;
			if ( BlockFound )
			{
				blk = new byte[BlockLength];
				Array.Copy(Data, Begin, blk, 0, BlockLength);
				Begin += BlockLength;
				BlockLength = 0;
				BlockStarted = false;
				BlockFound = false;
			}
			return blk;
		}

		// Clear the Network Buffer
		public void Clear()
		{
			Begin = 0;
			End = 0;
			BlockLength = 0;
			BlockFound = false;
		}

		// Close buffer
		public void Close()
		{
			Clear();
			Data = null;
		}
	}


	public class NetLayerClient
	{
		public Socket Socket;
		public IPEndPoint Address;
		private byte[] Buffer;
		
	}

	public struct ValueRange
	{
		public int Begin;
		public int End;
		public int Size
		{
			get
			{ return End - Begin; }
		}

		public ValueRange(int begin, int end)
		{
			Begin = begin;
			End = end;
		}
		public bool IsBeside(ValueRange range)
		{
			return End == range.Begin || Begin == range.End;
		}
	}

	public class NetLayerAssembler
	{
		private byte[] Data;
		private List<ValueRange> GotRanges;
		private bool GotTerminal;
		private bool Complete;
		private int Size
		{
			get
			{ return Data.Length; }
			set
			{
				byte[] data = new byte[value];
				Array.Copy(Data, 0, data, 0, Math.Min(Data.Length, data.Length));
			}
		}
		private const int DefaultSize = 8192;

		public NetLayerAssembler(int initial_size)
		{
			Data = new byte[initial_size];
			GotRanges = new List<ValueRange>();
		}
		public NetLayerAssembler()
			: this(DefaultSize)
		{
		}
	}

	public class NetLayer
	{
		private Socket socket;
		private List<Socket> Client;
		public IPEndPoint Address;
		public int Sent;
		public int Received;

		public static IPAddress[] IpAddresses
		{
			get
			{ return Dns.GetHostAddresses(Dns.GetHostName()); }
		}
		public static IPAddress Ipv4Address
		{
			get
			{ return (IPAddress) IpAddresses.First(ip => (ip.AddressFamily == AddressFamily.InterNetwork)); }
		}
		public static IPAddress Ipv6Address
		{
			get
			{ return (IPAddress) IpAddresses.First(ip => (ip.AddressFamily == AddressFamily.InterNetworkV6)); }
		}

		public NetLayer(IPEndPoint address)
		{
			Address = address;
			socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			socket.Bind(address);
			socket.Listen(64);
			Client = new List<Socket>();
			ThreadPool.QueueUserWorkItem(new WaitCallback(acceptClient));
		}
		public NetLayer(IPAddress address, int port)
			: this(new IPEndPoint(address, port))
		{
		}
		public NetLayer(int port)
			: this(new IPEndPoint(Ipv4Address, port))
		{
		}

		private void acceptClient(object e)
		{
			try
			{
				while ( true )
				{
					Socket client_socket = socket.Accept();
					Client.Add(client_socket);
					// new client event
				}
			}
			catch ( Exception ) { }
		}

	}
}
