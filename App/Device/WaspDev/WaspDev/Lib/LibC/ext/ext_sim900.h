/*
----------------------------------------------------------------------------------------
	sim900: SIM900 GSM/GPRS support module
	File: sim900.h

	This file is part of WASP. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WASP is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WASP is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WASP.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	sim900 is an SIM900 GSM/GPRS support module for WASP. It has been developed for
	for Flood Prediction and Alert System project. The sensor is available at element14 > 
	Semiconductors - ICs > Sensors > Pressure > Max Operating Pressure = 700 kPa. For more
	information, please visit: https://github.com/wolfram77/WASP.
*/



#ifndef	_ext_sim900_h_
#define	_ext_sim900_h_



// Requisite headers
#include "std/macro.h"
#include "std/memory.h"
#include "std/register.h"
#include "std/type.h"



// Select abbreviation level
// 
// The default abbreviation level is 7 i.e., members of this
// module can be accessed as sim900<function_name> directly.
// The abbreviation level can be selected in the main header
// file of WASP library
#ifndef	sim900_abbr
#define	sim900_abbr		7
#endif



// pressure sensor object
typedef struct _sim900
{
	int		pin;		// pin number connected
	float	mulf;		// multiplication factor
	float	addf;		// addition factor
}sim900;

#if sim900_abbr >= 1
typedef	sim900	sim900;
#endif



// Function:
// Init(*snsr)
// 
// Initializes a new SIM900 sensor object that can be used for
// reading from the sensor
// 
// Parameters:
// *snsr:	pointer to SIM900 sensor object
// pin:		pin to which the sensor is connected
// supply:	supply voltage to the sensor (in volts)
// offset:	fractional offset (of supply) of the sensor
// ref:		reference voltage used with ADC (in volts)
// bits:	number of bits of resolution of ADC
// max:		maximum pressure read by sensor
// 
// Returns:
// value:	the value recorded by sensor
// 
void sim900_Init(sim900* snsr, int pin, float supply, float offset, float ref, int bits, float max)
{
	snsr->pin = pin;
	snsr->mulf = ((ref / ((1 << bits) - 1)) / supply) * max;
	snsr->addf = (-offset) * max;
}

#if sim900_abbr >= 1
#define	sim900Init		sim900_Init
#endif

#if	sim900_abbr >= 2
#define	Init		sim900_Init
#endif



// Function:
// Read(*snsr)
// 
// Reads pressure value using a sensor object
// 
// Parameters:
// *snsr:	sensor object to use for reading
// 
// Returns:
// value:	the value recorded by sensor
// 
#define	sim900_Read(snsr)	\
	((analogRead((snsr)->pin) * (snsr)->mulf) + (snsr)->addf)

#if sim900_abbr >= 1
#define	sim900Read			sim900_Read
#endif

#if	sim900_abbr >= 2
#define	Read			sim900_Read
#endif



#endif
