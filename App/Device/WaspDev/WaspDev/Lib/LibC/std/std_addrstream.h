/*
----------------------------------------------------------------------------------------
	stream: Static data-chunk stream module
	File: std_stream.h

    This file is part of WASP. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

    WASP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WASP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WASP.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	stream is a static data-chunk stream module for WASP. For more information,
	please visit: https://github.com/wolfram77/WASP.
*/



#ifndef	_std_stream_h_
#define	_std_stream_h_



// Requisite headers
#include "std_macro.h"
#include "std_type.h"
#include "std_task.h"



// Select abbreviation level
// 
// The default abbreviation level is 7 i.e., members of this
// module can be accessed as stm<function_name> directly.
#ifndef	stream_abbr
#define	stream_abbr		7
#endif



// Stream Mold Making
// [Make / Mk]
// 
// Stream molds (structures) can be created with desired sizes. The Make() can be used to create such
// molds. The molds can then be used to create objects as stream_<size><type> <object>;
// 
typedef struct _stream_chunk
{
	byte*	dst;
	byte*	src;
	uint	len;
	byte*	busy;
}stream_chunk;

typedef stream_chunk	stream_chnk;

#define	stream_Make(sz)	\
typedef struct _stream##sz	\
{	\
	num		front;		\
	num		rear;		\
	num		count;		\
	num		size;		\
	stream_chunk	chunk[sz];	\
}stream##sz

#define	stream_Mk	stream_Make

typedef struct _stream_header
{
	num		front;
	num		rear;
	num		count;
	num		size;
}stream_header;

typedef stream_header	stream_hdr;

stream_Make(2);
stream_Make(4);
stream_Make(8);
stream_Make(16);
stream_Make(32);
stream_Make(64);
stream_Make(128);
stream_Make(256);

#ifndef	stream_Def
#define	stream_Def		stream4
#endif

typedef stream_Def		stream;

#if stream_abbr >= 3
#define	stm_Make	stream_Make
#define	stm_Mk		stream_Make
#endif

#if stream_abbr >= 5
#define	streamMake	stream_Make
#define	streamMk	stream_Make
#endif

#if stream_abbr >= 7
#define	stmMake		stream_Make
#define	stmMk		stream_Make
#endif

#if	stream_abbr >= 10
#define	Make	stream_Make
#define	Mk		stream_Make
#endif



// Function:
// Initialize(*strm)
// [Initialize / Init]
// 
// Initializes a stream before use.
// 
// Parameters:
// *strm:	the stream to initialize
// 
// Returns:
// nothing
//
#define	stream_Initialize(strm)	\
	macro_Begin	\
		(strm)->front = 0;	\
		(strm)->rear = 0;	\
		(strm)->count = 0;	\
		(strm)->size = (sizeof(*(strm)) - sizeof(stream_hdr)) / sizeof(stream_chunk);	\
	macro_End

#define	stream_Init		stream_Initialize

#if stream_abbr >= 3
#define	stm_Initialize	stream_Initialize
#define	stm_Init		stream_Initialize
#endif

#if stream_abbr >= 5
#define	streamInitialize	stream_Initialize
#define	streamInit			stream_Initialize
#endif

#if stream_abbr >= 7
#define	stmInitialize	stream_Initialize
#define	stmInit			stream_Initialize
#endif

#if	stream_abbr >= 10
#define	Initialize	stream_Initialize
#define	Init		stream_Initialize
#endif



// Function:
// Clear(*strm)
// [Clear / Clr]
// 
// Clears a stream of all data.
// 
// Parameters:
// *strm:	the stream to clear
// 
// Returns:
// nothing
//
void stream_ClearF(stream* strm)
{
	strm->front = 0;
	strm->rear = 0;
	strm->count = 0;
}

#define	stream_Clear(strm)	\
	stream_ClearF((stream*)(strm))

#define	stream_Clr		stream_Clear

#if stream_abbr >= 3
#define	stm_Clear	stream_Clear
#define	stm_Clr		stream_Clear
#endif

#if stream_abbr >= 5
#define	streamClear		stream_Clear
#define	streamClr		stream_Clear
#endif

#if stream_abbr >= 7
#define	stmClear	stream_Clear
#define	stmClr		stream_Clear
#endif

#if	stream_abbr >= 10
#define	Clear	stream_Clear
#define	Clr		stream_Clear
#endif



// Function:
// GetAvailable(*strm)
// [GetAvailable / GetAvail]
// 
// Gives the number of cells available in a stream. Can be used to check if
// sufficient data is available before reading.
// 
// Parameters:
// *strm:	the stream whose available cells are to be known
// 
// Returns:
// cells:	number of available cells in stream
//
#define	stream_GetAvailable(strm)	\
	((strm)->count)

#define	stream_GetAvail		stream_GetAvailable

#if stream_abbr >= 3
#define	stm_GetAvailable	stream_GetAvailable
#define	stm_GetAvail		stream_GetAvailable
#endif

#if stream_abbr >= 5
#define	streamGetAvailable	stream_GetAvailable
#define	streamGetAvail		stream_GetAvailable
#endif

#if stream_abbr >= 7
#define	stmGetAvailable		stream_GetAvailable
#define	stmGetAvail			stream_GetAvailable
#endif

#if	stream_abbr >= 10
#define	GetAvailable	stream_GetAvailable
#define	GetAvail		stream_GetAvailable
#endif


 
// Function:
// GetFree(*strm)
// [GetFree]
// 
// Gives the number of free cells available in a stream. Can be used to
// check if sufficient free cells are available before writing.
// 
// Parameters:
// *strm:	the stream whose number of free cells are to be known
// 
// Returns:
// cells:	number of free cells in stream
//
#define	stream_GetFree(strm)	\
	((strm)->size - (strm)->count)

#if stream_abbr >= 3
#define	stm_GetFree		stream_GetFree
#endif

#if stream_abbr >= 5
#define	streamGetFree	stream_GetFree
#endif

#if stream_abbr >= 7
#define	stmGetFree		stream_GetFree
#endif

#if	stream_abbr >= 10
#define	GetFree		stream_GetFree
#endif



// Function:
// Write(*strm, *dst, *src, len, *busy)
// Write(*strm, *src, len, *busy)
// [Write / Wrt]
// 
// Writes a data buffer to the stream. The data buffer should not be modified
// as long as busy is indicated.
// 
// Parameters:
// *strm:	the stream to which data buffer is to be written
// *dst:	destination address (on an addressable device)
// *src:	source data buffer to be written to the stream
// len:		number of bytes to be written to stream from source
// *busy:	pointer to a "busy" byte indicating that source data buffer is being used
// 
// Returns:
// status: 0 for success, -1 / 0xFF for failure
//
num stream_WriteF(stream* strm, byte* dst, byte* src, uint len, byte* busy)
{
	num rear;
	if(stream_GetFree(strm))
	{
		rear = strm->rear;
		strm->chunk[rear].dst = dst;
		strm->chunk[rear].src = src;
		strm->chunk[rear].len = len;
		strm->chunk[rear].busy = busy;
		*busy = 1;
		if(++rear == strm->size) rear = 0;
		strm->rear = rear;
		strm->count++;
		return 0;
	}
	return (num)-1;
}

#define	stream_WriteD(strm, dst, src, len, busy)	\
	stream_WriteF((stream*)(strm), (byte*)(dst), (byte*)(src), (uint)(len), (byte*)(busy))

#define	stream_WriteS(strm, src, len, busy)	\
	stream_WriteF((stream*)(strm), null, (byte*)(src), (uint)(len), (byte*)(busy))

#define	stream_Write(...)	\
	macro_Fn(macro_Fn5(__VA_ARGS__, stream_WriteD, stream_WriteS)(__VA_ARGS__))

#define	stream_Wrt		stream_Write

#if stream_abbr >= 3
#define	stm_Write	stream_Write
#define	stm_Wrt		stream_Write
#endif

#if stream_abbr >= 5
#define	streamWrite		stream_Write
#define	streamWrt		stream_Write
#endif

#if stream_abbr >= 7
#define	stmWrite	stream_Write
#define	stmWrt		stream_Write
#endif

#if	stream_abbr >= 10
#define	Write	stream_Write
#define	Wrt		stream_Write
#endif



// Function:
// Peek(*strm)
// [Peek / Pk]
// 
// Peeks a chunk of data from the stream. Returns pointer to the stream chunk.
// Does not pop (unbusy) the data from stream.
// 
// Parameters:
// *strm:	the stream from which a stream_chunk is to be peeked
// 
// Returns:
// chnk:	stream_chunk to read
//
#define	stream_Peek(strm)	\
	(&((strm)->chunk[(strm)->front]))

#define	stream_Pk	stream_Peek

#if stream_abbr >= 3
#define	stm_Peek	stream_Peek
#define	stm_Pk		stream_Peek
#endif

#if stream_abbr >= 5
#define	streamPeek	stream_Peek
#define	streamPk	stream_Peek
#endif

#if stream_abbr >= 7
#define	stmPeek		stream_Peek
#define	stmPk		stream_Peek
#endif

#if	stream_abbr >= 10
#define	Peek	stream_Peek
#define	Pk		stream_Peek
#endif



// Function:
// Pop(*strm)
// [Pop]
// 
// Pops a chunk of data from the stream (unbusy the memory block).
// 
// Parameters:
// *strm:	the stream from which a chunk of data is to be popped
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
//
num stream_PopF(stream* strm)
{
	if(stream_GetAvail(strm))
	{
		*(strm->chunk[strm->front].busy) = 0;
		if(++(strm->front) == strm->size) strm->front = 0;
		strm->count--;
		return 0;
	}
	return (num)-1;
}

#define	stream_Pop(strm)	\
	stream_PopF((stream*)(strm))

#if stream_abbr >= 3
#define	stm_Pop		stream_Pop
#endif

#if stream_abbr >= 5
#define	streamPop	stream_Pop
#endif

#if stream_abbr >= 7
#define	stmPop		stream_Pop
#endif

#if	stream_abbr >= 10
#define	Pop		stream_Pop
#endif



// Function:
// Read(*strm, *dst, *len)
// Read(*strm, *len)
// [Read / Rd]
//
// Reads a chunk of data from the stream. Returns pointer to the data block.
// Does not pop the data from stream (use Pop() after data usage is done).
//
// Parameters:
// *strm:	the stream from which a data block is to be read
// *dst:	destination address for an addressable end (device)
// *len:	length of the data block to be read
//
// Returns:
// src:		source address of data block (null for no data)
//
byte* stream_ReadF(stream* strm, byte** dst, uint* len)
{
	num front;
	if(stream_GetAvail(strm))
	{
		front = strm->front;
		if(dst) *dst = strm->chunk[front].dst;
		if(len) *len = strm->chunk[front].len;
		return strm->chunk[front].src;
	}
	return null;
}

#define	stream_ReadA(strm, dst, len)	\
	stream_ReadF((stream*)(strm), (byte**)(dst), (uint*)(len))

#define stream_ReadNA(strm, len)	\
	stream_ReadF((stream*)(strm), null, (uint*)(len))

#define	stream_Read(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, stream_ReadA, stream_ReadNA)(__VA_ARGS__))

#define	stream_Rd	stream_Read

#if stream_abbr >= 3
#define	stm_Read	stream_Read
#define	stm_Rd		stream_Read
#endif

#if stream_abbr >= 5
#define	streamRead	stream_Read
#define	streamRd	stream_Read
#endif

#if stream_abbr >= 7
#define	stmRead		stream_Read
#define	stmRd		stream_Read
#endif

#if	stream_abbr >= 10
#define	Read	stream_Read
#define	Rd		stream_Read
#endif



// Function:
// ReadByte(*strm, *dst)
// ReadByte(*strm)
// [ReadByte / RdByte]
// 
// Reads a byte from a stream. Use this function only if data is available.
// This function "pops" a byte from stream.
// 
// Parameters:
// *strm:	the stream from which a byte is to be read
// *dst:	destination address on an addressable end (device)
// 
// Returns:
// val:		byte value, which is valid only if stream has some data
//
byte stream_ReadByteF(stream* strm, byte** dst)
{
	num front;
	byte val;
	if(stream_GetAvail(strm))
	{
		front = strm->front;
		val = *(strm->chunk[front].src);
		if(dst) *dst = strm->chunk[front].dst;
		(strm->chunk[front].dst)++;
		(strm->chunk[front].src)++;
		if(--(strm->chunk[front].len) == 0)
		{
			*(strm->chunk[front].busy) = 0;
			if(++front == strm->size) front = 0;
			strm->front = front;
			strm->count--;
		}
		return val;
	}
	return (byte)-1;
}

#define stream_ReadByteA(strm, dst)	\
	stream_ReadByteF((stream*)(strm), (byte**)(dst))

#define	stream_ReadByteNA(strm)	\
	stream_ReadByteF((stream*)(strm), null)

#define	stream_ReadByte(...)	\
macro_Fn(macro_Fn2(__VA_ARGS__, stream_ReadByteA, stream_ReadByteNA)(__VA_ARGS__))

#define	stream_RdByte	stream_ReadByte

#if stream_abbr >= 3
#define	stm_ReadByte	stream_ReadByte
#define	stm_RdByte		stream_ReadByte
#endif

#if stream_abbr >= 5
#define	streamReadByte	stream_ReadByte
#define	streamRdByte	stream_ReadByte
#endif

#if stream_abbr >= 7
#define	stmReadByte		stream_ReadByte
#define	stmRdByte		stream_ReadByte
#endif

#if	stream_abbr >= 10
#define	ReadByte	stream_ReadByte
#define	RdByte		stream_ReadByte
#endif



#endif
