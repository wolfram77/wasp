/*
----------------------------------------------------------------------------------------
	expint: Expandable Integer module
	File: std_expint.h

	Copyright � 2013 Subhajit Sahu. All rights reserved.

	This file is part of WASP. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WASP is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WASP is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WASP.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/


#ifndef	_std_expint_h_
#define	_std_expint_h_


// Format of Expandable integer
// 
// -------------------------------------
// | byte 0 | byte 1 |  ...   | byte n |
// -------------------------------------
// The integer is stored in little endian format. The number of least significant trailing '1' bits indicate
// the size of the expandable number in multiples of 7 bits. These trailing '1' bits is followed by a '0' bit
// which marks the end of integer size count and start of value. If there are no trailing '1' s in the least
// significant side, it indicates, that the number is of 7 bits size. Size of number = (N + 1) * 7, where
// N = number of '1' trailing bits.


#include "std_macro.h"
#include "std_type.h"


// Data types
#if lib_word_size >= 32
typedef int64		expint;
#else
typedef	int32		expint;
#endif


// Function:
// Read(*src, off, *bit_sz)
// [Read / Rd]
// 
// Reads an expandable integer from specified source address
// 
// Parameters:
// *src:	source base address
// off:		offset of expandable integer from base address
// *bit_sz:	pointer to variable for getting size of expandable integer in bits
// 
// Returns:
// value:		the value of expandable integer as expint
// 
expint expint_ReadF(void* src, byte* bit_sz)
{
	byte bitsz = 7;
	expint sel = 0x80;
	expint val = *((expint*)src);
	while(val & 1)
	{
		bitsz += 7;
		sel <<= 7;
		val >>= 1;
	}
	sel -= 1;
	val = (val >> 1) & sel;
	*bit_sz = bitsz;
	return val;
}

#define	expint_Read(src, off, bit_sz)	\
	expint_ReadF((byte*)(src) + (off), (byte*)(bit_sz))

#define	expint_Rd	expint_Read


// Function:
// Write(*dst, off, val, bitsz)
// [Write / Wrt]
// 
// Writes an expandable integer to specified destination address
// 
// Parameters:
// dst:		destination base address
// off:		offset of expandable integer from base address
// val:		the value of expandable integer as expint
// bitsz:	bit size of value
// 
// Returns:
// bytesz:	byte size of value in destination
// 
num expint_WriteD(void* dst, expint val, uint bitsz)
{
	num bytesz = 1;
	val <<= 1;
	bitsz -= 7;
	while(bitsz > 0)
	{
		val = (val << 1) | 1;
		bitsz -= 7;
		bytesz += 1;
	}
	*((expint*)dst) = val;
	return bytesz;
}

#define	expint_Write(dst, off, val, bitsz)	\
	expint_WriteD((byte*)(dst) + (off), val, bitsz)

#define	expint_Wrt	expint_Write


#endif
