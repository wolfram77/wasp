/*
----------------------------------------------------------------------------------------
	ProgMem: Program Memory test program
	File: ProgMem.ino

	This file is part of WASP. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WASP is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WASP is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WASP.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/

/*
	ProgMem is a test code for monitoring the memory requirement for using WASP Lib. For more information,
	please visit: https://github.com/wolfram77/WASP
*/

// Requisite headers
#include "std.h"

// Test cases:
// 
// Nothing used: 466 bytes
// 
// 1 / 2 WASP Lib commands: 474 bytes
// 
//
// 
char Data[10];
expint val;

void setup()
{
    Serial.begin(9600);
    regSetBit(PORTC, 1);
    val = exiRead(Data, 0);
}

void loop()
{
    Serial.print("aaaaaaa\n");
}

