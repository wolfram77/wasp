void setup()
{
  Serial.begin(57600);
  pinMode(A0, INPUT);
}

void loop()
{
  int val = analogRead(A0);
  Serial.write(val & 0xFF);
  Serial.write(val >> 8);
  delay(100);
}

