void setup()
{
  Serial.begin(9600);
  pinMode(A0, INPUT);
}

void loop()
{
  int val = analogRead(A0);
  float pr = (val / 1024.0f);
  pr = (pr - 0.2f / 5.0f) * 700;
  Serial.println("AT");
  delay(2000);
  while(Serial.available())Serial.read();
  Serial.println("AT+CMGS=\"9438484151\"");
  delay(2000);
  while(Serial.available())Serial.read();
  Serial.print("Pressure: ");
  delay(2000);
  while(Serial.available())Serial.read();
  Serial.print(pr);
  delay(2000);
  while(Serial.available())Serial.read();
  Serial.println(" kPa");
  delay(2000);
  while(Serial.available())Serial.read();
  Serial.write(26);
  delay(2000);
  while(Serial.available())Serial.read();
  Serial.println("");
  delay(20000);
  while(Serial.available())Serial.read();
}

