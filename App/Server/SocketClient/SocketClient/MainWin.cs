﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace SocketClient
{
	public partial class MainWin : Form
	{
		Client Clnt;
		bool SendHex;
		bool ReceiveHex;
		bool UiConnected
		{
			set
			{
				if ( !ChkBxConnected.InvokeRequired ) ChkBxConnected.Checked = value;
				ChkBxConnected.Invoke((Action) (() => ChkBxConnected.Checked = value));
			}
		}
		bool UiDataSent
		{
			set
			{
				if ( !ChkBxDataSent.InvokeRequired ) ChkBxDataSent.Checked = value;
				Invoke((Action) (() => ChkBxDataSent.Checked = value));
			}
		}
		bool UiDataAvailable
		{
			set
			{
				if ( !ChkBxDataAvailable.InvokeRequired ) ChkBxDataAvailable.Checked = value;
				Invoke((Action) (() => ChkBxDataAvailable.Checked = value));
			}
		}
		bool UiDataReceived
		{
			set
			{
				if ( !ChkBxDataReceived.InvokeRequired ) ChkBxDataReceived.Checked = value;
				Invoke((Action) (() => ChkBxDataReceived.Checked = value));
			}
		}

		public MainWin()
		{
			InitializeComponent();
			Clnt = new Client();
			Clnt.Callback = ClientEventhandler;
		}
		static string HexToAscii(string hex)
		{
			StringBuilder ascii = new StringBuilder();
			for ( int i = 0; i < hex.Length; i += 3 )
			{
				string hs = hex.Substring(i, 2);
				ascii.Append(Convert.ToChar(Convert.ToUInt32(hs, 16)));
			}
			return ascii.ToString();
		}
		static string AsciiToHex(string ascii)
		{
			StringBuilder hex = new StringBuilder();
			foreach ( char c in ascii )
				hex.AppendFormat("{0:X2} ", (int) c);
			return hex.ToString().Trim();
		}
		void UpdateStatus(string msg)
		{
			ChkBxConnected.Checked = Clnt.IsConnected;
			ChkBxDataAvailable.Checked = Clnt.IsAvailable;
			ChkBxDataSent.Checked = Clnt.WasSent;
			ChkBxDataReceived.Checked = Clnt.WasReceived;
			LblStatus.Text = msg;
		}
		void UpdateReceive(string recv, string msg)
		{
			UpdateStatus(msg);
			if ( ReceiveHex ) TxtReceive.Text = AsciiToHex(recv);
			else TxtReceive.Text = recv;
		}
		void ClientEventhandler(string function, string msg, object data)
		{
			switch ( function )
			{
				case "Connect":
				case "Disconnect":
				case "Available":
				case "Send":
					Invoke((Action) (() => UpdateStatus(msg)));
					break;
				case "Receive":
					Invoke((Action) (() => UpdateReceive((string) data, msg)));
					break;
			}
		}
		private void BtnConnect_Click(object sender, EventArgs e)
		{
			Clnt.Call("Connect", TxtServer.Text);
		}
		private void BtnDisconnect_Click(object sender, EventArgs e)
		{
			Clnt.Call("Disconnect", null);
		}
		private void BtnSend_Click(object sender, EventArgs e)
		{
			string data = TxtSend.Text;
			if ( SendHex ) data = HexToAscii(data);
			Clnt.Call("Send", data);
		}
		private void BtnSendMode_Click(object sender, EventArgs e)
		{
			SendHex = !SendHex;
			if ( SendHex )
			{
				TxtSend.Text = AsciiToHex(TxtSend.Text);
				BtnSendMode.Text = "Mode: Hex";
			}
			else
			{
				TxtSend.Text = HexToAscii(TxtSend.Text);
				BtnSendMode.Text = "Mode: ASCII";
			}
		}
		private void BtnReceive_Click(object sender, EventArgs e)
		{
			Clnt.Call("Receive", null);
		}
		private void BtnReceiveMode_Click(object sender, EventArgs e)
		{
			ReceiveHex = !ReceiveHex;
			if ( ReceiveHex )
			{
				TxtReceive.Text = AsciiToHex(TxtReceive.Text);
				BtnReceiveMode.Text = "Mode: Hex";
			}
			else
			{
				TxtReceive.Text = HexToAscii(TxtReceive.Text);
				BtnReceiveMode.Text = "Mode: ASCII";
			}
		}
	}
}
