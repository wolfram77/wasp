﻿namespace SocketClient
{
	partial class MainWin
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if ( disposing && (components != null) )
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWin));
			this.LblServer = new System.Windows.Forms.Label();
			this.TxtServer = new System.Windows.Forms.TextBox();
			this.BtnConnect = new System.Windows.Forms.Button();
			this.BtnDisconnect = new System.Windows.Forms.Button();
			this.TxtSend = new System.Windows.Forms.TextBox();
			this.BtnSend = new System.Windows.Forms.Button();
			this.BtnSendMode = new System.Windows.Forms.Button();
			this.TxtReceive = new System.Windows.Forms.TextBox();
			this.BtnReceiveMode = new System.Windows.Forms.Button();
			this.BtnReceive = new System.Windows.Forms.Button();
			this.ChkBxDataReceived = new System.Windows.Forms.CheckBox();
			this.ChkBxDataAvailable = new System.Windows.Forms.CheckBox();
			this.ChkBxDataSent = new System.Windows.Forms.CheckBox();
			this.ChkBxConnected = new System.Windows.Forms.CheckBox();
			this.LblStatus = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// LblServer
			// 
			this.LblServer.AutoSize = true;
			this.LblServer.Location = new System.Drawing.Point(13, 15);
			this.LblServer.Name = "LblServer";
			this.LblServer.Size = new System.Drawing.Size(66, 13);
			this.LblServer.TabIndex = 0;
			this.LblServer.Text = "Server : Port";
			// 
			// TxtServer
			// 
			this.TxtServer.Location = new System.Drawing.Point(16, 31);
			this.TxtServer.Name = "TxtServer";
			this.TxtServer.Size = new System.Drawing.Size(141, 20);
			this.TxtServer.TabIndex = 1;
			// 
			// BtnConnect
			// 
			this.BtnConnect.Location = new System.Drawing.Point(16, 158);
			this.BtnConnect.Name = "BtnConnect";
			this.BtnConnect.Size = new System.Drawing.Size(62, 50);
			this.BtnConnect.TabIndex = 2;
			this.BtnConnect.Text = "Connect";
			this.BtnConnect.UseVisualStyleBackColor = true;
			this.BtnConnect.Click += new System.EventHandler(this.BtnConnect_Click);
			// 
			// BtnDisconnect
			// 
			this.BtnDisconnect.Location = new System.Drawing.Point(84, 158);
			this.BtnDisconnect.Name = "BtnDisconnect";
			this.BtnDisconnect.Size = new System.Drawing.Size(73, 50);
			this.BtnDisconnect.TabIndex = 3;
			this.BtnDisconnect.Text = "Disconnect";
			this.BtnDisconnect.UseVisualStyleBackColor = true;
			this.BtnDisconnect.Click += new System.EventHandler(this.BtnDisconnect_Click);
			// 
			// TxtSend
			// 
			this.TxtSend.Location = new System.Drawing.Point(185, 12);
			this.TxtSend.Multiline = true;
			this.TxtSend.Name = "TxtSend";
			this.TxtSend.Size = new System.Drawing.Size(179, 131);
			this.TxtSend.TabIndex = 4;
			// 
			// BtnSend
			// 
			this.BtnSend.Location = new System.Drawing.Point(185, 158);
			this.BtnSend.Name = "BtnSend";
			this.BtnSend.Size = new System.Drawing.Size(62, 50);
			this.BtnSend.TabIndex = 5;
			this.BtnSend.Text = "Send";
			this.BtnSend.UseVisualStyleBackColor = true;
			this.BtnSend.Click += new System.EventHandler(this.BtnSend_Click);
			// 
			// BtnSendMode
			// 
			this.BtnSendMode.Location = new System.Drawing.Point(253, 158);
			this.BtnSendMode.Name = "BtnSendMode";
			this.BtnSendMode.Size = new System.Drawing.Size(111, 50);
			this.BtnSendMode.TabIndex = 6;
			this.BtnSendMode.Text = "Mode: ASCII";
			this.BtnSendMode.UseVisualStyleBackColor = true;
			this.BtnSendMode.Click += new System.EventHandler(this.BtnSendMode_Click);
			// 
			// TxtReceive
			// 
			this.TxtReceive.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.TxtReceive.Location = new System.Drawing.Point(393, 12);
			this.TxtReceive.Multiline = true;
			this.TxtReceive.Name = "TxtReceive";
			this.TxtReceive.ReadOnly = true;
			this.TxtReceive.Size = new System.Drawing.Size(179, 131);
			this.TxtReceive.TabIndex = 7;
			// 
			// BtnReceiveMode
			// 
			this.BtnReceiveMode.Location = new System.Drawing.Point(461, 158);
			this.BtnReceiveMode.Name = "BtnReceiveMode";
			this.BtnReceiveMode.Size = new System.Drawing.Size(111, 50);
			this.BtnReceiveMode.TabIndex = 9;
			this.BtnReceiveMode.Text = "Mode: ASCII";
			this.BtnReceiveMode.UseVisualStyleBackColor = true;
			this.BtnReceiveMode.Click += new System.EventHandler(this.BtnReceiveMode_Click);
			// 
			// BtnReceive
			// 
			this.BtnReceive.Location = new System.Drawing.Point(393, 158);
			this.BtnReceive.Name = "BtnReceive";
			this.BtnReceive.Size = new System.Drawing.Size(62, 50);
			this.BtnReceive.TabIndex = 8;
			this.BtnReceive.Text = "Receive";
			this.BtnReceive.UseVisualStyleBackColor = true;
			this.BtnReceive.Click += new System.EventHandler(this.BtnReceive_Click);
			// 
			// ChkBxDataReceived
			// 
			this.ChkBxDataReceived.AutoSize = true;
			this.ChkBxDataReceived.Enabled = false;
			this.ChkBxDataReceived.Location = new System.Drawing.Point(16, 126);
			this.ChkBxDataReceived.Name = "ChkBxDataReceived";
			this.ChkBxDataReceived.Size = new System.Drawing.Size(98, 17);
			this.ChkBxDataReceived.TabIndex = 10;
			this.ChkBxDataReceived.Text = "Data Received";
			this.ChkBxDataReceived.UseVisualStyleBackColor = true;
			// 
			// ChkBxDataAvailable
			// 
			this.ChkBxDataAvailable.AutoSize = true;
			this.ChkBxDataAvailable.Enabled = false;
			this.ChkBxDataAvailable.Location = new System.Drawing.Point(16, 103);
			this.ChkBxDataAvailable.Name = "ChkBxDataAvailable";
			this.ChkBxDataAvailable.Size = new System.Drawing.Size(95, 17);
			this.ChkBxDataAvailable.TabIndex = 11;
			this.ChkBxDataAvailable.Text = "Data Available";
			this.ChkBxDataAvailable.UseVisualStyleBackColor = true;
			// 
			// ChkBxDataSent
			// 
			this.ChkBxDataSent.AutoSize = true;
			this.ChkBxDataSent.Enabled = false;
			this.ChkBxDataSent.Location = new System.Drawing.Point(16, 80);
			this.ChkBxDataSent.Name = "ChkBxDataSent";
			this.ChkBxDataSent.Size = new System.Drawing.Size(74, 17);
			this.ChkBxDataSent.TabIndex = 12;
			this.ChkBxDataSent.Text = "Data Sent";
			this.ChkBxDataSent.UseVisualStyleBackColor = true;
			// 
			// ChkBxConnected
			// 
			this.ChkBxConnected.AutoSize = true;
			this.ChkBxConnected.Enabled = false;
			this.ChkBxConnected.Location = new System.Drawing.Point(16, 57);
			this.ChkBxConnected.Name = "ChkBxConnected";
			this.ChkBxConnected.Size = new System.Drawing.Size(78, 17);
			this.ChkBxConnected.TabIndex = 13;
			this.ChkBxConnected.Text = "Connected";
			this.ChkBxConnected.UseVisualStyleBackColor = true;
			// 
			// LblStatus
			// 
			this.LblStatus.AutoSize = true;
			this.LblStatus.Location = new System.Drawing.Point(13, 230);
			this.LblStatus.Name = "LblStatus";
			this.LblStatus.Size = new System.Drawing.Size(40, 13);
			this.LblStatus.TabIndex = 14;
			this.LblStatus.Text = "Status:";
			// 
			// MainWin
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(590, 255);
			this.Controls.Add(this.LblStatus);
			this.Controls.Add(this.ChkBxConnected);
			this.Controls.Add(this.ChkBxDataSent);
			this.Controls.Add(this.ChkBxDataAvailable);
			this.Controls.Add(this.ChkBxDataReceived);
			this.Controls.Add(this.BtnReceiveMode);
			this.Controls.Add(this.BtnReceive);
			this.Controls.Add(this.TxtReceive);
			this.Controls.Add(this.BtnSendMode);
			this.Controls.Add(this.BtnSend);
			this.Controls.Add(this.TxtSend);
			this.Controls.Add(this.BtnDisconnect);
			this.Controls.Add(this.BtnConnect);
			this.Controls.Add(this.TxtServer);
			this.Controls.Add(this.LblServer);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainWin";
			this.Text = "SocketClient v0.2";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LblServer;
		private System.Windows.Forms.TextBox TxtServer;
		private System.Windows.Forms.Button BtnConnect;
		private System.Windows.Forms.Button BtnDisconnect;
		private System.Windows.Forms.TextBox TxtSend;
		private System.Windows.Forms.Button BtnSend;
		private System.Windows.Forms.Button BtnSendMode;
		private System.Windows.Forms.TextBox TxtReceive;
		private System.Windows.Forms.Button BtnReceiveMode;
		private System.Windows.Forms.Button BtnReceive;
		private System.Windows.Forms.CheckBox ChkBxDataReceived;
		private System.Windows.Forms.CheckBox ChkBxDataAvailable;
		private System.Windows.Forms.CheckBox ChkBxDataSent;
		private System.Windows.Forms.CheckBox ChkBxConnected;
		private System.Windows.Forms.Label LblStatus;

	}
}

