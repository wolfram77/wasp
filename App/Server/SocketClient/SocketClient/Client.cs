﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace SocketClient
{
	public delegate void ClientCallback(string function, string msg, object data);

	public class Client
	{
		public Socket Skt;
		public bool IsConnected;
		public bool IsAvailable;
		public bool WasSent, WasReceived;
		public ClientCallback Callback;
		static string Name
		{
			get
			{ return Dns.GetHostName(); }
		}
		static IPAddress[] IpAddrs
		{
			get
			{ return Dns.GetHostAddresses(Name); }
		}
		static IPAddress IPv4Addr
		{
			get
			{ return (IPAddress) IpAddrs.First(ip => (ip.AddressFamily == AddressFamily.InterNetwork)); }
		}
		static IPAddress IPv6Addr
		{
			get
			{ return (IPAddress) IpAddrs.First(ip => (ip.AddressFamily == AddressFamily.InterNetworkV6)); }
		}

		public static string EndPointString(EndPoint endPoint)
		{
			IPEndPoint ipEndPoint = (IPEndPoint) endPoint;
			return "[" + ipEndPoint.Address + ":" + ipEndPoint.Port + "]";
		}
		public Client()
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(thread_CheckAvailable));
		}
		void thread_CheckAvailable(object nothing)
		{
			int oldavail = 0;
			while ( true )
			{
				Thread.Sleep(100);
				try
				{
					if ( Skt != null && Skt.Connected && Skt.Available != oldavail )
					{
						oldavail = Skt.Available;
						string msg = Skt.Available + " byte(s) available.";
						IsAvailable = (oldavail > 0);
						Callback("Available", msg, null);
					}
				}
				catch ( Exception ) { }
			}
		}
		void Connect(object objEndPoint)
		{
			if ( Skt != null ) Disconnect(null);
			IsConnected = false;
			string msg = "Improper input data";
			try
			{
				string endPoint = (string) objEndPoint;
				string[] ipParts = endPoint.Split(':');
				IPAddress ipAddr = IPAddress.Parse(ipParts[0]);
				int port = int.Parse(ipParts[1]);
				IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);
				try
				{
					Skt = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
					Skt.Connect(ipEndPoint);
					msg = "Connected to " + EndPointString(Skt.RemoteEndPoint) + ".";
					IsConnected = Skt.Connected;
				}
				catch ( Exception )
				{ msg = "Failed to connect."; }
			}
			catch ( Exception )
			{ msg = "Invalid IP Endpoint format."; }
			if ( Callback != null ) Callback("Connect", msg, null);
		}
		void Disconnect(object nothing)
		{
			IsConnected = false;
			string msg = "Already disconnected.";
			if ( Skt != null && Skt.Connected )
			{
				msg = "Diconnected from " + EndPointString(Skt.RemoteEndPoint) + ".";
				try
				{ Skt.Disconnect(false); }
				finally
				{
					Skt.Close();
					Skt = null;
				}
			}
			Callback("Disconnect", msg, null);
		}
		void Send(object objSendString)
		{
			WasSent = false;
			string msg = "Not connected.";
			if ( Skt != null && Skt.Connected )
			{
				try
				{
					string sendString = (string) objSendString;
					byte[] sendData = Encoding.ASCII.GetBytes(sendString);
					Skt.Send(sendData);
					msg = sendData.Length + " byte(s) sent to " + EndPointString(Skt.RemoteEndPoint) + ".";
					WasSent = true;
				}
				catch ( Exception )
				{ msg = "Sending to " + EndPointString(Skt.RemoteEndPoint) + " failed."; }
			}
			Callback("Send", msg, null);
		}
		void Receive(object nothing)
		{
			WasReceived = false;
			string msg = "Not connected.";
			string recvString = "";
			if ( Skt != null && Skt.Connected )
			{
				try
				{
					int availBytes = Skt.Available;
					byte[] recvData = new byte[availBytes];
					Skt.Receive(recvData);
					recvString = Encoding.ASCII.GetString(recvData);
					msg = "Data received from " + EndPointString(Skt.RemoteEndPoint) + ".";
					WasReceived = true;
				}
				catch ( Exception )
				{ msg = "Failed to receive from " + EndPointString(Skt.RemoteEndPoint) + " ."; }
			}
			Callback("Receive", msg, recvString);
		}
		public void Call(string function, object data)
		{
			switch ( function )
			{
				case "Connect":
					ThreadPool.QueueUserWorkItem(new WaitCallback(Connect), data);
					break;
				case "Disconnect":
					ThreadPool.QueueUserWorkItem(new WaitCallback(Disconnect), data);
					break;
				case "Send":
					ThreadPool.QueueUserWorkItem(new WaitCallback(Send), data);
					break;
				case "Receive":
					ThreadPool.QueueUserWorkItem(new WaitCallback(Receive), data);
					break;
			}
		}
	}
}
