﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Data;
using System.Data.SQLite;


namespace UhuruClient
{
	public struct SensorData
	{
		public int Id;
		public int Time;
		public double Value;

		public SensorData(int id, int time, double value)
		{
			Id = id;
			Time = time;
			Value = value;
		}
		public SensorData(byte[] source, int offset)
		{
			Id = BitConverter.ToInt32(source, offset);
			offset += 4;
			Time = BitConverter.ToInt32(source, offset);
			offset += 4;
			Value = BitConverter.ToSingle(source, offset);
		}
	}

	class Program
	{
		static WebClient Client;
		const string DatabaseFile = "Wasp.db";
		const string DatabasePath = @"Data\";
		const string Database = DatabasePath + DatabaseFile;
		const string ConnString = @"Data Source=" + Database + ";Version=3;";
		const string WebServer = "http://webgateway.apphb.com";
		const string TagStart = "<span id=\"lblData\" class=\"lead\">";
		const string TagStop = "</span>";
		const string Separator = ".<br/>";

		static void Main(string[] args)
		{
			OpenDatabase();
			Console.WriteLine("Database {0} initialized.\n\n", Database);
			Client = new WebClient();
			while ( true )
			{
				string waspData = "";
				try { waspData = Client.DownloadString(WebServer); }
				catch ( Exception ) { continue; }
				try
				{
					int dataOff = waspData.IndexOf(TagStart);
					waspData = waspData.Substring(dataOff + TagStart.Length);
					dataOff = waspData.IndexOf(TagStop);
					waspData = waspData.Remove(dataOff);
					string[] waspPackets = waspData.Split(new string[] {Separator}, StringSplitOptions.RemoveEmptyEntries);
					SensorData[] sensorDatas = new SensorData[waspPackets.Length];
					for ( int i = 0; i < waspPackets.Length; i++ )
					{
						string packet = HexToAscii(waspPackets[i]);
						byte[] rawPacket = Encoding.ASCII.GetBytes(packet);
						if ( rawPacket.Length < 12 ) continue;
						sensorDatas[i] = new SensorData(rawPacket, 0);
						Console.Write("Data:");
						Console.WriteLine("Sensor Id: {0}", sensorDatas[i].Id);
						Console.WriteLine("Record Time: {0}", sensorDatas[i].Time);
						Console.WriteLine("Value: {0}", sensorDatas[i].Value);
						Console.WriteLine();
						SaveToStore(sensorDatas);
					}
					if ( waspPackets.Length == 0 ) Console.WriteLine("\n\nNo Data.");
				}
				catch ( Exception ) { Console.WriteLine("\n\nNo data."); }
				Thread.Sleep(10000);
			}
		}
		static void OpenDatabase()
		{
			// Create database, if not yet created
			if ( !File.Exists(Database) )
			{
				Directory.CreateDirectory(DatabasePath);
				SQLiteConnection.CreateFile(Database);
			}
			// prepare tables
			using ( SQLiteConnection link = new SQLiteConnection(ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("CREATE TABLE IF NOT EXISTS Temp (Id INTEGER NOT NULL, " +
					"Time INTEGER NOT NULL, Value REAL NOT NULL);", link);
				cmd.ExecuteNonQuery();
			}
		}
		static void SaveToStore(SensorData[] sensorDatas)
		{
			using ( SQLiteConnection link = new SQLiteConnection(ConnString) )
			{
				link.Open();
				SQLiteTransaction txn = link.BeginTransaction();
				SQLiteCommand cmd = new SQLiteCommand("", link, txn);
				foreach ( SensorData sensorData in sensorDatas )
				{
					cmd.CommandText = "INSERT INTO Temp VALUES (@Id, @Time, @Value);";
					cmd.Parameters.AddWithValue("@Id", sensorData.Id);
					cmd.Parameters.AddWithValue("@Time", sensorData.Time);
					cmd.Parameters.AddWithValue("@Value", sensorData.Value);
					cmd.ExecuteNonQuery();
				}
				txn.Commit();
			}
		}
		static string HexToAscii(string hex)
		{
			// Convert a hexadecimal string to ASCII (binary)
			StringBuilder ascii = new StringBuilder();
			for ( int i = 0; i < hex.Length; i += 2 )
			{
				string hs = hex.Substring(i, 2);
				ascii.Append(Convert.ToChar(Convert.ToUInt32(hs, 16)));
			}
			return ascii.ToString();
		}
	}
}
