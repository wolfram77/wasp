﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using Wasp.Network;

namespace GprsGateway
{
	class Program
	{
		const string DataOpenTag = "<data>";
		const string DataCloseTag = "</data>";
		static void Main(string[] args)
		{
			NetHost gprsHost = new NetHost(80);
			NetHost appHost = new NetHost(64);
			Console.WriteLine("gprsHost = {0}", gprsHost.ToString());
			Console.WriteLine("appHost = {0}", appHost.ToString());
			Console.WriteLine();
			while ( true )
			{
				int dataOff = 0;
				Console.WriteLine("\n\nWaiting for data...");
				NetPacket packet = gprsHost.Receive();
				string data = Encoding.ASCII.GetString(packet.Data.Array, packet.Data.Offset, packet.Data.Length);
				Console.WriteLine("Data received:\n{0}", data);
				try
				{
					dataOff = data.IndexOf(DataOpenTag);
					data = data.Substring(dataOff + DataOpenTag.Length);
					dataOff = data.IndexOf(DataCloseTag);
					data = data.Remove(dataOff);
				}
				catch ( Exception )
				{
					Console.WriteLine("Data processing failed.");
					continue;
				}
				// Display data
				Console.WriteLine("Data extracted:\n{0}", data);
				try { data = HexToAscii(data); }
				catch ( Exception )
				{
					Console.WriteLine("Conversion to binary failed.");
					continue;
				}
				byte[] array = Encoding.ASCII.GetBytes(data);
				// send binary data to all clients
				foreach(NetClient client in appHost.Client.Values)
				{
					client.Send(array);
				}
				Console.WriteLine("Data sent to all clients.");
			}
		}
		static string HexToAscii(string hex)
		{
			StringBuilder ascii = new StringBuilder();
			for ( int i = 0; i < hex.Length; i += 2 )
			{
				string hs = hex.Substring(i, 2);
				ascii.Append(Convert.ToChar(Convert.ToUInt32(hs, 16)));
			}
			return ascii.ToString();
		}
	}
}
