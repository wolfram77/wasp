import processing.serial.*;

Serial device;
int[] ypoint;

void setup()
{
  size(1024, 640);
  background(0);
  device = new Serial(this, Serial.list()[0], 57600);
  ypoint = new int[50];
}

void draw()
{
  if(device.available() < 2) return;
  background(0);
  int newY = device.read() | (device.read() << 8);
  newY = newY * 640 / 1024;
  int[] newYpoint = new int[50];
  arrayCopy(ypoint, 1, newYpoint, 0, 49);
  ypoint = newYpoint;
  ypoint[49] = newY;
  drawArray(ypoint);
  textSize(24);
  textAlign(CENTER, CENTER);
  text("Pressure Graph", 0, 0, 1024, 60);
}

void drawArray(int[] ypos)
{
  int size = ypos.length;
  int spc = width / size;
  int x = 0;
  noFill();
  stroke(255);
  strokeWeight(2);
  beginShape();
  for(int i=0; i<size; i++)
  {
    vertex(x, ypos[i]);
    x += spc;
  }
  endShape();
}

