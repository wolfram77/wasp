﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using Wasp.Storage;

namespace Wasp.Network
{
	public struct NetUniquePacket
	{
		public int Id;
		public long Address;
		public int Offset;
		public int AckSize;

		public NetUniquePacket(int id)
		{
			Id = id;
			Address = 0;
			Offset = 0;
			AckSize = 0;
		}
		public NetUniquePacket(int id, long address)
		{
			Id = id;
			Address = address;
			Offset = 0;
			AckSize = 0;
		}
		public NetUniquePacket(int id, long address, int offset)
		{
			Id = id;
			Address = address;
			Offset = offset;
			AckSize = 0;
		}
		public NetUniquePacket(int id, long address, int offset, int ackSize)
		{
			Id = id;
			Address = address;
			Offset = offset;
			AckSize = ackSize;
		}
	}

	public class NetLayer
	{
		NetHost Host;	// host server
		NetEventFnGroup EventFn;	// event handlers for net layer
		StorDictionary<long, long> DeviceGateway;	// device to gateway address table
		Dictionary<long, IPEndPoint> GatewayEndpoint;	// gateway address to ipendpoint table
		Dictionary<IPEndPoint, long> EndpointGateway;	// ipendpoint to gatewayaddress table
		BlockingCollection<TransPacket> ForwardPacket;	// packets pending to send
		BlockingCollection<TransPacket> ReceivePacket;	// packets received after routing
		BlockingCollection<TransPacket> PendingPacket;	// packets pending to be sent / forwarded
		BlockingCollection<TransPacket> WaitAckPacket;	// packets waiting for acknowledge
		public int Sent, Received, Forwarded, Dropped;	// count of packets
		CancellationTokenSource ForwardCancel, FetchCancel;	// cancel tokens
		CancellationTokenSource SendCancel, ReceiveCancel;	// cancel tokens
		ConcurrentDictionary<NetUniquePacket, NetPacket> AckRcvd;	// packets for which ack is received
		ConcurrentDictionary<NetUniquePacket, DateTime> UniqueId;	// keeps track of unique packets
		static TimeSpan PacketDropTime = new TimeSpan(0, 2, 0);	// time before a packet is dropped
		const int PeriodicWaitTime = 1000;		// wait time before sending pending packets
		public const long ThisAddress = 1;		// address of this machine (AppServer)

		public NetLayer(NetHost host)
		{
			// Initialize
			Host = host;
			EventFn = new NetEventFnGroup();
			SendCancel = new CancellationTokenSource();
			ReceiveCancel = new CancellationTokenSource();
			GatewayEndpoint = new Dictionary<long, IPEndPoint>();
			EndpointGateway = new Dictionary<IPEndPoint, long>();
			ForwardPacket = new BlockingCollection<TransPacket>();
			ReceivePacket = new BlockingCollection<TransPacket>();
			PendingPacket = new BlockingCollection<TransPacket>();
			WaitAckPacket = new BlockingCollection<TransPacket>();
			DeviceGateway = new StorDictionary<long, long>("Net_TransRoute", "INTEGER", "INTEGER");
			// start processing packets
			ThreadPool.QueueUserWorkItem(new WaitCallback(SendHandler));
			ThreadPool.QueueUserWorkItem(new WaitCallback(ReceiveHandler));
		}

		void FetchHandler(object e)
		{
			try
			{
				while ( Host != null )
				{
					// get and process a packet
					NetPacket netPacket = Host.Receive(FetchCancel.Token);
					if ( netPacket == null ) break;
					TransPacket packet = new TransPacket();
					packet.Load(netPacket.Data.Array, netPacket.Data.Offset);
					// on processing error, send a nack packet
					if ( packet.HasError )
					{
						// build nack flags
						TransPacketFlags nackFlags = new TransPacketFlags();
						nackFlags.Address = TransPacketFlags.AddressFlag.Global;
						nackFlags.Checksum = TransPacketFlags.ChecksumFlag.None;
						nackFlags.RequestAck = TransPacketFlags.RequestAckFlag.None;
						nackFlags.AckReply = TransPacketFlags.AckReplyFlag.Nack;
						nackFlags.Sequencing = packet.Flags.Sequencing;
						nackFlags.Encryption = TransPacketFlags.EncryptionFlag.None;
						nackFlags.Purpose = TransPacketFlags.PurposeFlag.Normal;
						// build nack packet
						TransPacket nackPacket = new TransPacket();
						nackPacket.Id = packet.Id;
						nackPacket.Source = packet.Destination;
						nackPacket.Destination = packet.Source;
						if ( packet.Flags.SequencingUsed )
						{
							nackPacket.Offset = packet.Offset;
							nackPacket.AckSize = packet.Data.Length;
						}
						nackPacket.Save();
						ForwardPacket.Add(nackPacket);
						continue;
					}
					// if it is a forward packet, forward it
					if ( packet.Destination != ThisAddress )
					{
						ForwardPacket.Add(packet);
						continue;
					}
					// it is a packet for me
					if ( packet.Flags.AckReply != TransPacketFlags.AckReplyFlag.None )
					{
						// inform to forward handler
						// if it contains data, put it in receiveq
					}
					// need to eliminate duplication
					// data packet receive
					Received++;
					ReceivePacket.Add(packet);
					if ( packet.Flags.RequestAck == TransPacketFlags.RequestAckFlag.AckOnly ||
						packet.Flags.RequestAck == TransPacketFlags.RequestAckFlag.AckAndNack )
					{
						// build ack flags
						TransPacketFlags ackFlags = new TransPacketFlags();
						ackFlags.Address = TransPacketFlags.AddressFlag.Global;
						ackFlags.Checksum = TransPacketFlags.ChecksumFlag.None;
						ackFlags.RequestAck = TransPacketFlags.RequestAckFlag.None;
						ackFlags.AckReply = TransPacketFlags.AckReplyFlag.Ack;
						ackFlags.Sequencing = packet.Flags.Sequencing;
						ackFlags.Encryption = TransPacketFlags.EncryptionFlag.None;
						ackFlags.Purpose = TransPacketFlags.PurposeFlag.Normal;
						// build ack packet
						TransPacket ackPacket = new TransPacket();
						ackPacket.Id = packet.Id;
						ackPacket.Source = ThisAddress;
						ackPacket.Destination = packet.Source;
						if ( packet.Flags.SequencingUsed )
						{
							ackPacket.Offset = packet.Offset;
							ackPacket.AckSize = packet.Data.Length;
						}
						ackPacket.Save();
						ForwardPacket.Add(ackPacket);
					}
				}
			}
			catch ( Exception ) { }
		}
		void ForwardHandler(object e)
		{

		}

		void PendingHandler(object e)
		{
			// Send packets that failed to be sent in one go
			try
			{
				// host == null => end
				while ( Host != null )
				{
					// sleep for some time
					Thread.Sleep(PeriodicWaitTime);
					// process each pending packet
					int count = PendingPacket.Count;
					for ( int i = 0; i < count; i++ )
					{
						TransPacket packet = PendingPacket.Take();
						// try to send the packet
						try
						{
							SendSync(packet);
							// if acknowledge required, wait for ack
							if ( packet.Flags.RequestAck != TransPacketFlags.RequestAckFlag.None )
								WaitAckPacket.Add(packet);
						}
						// on failure, add it back to pending list (if not timedout)
						catch ( Exception )
						{
							DateTime timeNow = DateTime.UtcNow;
							if(timeNow - packet.LastModified < PacketDropTime)
								PendingPacket.Add(packet);
						}
					}
				}
			}
			catch ( Exception ) { }
		}

		void WaitAckHandler(object e)
		{

		}




		void SendHandler(object e)
		{
			try
			{
				// host == null => end
				while ( Host != null )
				{
					// prepare to send (cancellable)
					TransPacket packet = ForwardPacket.Take(SendCancel.Token);
					if ( packet == null ) break;
					packet.Source = ThisAddress;
					packet.Save();
					// try send
					try
					{
						SendSync(packet);
						if ( packet.Flags.RequestAck != TransPacketFlags.RequestAckFlag.None )
						{ }
						if ( EventFn.Send != null ) EventFn.Send(this, new NetEventArgs(NetEvent.Send, packet));
						Sent++;
					}
					catch ( Exception )
					{
						continue;
					}
				}
			}
			catch ( Exception ) { }
		}
		void ReceiveHandler(object e)
		{
			try
			{
				// Until host is null, continue (terminating condition)
				while ( Host != null )
				{
					// get a received packet
					NetPacket netPacket = Host.Receive();
					if ( netPacket == null ) break;		// terminating condition
					// read packet
					TransPacket transPacket = new TransPacket();
					try
					{ transPacket.Load(netPacket.Data.Array, netPacket.Data.Offset); }
					catch ( Exception ) { continue; }
					// check if the packet is for me
					if ( transPacket.Destination == ThisAddress )
					{
						ReceivePacket.Add(transPacket);
						Received++;
						continue;
					}
					// if error present, delete packet (no ack, nack)
					if ( transPacket.HasError ) continue;
					// if ok, update routing table
					if ( transPacket.Flags.Purpose == TransPacketFlags.PurposeFlag.Gateway )
					{
						EndpointGateway[netPacket.Address] = transPacket.Source;
						GatewayEndpoint[transPacket.Source] = netPacket.Address;
						DeviceGateway[transPacket.Source] = transPacket.Source;
					}
					else
					{
						long gateway = EndpointGateway[netPacket.Address];
						DeviceGateway[transPacket.Source] = gateway;
					}
					// add the packet to received list
					ReceivePacket.Add(transPacket);
				}
			}
			catch ( Exception ) { }
		}
		void PendingSendHandler(object e)
		{
			try
			{
				while ( Host != null )
				{
					Thread.Sleep(PeriodicWaitTime);
					int count = PendingPacket.Count;
					for ( int i = 0; i < count; i++ )
					{

					}
				}
			}
			catch ( Exception ) { }
		}


		void SendSync(TransPacket packet)
		{
			// Sends a saved packet
			long gateway = DeviceGateway[packet.Destination];
			IPEndPoint endPoint = GatewayEndpoint[gateway];
			NetClient client = Host[endPoint];
			client.Send(packet.Header);
			client.Send(packet.Data);
		}
		TransPacket ReceiveSync()
		{
			NetPacket netPacket = Host.Receive(ReceiveCancel.Token);
			TransPacket packet = new TransPacket();
			packet.Load(netPacket.Data.Array, netPacket.Data.Offset);
			if ( packet.HasError ) packet = null; // send back
			return null;
		}
		public void Send(TransPacket packet)
		{
			packet = (packet == null) ? new TransPacket() : packet;
			ForwardPacket.Add(packet);
		}
		public TransPacket Receive()
		{
			return ReceivePacket.Take();
		}

		public void Close()
		{
			Host = null;
			ForwardPacket.Add(null);
		}
	}
}
