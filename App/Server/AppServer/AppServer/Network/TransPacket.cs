﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Wasp.Network
{
	public struct TransPacketFlags
	{
		public enum AddressFlag
		{
			None,		// No address (used in lower layers)
			Local,		// 16-bit Local address
			Global,		// 64-bit global address
		}	// type of address, for both source and destination
		public enum ChecksumFlag
		{
			None,	// no checksum
			Xor16	// 16-bit xor checksum
		}	// type of checksum in use
		public enum RequestAckFlag
		{
			None,		// no priority
			AckOnly,	// low priority
			AckAndNack,	// medium priority
			QuickNack	// high priority
		}	// type of acknowledgement request
		public enum AckReplyFlag
		{
			None,
			Ack,
			Nack
		}	// type of acknowledgement reply flag
		public enum SequencingFlag
		{
			None,			// No sequencing
			SequenceMid,	// First or middle sequence packet 
			SequenceEnd		// Last sequence packet
		}	// Indicates if sequencing is in use, and its status
		public enum EncryptionFlag
		{
			None,		// No encryption
			AES128		// AES encryption used, password must be known to source
		}	// Indicates if encryption is in use, and its type
		public enum PurposeFlag
		{
			Normal,
			Gateway
		}   // Indicates the purpose of the packet
		public AddressFlag Address;
		public ChecksumFlag Checksum;
		public RequestAckFlag RequestAck;
		public AckReplyFlag AckReply;
		public SequencingFlag Sequencing;
		public EncryptionFlag Encryption;
		public PurposeFlag Purpose;
		public bool AddressUsed
		{
			get
			{ return Address != AddressFlag.None; }
		}
		public bool ChecksumUsed
		{
			get
			{ return Checksum != ChecksumFlag.None; }
		}
		public bool SequencingUsed
		{
			get
			{ return Sequencing != SequencingFlag.None; }
		}
		public bool AckSizeUsed
		{
			get
			{
				return SequencingUsed && (AckReply != AckReplyFlag.None);
			}
		}
		public ushort Value
		{
			get
			{
				// Get flags value
				ushort value = 0;
				value |= (ushort) Purpose;
				value <<= 4;
				value |= (ushort) Encryption;
				value <<= 2;
				value |= (ushort) Sequencing;
				value <<= 2;
				value |= (ushort) AckReply;
				value <<= 2;
				value |= (ushort) RequestAck;
				value <<= 2;
				value |= (ushort) Checksum;
				value <<= 2;
				value |= (ushort) Address;
				return value;
			}
			set
			{
				// Set flags from a source
				Address = (AddressFlag) (value & 3);
				value >>= 2;
				Checksum = (ChecksumFlag) (value & 3);
				value >>= 2;
				RequestAck = (RequestAckFlag) (value & 3);
				value >>= 2;
				AckReply = (AckReplyFlag) (value & 3);
				value >>= 2;
				Sequencing = (SequencingFlag) (value & 3);
				value >>= 2;
				Encryption = (EncryptionFlag) (value & 3);
				value >>= 2;
				Purpose = (PurposeFlag) (value & 0xF);
			}
		}

		public TransPacketFlags(AddressFlag address, ChecksumFlag checksum, RequestAckFlag requestAck,
			AckReplyFlag ackReply, SequencingFlag sequencing, EncryptionFlag encryption, PurposeFlag purpose)
		{
			Address = address;
			Checksum = checksum;
			RequestAck = requestAck;
			AckReply = ackReply;
			Sequencing = sequencing;
			Encryption = encryption;
			Purpose = purpose;
		}
	}

	public class TransPacket
	{
		public int Size;	// Size of the packet, inluding this field
		public TransPacketFlags Flags;	// Flags associated with this packet
		public int Id;		// Identifier of this packet (couple with source for uniqueness)
		public long Source;		// Source device address
		public long Destination;	// Destination device address
		public int Checksum;	// Checksum of the packet
		public int Offset;		// Offset of packet if it is sequential
		public int AckSize;		// Size acknowledging
		public DataBlock Header;	// Header raw bytes
		public DataBlock Data;		// Data contained in the packet
		public bool HasError
		{
			get
			{
				bool error = false;
				error |= Flags.Address != TransPacketFlags.AddressFlag.Global;
				error |= (int) Flags.Checksum >= 2;
				error |= (int) Flags.RequestAck == 3;
				error |= (int) Flags.Sequencing == 3;
				error |= (int) Flags.Encryption >= 2;
				error |= (int) Flags.Purpose >= 2;
				error |= (NetChecksum.Xor16(Header) ^ NetChecksum.Xor16(Data)) != 0;
				return error;
			}
		}		// Tells whether a packet has any error
		public DateTime LastModified;	// time at which this packet was last modified (used internally)
		public const int MinimumSize = 6;	// Minimum possible packet size

		public TransPacket()
		{
			LastModified = DateTime.UtcNow;
		}
		public TransPacket(TransPacketFlags flags, int id, long source, long destination, int offset, int ackSize, DataBlock data)
		{
			Flags = flags;
			Id = id;
			Source = source;
			Destination = destination;
			Offset = offset;
			AckSize = ackSize;
			Data = data;
			LastModified = DateTime.UtcNow;
		}

		public void Load(byte[] source, int offset)
		{
			// Set a transport layer packet from raw data
			// get mandatory fields
			Header = new DataBlock(source, offset);
			Size = BitConverter.ToUInt16(source, offset);
			offset += 2;
			if ( source.Length - offset < Size + 2 )
				throw new SocketException((int) SocketError.MessageSize);
			Flags = new TransPacketFlags();
			Flags.Value = BitConverter.ToUInt16(source, offset);
			offset += 2;
			Id = BitConverter.ToUInt16(source, offset);
			offset += 2;
			// Get Source and Destination
			switch ( Flags.Address )
			{
				case TransPacketFlags.AddressFlag.Local:
					Source = BitConverter.ToUInt16(source, offset);
					offset += 2;
					Destination = BitConverter.ToUInt16(source, offset);
					offset += 2;
					break;
				case TransPacketFlags.AddressFlag.Global:
					Source = BitConverter.ToInt64(source, offset);
					offset += 8;
					Destination = BitConverter.ToInt64(source, offset);
					offset += 8;
					break;
			}
			// Get Checksum (value)
			if ( Flags.ChecksumUsed )
			{
				Checksum = BitConverter.ToUInt16(source, offset);
				offset += 2;
			}
			// Get Offset (of fragment)
			if ( Flags.SequencingUsed )
			{
				Offset = BitConverter.ToUInt16(source, offset);
				offset += 2;
			}
			// Get Field (for now only SizeAck)
			if ( Flags.AckSizeUsed )
			{
				AckSize = BitConverter.ToUInt16(source, offset);
				offset += 2;
			}
			Header.Length = offset - Header.Offset;
			Data = new DataBlock(source, offset, Size - Header.Length);
			LastModified = DateTime.UtcNow;
		}
		public void Save()
		{
			// Get the raw byte array header to send
			// find required buffer size
			int buffSize = 6;
			switch ( Flags.Address )
			{
				case TransPacketFlags.AddressFlag.Local:
					buffSize += 4;
					break;
				case TransPacketFlags.AddressFlag.Global:
					buffSize += 16;
					break;
				default:
					throw new SocketException((int) SocketError.AddressNotAvailable);
			}
			if ( Flags.ChecksumUsed ) buffSize += 2;
			if ( Flags.SequencingUsed ) buffSize += 2;
			if ( Flags.AckSizeUsed ) buffSize += 2;
			// put header info into buffer
			byte[] buffer = new byte[buffSize];
			int offset = 0;
			Array.Copy(BitConverter.GetBytes(Size), 0, buffer, offset, 2);
			offset += 2;
			Array.Copy(BitConverter.GetBytes(Flags.Value), 0, buffer, offset, 2);
			offset += 2;
			Array.Copy(BitConverter.GetBytes(Id), 0, buffer, offset, 2);
			offset += 2;
			// put address
			switch ( Flags.Address )
			{
				case TransPacketFlags.AddressFlag.Local:
					Array.Copy(BitConverter.GetBytes(Source), 0, buffer, offset, 2);
					offset += 2;
					Array.Copy(BitConverter.GetBytes(Destination), 0, buffer, offset, 2);
					offset += 2;
					break;
				case TransPacketFlags.AddressFlag.Global:
					Array.Copy(BitConverter.GetBytes(Source), 0, buffer, offset, 8);
					offset += 8;
					Array.Copy(BitConverter.GetBytes(Destination), 0, buffer, offset, 8);
					offset += 8;
					break;
			}
			// put the rest fields
			int checksumOffset = offset;
			if ( Flags.ChecksumUsed )
			{
				Array.Copy(BitConverter.GetBytes(0), 0, buffer, offset, 2);
				offset += 2;
			}
			if ( Flags.SequencingUsed )
			{
				Array.Copy(BitConverter.GetBytes(Offset), 0, buffer, offset, 2);
				offset += 2;
			}
			if ( Flags.AckSizeUsed )
			{
				Array.Copy(BitConverter.GetBytes(AckSize), 0, buffer, offset, 2);
				offset += 2;
			}
			Header = new DataBlock(buffer);
			// update size
			Size = offset + Data.Length;
			Array.Copy(BitConverter.GetBytes(Size), 0, Header.Array, 0, 2);
			// update checksum if used
			if ( Flags.ChecksumUsed )
			{
				int checksum = 0;
				switch ( Flags.Checksum )
				{
					case TransPacketFlags.ChecksumFlag.Xor16:
						checksum = NetChecksum.Xor16(Header) ^ NetChecksum.Xor16(Data);
						break;
				}
				Array.Copy(BitConverter.GetBytes(checksum), 0, Header.Array, checksumOffset, 2);
			}
			LastModified = DateTime.UtcNow;
		}
		public void Close()
		{
			Size = 0;
			Flags = new TransPacketFlags();
			Id = 0;
			Source = 0;
			Destination = 0;
			Checksum = 0;
			Offset = 0;
			AckSize = 0;
			Header = new DataBlock();
			Data = new DataBlock();
		}
	}
}
