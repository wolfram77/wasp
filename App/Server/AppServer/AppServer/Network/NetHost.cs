﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Wasp.Network
{
	// Network Host (Server)
	public class NetHost
	{
		public Socket Link;		// Host Socket
		public bool PacketFormat;		// Tells if a packet format is in use
		public bool KeepDisconnected;	// If true, diconnected clients are still kept in client list
		public NetEventFnGroup EventFn;	// Event functions
		BlockingCollection<NetPacket> ReceivePacket;	// List of received packets
		public Dictionary<IPEndPoint, NetClient> Client;	// List of Clients
		public NetClient this[IPEndPoint client]
		{
			get
			{ return Client[client]; }
		}		// The ith client
		public IPEndPoint Address
		{
			get
			{ return (IPEndPoint) Link.LocalEndPoint; }
		}		// Endpoint (address) of the Host
		public static string Name
		{
			get
			{ return Dns.GetHostName(); }
		}		// Host name (computer name)
		public static IPAddress[] IpAddresses
		{
			get
			{ return Dns.GetHostAddresses(Name); }
		}	// Current Addresses (available)
		public static IPAddress Ipv4Address
		{
			get
			{ return (IPAddress) IpAddresses.First(ip => (ip.AddressFamily == AddressFamily.InterNetwork)); }
		}		// Default IPv4 Address (available)
		public static IPAddress Ipv6Address
		{
			get
			{ return (IPAddress) IpAddresses.First(ip => (ip.AddressFamily == AddressFamily.InterNetworkV6)); }
		}		// Default IPv6 Address (available)
		public static int PeriodicWaitTime = 1 * 1000;	// Time to wait before performing perodic checks
		const int DefWaitingConns = 64;		// Default max. number of waiting connections

		public NetHost(IPEndPoint address)
		{
			// Initialize
			PacketFormat = false;
			KeepDisconnected = false;
			EventFn = new NetEventFnGroup();
			Client = new Dictionary<IPEndPoint, NetClient>();
			ReceivePacket = new BlockingCollection<NetPacket>();
			// Create Host (server)
			Link = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			Link.Bind(address);
			Link.Listen(DefWaitingConns);
			// Start the handlers
			ThreadPool.QueueUserWorkItem(new WaitCallback(AcceptHandler));
			ThreadPool.QueueUserWorkItem(new WaitCallback(ClientHandler));
		}
		public NetHost(IPAddress address, int port)
			: this(new IPEndPoint(address, port))
		{
		}
		public NetHost(int port)
			: this(new IPEndPoint(Ipv4Address, port))
		{
		}

		void AcceptHandler(object e)
		{
			try
			{
				// As long as host socket is available, else terminate
				while ( Link != null )
				{
					// Accept connection from a client
					Socket clientLink = Link.Accept();
					// Add it to client list
					NetClient client = new NetClient(clientLink);
					client.EventFn = EventFn;
					client.Reuse = KeepDisconnected;
					client.PacketFormat = PacketFormat;
					client.ReceivePacket = ReceivePacket;
					Client[client.Address] = client;
				}
			}
			catch ( Exception ) { }
		}
		void ClientHandler(object e)
		{
			try
			{
				// If socket is available, then (else terminate)
				while ( Link != null )
				{
					// Wait for the periodic wait time
					Thread.Sleep(PeriodicWaitTime);
					foreach ( KeyValuePair<IPEndPoint, NetClient> clientKV in Client )
					{
						NetClient client = clientKV.Value;
						bool connected = client.FullyConnected;
						if ( !KeepDisconnected && !connected )
						{
							client.Disconnect();
							Client.Remove(clientKV.Key);
						}
					}
				}
			}
			catch ( Exception ) { }
		}

		public void Add(IPEndPoint address)
		{
			NetClient client = new NetClient(address);
			client.Connect();
			Add(client);
		}
		public void Add(NetClient client)
		{
			Client[client.Address] = client;
		}
		public bool Remove(IPEndPoint address)
		{
			return Client.Remove(address);
		}
		public bool Exists(IPEndPoint address)
		{
			return Client.ContainsKey(address);
		}
		public NetPacket Receive(CancellationToken cancelReceive)
		{
			return ReceivePacket.Take(cancelReceive);
		}
		public NetPacket Receive()
		{
			return ReceivePacket.Take();
		}

		public new string ToString()
		{
			if ( Address == null ) return "[]";
			return "[" + Address.Address + ":" + Address.Port + "]";
		}
		public void Close()
		{
			// end Accept Handler
			Link.Close();
			Link = null;
			// close all clients
			foreach ( KeyValuePair<IPEndPoint, NetClient> client in Client )
			{ client.Value.Close(); }
			// clean up
			Client.Clear();
			Client = null;
			ReceivePacket.Dispose();
			ReceivePacket = null;
		}
	}
}
