﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Wasp.Network
{
	// Network Client
	public class NetClient
	{
		byte[] Buffer;		// Packet Buffer [65536]
		public bool Reuse;	// Tells if client is to be reused after disconnect
		public Socket Link;		// Link to the Client
		int ReceiveZeroCount;	// No. of times nothing was received from client
		bool ReceiveSizeRead;	// Tells if Receive Packet Size was read
		public bool PacketFormat;	// Tells if a packet format is in use
		public bool Connected
		{
			get
			{ return (Link != null) && Link.Connected;}
		}		// Normal Connection Check
		public bool FullyConnected
		{
			get
			{
				if ( !Connected ) return false;
				try { Link.Send(new byte[0]); }
				catch ( Exception ) { }
				return Link.Connected;
			}
		}	// Manual Connection Check
		public DateTime LastUsed;	// Last time any operation was performed
		public IPEndPoint Address;	// Endpoint (address) of the client
		public int Sent, Received;	// Sent, Received bytes of data
		int ReceiveOff, ReceiveSize;	// Cursor offset in Receive buffer, Packet Size
		public NetEventFnGroup EventFn;	// Event Functions (normally filled up by NetHost)
		BlockingCollection<NetPacket> SendPacket;		// List of requests made (connect / diconnect / send)
		public BlockingCollection<NetPacket> ReceivePacket;	// List of packets received (normally filled up by NetHost)
		const int DefZeroReceiveLimit = 1;		// Default Zero data receive limit, after this limit is crossed, client is disconnected
		const int DefBufferSize = 65536;		// Default size of Packet Buffer
		
		public NetClient()
		{
			// Initialize
			Reuse = false;
			PacketFormat = false;
			LastUsed = DateTime.UtcNow;
			EventFn = new NetEventFnGroup();
			Buffer = new byte[DefBufferSize];
			Address = new IPEndPoint(IPAddress.None, 0);
			SendPacket = new BlockingCollection<NetPacket>();
		}
		public NetClient(IPEndPoint address)
			: this()
		{
			Address = address;
		}
		public NetClient(Socket link)
			: this((IPEndPoint) link.RemoteEndPoint)
		{
			Link = link;
			// Start the send and receive handlers
			ThreadPool.QueueUserWorkItem(new WaitCallback(SendHandler));
			ThreadPool.QueueUserWorkItem(new WaitCallback(ReceiveHandler));
		}
		
		void SendHandler(object e)
		{
			try
			{
				// Until Link is active (terminating condition)
				while ( Link != null )
				{
					// Take a request
					Exception error = null;
					NetPacket packet = SendPacket.Take();
					if ( packet == null ) break;	// terimination if null send
					if ( packet.Data.Array == null ) continue;
					// send the packet to client
					try
					{
						Link.Send(packet.Data.Array, packet.Data.Offset, packet.Data.Length, SocketFlags.None);
						Interlocked.Add(ref Sent, packet.Data.Length);
					}
					catch ( Exception err ) { error = err; }
					if ( EventFn.Send != null ) EventFn.Send(this, new NetEventArgs(NetEvent.Send, packet, error));
					// update Last used time, exit on not connected
					if ( error == null ) LastUsed = DateTime.UtcNow;
					else if ( !Connected ) break;	// terminate if not connected
				}
			}
			catch ( Exception ) { }
		}
		void ReceiveHandler(object e)
		{
			try
			{
				// If link is open, then (if not, terminate)
				while ( Link != null )
				{
					// Receive available data to buffer
					int avail = Link.Available;
					Link.Receive(Buffer, ReceiveOff, avail, SocketFlags.None);
					ReceiveOff += avail;
					// If zero data received too many times, disconnect and terminate
					if ( avail == 0 ) ReceiveZeroCount++;
					if ( ReceiveZeroCount > DefZeroReceiveLimit ) break;
					// Update last used time
					if ( avail > 0 ) LastUsed = DateTime.UtcNow;
					// Packet processing loop
					while ( true )
					{
						if ( PacketFormat )
						{
							// Update receive buffer, and read packet size if available
							if ( ReceiveOff >= 2 && !ReceiveSizeRead )
							{
								ReceiveSize = BitConverter.ToUInt16(Buffer, 0);
								if ( ReceiveSize < 2 ) ReceiveSize = 2;
								ReceiveSizeRead = true;
							}
							// Go out if a full packet is available
							if ( !ReceiveSizeRead || ReceiveOff < ReceiveSize ) break;
						}
						else ReceiveSize = ReceiveOff;
						// Obtain the packet from buffer
						byte[] data = new byte[ReceiveSize];
						Array.Copy(Buffer, data, ReceiveSize);
						NetPacket pkt = new NetPacket(new DataBlock(data), this);
						if ( ReceivePacket != null ) ReceivePacket.Add(pkt);
						// Update received bytes
						Interlocked.Add(ref Received, ReceiveSize);
						if ( EventFn.Receive != null ) EventFn.Receive(this, new NetEventArgs(NetEvent.Receive, pkt));
						// Clean buffer for next packet
						Array.Copy(Buffer, ReceiveSize, Buffer, 0, ReceiveOff - ReceiveSize);
						ReceiveOff -= ReceiveSize;
						ReceiveSizeRead = false;
						ReceiveSize = 0;
					}
				}
			}
			catch ( Exception ) { }
			Disconnect();
		}

		void ConnectSync(object e)
		{
			Exception error = null;
			// Close link, if still open
			if ( Link != null ) DisconnectSync(null);
			try
			{
				// Connect to client
				Link = new Socket(Address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
				Link.Connect(Address);
				// Check connection success, if not
				if ( !FullyConnected )
				{
					// Close the link
					Link.Close();
					Link = null;
					throw new SocketException((int) SocketError.ConnectionReset);
				}
				// On success, start the send and receive handlers
				ThreadPool.QueueUserWorkItem(new WaitCallback(SendHandler));
				ThreadPool.QueueUserWorkItem(new WaitCallback(ReceiveHandler));
			}
			catch ( Exception err ) { error = err; }
			if ( EventFn.Connect != null ) EventFn.Connect(this, new NetEventArgs(NetEvent.Connect, null, error));
		}
		void DisconnectSync(object e)
		{
			// Stop the Send Handler
			try { SendPacket.Add(null); }
			catch ( Exception ) { }
			// Disconnect client (stops Receive Handler)
			try
			{
				if ( Link.Connected )
				{
					Link.Shutdown(SocketShutdown.Both);
					Link.Disconnect(false);
				}
			}
			catch ( Exception ) { }
			// Close link
			try
			{ Link.Close(); Link = null; }
			catch ( Exception ) { }
			try { if ( EventFn.Disconnect != null ) EventFn.Disconnect(this, new NetEventArgs(NetEvent.Disconnect)); }
			catch ( Exception ) { }
			if ( !Reuse ) CleanUp();
		}
		public void Connect()
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(ConnectSync));
		}
		public void Disconnect()
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(DisconnectSync));
		}
		public void Send(NetPacket packet)
		{
			packet = (packet == null)? new NetPacket() : packet;
			SendPacket.Add(packet);
		}
		public void Send(DataBlock data)
		{
			Send(new NetPacket(data));
		}
		public void Send(byte[] data)
		{
			Send(new DataBlock(data));
		}
		public NetPacket Receive(CancellationToken cancelReceive)
		{
			return ReceivePacket.Take(cancelReceive);
		}
		public NetPacket Receive()
		{
			return ReceivePacket.Take();
		}

		public new string ToString()
		{
			if ( Address == null ) return "[]";
			return "[" + Address.Address + ":" + Address.Port + "]";
		}
		public void Close()
		{
			// Break Current Operation
			// Ask handlers to terminate
			Disconnect();
			// Clean Up
			CleanUp();
		}
		void CleanUp()
		{
			Buffer = null;
			Address = null;
			EventFn = null;
			Address = null;
			if ( SendPacket != null ) SendPacket.Dispose();
			SendPacket = null;
			ReceivePacket = null;
		}
	}
}
