﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Wasp.Network
{
	// Network Event / Request
	public enum NetEvent
	{
		Connect,		// New Client / Connect Success / Connect Failed
		Disconnect,		// Disconnect Success
		Receive,		// Receive Success
		Send,			// Send Success / Send Failed
	}

	// Network Event Arguments / Requests
	public class NetEventArgs : EventArgs
	{
		public NetEvent Event;		// Event / Request
		public object Data;			// Event / Request Data
		public Exception Error;		// null if Success

		public NetEventArgs()
			: base()
		{
		}
		public NetEventArgs(NetEvent eventType)
			: base()
		{
			Event = eventType;
		}
		public NetEventArgs(NetEvent eventType, object data)
			: base()
		{
			Event = eventType;
			Data = data;
		}
		public NetEventArgs(NetEvent eventType, object data, Exception error)
			: base()
		{
			Event = eventType;
			Data = data;
			Error = error;
		}
	}

	// Network Event Function form
	public delegate void NetEventFn(object sender, NetEventArgs eventArgs);

	// Network Event Function Group
	// Each Handler can separately be null
	public class NetEventFnGroup
	{
		public NetEventFn Connect, Disconnect;		// Connect Handler, Disconnect Handler
		public NetEventFn Receive, Send;			// Receive Handler, Send Handler

		// New Network Event Function Group
		public NetEventFnGroup() { }
	}

	public struct DataBlock
	{
		public byte[] Array;
		public int Offset;
		public int Length;

		public DataBlock(byte[] source)
		{
			Array = source;
			Offset = 0;
			Length = source.Length;
		}
		public DataBlock(byte[] source, int offset)
		{
			Array = source;
			Offset = offset;
			Length = source.Length - offset;
		}
		public DataBlock(byte[] source, int offset, int length)
		{
			Array = source;
			Offset = offset;
			Length = length;
		}
	}

	// Network Packet (from a NetClient)
	// [LE: WORD: DataSize] [BYTE*DataSize: Data]
	// LE = Little Endian
	public class NetPacket
	{
		public DataBlock Data;		// Packet Data (including DataSize)
		public NetClient Client;	// Client which received the data (or to be sent to this Client)
		public IPEndPoint Address;	// IP endpoint of the client
		
		public NetPacket() { }
		public NetPacket(DataBlock data)
		{
			Data = data;
			Client = null;
			Address = null;
		}
		public NetPacket(DataBlock data, NetClient client)
		{
			Data = data;
			Client = client;
			Address = client.Address;
		}
		public NetPacket(DataBlock data, IPEndPoint address)
		{
			Data = data;
			Client = null;
			Address = address;
		}
	}

	// Network Checksum
	public class NetChecksum
	{
		public static ushort Xor16(DataBlock data)
		{
			// Find 16-bit XORed checksum
			ushort chksum = 0;
			int limit = data.Length & (~1);
			for ( int i = 0; i < limit; i += 2 )
				chksum ^= BitConverter.ToUInt16(data.Array, data.Offset + i);
			if ( limit < data.Length ) chksum ^= data.Array[data.Offset + limit];
			return chksum;
		}
	}
}
