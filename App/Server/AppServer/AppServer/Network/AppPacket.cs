﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wasp.Network
{
	public enum AppLayerSectionFlag
	{
		Encrypted = 0x80,
		SuppressReturn = 0x20,
		SuppressError = 0x10,
		Transactional = 0x08,
		SkipSection = 0x01
	}

	public class AppLayerSection
	{
		public enum FunctionReturnFlag
		{
			None,
			ReturnOnly,
			ReturnWithType,
			ReturnWithFullInfo
		}
		public enum ErrorReportingFlag
		{
			None,
			ErrorCodeOnly,
			ErrorDetail,
			ErrorWithFullInfo
		}
		public enum SectionSkipFlag
		{
			None,
			SkipByAmount,
			SkipToSpecific
		}
		public enum TransactionFlag
		{
			None,
			WithoutReport,
			WithDetail,
			WithFullInfo
		}
		public enum EncryptionFlag
		{
			None,
			AES128
		}
		public ushort Number;	// Section Number (manual)
		public ushort Flags;	// Section Flags
		public ushort Size;		// Section Size / Sections to Skip
		public byte[] Data;		// Section Data (if not skip section)
		public int DataOff;
		public bool IsEncrypted
		{
			get
			{ return GetFlag(AppLayerSectionFlag.Encrypted); }
			set
			{ SetFlag(AppLayerSectionFlag.Encrypted, value); }
		}
		public bool IsReturnSuppressed
		{
			get
			{ return GetFlag(AppLayerSectionFlag.SuppressReturn); }
			set
			{ SetFlag(AppLayerSectionFlag.SuppressReturn, value); }
		}
		public bool IsErrorSuppressed
		{
			get
			{ return GetFlag(AppLayerSectionFlag.SuppressError); }
			set
			{ SetFlag(AppLayerSectionFlag.SuppressError, value); }
		}
		public bool IsTransactional
		{
			get
			{ return GetFlag(AppLayerSectionFlag.Transactional); }
			set
			{ SetFlag(AppLayerSectionFlag.Transactional, value); }
		}
		public bool IsSkipSection
		{
			get
			{ return GetFlag(AppLayerSectionFlag.SkipSection); }
			set
			{ SetFlag(AppLayerSectionFlag.SkipSection, value); }
		}
		public int SectionsToSkip
		{
			get
			{ return (IsSkipSection) ? Size : 0; }
		}
		public bool IsTerminal
		{
			get
			{ return IsSkipSection && (Size == 0); }
		}

		public AppLayerSection()
		{
		}
		public AppLayerSection(byte[] rawdata, ref int start)
		{
			Flags = rawdata[start];
			Size = BitConverter.ToUInt16(rawdata, start + 1);
			start += 3;
			if ( !IsSkipSection )
			{
				Data = new byte[Size];
				Array.Copy(rawdata, start + 3, Data, 0, Size);
				start += Size;
			}
		}
		public bool GetFlag(AppLayerSectionFlag flag)
		{
			return (Flags & (byte) flag) > 0;
		}
		public void SetFlag(AppLayerSectionFlag flag, bool value)
		{
			if ( value ) Flags |= (byte) flag;
			else Flags &= (byte) ~((byte) flag);
		}
	}

	public class AppLayerPacket
	{
		public Guid Source;						// Source Address
		public Guid Destination;				// Destination Address
		public List<AppLayerSection> Sections;	// List of Sections

		public AppLayerPacket()
		{
		}
		public AppLayerPacket(Guid source, Guid destination, byte[] rawdata, ref int start)
		{
			int section_no = 0;
			AppLayerSection section;
			Sections = new List<AppLayerSection>();
			Source = source;
			Destination = destination;
			while ( true )
			{
				section = new AppLayerSection(rawdata, ref start);
				if ( section.IsTerminal ) break;
				else if ( section.IsSkipSection ) section_no += section.Size;
				else
				{
					section.Number = (ushort) section_no;
					Sections.Add(section);
					section_no++;
				}
			}
		}
	}
}
