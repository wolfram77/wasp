﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace Wasp.Storage
{
	public class StorEntity
	{
		public long Id;		// the lower 64-bit id assigned to entity
		public Guid Key;	// encryption key required to encode / decode packet
		public long Creator;	// one who created this entity
		public DateTime LastActive;	// Last time the entity communicated / accessed the server
		public StorDictionary<byte, byte[]> Variable;	// List of variable belonging to this entity (not a column)
		public static StorBag<long> OldId;	// list where the old entity ids are stoed
		public static long NextId;		// Id of the next entity to be created
		public static long Prefix;		// GUID Prefix of all entities created on this server
		public static bool OldIdAvail;	// Tells if any old id is available for use as id for a new entity
		static long NewRandomId
		{
			get
			{
				Random numRnd = new Random();
				double rndDbl = numRnd.NextDouble();
				return BitConverter.DoubleToInt64Bits(rndDbl);
			}
		}	// generates a new random id (for prefix)
		static long NewEntityId
		{
			get
			{
				if ( OldIdAvail )
				{
					try
					{ return OldId.Value; }
					catch ( Exception ) { OldIdAvail = false; }
				}
				return NextId++;
			}
		}	// gives the next entity id considering the old available entity ids
		const string ErrorNoEntity = "No such entity exists.";
		const long DefIdStart = 16;	// The very first id value of entities

		public static void GetSettings()
		{
			// Get static properties from the database
			OldIdAvail = true;
			OldId = new StorBag<long>("Entity_Old", "INTEGER");
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				SQLiteTransaction txn = link.BeginTransaction();
				string cmdString = "CREATE TABLE IF NOT EXISTS Entity_Info (Prefix INTEGER, LastId INTEGER);";
				SQLiteCommand cmd = new SQLiteCommand(cmdString, link, txn);
				cmd.ExecuteNonQuery();
				cmd.CommandText = "CREATE TABLE IF NOT EXISTS Entity (Id INTEGER, Key GUID, Creator INTEGER, LastActive INTEGER);";
				cmd.ExecuteNonQuery();
				cmd.CommandText = "SELECT * FROM Entity_Info;";
				using ( SQLiteDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow) )
				{
					if ( reader == null )
					{
						NextId = DefIdStart; Prefix = NewRandomId;
						cmd.CommandText = "INSERT INTO Entity_Info VALUES (@NextId, @Prefix);";
						cmd.Parameters.AddWithValue("@NextId", NextId);
						cmd.Parameters.AddWithValue("@Prefix", Prefix);
						cmd.ExecuteNonQuery();
					}
					else
					{
						reader.Read();
						NextId = reader.GetInt64(0);
						Prefix = reader.GetInt64(1);
					}
				}
				txn.Commit();
			}
		}
		public static void FlushSettings()
		{
			// Flush static properties to the database
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("UPDATE Entity_Info SET NextId = @NextId, Prefix = @Prefix;", link);
				cmd.Parameters.AddWithValue("@NextId", NextId);
				cmd.Parameters.AddWithValue("@Prefix", Prefix);
				cmd.ExecuteNonQuery();
			}
		}

		public void Load(long id)
		{
			// Get entity from the database
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM Entity WHERE Id = @Id;", link);
				cmd.Parameters.AddWithValue("@Id", id);
				using ( SQLiteDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow) )
				{
					if ( reader == null ) throw new FieldAccessException(ErrorNoEntity);
					reader.Read();
					Id = reader.GetInt64(0);
					Key = reader.GetGuid(1);
					Creator = reader.GetInt64(2);
					LastActive = reader.GetDateTime(3);
				}
			}
		}


		public static StorEntity Fetch(long id)
		{
			StorEntity entity = new StorEntity();
			entity.Set(id);
			return entity;
		}
		public static void Delete(ulong entity, ulong creator)
		{
			// Delete an entity (only possible by creator)
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("SELECT Creator FROM Entity WHERE Id = @Id;", link);
				cmd.Parameters.AddWithValue("@Id", entity);
				object creatorObj = cmd.ExecuteScalar();
				if ( creatorObj == null ) throw new FieldAccessException(ErrorNoEntity);
                if ((ulong)creatorObj != creator) throw new FieldAccessException("Entity could not be deleted.");
				cmd = new SQLiteCommand("DELETE FROM Entity WHERE Id = @Id;", link);
				cmd.Parameters.AddWithValue("@Id", entity);
				cmd.ExecuteNonQuery();
				cmd = new SQLiteCommand("INSERT INTO EntityOld VALUES (@Id);", link);
				cmd.Parameters.AddWithValue("@Id", entity);
				cmd.ExecuteNonQuery();
				OldIdAvail = true;
			}
		}

		StorEntity() { }
		public StorEntity(long creator)
		{
			// Create a entity, which must be manually saved / created
			Id = NewEntityId;
			Key = Guid.NewGuid();
			Creator = creator;
			LastActive = DateTime.UtcNow;
		}

		public void Set(long entity)
		{
		}
		public void Flush()
		{
			// Flush (changes) to the database
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("UPDATE Entity SET Key = @Key, Creator = @Creator, " +
					"LastAccessed = @LastAccessed WHERE Id = @Id;", link);
				cmd.Parameters.AddWithValue("@Id", Id);
				cmd.Parameters.AddWithValue("@Key", Key);
				cmd.Parameters.AddWithValue("@Creator", Creator);
				cmd.Parameters.AddWithValue("@LastAccessed", LastActive);
				int rowsAffected = cmd.ExecuteNonQuery();
				if ( rowsAffected != 1 )
				{
					cmd = new SQLiteCommand("INSERT INTO Entity VALUES (@Id, @Key, @Creator, @LastAccessed);", link);
					cmd.Parameters.AddWithValue("@Id", Id);
					cmd.Parameters.AddWithValue("@Key", Key);
					cmd.Parameters.AddWithValue("@Creator", Creator);
					cmd.Parameters.AddWithValue("@LastAccessed", LastActive);
					cmd.ExecuteNonQuery();
				}
			}
		}
		public void RefreshKey()
		{
			// Obtain a new encryption key
			Key = Guid.NewGuid();
		}
		public void WasAccessed()
		{
			// Remember that an object was recently accessed
			LastActive = DateTime.UtcNow;
		}

		public void Dispose()
		{
			Variable.Close();
		}
		public void Close()
		{
			Dispose();
		}
	}
}
