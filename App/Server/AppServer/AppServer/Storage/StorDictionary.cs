﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace Wasp.Storage
{
	public class StorDictionary<TKey, TValue>
	{
		Dictionary<TKey, TValue> Cache;	// dictionary cache (data == null => load from database)
		Dictionary<TKey, TValue> Write;	// write pending data
		public TValue this[TKey key]
		{
			get
			{
				// Get value, and if not available, fetch it from database
				TValue value;
				if ( Cache.TryGetValue(key, out value) ) return value;
				using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
				{
					link.Open();
					string cmdString = StorMain.BuildString("SELECT Value FROM {0} WHERE Key = @Key;", Table);
					SQLiteCommand cmd = new SQLiteCommand(cmdString, link);
					cmd.Parameters.AddWithValue("@Key", key);
					object valueObj = cmd.ExecuteScalar(CommandBehavior.SingleRow);
					if ( valueObj == null ) throw new SQLiteException(SQLiteErrorCode.NotFound, ErrorKeyUnavail);
					value = (TValue) valueObj; Cache[key] = value;
					return value;
				}
			}
			set
			{
				// Set value of a key
				Cache[key] = value;
				Write[key] = value;
			}
		}	// get value
		string Table, KeyType, ValueType;	// table name, key sql datatype, value sql datatype
		const string ErrorKeyUnavail = "Key unavailable.";

		public StorDictionary(string table, string keyType, string valueType)
		{
			// Initialize a new cached table (no create)
			Table = table;
			KeyType = keyType;
			ValueType = valueType;
			Cache = new Dictionary<TKey, TValue>();
			Write = new Dictionary<TKey, TValue>();
		}
		public StorDictionary(string table, string keyType)
			: this(table, keyType, "BLOB")
		{
		}
		public StorDictionary(string table)
			: this(table, "INTEGER", "BLOB")
		{
		}

		public void Clear()
		{
			// Clears all dictionary data
			Cache.Clear();
			Write.Clear();
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				string cmdString = StorMain.BuildString("DROP TABLE {0};", Table);
				SQLiteCommand cmd = new SQLiteCommand(cmdString, link);
				cmd.ExecuteNonQuery();
			}
		}
		public void Flush()
		{
			// Flush data to the database, only if required
			if ( Write.Count == 0 ) return;
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteTransaction txn = link.BeginTransaction();
				string cmdString = StorMain.BuildString("CREATE TABLE IF NOT EXISTS {0} (Key {1} PRIMARY KEY ASC NOT NULL, Value {2} NOT NULL);",
					Table, KeyType, ValueType);
				SQLiteCommand cmd = new SQLiteCommand(cmdString, link, txn);
				cmd.ExecuteNonQuery();
				cmdString = StorMain.BuildString("INSERT OR REPLACE INTO {0} VALUES (@Key, @Value);", Table);
				foreach ( KeyValuePair<TKey, TValue> item in Write )
				{
					cmd.CommandText = cmdString;
					cmd.Parameters.AddWithValue("@Key", item.Key);
					cmd.Parameters.AddWithValue("@Value", item.Value);
					cmd.ExecuteNonQuery();
				}
				txn.Commit();
			}
			Write.Clear();
		}
		public void ForceReload()
		{
			// Force dictionary to reload each data from database
			Flush();
			Cache.Clear();
		}
		public bool Remove(TKey key)
		{
			// Removes value of a particular key
			bool removed = Write.Remove(key);
			removed |= Cache.Remove(key);
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				string cmdString = StorMain.BuildString("DELETE FROM {0} WHERE Key = @Key;", Table);
				SQLiteCommand cmd = new SQLiteCommand(cmdString, link);
				cmd.Parameters.AddWithValue("@Key", key);
				int rowsAffected = cmd.ExecuteNonQuery();
				removed |= (rowsAffected == 1);
			}
			return removed;
		}
		public bool Exists(TKey key)
		{
			try
			{ TValue value = this[key]; return true; }
			catch ( Exception ) { }
			return false;
		}

		public void Close()
		{
			Flush();
			Write.Clear();
			Write = null;
			Cache.Clear();
			Cache = null;
		}
	}
}
