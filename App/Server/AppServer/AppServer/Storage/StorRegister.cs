﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace Wasp.Storage
{
	// Cached Registers (for an entity)
	public class StorRegister
	{
		ulong Entity;	// the entity to which these variables belong
		string RegTable;	// table name of the entity's variable list
		Dictionary<byte, byte[]> Registers;	// dictionary cache for variables (data == null => load from database)
		const string RegPrefix = "Reg_";	// Prefix used for database table name of variables
		const int RegMaxDataLength = 1024;	// Max allowed length of variable data
		const string ErrorRegEmpty = "Register is empty.";
		public byte[] this[byte id]
		{
			get
			{ return Get(id); }
			set
			{ Set(id, value); }
		}

		public StorRegister(ulong entity)
		{
			// Initialize a new variable list
			// This does not create a new table or fetch data from it
			Entity = entity;
			Registers = new Dictionary<byte, byte[]>();
			RegTable = RegPrefix + Entity.ToString("x8");
		}

		public void Clear()
		{
			// Clears all the registers
			Registers = new Dictionary<byte, byte[]>();
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("DROP TABLE " + RegTable + ";", link);
				cmd.ExecuteNonQuery();
			}
		}
		public void Flush()
		{
			// Flush data to the database, only if required
			if ( Registers.Count == 0 ) return;
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("CREATE TABLE IF NOT EXISTS " + RegTable + " (Id INTEGER, Value BLOB);", link);
				cmd.ExecuteNonQuery();
				foreach ( KeyValuePair<byte, byte[]> register in Registers )
				{
					cmd = new SQLiteCommand("UPDATE " + RegTable + " SET Value = @Value WHERE Id = @Id;", link);
					cmd.Parameters.AddWithValue("@Id", register.Key);
					cmd.Parameters.AddWithValue("@Value", register.Value);
					int rowsAffected = cmd.ExecuteNonQuery();
					if ( rowsAffected != 1 )
					{
						cmd = new SQLiteCommand("INSERT INTO " + RegTable + " VALUES (@Id, @Value);", link);
						cmd.Parameters.AddWithValue("@Id", register.Key);
						cmd.Parameters.AddWithValue("@Value", register.Value);
						cmd.ExecuteNonQuery();
					}
				}
			}
		}
		public void ForceReload()
		{
			// Force registers to reload each data from database
			Flush();
			Registers.Clear();
			Registers = new Dictionary<byte, byte[]>();
		}
		public byte[] Get(byte id)
		{
			// Get a register value, and if not available, fetch it from database
			byte[] value;
			if ( Registers.TryGetValue(id, out value) ) return value;
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("SELECT Value FROM " + RegTable + " WHERE Id = @Id;", link);
				cmd.Parameters.AddWithValue("@Id", id);
				object dataObj = cmd.ExecuteScalar(CommandBehavior.SingleRow);
				if ( dataObj == null ) throw new FieldAccessException(ErrorRegEmpty);
				value = (byte[]) dataObj; Registers[id] = value;
				return value;
			}
		}
		public void Remove(byte id)
		{
			// Removes / empties a particular register from list
			Registers.Remove(id);
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("DELETE FROM " + RegTable + " WHERE Id = @Id;", link);
				cmd.Parameters.AddWithValue("@Id", id);
				int rowsAffected = cmd.ExecuteNonQuery();
				if ( rowsAffected != 1 ) throw new FieldAccessException(ErrorRegEmpty);
			}
		}
		public void Set(byte id, byte[] value)
		{
			// Set a variable value, only if data is below size limits
            if (value.Length > RegMaxDataLength) throw new OverflowException("Register data overflow.");
			Get(id); Registers[id] = value;
		}

		public void Close()
		{
			Flush();
			Entity = 0;
			Registers.Clear();
			Registers = null;
			RegTable = null;
		}
	}
}
