﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace Wasp.Storage
{
	// Cached unordered list for database
	public class StorBag<T>
	{
		List<T> Cache;	// cache bag (empty => load from database)
		List<T> Write;	// write pending data
		string Table, ValueType;	// table name, value sql datatype
		const string ErrorValUnavail = "Value unavailable.";
		const int DefValuesLoad = 64;	// default no. of values to load from database, when empty
		public bool IsEmpty
		{
			get
			{
				try
				{
					T value = Value;
					Value = value;
					return false;
				}
				catch ( Exception ) { }
				return true;
			}
		}
		public T Value
		{
			get
			{
				// Take value, and if not available, fetch it from database
				if ( Cache.Count > 0 )
				{
					T value = Cache[0];
					Cache.RemoveAt(0);
					Write.Remove(value);
					return value;
				}
				using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
				{
					link.Open();
					string cmdString = StorMain.BuildString("SELECT Value FROM {0} LIMIT {1};", Table, DefValuesLoad);
					SQLiteCommand cmd = new SQLiteCommand(cmdString, link);
					using ( SQLiteDataReader valueRdr = cmd.ExecuteReader() )
					{
						if ( valueRdr == null ) throw new SQLiteException(SQLiteErrorCode.NotFound, ErrorValUnavail);
						while ( valueRdr.Read() )
							Cache.Add((T) valueRdr[0]);
					}
					T value = Cache[0];
					Cache.Remove(value);
					return value;
				}
			}
			set
			{
				// Add a value
				Cache.Add(value);
				Write.Add(value);
			}
		}
		
		public StorBag(string table, string valueType)
		{
			// Initialize new list (no create)
			Table = table;
			ValueType = valueType;
			Cache = new List<T>();
			Write = new List<T>();
		}
		public StorBag(string table)
			: this(table, "BLOB")
		{
		}

		public void Clear()
		{
			// Clears all list data
			Cache.Clear();
			Write.Clear();
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				string cmdString = StorMain.BuildString("DROP TABLE {0};", Table);
				SQLiteCommand cmd = new SQLiteCommand(cmdString, link);
				cmd.ExecuteNonQuery();
			}
		}
		public void Flush()
		{
			// Flush data to the database, only if required
			if ( Write.Count == 0 ) return;
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteTransaction txn = link.BeginTransaction();
				string cmdString = StorMain.BuildString("CREATE TABLE IF NOT EXISTS {0} (Value {1} NOT NULL);", Table, ValueType);
				SQLiteCommand cmd = new SQLiteCommand(cmdString, link, txn);
				cmd.ExecuteNonQuery();
				cmdString = StorMain.BuildString("INSERT INTO {0} VALUES (@Value);", Table);
				foreach ( T value in Write )
				{
					cmd.CommandText = cmdString;
					cmd.Parameters.AddWithValue("@Value", value);
					cmd.ExecuteNonQuery();
				}
				txn.Commit();
			}
			Write.Clear();
		}
		public void ForceReload()
		{
			// Force list to reload each data from database
			Flush();
			Cache.Clear();
		}

		public void Close()
		{
			Flush();
			Write.Clear();
			Write = null;
			Cache.Clear();
			Cache = null;
		}
	}
}
