﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Wasp.Storage
{
	public class StorBuffer : StorResource
	{
		public FileStream Handle;
		public long Size
		{
			get
			{ WasUsed(); return Handle.Length; }
			set
			{
				if ( value > MaxBufferLength ) throw new OverflowException("Buffer Overflow.");
				Handle.SetLength(value);
				WasUsed();
			}
		}
		const long MaxBufferLength = 1024 * 1024;
		const string DefPath = @"Data\Buffer\";

		public static new StorBuffer Get(ulong resource)
		{
			StorResource resourceObj = StorResource.Get(resource);
			return Get(resourceObj);
		}
		public static StorBuffer Get(StorResource resource)
		{
			if ( resource.ResourceType != StorResourceType.StorBuffer ) throw new InvalidCastException("Could not cast to a Buffer.");
			StorBuffer buffer = new StorBuffer();
			buffer.Copy(resource);
			return buffer;
		}
		public static new void Delete(ulong resource, ulong creator)
		{
			StorResource.Delete(resource, creator);
			string fileName = ResourcePrefix + resource.ToString("x8");
			if ( File.Exists(fileName) ) File.Delete(fileName);
		}

		public StorBuffer() { }
		public StorBuffer(ulong owner)
			: base(owner, StorResourceType.StorBuffer)
		{
		}

		void Open()
		{
			if ( Handle != null )
				Handle = new FileStream(DefPath + ResourceName, FileMode.OpenOrCreate,
					FileAccess.ReadWrite, FileShare.ReadWrite | FileShare.Delete); 
		}
		public override void Flush()
		{
			base.Flush();
			if ( Handle != null ) Handle.Flush();
		}
		public int Read(long address, byte[] data, int offset, int count)
		{
			if ( address + count > Size ) throw new FieldAccessException("Buffer underflow.");
			Open(); Handle.Seek(address, SeekOrigin.Begin);
			int bytesRead = Handle.Read(data, offset, count);
			WasUsed();
			return bytesRead;
		}
		public void Write(long address, byte[] data, int offset, int count)
		{
			Open(); Handle.Seek(address, SeekOrigin.Begin);
			if ( Size > MaxBufferLength )
			{
				Size = MaxBufferLength;
				throw new OverflowException("Buffer overflow.");
			}
			Handle.Write(data, offset, count);
			WasUsed();
		}

		public override void Dispose()
		{
			if ( Handle != null )
			{
				Flush();
				Handle.Close();
				Handle = null;
			}
		}
	}
}
