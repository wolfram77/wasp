﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace Wasp.Storage
{
	public class StorTable : StorResource
	{
		public static new StorTable Get(ulong resource)
		{
			StorResource resourceObj = StorResource.Get(resource);
			return Get(resourceObj);
		}
		public static StorTable Get(StorResource resource)
		{
			if ( resource.ResourceType != StorResourceType.StorBuffer ) throw new InvalidCastException("Could not cast to a Table.");
			StorTable table = new StorTable();
			table.Copy(resource);
			return table;
		}
		public static new void Delete(ulong resource, ulong creator)
		{
			StorResource.Delete(resource, creator);
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				string resourceName = ResourcePrefix + resource.ToString("x8");
				SQLiteCommand cmd = new SQLiteCommand("DROP TABLE " + resourceName, link);
				cmd.ExecuteNonQuery();
			}
		}

		public StorTable() { }
		public StorTable(ulong owner)
			: base(owner, StorResourceType.StorTable)
		{
		}

		void Create(string[] fields)
		{
			// Create a new resource table
			// build command string for creating table
			StringBuilder cmdString = new StringBuilder("CREATE TABLE IF NOT EXISTS " + ResourceName + " (");
			foreach ( string field in fields )
				cmdString.AppendFormat("{0}, ", field);
			cmdString.Remove(cmdString.Length - 2, 2);
			cmdString.Append(");");
			// create the resource table
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand(cmdString.ToString(), link);
				cmd.ExecuteNonQuery();
			}
		}
		public void Set(object[] values)
		{
			// Add a new row to the table
			// build command string for updating data
			StringBuilder cmdString = new StringBuilder("UPDATE INTO " + ResourceName + " VALUES (");
			for ( int i = 0; i < values.Length; i++ )
				cmdString.AppendFormat("@P{0}, ", i);
			cmdString.Remove(cmdString.Length - 2, 2);
			cmdString.Append(");");
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand(cmdString.ToString(), link);
				for ( int i = 0; i < values.Length; i++ )
					cmd.Parameters.AddWithValue("@P" + i, values[i]);
				int rowsAffected = cmd.ExecuteNonQuery();
				if ( rowsAffected != 1 )
				{
					// build command string for updating data
				}
			}
		}
	}
}
