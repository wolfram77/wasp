﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SQLite;

namespace Wasp.Storage
{
	public enum StorError
	{
	}

	public class StorException : Exception
	{
		public StorError Code;

		public StorException(StorError code)
			: base()
		{
			Code = code;
		}
		public StorException(StorError code, string message)
			: base(message)
		{
			Code = code;
		}
		public StorException(StorError code, string message, Exception innerException)
			: base(message, innerException)
		{
		}
	}

	public class StorMain
	{
		public const string DatabaseFile = "Wasp.db";
		public const string DatabasePath = @"Data\";
		public const string Database = DatabasePath + DatabaseFile;
		public const string ConnString = @"Data Source=" + Database + ";Version=3;";

		public static void Init()
		{
			// Create database, if not yet created
			if ( !File.Exists(Database) )
			{
				Directory.CreateDirectory(DatabasePath);
				SQLiteConnection.CreateFile(Database);
			}
			// WaspResource.Init();
		}
		public static string BuildString(string format, params object[] args)
		{
			return new StringBuilder().AppendFormat(format, args).ToString();
		}
	}
}
