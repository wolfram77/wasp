﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace Wasp.Storage
{
	public enum StorResourceType
	{
		StorBuffer,
		StorTable
	}

	public class StorResource
	{
		public ulong Id;		// Lower 64-bit id of the resource
		public ulong Owner;		// Entity who created this resource (and thus owns it)
        public ulong Locker;	// One who Locked the resource (requires R/RW)
        public DateTime LastUsed;		// Last time the resource was used
        public DateTime LastLocked;	    // Last time the resource was locked
		public Guid KeyR, KeyW, KeyRW;	// Read / write / read-write keys for using resource
		public static ulong NextId;		// Id of the next resource to be created
        public static ulong Prefix;		// GUID Prefix of all resources created on this server
        public static bool OldIdAvail;	// Tells if any old id is available for use as id for a new entity
        public string ResourceName
        {
            get
            { return ResourcePrefix + Id.ToString("x8"); }
        }   // name of the resource
		public StorResourceType ResourceType;	// Type of the resource
		protected const string ErrorNoResource = "No such resource exists.";
		protected static ulong NewRandomId
		{
			get
			{ return (ulong) BitConverter.DoubleToInt64Bits(new Random().NextDouble()); }
		}	// generates a new random id (for prefix)
		protected static ulong NewResourceId
		{
			get
			{
				if ( OldIdAvail )
				{
					using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
					{
						link.Open();
						SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM ResourceOld LIMIT 1;", link);
						object oldIdObj = cmd.ExecuteScalar(CommandBehavior.SingleRow);
						if ( oldIdObj == null ) { OldIdAvail = false; return NextId++; }
						cmd = new SQLiteCommand("DELETE FROM ResourceOld WHERE Id = @Id;", link);
						cmd.Parameters.AddWithValue("@Id", oldIdObj);
						cmd.ExecuteNonQuery();
						return (ulong) oldIdObj;
					}
				}
				return NextId++;
			}
		}	// gives the next entity id considering the old available entity ids
        protected const string ResourcePrefix = "Res_";	// Prefix used for resource names
		protected const ulong DefIdStart = 16;	// The very first id value of resources
        public bool IsLocked
        {
            get
            { return Locker != 0; }
        }

        public static void GetSettings()
        {
            // Get static properties from the database
            using (SQLiteConnection link = new SQLiteConnection(StorMain.ConnString))
            {
                SQLiteCommand cmd = new SQLiteCommand("CREATE TABLE IF NOT EXISTS ResourceOld (Id INTEGER);", link);
                cmd.ExecuteNonQuery();
                cmd = new SQLiteCommand("CREATE TABLE IF NOT EXISTS ResourceInfo (Prefix INTEGER, LastId INTEGER);", link);
                cmd.ExecuteNonQuery();
                cmd = new SQLiteCommand("CREATE TABLE IF NOT EXISTS Resource (Id INTEGER, Owner INTEGER, Locker INTEGER, " +
                    "LastUsed INTEGER, LastLocked INTEGER, Type INTEGER, KeyR GUID, KeyW GUID, KeyRW GUID);", link);
                cmd.ExecuteNonQuery();
                cmd = new SQLiteCommand("SELECT * FROM ResourceInfo;", link);
                using (SQLiteDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
                {
                    if (reader == null)
                    {
                        NextId = DefIdStart; Prefix = NewRandomId;
                        cmd = new SQLiteCommand("INSERT INTO ResourceInfo VALUES (@NextId, @Prefix);", link);
                        cmd.Parameters.AddWithValue("@NextId", NextId);
                        cmd.Parameters.AddWithValue("@Prefix", Prefix);
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        NextId = (ulong)reader.GetInt64(0);
                        Prefix = (ulong)reader.GetInt64(1);
                    }
                }
                cmd = new SQLiteCommand("SELECT * FROM ResourceOld LIMIT 1;", link);
                object oldId = cmd.ExecuteScalar(CommandBehavior.SingleRow);
                OldIdAvail = oldId != null;
            }
        }
        public static void FlushSettings()
        {
            // Flush static properties to the database
            using (SQLiteConnection link = new SQLiteConnection(StorMain.ConnString))
            {
                link.Open();
                SQLiteCommand cmd = new SQLiteCommand("UPDATE ResourceInfo SET NextId = @NextId, Prefix = @Prefix;", link);
                cmd.Parameters.AddWithValue("@NextId", (long)NextId);
                cmd.Parameters.AddWithValue("@Prefix", (long)Prefix);
                cmd.ExecuteNonQuery();
            }
        }
        public static StorResource Get(ulong resource)
		{
            // Get a resource from the database
            using (SQLiteConnection link = new SQLiteConnection(StorMain.ConnString))
            {
                link.Open();
                SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM Resource WHERE Id = @Id;", link);
                cmd.Parameters.AddWithValue("@Id", resource);
                using (SQLiteDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
                {
                    if (reader == null) throw new FieldAccessException(ErrorNoResource);
                    StorResource resourceObj = new StorResource();
                    resourceObj.Id = (ulong) reader.GetInt64(0);
                    resourceObj.Owner = (ulong) reader.GetInt64(1);
                    resourceObj.Locker = (ulong) reader.GetInt64(2);
                    resourceObj.LastUsed = reader.GetDateTime(3);
                    resourceObj.LastLocked = reader.GetDateTime(4);
                    resourceObj.ResourceType = (StorResourceType) reader.GetInt32(5);
                    resourceObj.KeyR = reader.GetGuid(6);
                    resourceObj.KeyW = reader.GetGuid(7);
                    resourceObj.KeyRW = reader.GetGuid(8);
                    return resourceObj;
                }
            }
        }
        public static void Delete(ulong resource, ulong owner)
        {
            // Delete a resource (only possible by owner)
            using (SQLiteConnection link = new SQLiteConnection(StorMain.ConnString))
            {
                link.Open();
                SQLiteCommand cmd = new SQLiteCommand("SELECT Owner FROM Resource WHERE Id = @Id;", link);
                cmd.Parameters.AddWithValue("@Id", resource);
                object ownerObj = cmd.ExecuteScalar();
                if (ownerObj == null) throw new FieldAccessException(ErrorNoResource);
                if ((ulong)ownerObj != owner) throw new FieldAccessException("Resource could not be deleted.");
                cmd = new SQLiteCommand("DELETE FROM Resource WHERE Id = @Id;", link);
                cmd.Parameters.AddWithValue("@Id", resource);
                cmd.ExecuteNonQuery();
                cmd = new SQLiteCommand("INSERT INTO ResourceOld VALUES (@Id);", link);
                cmd.Parameters.AddWithValue("@Id", resource);
                cmd.ExecuteNonQuery();
                OldIdAvail = true;
            }
        }

		protected StorResource() { }
        public StorResource(StorResource copy)
        {
			Copy(copy);
        }
		public StorResource(ulong owner, StorResourceType type)
		{
            // Create a resource, which must be manually saved / created
            Id = NewResourceId;
            Locker = 0;
			ResourceType = type;
			Owner = owner;
			KeyR = Guid.NewGuid();
			KeyW = Guid.NewGuid();
			KeyRW = Guid.NewGuid();
			LastUsed = DateTime.UtcNow;
			LastLocked = DateTime.MinValue;
		}

        public virtual void Flush()
		{
            // flushes data to the database
			using ( SQLiteConnection link = new SQLiteConnection(StorMain.ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("UPDATE Resource SET Locker = @Locker, Owner = @Owner, LastUsed = @LastUsed, " +
					"LastLockered = @LastLockered, Type = @Type, KeyR = @KeyR, KeyW = @KeyW, KeyRW = @KeyRW WHERE Id = @Id;", link);
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@Locker", Locker);
                cmd.Parameters.AddWithValue("@Owner", Owner);
                cmd.Parameters.AddWithValue("@LastUsed", LastUsed);
                cmd.Parameters.AddWithValue("@LastLockered", LastLocked);
                cmd.Parameters.AddWithValue("@Type", ResourceType);
                cmd.Parameters.AddWithValue("@KeyR", KeyR);
                cmd.Parameters.AddWithValue("@KeyW", KeyW);
                cmd.Parameters.AddWithValue("@KeyRW", KeyRW);
                int rowsAffected = cmd.ExecuteNonQuery();
                if (rowsAffected != 1)
                {
                    cmd = new SQLiteCommand("INSERT INTO Resource VALUES (@Id, @Locker, @Owner, @LastUsed, " +
                        "@LastLockered, @Type, @KeyR, @KeyW, @KeyRW;", link);
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@Locker", Locker);
                    cmd.Parameters.AddWithValue("@Owner", Owner);
                    cmd.Parameters.AddWithValue("@LastUsed", LastUsed);
                    cmd.Parameters.AddWithValue("@LastLockered", LastLocked);
                    cmd.Parameters.AddWithValue("@Type", ResourceType);
                    cmd.Parameters.AddWithValue("@KeyR", KeyR);
                    cmd.Parameters.AddWithValue("@KeyW", KeyW);
                    cmd.Parameters.AddWithValue("@KeyRW", KeyRW);
                    cmd.ExecuteNonQuery();
                }
			}
		}
		public void WasUsed()
		{
			LastUsed = DateTime.UtcNow;
		}
		public void RefreshKeys()
		{
			KeyR = Guid.NewGuid();
			KeyW = Guid.NewGuid();
			KeyRW = Guid.NewGuid();
		}
		public new string ToString()
		{
			return "Res_" + Id.ToString("x8");
		}
		public void Unlock(ulong entity)
		{
			if ( !IsLocked ) throw new FieldAccessException("Resource has not been locked.");
			if ( entity != Locker ) throw new FieldAccessException("Recource could not be unlocked.");
			Locker = 0;
		}
		public void Copy(StorResource copy)
		{
			Id = copy.Id;
			Owner = copy.Owner;
			Locker = copy.Locker;
			LastUsed = copy.LastUsed;
			LastLocked = copy.LastLocked;
			ResourceType = copy.ResourceType;
			KeyR = copy.KeyR;
			KeyW = copy.KeyW;
			KeyRW = copy.KeyRW;
		}
		public void Lock(ulong entity, Guid key)
        {
            if (key != KeyW && key != KeyRW) throw new FieldAccessException("Resource could not be locked.");
            Locker = entity; LastLocked = DateTime.UtcNow;
        }

		public virtual void Dispose()
		{
		}
		public void Close()
		{
			Dispose();
		}
	}
}
