﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Data;
using System.Data.SQLite;
using System.IO;


namespace PressureStore
{
	class Program
	{
		public const string DatabaseFile = "Wasp.db";
		public const string DatabasePath = @"Data\";
		public const string Database = DatabasePath + DatabaseFile;
		public const string ConnString = @"Data Source=" + Database + ";Version=3;";
		static byte[] Buffer = new byte[256];

		static void Main(string[] args)
		{
			IPEndPoint endPoint = InputEndpoint();
			// Connect
			Socket link = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			try
			{
				link.Connect(endPoint);
				link.Send(new byte[0]);
			}
			catch ( Exception )
			{
				Console.WriteLine("Failed to connect.");
				Console.ReadLine(); return;
			}
			Console.WriteLine("Connection success.");
			CreateDb();
			Console.WriteLine("Database {0} initialized.\n\n", Database);
			// Receive data from GPRS  gateway
			while ( true )
			{
				Console.WriteLine("\n\nWaiting for data ...");
				int read = 0;
				try { read = link.Receive(Buffer); }
				catch ( Exception ) { }
				if ( read == 0 )
				{
					Console.WriteLine("Receive failed.");
					Thread.Sleep(100);
					continue;
				}
				if ( read < 12 )
				{
					Console.WriteLine("Low data.");
					continue;
				}
				int sensorId = BitConverter.ToInt16(Buffer, 0);
				int recordTime = BitConverter.ToInt32(Buffer, 4);
				double value = BitConverter.ToSingle(Buffer, 8);
				Console.Write("Data:");
				Console.WriteLine("Sensor Id: {0}", sensorId);
				Console.WriteLine("Record Time: {0}", recordTime);
				Console.WriteLine("Value: {0}", value);
				Console.WriteLine();
				SaveToStore(sensorId, recordTime, value);
			}
		}
		static void CreateDb()
		{
			// Create database, if not yet created
			if ( !File.Exists(Database) )
			{
				Directory.CreateDirectory(DatabasePath);
				SQLiteConnection.CreateFile(Database);
			}
			using ( SQLiteConnection link = new SQLiteConnection(ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("CREATE TABLE IF NOT EXISTS Temp (Id INTEGER NOT NULL, " +
					"Time INTEGER NOT NULL, Value REAL NOT NULL);", link);
				cmd.ExecuteNonQuery();
			}
		}
		static IPEndPoint InputEndpoint()
		{
			IPEndPoint endPoint;
			while ( true )
			{
				try
				{
					Console.WriteLine("Enter GPRS Gateway Address> ");
					string[] endPointParts = Console.ReadLine().Split(':');
					IPAddress ipAddress = IPAddress.Parse(endPointParts[0]);
					int port = int.Parse(endPointParts[1]);
					endPoint = new IPEndPoint(ipAddress, port);
					break;
				}
				catch ( Exception )
				{ Console.WriteLine("Invalid IpEndPoint.\n"); }
			}
			return endPoint;
		}
		static void SaveToStore(int sensorId, int recordTime, double value)
		{
			using ( SQLiteConnection link = new SQLiteConnection(ConnString) )
			{
				link.Open();
				SQLiteCommand cmd = new SQLiteCommand("INSERT INTO Temp VALUES (@Id, @Time, @Value);", link);
				cmd.Parameters.AddWithValue("@Id", sensorId);
				cmd.Parameters.AddWithValue("@Time", recordTime);
				cmd.Parameters.AddWithValue("@Value", value);
				cmd.ExecuteNonQuery();
			}
		}
	}
}
