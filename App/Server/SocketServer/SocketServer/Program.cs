﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace SocketClient
{
	class Program
	{
		static Socket Srvr, Clnt;
		static string Name
		{
			get
			{ return Dns.GetHostName(); }
		}
		static IPAddress[] IpAddrs
		{
			get
			{ return Dns.GetHostAddresses(Name); }
		}
		static IPAddress IPv4Addr
		{
			get
			{ return (IPAddress) IpAddrs.First(ip => (ip.AddressFamily == AddressFamily.InterNetwork)); }
		}
		static IPAddress IPv6Addr
		{
			get
			{ return (IPAddress) IpAddrs.First(ip => (ip.AddressFamily == AddressFamily.InterNetworkV6)); }
		}

		static void Main(string[] args)
		{
			string msg = "";
			Console.WriteLine("SocketServer v0.1");
			Console.WriteLine("-----------------");
			while ( true )
			{
				if ( Srvr == null ) Console.Write("\n\nSocketServer> ");
				else Console.Write("\n\nSocketServer " + EndPointString(Srvr.LocalEndPoint) + "> ");
				string cmdLine = Console.ReadLine();
				int cmdSpace = cmdLine.IndexOf(' ');
				string[] cmdParts;
				if ( cmdSpace < 0 ) cmdParts = new string[] { cmdLine };
				else cmdParts = new string[] { cmdLine.Substring(0, cmdSpace), cmdLine.Substring(cmdSpace + 1, cmdLine.Length - cmdSpace - 1) };
				switch ( cmdParts[0] )
				{
					case "system":
					case "exit":
					case "quit":
					case "end":
						msg = SocketDisconnect();
						return;
					case "start":
						msg = SocketStart(cmdParts[1]);
						break;
					case "stop":
						msg = SocketStop();
						break;
					case "diconnect":
					case "disconn":
						msg = SocketDisconnect();
						break;
					case "transmit":
					case "send":
					case "snd":
					case "tx":
						msg = SocketSend(cmdParts[1]);
						break;
					case "available":
					case "avail":
						msg = SocketAvailable();
						break;
					case "receive":
					case "recv":
					case "rx":
						msg = SocketReceive();
						break;
				}
				Console.WriteLine("\n{0}", msg);
				if ( Srvr != null && Clnt == null )
				{
					Console.WriteLine("\nWaiting for new client ...");
					Clnt = Srvr.Accept();
					Console.WriteLine("Client " + EndPointString(Clnt.RemoteEndPoint) + " connected.");
				}
			}
		}
		static string EndPointString(EndPoint endPoint)
		{
			IPEndPoint ipEndPoint = (IPEndPoint) endPoint;
			return "[" + ipEndPoint.Address + ":" + ipEndPoint.Port + "]";
		}
		static string SocketStart(string portString)
		{
			string msg = "Invalid port number";
			if ( Srvr != null ) SocketStop();
			try
			{
				int port = int.Parse(portString);
				IPEndPoint ipEndPoint = new IPEndPoint(IPv4Addr, port);
				try
				{
					Srvr = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
					Srvr.Bind(ipEndPoint);
					Srvr.Listen(16);
					msg = "Server " + EndPointString(ipEndPoint) + " created.";
				}
				catch ( Exception )
				{ msg = "Failed to create server " + EndPointString(ipEndPoint) + "."; }
			}
			catch ( Exception ) { }
			return msg;
		}
		static string SocketStop()
		{
			string msg = "Server already stopped.";
			if ( Srvr != null )
			{
				if ( Clnt != null ) SocketDisconnect();
				msg = "Server " + EndPointString(Srvr.LocalEndPoint) + " stopped.";
				Srvr.Close();
				Srvr = null;
				msg = "Server stopped.";
			}
			return msg;
		}
		static string SocketDisconnect()
		{
			string msg = "Already disconnected.";
			if ( Clnt != null)
			{
				msg = "Diconnected from " + EndPointString(Clnt.RemoteEndPoint) + ".";
				try
				{ if(Clnt.Connected) Clnt.Disconnect(false); }
				finally
				{
					Clnt.Close();
					Clnt = null;
				}
			}
			return msg;
		}
		static string SocketSend(string sendString)
		{
			string msg = "Not connected.";
			if ( Clnt != null && Clnt.Connected )
			{
				try
				{
					byte[] sendData = Encoding.ASCII.GetBytes(sendString);
					Clnt.Send(sendData);
					msg = sendData.Length + " byte(s) sent to " + EndPointString(Clnt.RemoteEndPoint) + ".";
				}
				catch ( Exception )
				{ msg = "Sending to " + EndPointString(Clnt.RemoteEndPoint) + " failed."; }
			}
			return msg;
		}
		static string SocketAvailable()
		{
			string msg = "Not connected.";
			if ( Clnt != null && Clnt.Connected )
			{
				msg = Clnt.Available + " byte(s) available.";
			}
			return msg;
		}
		static string SocketReceive()
		{
			string msg = "Not connected";
			if ( Clnt != null && Clnt.Connected )
			{
				try
				{
					int availBytes = Clnt.Available;
					byte[] recvData = new byte[availBytes];
					Clnt.Receive(recvData);
					string recvString = Encoding.ASCII.GetString(recvData);
					msg = "Data received from " + EndPointString(Clnt.RemoteEndPoint) + ":\n" + recvString;
				}
				catch ( Exception )
				{ msg = "Failed to receive from " + EndPointString(Clnt.RemoteEndPoint) + "."; }
			}
			return msg;
		}
	}
}
