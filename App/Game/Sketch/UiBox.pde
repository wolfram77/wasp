// 
// UI Box
//
class UiBox extends UiElem
{
  // New UI Box
  public UiBox() {}
  public UiBox(UiBox copy)
  {
    super(copy);
  }
  public UiBox(UiDim dim, UiColor colors, int state, boolean inheritstate)
  {
    super(dim, colors, state, inheritstate);
  }
  public UiBox(UiDim dim, UiColor colors, boolean inheritstate)
  {
    super(dim, colors, inheritstate);
  }
  public UiBox(UiDim dim, UiColor colors)
  {
    super(dim, colors);
  }
  
  
  // Draw this UI Image
  protected void DrawThis()
  {
    if(Hide) return;
    noStroke();
    rectMode(UseDim.Mode);
    fill(UseColor);
    translate(UseDim.PosX, UseDim.PosY);
    if(UseDim.Angle != 0.0f) rotate(UseDim.Angle);
    rect(0, 0, UseDim.SizeX, UseDim.SizeY);
  }
}

