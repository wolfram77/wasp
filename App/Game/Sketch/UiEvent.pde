// 
// UI Event
//
class UiEvent
{
  final static int HierarchyChanged = 0x00;
  final static int PositionChanged = 0x01;
  final static int StateChanged = 0x02;
  final static int ColorChanged = 0x03;
  final static int MouseEvent = 0x10;
  final static int MouseClicked = 0x10;
  final static int MouseDragged = 0x11;
  final static int MouseMoved = 0x12;
  final static int MousePressed = 0x13;
  final static int MouseReleased = 0x14;
  final static int MouseWheel = 0x15;
  final static int KeyEvent = 0x20;
  final static int KeyPressed = 0x20;
  final static int KeyReleased = 0x21;
  final static int KeyTyped = 0x22;
  final static int ManualEvent = 0x30;
  final static int ButtonEvent = 0x40;
  final static int ButtonClicked = 0x40;
  final static int TextBoxEvent = 0x50;
  final static int TextBoxActive = 0x50;
  final static int TextBoxNotActive = 0x51;
  final static int TextBoxUsed = 0x52;
  final static int TextBoxTextChanged = 0x53;
}

