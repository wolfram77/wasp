// 
// UI Text
//
class UiText extends UiElem
{
  String Text;
  UiFont Font;
  
  
  // New UI Text
  public UiText() {}
  public UiText(UiText copy)
  {
    super(copy);
    Text = copy.Text;
    Font = copy.Font;
  }
  public UiText(UiDim dim, UiColor colors, int state, boolean inheritstate, String text, UiFont font)
  {
    super(dim, colors, state, inheritstate);
    Text = text;
    Font = font;
  }
  public UiText(UiDim dim, UiColor colors, boolean inheritstate, String text, UiFont font)
  {
    super(dim, colors, inheritstate);
    Text = text;
    Font = font;
  }
  public UiText(UiDim dim, UiColor colors, String text, UiFont font)
  {
    super(dim, colors);
    Text = text;
    Font = font;
  }


  // Draw this UI Text
  protected void DrawThis()
  {
    if(Hide) return;
    fill(UseColor);
    rectMode(UseDim.Mode);
    if(Font != null)
    {
      if(Font.Font != null) textFont(Font.Font);
      textAlign(Font.AlignX, Font.AlignY);
      textSize(Font.Size);
    }
    translate(UseDim.PosX, UseDim.PosY);
    if(UseDim.Angle != 0.0f) rotate(UseDim.Angle);
    text(Text, 0, 0, UseDim.SizeX, UseDim.SizeY);
  }
}

