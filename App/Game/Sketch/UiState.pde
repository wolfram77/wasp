// 
// UI State
//
class UiState
{
  final static int Ready = 1;
  final static int Hover = 2;
  final static int Active = 3;
  final static int Pressed = 4;
  final static int Inactive = 0;
}

