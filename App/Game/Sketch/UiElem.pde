// 
// Base UI Element
//
class UiElem
{
  int State;
  UiElem Root;
  boolean Hide;
  UiColor Color;
  color UseColor;
  UiDim Dim, UseDim;
  boolean InheritState;
  ArrayList<UiElem> Children;
  
  
  // New UI Element
  public UiElem() {}
  public UiElem(UiElem copy)
  {
    Dim = copy.Dim;
    Root = copy.Root;
    Hide = copy.Hide;
    State = copy.State;
    Color = copy.Color;
    UseDim = new UiDim(Dim);
    InheritState = copy.InheritState;
    if(Color != null) UseColor = Color.Get(0xFF000000, 0);
  }
  public UiElem(UiDim dim, UiColor colors, int state, boolean inheritstate)
  {
    Dim = dim;
    Hide = false;
    State = state;
    Color = colors;
    InheritState = inheritstate;
    if(Color != null) UseColor = Color.Get(0xFF000000, 0);
  }
  public UiElem(UiDim dim, UiColor colors, boolean inheritstate)
  {
    this(dim, colors, UiState.Ready, inheritstate);
  }
  public UiElem(UiDim dim, UiColor colors)
  {
    this(dim, colors, UiState.Ready, false);
  }
  
  
  // Add a child to a UI Element
  public void AddChild(UiElem child)
  {
    if(Children == null) Children = new ArrayList<UiElem>();
    Children.add(child);
    child.Root = this;
  }
  
  
  // Set a UI Element as parent
  public void SetParent(UiElem parent)
  {
    if(parent.Children == null) parent.Children = new ArrayList<UiElem>();
    parent.Children.add(this);
    Root = parent;
  }
  
  
  // Remove a child from a UI Element
  public void RemoveChild(UiElem child)
  {
    if(Children == null) return;
    Children.remove(child);
    child.Root = null;
  }
  
  
  // Separate out the UI Element from its parent
  public void ClearParent()
  {
    if(Root == null) return;
    if(Root.Children != null) Root.Children.remove(this);
    Root = null;
  }
  
  
  // Mouse Event Handler for this UI Element
  protected UiElem OnMouseEventThis(int event)
  {
    RefreshThis();
    return null;
  }
  
  
  // Keyboard Event Handler for this UI Element
  protected UiElem OnKeyEventThis(int event) { return null; }
  
  
  // Event Handler for this UI Element
  protected UiElem OnEventThis(int event)
  {
    if(Hide) return null;
    UiElem elem = null;
    RefreshThis();
    switch(event & 0xF0)
    {
      case UiEvent.MouseEvent:
      elem = OnMouseEventThis(event); break;
      case UiEvent.KeyEvent:
      elem = OnKeyEventThis(event); break;
    }
    return elem;
  }
  
  
  // Event Handler for this, and its children
  public UiElem OnEvent(int event)
  {
    if(Hide) return null;
    UiElem elem = null;
    elem = OnEventThis(event);
    if(elem != null) return elem;
    if(Children == null) return elem;
    int size = Children.size();
    for(int i=0; i<size; i++)
    {
      elem = Children.get(i).OnEvent(event);
      if(elem != null) break;
    }
    return elem;
  }
  
  
  // Refresh Hierarchy of this UI element 
  protected void RefreshHierarchyThis()
  {
    if(Root != null)
    {
      if(Root.Children == null) Root.Children = new ArrayList<UiElem>();
      if(!Root.Children.contains(this)) Root.Children.add(this);
    }
    if(Children != null)
    {
      int size = Children.size();
      for(int i=0; i<size; i++)
        Children.get(i).Root = this;
    }
  }
  
    
  // Refresh dimension of this UI Element
  protected void RefreshDimThis()
  {
    UseDim.Refresh((Root != null)? Root.UseDim : null, Dim);
  }
  
  
  // Refesh state of this UI Element
  protected void RefreshStateThis()
  {
    if(Root != null && (InheritState || Root.State == UiState.Inactive)) { State = Root.State; return; }
    if(State == UiState.Active || State == UiState.Inactive) return;
    int curx = mouseX - UseDim.PosX, cury = mouseY - UseDim.PosY;
    State = UiState.Ready;
    if(UseDim.Angle != 0.0f)
    {
      curx = (int)(curx * cos(-UseDim.Angle) - cury * sin(-UseDim.Angle));
      cury = (int)(cury * cos(-UseDim.Angle) + curx * sin(-UseDim.Angle));
    }
    if((curx < 0 || curx >= UseDim.SizeX) || (cury < 0 || cury >= UseDim.SizeY)) return;
    State = UiState.Hover;
    if(mousePressed) State = UiState.Pressed;
  }
  
  
  // Refresh color of this UI Element
  protected void RefreshColorThis()
  {
    if(Color == null) return;
    UseColor = Color.Get((Root != null)? Root.UseColor : 0xFF000000, State);
  }
  
  
  // Refresh this UI Element
  protected void RefreshThis()
  {
    if(Hide) return;
    RefreshDimThis();
    RefreshStateThis();
    RefreshColorThis();
  }
  
  
  // Refresh this UI Element, and all its children
  protected void Refresh()
  {
    if(Hide) return;
    RefreshThis();
    if(Children == null) return;
    int size = Children.size();
    for(int i=0; i<size; i++)
      Children.get(i).Refresh();
  }
  
  
  // Draw this UI Element (no refresh)
  protected void DrawThis() {}
  
  
  // Draw this UI Element, and its children
  public void Draw()
  {
    if(Hide) return;
    pushMatrix();
    DrawThis();
    if(Children != null)
    {
      int size = Children.size();
      for(int i=size-1; i>=0; i--)
        Children.get(i).Draw();
    }
    popMatrix();
  }
}

