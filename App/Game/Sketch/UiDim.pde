// 
// UI Dimension
//
class UiDim
{
  int Mode;
  float Angle;
  int PosX, PosY;
  int SizeX, SizeY;
  boolean InheritPos;
  boolean InheritSize;
  
  
  // New UI Dimension
  UiDim() {}
  UiDim(UiDim copy)
  {
    Mode = copy.Mode;
    PosX = copy.PosX;
    PosY = copy.PosY;
    SizeX = copy.SizeX;
    SizeY = copy.SizeY;
    Angle = copy.Angle;
    InheritPos = copy.InheritPos;
    InheritSize = copy.InheritSize;
  }
  UiDim(int mode, int posx, int posy, float angle, int sizex, int sizey, boolean inheritpos, boolean inheritsize)
  {
    Mode = mode;
    PosX = posx;
    PosY = posy;
    SizeX = sizex;
    SizeY = sizey;
    Angle = angle;
    InheritPos = inheritpos;
    InheritSize = inheritsize;
  }
  
  
  // Get updated dimesion
  public void Refresh(UiDim root, UiDim dim)
  {
    Mode = dim.Mode;
    PosX = dim.PosX;
    PosY = dim.PosY;
    Angle = dim.Angle;
    SizeX = dim.SizeX;
    SizeY = dim.SizeY;
    if(root == null) return;
    if(dim.InheritPos)
    {
      Mode = root.Mode;
      PosX += root.PosX;
      PosY += root.PosY;
      Angle = (Angle + root.Angle) % TWO_PI;
    }
    if(dim.InheritSize)
    {
      SizeX += root.SizeX;
      SizeY += root.SizeY;
    }
  }
}

