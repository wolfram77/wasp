// 
// UI Image
//
class UiImage extends UiElem
{
  PImage Image;
  
  
  // New UI Image
  public UiImage() {}
  public UiImage(UiImage copy)
  {
    super(copy);
    Image = copy.Image;
  }
  public UiImage(UiDim dim, UiColor colors, int state, boolean inheritstate, PImage image)
  {
    super(dim, colors, state, inheritstate);
    Image = image;
  }
  public UiImage(UiDim dim, UiColor colors, boolean inheritstate, PImage image)
  {
    super(dim, colors, inheritstate);
    Image = image;
  }
  public UiImage(UiDim dim, UiColor colors, PImage image)
  {
    super(dim, colors);
    Image = image;
  }
  public UiImage(UiDim dim, PImage image)
  {
    this(dim, null, image);
  }
  
  
  // Draw this UI Image
  protected void DrawThis()
  {
    if(Hide) return;
    imageMode(UseDim.Mode);
    if(UseColor != 0) tint(UseColor);
    else noTint();
    translate(UseDim.PosX, UseDim.PosY);
    if(UseDim.Angle != 0.0f) rotate(UseDim.Angle);
    image(Image, 0, 0, UseDim.SizeX, UseDim.SizeY);
  }
}

