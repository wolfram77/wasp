// 
// UI Rect
//
class UiRect extends UiElem
{
  int Thickness;
  
  
  // New UI Rect
  public UiRect() {}
  public UiRect(UiRect copy)
  {
    super(copy);
    Thickness = copy.Thickness;
  }
  public UiRect(UiDim dim, UiColor colors, int state, boolean inheritstate, int thickness)
  {
    super(dim, colors, state, inheritstate);
    Thickness = thickness;
  }
  public UiRect(UiDim dim, UiColor colors, boolean inheritstate, int thickness)
  {
    super(dim, colors, inheritstate);
    Thickness = thickness;
  }
  public UiRect(UiDim dim, UiColor colors, int thickness)
  {
    super(dim, colors);
    Thickness = thickness;
  }
  
  
  // Draw this UI Image
  protected void DrawThis()
  {
    if(Hide) return;
    noFill();
    rectMode(UseDim.Mode);
    stroke(UseColor);
    strokeWeight(Thickness);
    translate(UseDim.PosX, UseDim.PosY);
    if(UseDim.Angle != 0.0f) rotate(UseDim.Angle);
    rect(0, 0, UseDim.SizeX, UseDim.SizeY);
  }
}

