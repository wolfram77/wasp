// 
// UI Font
//
class UiFont
{
  int Size;
  PFont Font;
  int AlignX, AlignY;
  
  
  // New UI Font
  public UiFont() {}
  public UiFont(UiFont copy)
  {
    Font = copy.Font;
    Size = copy.Size;
    AlignX = copy.AlignX;
    AlignY = copy.AlignY;
  }
  public UiFont(PFont font, int size, int alignx, int aligny)
  {
    Font = font;
    Size = size;
    AlignX = alignx;
    AlignY = aligny;
  }
  public UiFont(PFont font, int size)
  {
    this(font, size, CENTER, CENTER);
  }
}

