// Ui data
XML Ui;
HashMap<String, PFont> PFontMap;
HashMap<String, UiFont> UiFontMap;

// global event data
int lastEvent;
int mouseWheel;


void loadPFont()
{
  PFontMap = new HashMap<String, PFont>();
  XML[] PFontList = Ui.getChildren("pfont");
  if(PFontUi == null) return;
  int size = PFontList.length;
  for(int i=0; i<size; i++)
  {
    String id = PFontList[i].getString("id");
    String file = PFontList[i].getString("file");
    PFont PFontData = loadFont(file);
    PFontMap.put(id, PFontData);
  }
}

    Font = copy.Font;
    Size = copy.Size;
    AlignX = copy.AlignX;
    AlignY = copy.AlignY;

void loadUiFont()
{
  UiFontMap = new HashMap<String, UiFont>();
  XML[] UiFontList = Ui.getChildren("pfont");
  if(UiFontList == null) return;
  int size = UiFontList.length;
  for(int i=0; i<size; i++)
  {
    String id = UiFontList[i].getString("id");
    String pfont = UiFontList[i].getString("pfont");
    int size = UiFontList[i].getInt("size");
    int alignx = UiFontList[i].get
    PFont PFontData = loadFont(file);
    PFontMap.put(id, PFontData);
  }
}
void loadUi()
{
  Ui = loadXML("settings.xml");
  loadPFont();
}
