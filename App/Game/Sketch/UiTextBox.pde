// 
// UI Text Box
//
class UiTextBox extends UiButton
{
  String TextInput;
  int Cursor, Frame;
  
  
  // New UI Text Box
  public UiTextBox() {}
  public UiTextBox(UiTextBox copy)
  {
    super(copy);
    TextInput = copy.TextInput;
    Cursor = copy.Cursor;
  }
  public UiTextBox(UiDim dim, UiColor colors, UiBox box, UiRect rect, UiText text, UiImage image, String textinput)
  {
    super(dim, colors, box, rect, text, image);
    TextInput = textinput;
  }
  public UiTextBox(UiDim dim, UiBox box, UiRect rect, UiText text, UiImage image, String textinput)
  {
    this(dim, null, box, rect, text, image, textinput);
  }
  
  
  // Mouse Event Handler for this UI Text Box
  protected UiElem OnMouseEventThis(int event)
  {
    RefreshThis();
    if(event != UiEvent.MouseClicked) return null;
    if(State == UiState.Hover || State == UiState.Pressed)
    {
      State = UiState.Active;
      lastEvent = UiEvent.TextBoxActive;
      return this;
    }
    State = UiState.Ready;
    lastEvent = UiEvent.TextBoxNotActive;
    return this;
  }
  
  
  // Key Event Handler for this UI Text Box
  protected UiElem OnKeyEventThis(int event)
  {
    if(State != UiState.Active) return null;
    if(event != UiEvent.KeyTyped) return null;
    if(key == CODED)
    {
      switch(keyCode)
      {
        case LEFT:
        Cursor--;
        if(Cursor < 0) Cursor = 0;
        lastEvent = UiEvent.TextBoxUsed;
        return this;
        case RIGHT:
        Cursor++;
        if(Cursor > TextInput.length()) Cursor = TextInput.length();
        lastEvent = UiEvent.TextBoxUsed;
        return this;
      }
    }
    else
    {
      switch(key)
      {
        case BACKSPACE:
        if(Cursor == 0) break;
        TextInput = TextInput.substring(0, Cursor-1) + TextInput.substring(Cursor);
        lastEvent = UiEvent.TextBoxTextChanged;
        return this;
        case TAB:
        return null;
        case ENTER:
        case RETURN:
        case ESC:
        State = UiState.Ready;
        lastEvent = UiEvent.TextBoxNotActive;
        return this;
        case DELETE:
        if(Cursor == TextInput.length()) break;
        TextInput = TextInput.substring(0, Cursor) + TextInput.substring(Cursor+1);
        lastEvent = UiEvent.TextBoxTextChanged;
        return this;
        default:
        TextInput = TextInput.substring(0, Cursor) + key + TextInput.substring(Cursor+1);
        lastEvent = UiEvent.TextBoxTextChanged;
        return this;
      }
    }
    return null;
  }
  
  
  // Draw this UI Text Box only
  protected void DrawThis()
  {
    if(Hide) return;
    if(Text != null)
    {
      if(State == UiState.Active && Frame >= 30) Text.Text = TextInput + "|";
      else Text.Text = TextInput;
    }
  }
}


