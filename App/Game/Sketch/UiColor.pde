// 
// UI Color
//
class UiColor
{
  color[] Color;
  boolean InheritColor;
  boolean InheritAlpha;
  
  
  // New UI Color
  public UiColor() {}
  public UiColor(UiColor copy)
  {
    Color = copy.Color;
    InheritColor = copy.InheritColor;
    InheritAlpha = copy.InheritAlpha;
  }
  public UiColor(color[] colors, boolean inheritcolor, boolean inheritalpha)
  {
    Color = colors;
    InheritColor = inheritcolor;
    InheritAlpha = inheritalpha;
  }
  public UiColor(color[] colors)
  {
    this(colors, false, false);
  }
  
  
  // Update colors wrt a root color
  public void Update(color root)
  {
    if(Color == null) return;
    for(int i=0; i<Color.length; i++)
    {
      if(InheritColor) Color[i] ^= root;
      if(InheritAlpha) Color[i] &= 0x00FFFFFF;
    }
  }
  
  
  // Get color wrt a root color, by state
  public color Get(color root, int state)
  {
    color use = (Color != null)? ((Color.length >= state)? Color[state]: Color[0]) : 0xFF000000;
    if(InheritColor) use ^= root & 0x00FFFFFF;
    if(InheritAlpha) use ^= (root & 0xFF000000);
    return use;
  }
  
  
  // Set alpha value for all colors
  public void SetAlpha(int alpha)
  {
    if(Color == null) return;
    int val = alpha << 24;
    for(int i=0; i<Color.length; i++)
      Color[i] = (Color[i] & 0x00FFFFFF) | val;
  }
}

