// 
// UI Button
//
class UiButton extends UiElem
{
  UiBox Box;
  UiRect Rect;
  UiText Text;
  UiImage Image;
  
  
  // Add Parts
  private void AddParts()
  {
    if(Text != null) AddChild(Text);
    if(Image != null) AddChild(Image);
    if(Rect != null) AddChild(Rect);
    if(Box != null) AddChild(Box);
  }
  
  
  // New UI Button
  public UiButton() {}
  public UiButton(UiButton copy)
  {
    super(copy);
    Box = copy.Box;
    Rect = copy.Rect;
    Text = copy.Text;
    Image = copy.Image;
    AddParts();
  }
  public UiButton(UiDim dim, UiColor colors, UiBox box, UiRect rect, UiText text, UiImage image)
  {
    super(dim, colors, UiState.Ready, false);
    Box = box;
    Rect = rect;
    Text = text;
    Image = image;
    AddParts();
  }
  public UiButton(UiDim dim, UiBox box, UiRect rect, UiText text, UiImage image)
  {
    this(dim, null, box, rect, text, image);
  }
  
  
  // Mouse Event Handler for this UI Button
  protected UiElem OnMouseEventThis(int event)
  {
    if((State == UiState.Hover || State == UiState.Pressed) && event == UiEvent.MouseClicked)
    {
      lastEvent = UiEvent.ButtonClicked;
      return this;
    }
    return null;
  }
}

