// Camera
float CamX = 0, CamY = 0, CamZ = 0;
float CamP = 0, CamT = 0;
float CamSpd = 1;

void setup()
{
  size(1024, 600, P3D);
}

void drawScene()
{
  sphere(50);
}

void draw()
{
  background(255);
  translate(width/2, height/2, 466);
  isKeyPressed();
  isMouseMoved();
  rotateY(CamP);
  rotateX(-CamT);
  translate(-CamX, -CamY, -CamZ);
  println(CamZ);
  drawScene();
}

void isMouseMoved()
{
  int delMouseX = mouseX - pmouseX;
  int delMouseY = mouseY - pmouseY;
  if(delMouseX == 0 && delMouseY == 0) return;
  CamP += delMouseX * 0.005;
  CamT += delMouseY * 0.01;
}

void isKeyPressed()
{
  if(!keyPressed) return;
  switch(key)
  {
    case 'W':
    case 'w':
    CamX += CamSpd * cos(CamT) * sin(CamP);
    CamZ -= CamSpd * cos(CamT) * cos(CamP);
    CamY -= CamSpd * sin(CamT);
    break;
    case 'S':
    case 's':
    CamX -= CamSpd * cos(CamT) * sin(CamP);
    CamZ += CamSpd * cos(CamT) * cos(CamP);
    CamY += CamSpd * sin(CamT);
    break;
    case 'A':
    case 'a':
    // CamX -= CamSpd;
    break;
    case 'D':
    case 'd':
    // CamX += CamSpd;
    break;
  }
}
