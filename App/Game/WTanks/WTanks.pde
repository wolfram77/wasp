Mud[][] Ground;
float gndCursor, gndRange, gndRangeStep;

void setup()
{
  size(640, 480, P2D);
  Ground = new Mud[640][];
  gndCursor = 200;
  gndRange = 8;
  gndRangeStep = -0.1;
  for(int i=0; i < Ground.length; i++)
  {
    Ground[i] = new Mud[1];
    gndCursor += gndRange * random(1);
    gndRange += gndRangeStep;
    if(gndRange > 2 || gndRange < -2) gndRange = -gndRange; 
    Ground[i][0] = new Mud(color(0, 255, 0), 479, 480-(int)(gndCursor));
  }
}

void draw()
{
  background(0);
  for(int i = 0; i < 640; i++)
  {
    Mud thisMud = Ground[i][0];
    stroke(thisMud.Type);
    line(i, thisMud.Start, i, thisMud.Stop);
  }
}
