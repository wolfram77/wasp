#ifndef	_Task_StatusCode_h_
#define	_Task_StatusCode_h_


// Exit Status
#define task_end			0x00
#define	task_end_normal		0x01
#define	task_end_abrupt		0x02
#define	task_end_error		0x03
#define task_run			0x10
#define task_run_waittime	0x11
#define task_run_waitcond	0x12
#define	task_run_yielded	0x13
#define task_run_sleeptime	0x14
#define task_run_sleepcond	0x15

#define task_StatusGroup(status)	\
	((status) & (uword)0xF0)


#endif
