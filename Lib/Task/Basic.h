#ifndef	_Task_Basic_h_
#define	_Task_Basic_h_


// Definition
#define	task_Define(sz)		\
typedef	struct _Task##sz	\
{	\
	void*	Addr;	\
	uword	Size;	\
	uword	Group;	\
	byte	State[sz];	\
}Task##sz


// Default Task
task_Define(2);
task_Define(4);
task_Define(8);
task_Define(16);
task_Define(32);
task_Define(64);
task_Define(128);
task_Define(256);

#ifndef	task_Default
#define	task_Default	Task4
#endif

typedef task_Default	Task;


// Header
typedef struct _task_Header
{
	void*	Addr;
	uword	Size;
	uword	Group;
}task_Header;

typedef uword	(*task_FnPtr)(Task*);
typedef uword	(*task_Header_FnPtr)(task_Header*);


// Init
#define	task_Init(tsk, tsk_fn, group)	\
macro_Begin	\
(tsk)->Addr = (tsk_fn);	\
(tsk)->Size = (uword)sizeof(*tsk);	\
(tsk)->Group = (uword)(group);	\
macro_End


#endif
