#ifndef	_Task_Scheduler_h_
#define	_Task_Scheduler_h_


// Main
uword task_Main()
{
	uword ret;
	while(true)
	{
		task_LastExec = queue_Remove(task_Queue);
		// wait while tsk == -1
		ret = ((task_FnPtr)(task_LastExec->Addr))(task_LastExec));
		if(task_StatusGroup(ret) != task_end) queue_Add(task_Queue, task_LastExec);
	}
	return (uword)0;
}


#endif
