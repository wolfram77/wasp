#ifndef	_Task_Event_Basic_h_
#define	_Task_Event_Basic_h_


// AVR8 Events
#ifdef ADC_vect
#define	event_adc_ready			ADC_vect
#endif

#ifdef ANALOG_COMP_0_vect
#define	event_analogcomp0_ready		ANALOG_COMP_0_vect
#endif

#ifdef ANALOG_COMP_1_vect
#define	event_analogcomp1_ready		ANALOG_COMP_1_vect
#endif

#ifdef ANALOG_COMP_2_vect
#define	event_analogcomp2_ready		ANALOG_COMP_2_vect
#endif

#ifdef ANALOG_COMP_vect
#define	event_analogcomp_ready		ANALOG_COMP_vect
#define	event_analogcomp0_ready		event_analogcomp0_ready
#endif

#ifdef ANA_COMP_vect
#define	event_analogcomp_ready		ANA_COMP_vect
#define	event_analogcomp0_ready		event_analogcomp0_ready
#endif

#ifdef CANIT_vect
#define	event_can_ready				CANIT_vect
#endif

#ifdef EEPROM_READY_vect
#define event_eeprom_ready		EEPROM_READY_vect
#endif
#ifdef EE_RDY_vect
#define event_eeprom_ready		EE_RDY_vect
#endif
#ifdef EE_READY_vect
#define event_eeprom_ready		EE_READY_vect
#endif

#ifdef EXT_INT0_vect
#define event_interrupt0		EXT_INT0_vect
#endif
#ifdef INT0_vect
#define event_interrupt0		INT0_vect
#endif

#ifdef INT1_vect
#define event_interrupt1		INT1_vect
#endif

#ifdef INT2_vect
#define event_interrupt2		INT2_vect
#endif

#ifdef INT3_vect
#define event_interrupt3		INT3_vect
#endif

#ifdef INT4_vect
#define event_interrupt4		INT4_vect
#endif

#ifdef INT5_vect
#define event_interrupt5		INT5_vect
#endif

#ifdef INT6_vect
#define event_interrupt6		INT6_vect
#endif

#ifdef INT7_vect
#define event_interrupt7		INT7_vect
#endif

#ifdef IO_PINS_vect
#define event_interrupt			IO_PINS_vect
#endif
#ifdef LOWLEVEL_IO_PINS_vect
#define event_interrupt			LOWLEVEL_IO_PINS_vect
#endif

#ifdef OVRIT_vect
#define event_can_timeout		OVRIT_vect
#endif

#ifdef PCINT0_vect
#define event_pinchange0		PCINT0_vect
#endif

#ifdef PCINT1_vect
#define event_pinchange1		PCINT1_vect
#endif

#ifdef PCINT2_vect
#define event_pinchange2		PCINT2_vect
#endif

#ifdef PCINT3_vect
#define event_pinchange3		PCINT3_vect
#endif

#ifdef PCINT_vect
#define event_pinchange			PCINT_vect
#define event_pinchange0		event_pinchange
#endif

#ifdef PSC0_CAPT_vect
#define event_psc0_capture		PSC0_CAPT_vect
#endif

#ifdef PSC0_EC_vect
#define event_psc0_complete		PSC0_EC_vect
#endif

#ifdef PSC1_CAPT_vect
#define event_psc1_capture		PSC1_CAPT_vect
#endif

#ifdef PSC1_EC_vect
#define event_psc1_complete		PSC1_EC_vect
#endif

#ifdef PSC2_CAPT_vect
#define event_psc2_capture		PSC2_CAPT_vect
#endif

#ifdef PSC2_EC_vect
#define event_psc2_complete		PSC2_EC_vect
#endif

#ifdef SPI_STC_vect
#define event_spi_ready			SPI_STC_vect
#endif

#ifdef SPM_RDY_vect
#define event_spm_ready			SPM_RDY_vect
#endif
#ifdef SPM_READY_vect
#define event_spm_ready			SPM_READY_vect
#endif

#ifdef TIMER0_COMP_vect
#define event_timer0_match		TIMER0_COMP_vect
#define event_timer0_matcha		event_timer0_match
#endif
#ifdef TIM0_COMPA_vect
#define event_timer0_matcha		TIM0_COMPA_vect
#endif
#ifdef TIMER0_COMPA_vect
#define event_timer0_matcha		TIMER0_COMPA_vect
#endif
#ifdef TIMER0_COMP_A_vect
#define event_timer0_matcha		TIMER0_COMP_A_vect
#endif

#ifdef TIM0_COMPB_vect
#define event_timer0_matchb		TIM0_COMPB_vect
#endif
#ifdef TIMER0_COMPB_vect
#define event_timer0_matchb		TIMER0_COMPB_vect
#endif

#ifdef TIM1_COMPA_vect
#define event_timer1_matcha		TIM1_COMPA_vect
#endif
#ifdef TIMER1_CMPA_vect
#define event_timer1_matcha		TIMER1_CMPA_vect
#endif
#ifdef TIMER1_COMP1_vect
#define event_timer1_match		TIMER1_COMP1_vect
#define event_timer1_matcha		event_timer1_match
#endif
#ifdef TIMER1_COMPA_vect
#define event_timer1_matcha		TIMER1_COMPA_vect
#endif
#ifdef TIMER1_COMP_vect
#define event_timer1_match		TIMER1_COMP_vect
#define event_timer1_matcha		event_timer1_match
#endif

#ifdef TIM1_COMPB_vect
#define event_timer1_matchb		TIM1_COMPB_vect
#endif
#ifdef TIMER1_CMPB_vect
#define event_timer1_matchb		TIMER1_CMPB_vect
#endif
#ifdef TIMER1_COMPB_vect
#define event_timer1_matchb		TIMER1_COMPB_vect
#endif

#ifdef TIMER1_COMPC_vect
#define event_timer1_matchc		TIMER1_COMPC_vect
#endif

#ifdef TIMER1_COMPD_vect
#define event_timer1_matchd		TIMER1_COMPD_vect
#endif

#ifdef TIMER2_COMPA_vect
#define event_timer2_matcha		TIMER2_COMPA_vect
#endif
#ifdef TIMER2_COMP_vect
#define event_timer2_match		TIMER2_COMP_vect
#define event_timer2_matcha		event_timer2_match
#endif

#ifdef TIMER2_COMPB_vect
#define event_timer2_matchb		TIMER2_COMPB_vect
#endif

#ifdef TIMER3_COMPA_vect
#define event_timer3_matcha		TIMER3_COMPA_vect
#endif

#ifdef TIMER3_COMPB_vect
#define event_timer3_matchb		TIMER3_COMPB_vect
#endif

#ifdef TIMER3_COMPC_vect
#define event_timer3_matchc		TIMER3_COMPC_vect
#endif

#ifdef TIMER4_COMPA_vect
#define event_timer4_matcha		TIMER4_COMPA_vect
#endif

#ifdef TIMER4_COMPB_vect
#define event_timer4_matchb		TIMER4_COMPB_vect
#endif

#ifdef TIMER4_COMPC_vect
#define event_timer4_matchc		TIMER4_COMPC_vect
#endif

#ifdef TIMER5_COMPA_vect
#define event_timer5_matcha		TIMER5_COMPA_vect
#endif

#ifdef TIMER5_COMPB_vect
#define event_timer5_matchb		TIMER5_COMPB_vect
#endif

#ifdef TIMER5_COMPC_vect
#define event_timer5_matchc		TIMER5_COMPC_vect
#endif

#ifdef TIM0_OVF_vect
#define event_timer0_overflow	TIM0_OVF_vect
#endif
#ifdef TIMER0_OVF0_vect
#define event_timer0_overflow	TIMER0_OVF0_vect
#endif
#ifdef TIMER0_OVF_vect
#define event_timer0_overflow	TIMER0_OVF_vect
#endif

#ifdef TIM1_OVF_vect
#define event_timer1_overflow	TIM1_OVF_vect
#endif
#ifdef TIMER1_OVF1_vect
#define event_timer1_overflow	TIMER1_OVF1_vect
#endif
#ifdef TIMER1_OVF_vect
#define event_timer1_overflow	TIMER1_OVF_vect
#endif

#ifdef TIMER2_OVF_vect
#define event_timer2_overflow	TIMER2_OVF_vect
#endif

#ifdef TIMER3_OVF_vect
#define event_timer3_overflow	TIMER3_OVF_vect
#endif

#ifdef TIMER4_OVF_vect
#define event_timer4_overflow	TIMER4_OVF_vect
#endif

#ifdef TIMER5_OVF_vect
#define event_timer5_overflow	TIMER5_OVF_vect
#endif

#ifdef TIMER0_CAPT_vect
#define event_timer0_capture	TIMER0_CAPT_vect
#endif

#ifdef TIM1_CAPT_vect
#define event_timer1_capture	TIM1_CAPT_vect
#endif
#ifdef TIMER1_CAPT1_vect
#define event_timer1_capture	TIMER1_CAPT1_vect
#endif
#ifdef TIMER1_CAPT_vect
#define event_timer1_capture	TIMER1_CAPT_vect
#endif

#ifdef TIMER2_CAPT_vect
#define event_timer2_capture	TIMER2_CAPT_vect
#endif

#ifdef TIMER3_CAPT_vect
#define event_timer3_capture	TIMER3_CAPT_vect
#endif

#ifdef TIMER4_CAPT_vect
#define event_timer4_capture	TIMER4_CAPT_vect
#endif

#ifdef TIMER5_CAPT_vect
#define event_timer5_capture	TIMER5_CAPT_vect
#endif

#ifdef TWI_vect
#define event_i2c_ready			TWI_vect
#endif

#ifdef TXDONE_vect
#define event_rf_txdone			TXDONE_vect
#endif

#ifdef TXEMPTY_vect
#define event_rf_txempty		TXEMPTY_vect
#endif

#ifdef UART0_RX_vect
#define event_serial0_rxdone	UART0_RX_vect
#endif
#ifdef UART_RX_vect
#define event_serial_rxdone		UART_RX_vect
#define event_serial0_rxdone	event_serial_rxdone
#endif
#ifdef USART0_RXC_vect
#define event_serial0_rxdone	USART0_RXC_vect
#endif
#ifdef USART0_RX_vect
#define event_serial0_rxdone	USART0_RX_vect
#endif
#ifdef USART_RXC_vect
#define event_serial_rxdone		USART_RXC_vect
#define event_serial0_rxdone	event_serial_rxdone
#endif
#ifdef USART_RX_vect
#define event_serial_rxdone		USART_RX_vect
#define event_serial0_rxdone	event_serial_rxdone
#endif

#ifdef UART0_TX_vect
#define event_serial0_txdone	UART0_TX_vect
#endif
#ifdef UART_TX_vect
#define event_serial_txdone		UART_TX_vect
#define event_serial0_txdone	event_serial_txdone
#endif
#ifdef USART0_TXC_vect
#define event_serial0_txdone	USART0_TXC_vect
#endif
#ifdef USART0_TX_vect
#define event_serial0_txdone	USART0_TX_vect
#endif
#ifdef USART_TXC_vect
#define event_serial_txdone		USART_TXC_vect
#define event_serial0_txdone	event_serial_txdone
#endif
#ifdef USART_TX_vect
#define event_serial_txdone		USART_TX_vect
#define event_serial0_txdone	event_serial_txdone
#endif

#ifdef UART0_UDRE_vect
#define event_serial0_empty		UART0_UDRE_vect
#endif
#ifdef UART_UDRE_vect
#define event_serial_empty		UART_UDRE_vect
#define event_serial0_empty		event_serial_empty
#endif
#ifdef USART0_UDRE_vect
#define event_serial0_empty		USART0_UDRE_vect
#endif
#ifdef USART_UDRE_vect
#define event_serial_empty		USART_UDRE_vect
#define event_serial0_empty		event_serial_empty
#endif

#ifdef UART1_RX_vect
#define event_serial1_rxdone	UART1_RX_vect
#endif
#ifdef USART1_RXC_vect
#define event_serial1_rxdone	USART1_RXC_vect
#endif
#ifdef USART1_RX_vect
#define event_serial1_rxdone	USART1_RX_vect
#endif

#ifdef UART1_TX_vect
#define event_serial1_txdone	UART1_TX_vect
#endif
#ifdef USART1_TXC_vect
#define event_serial1_txdone	USART1_TXC_vect
#endif
#ifdef USART1_TX_vect
#define event_serial1_txdone	USART1_TX_vect
#endif

#ifdef UART1_UDRE_vect
#define event_serial1_empty		UART1_UDRE_vect
#endif
#ifdef USART1_UDRE_vect
#define event_serial1_empty		USART1_UDRE_vect
#endif

#ifdef USART2_RX_vect
#define event_serial2_rxdone	USART2_RX_vect
#endif

#ifdef USART2_TX_vect
#define event_serial2_txdone	USART2_TX_vect
#endif

#ifdef USART2_UDRE_vect
#define event_serial2_empty		USART2_UDRE_vect
#endif

#ifdef USART3_RX_vect
#define event_serial3_rxdone	USART3_RX_vect
#endif

#ifdef USART3_TX_vect
#define event_serial3_txdone	USART3_TX_vect
#endif

#ifdef USART3_UDRE_vect
#define event_serial3_empty		USART3_UDRE_vect
#endif

#ifdef USI_OVERFLOW_vect
#define event_usi_overflow		USI_OVERFLOW_vect
#endif
#ifdef USI_OVF_vect
#define event_usi_overflow		USI_OVF_vect
#endif

#ifdef USI_START_vect
#define event_usi_start		USI_START_vect
#endif
#ifdef USI_STRT_vect
#define event_usi_start		USI_STRT_vect
#endif
#ifdef USI_STR_vect
#define event_usi_start		USI_STR_vect
#endif

#ifdef WATCHDOG_vect
#define event_watchdog_timeout	WATCHDOG_vect
#endif
#ifdef WDT_OVERFLOW_vect
#define event_watchdog_timeout	WDT_OVERFLOW_vect
#endif
#ifdef WDT_vect
#define event_watchdog_timeout	WDT_vect
#endif

#define event_default			BADISR_vect



// Enable
#define	event_Enable0()	\
	sei()

#define event_Enable()	\
	event_Enable0()


// Allow
#define event_Allow		event_Enable


// Disable
#define	event_Disable0()	\
	cli()

#define event_Disable()	\
	event_Disable0()


// Block
#define event_Block		event_Disable


// Function
#define event_Function(evnt)	\
	ISR(evnt)

#define event_Fn		event_Function
#define event_Handler	event_Function
#define event_Hndlr		event_Function


// AliasFunction
#define event_AliasFunction(evnt, evnt_other)	\
	ISR_ALIAS(evnt, evnt_other)

#define event_AliasFn			event_AliasFunction
#define event_AliasHandler		event_AliasFunction
#define event_AliasHndlr		event_AliasFunction


#endif
