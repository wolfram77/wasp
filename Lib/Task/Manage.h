#ifndef	_Task_Manage_h_
#define	_Task_Manage_h_


// Task Queue
#ifndef task_QueueDefault
#define task_QueueDefault	Queue32
#endif

typedef task_QueueDefault	TaskQueue;

TaskQueue	task_Queue[1];


// LastExec
Task*	task_LastExec;


// Setup
#define	task_Setup()	\
	queue_Init(task_Queue)


// GetActive
uword task_GetQueued()
{
	return queue_GetAvail(task_Queue);
}


// GetFree
uword task_GetFree()
{
	return queue_GetFree(task_Queue);
}


// Add
uword task_AddF(Task* tsk)
{
	return queue_Add(task_Queue, tsk);
}

#define	task_Add(tsk)	\
	task_AddF((Task*)(tsk))


// Remove
uword task_RemoveF(Task* tsk)
{
	Task* tsk_get;
	uword count = queue_GetAvail(task_Queue);
	for(; count > (uword)0; count--)
	{
		tsk_get = queue_Remove(task_Queue);
		if(tsk_get != tsk) queue_Add(task_Queue, tsk_get);
	}
	return (uword)0;
}

#define	task_Remove(tsk)	\
	task_RemoveF((Task*)(tsk))


// RemoveGroup
uword task_RemoveGroupF(uword group)
{
	Task* tsk_get;
	uword count = queue_GetAvail(task_Queue);
	for(; count > (uword)0; count--)
	{
		tsk_get = queue_Remove(task_Queue);
		if(tsk_get->Group != group) queue_Add(task_Queue, tsk_get);
	}
	return (uword)0;
}

#define	task_RemoveGroup(group)	\
	task_RemoveGroupF((uword)(group))


#endif
