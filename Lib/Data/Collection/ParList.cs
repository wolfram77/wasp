﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Wasp.Data.Collection
{
	public class ParList<T> : List<T>
	{
		public object Lock;
		public Semaphore Sem;
		public int BareCount
		{
			get
			{ return base.Count; }
		}
		public new int Count
		{
			get
			{
				lock (Lock)
				{ return base.Count; }
			}
		}
		public new T this[int indx]
		{
			get
			{
				lock (Lock)
				{ return (indx < base.Count)? base[indx] : default(T); }
			}
			set
			{
				lock (Lock)
				{ base[indx] = value; }
			}
		}


		// Create Parallel access list
		private void init(bool semUsed)
		{
			Lock = new object();
			if (semUsed) Sem = new Semaphore(0, int.MaxValue);
		}
		public ParList(ParList<T> lst, bool semUsed)
			: base(lst)
		{
			init(semUsed);
		}
		public ParList(bool semUsed)
			: base()
		{
			init(semUsed);
		}
		public ParList(ParList<T> lst)
			: base(lst)
		{
			init(false);
		}
		public ParList()
			: base()
		{
			init(false);
		}

		// Get element
		public T BareElementAt(int indx)
		{
			return base[indx];
		}
		public T ElementAt(int indx)
		{
			return this[indx];
		}

		// Get next element
		public T BareNextElement(ref int indx)
		{
			if (++indx >= base.Count) indx = 0;
			return (indx < base.Count) ? base[indx] : default(T);
		}
		public T NextElement(ref int indx)
		{
			// wait until atleast one element is available
			if (Sem != null)
			{
				Sem.WaitOne();
				Sem.Release();
			}
			lock (Lock)
			{ return BareNextElement(ref indx); }
		}

		// Remove At
		public void BareRemoveAt(int indx)
		{
			base.RemoveAt(indx);
		}
		public new void RemoveAt(int indx)
		{
			lock (Lock)
			{ base.RemoveAt(indx); }
			if (Sem != null) Sem.WaitOne();
		}

		// Pop element
		public T BarePop()
		{
			T elem = default(T);
			if (base.Count > 0)
			{
				elem = base[0];
				base.RemoveAt(0);
			}
			return elem;
		}
		public T Pop()
		{
			// wait for atleast one elemnt
			if (Sem != null) Sem.WaitOne();
			lock (Lock)
			{ return BarePop(); }
		}

		// Add element (Push)
		public void BareAdd(T item)
		{
			base.Add(item);
		}
		public new void Add(T item)
		{
			lock (Lock)
			{ base.Add(item); }
			if (Sem != null) Sem.Release();
		}

		// Remove element
		public bool BareRemove(T item)
		{
			return base.Remove(item);
		}
		public new bool Remove(T item)
		{
			bool success = false;
			lock (Lock)
			{ success = base.Remove(item); }
			if (success && Sem != null) Sem.WaitOne();
			return success;
		}

		// Clear list
		public void BareClear()
		{
			base.Clear();
		}
		public new void Clear()
		{
			if (Sem != null) Sem.WaitOne(base.Count);
			lock (Lock)
			{ base.Clear(); }
		}

		// Close list
		public void Close()
		{
			Clear();
			Lock = null;
			if (Sem != null)
			{
				Sem.Dispose();
				Sem = null;
			}
		}
	}
}
