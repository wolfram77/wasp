﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Wasp.Data.Collection
{
	public class KeyVals : Dictionary<object, object>
	{
		public KeyVals()
			: base()
		{
		}
		public KeyVals(KeyVals kv)
			: base(kv)
		{
		}
		public KeyVals(params object[] pairs)
			: base()
		{
			Add(pairs);
		}
		public KeyVals(NameValueCollection nv)
			: base()
		{
			Add(nv);
		}


		// Add key value pairs
		public void Add(KeyVals kv)
		{
			Enumerator enm = kv.GetEnumerator();
			while (enm.MoveNext())
			{ base.Add(enm.Current.Key, enm.Current.Value); }
		}
		public void Add(params object[] pairs)
		{
			for (int i = 0; i < pairs.Length; i += 2)
			{ base[pairs[i]] = pairs[i + 1]; }
		}
		public void Add(NameValueCollection nv)
		{
			for (int i = 0; i < nv.Count; i++)
			{ base[nv.GetKey(i)] = nv.Get(i); }
		}

		// Remove key value pairs
		public bool Remove(object[] keys)
		{
			bool rmv = true;
			for (int i = 0; i < keys.Length; i++)
			{ rmv &= base.Remove(keys[i]); }
			return rmv;
		}

		// Get value
		public object GetValue(object key)
		{
			object val = "";
			base.TryGetValue(key, out val);
			return val;
		}

		// To string (JSON format)
		public new string ToString()
		{
			object val;
			bool isarr = false;
			Array arrs = null;
			KeyVals ptrs = this;
			int i = -1;
			int slen, dlen = ptrs.Count;
			StringBuilder ret = new StringBuilder();
			List<object> stck = new List<object>();
			while (true)
			{
				i++;
				// on end to limit, pop previous state from stack
				if (i >= dlen)
				{
					if (isarr) ret.Append(']');
					else ret.Append('}');
					slen = stck.Count;
					if (slen == 0) break;
					isarr = (bool)stck[slen - 1];
					dlen = (int)stck[slen - 2];
					i = (int)stck[slen - 3];
					if (isarr) arrs = (Array)stck[slen - 4];
					else ptrs = (KeyVals)stck[slen - 4];
					stck.RemoveRange(slen - 4, 4);
					continue;
				}
				if (i > 0) ret.Append(',');
				if (isarr) val = arrs.GetValue(i);
				else
				{
					val = ptrs.ElementAt(i);
					ret.AppendFormat("\"{0}\":", ((KeyValuePair<object, object>)val).Key);
					val = ((KeyValuePair<object, object>)val).Value;
				}
				// on new branch push current state and jump to that branch
				Type typ = val.GetType();
				if (typ.BaseType == typeof(Array))
				{
					if (isarr) stck.Add(arrs);
					else stck.Add(ptrs);
					stck.Add(i);
					stck.Add(dlen);
					stck.Add(isarr);
					ret.Append('[');
					arrs = (Array)val;
					i = -1;
					dlen = arrs.GetLength(0);
					isarr = true;
					continue;
				}
				else if (typ == typeof(KeyVals))
				{
					if (isarr) stck.Add(arrs);
					else stck.Add(ptrs);
					stck.Add(i);
					stck.Add(dlen);
					stck.Add(isarr);
					ret.Append('{');
					ptrs = (KeyVals)val;
					i = -1;
					dlen = ptrs.Count;
					isarr = false;
					continue;
				}
				// add value
				if (val.GetType() == typeof(string)) ret.AppendFormat("\"{0}\"", val);
				else ret.AppendFormat("{0}", val);
			}
			// insert start bracket
			ret.Insert(0, '{');
			return ret.ToString();
		}
	}
}
