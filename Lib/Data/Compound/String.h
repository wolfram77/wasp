#ifndef	_Data_Compound_String_h_
#define	_Data_Compound_String_h_


// Definition
typedef char*	string;


// Functions
#define	string_Length		strlen
#define	string_Compare		strcmp
#define	string_ToLower		strlwr
#define	string_ToUpper		strupr


// Equals
#define	string_Equals(str1, str2)	\
	(!string_Compare(str1, str2))


// Copy
#define	string_Copy(dst, str)	\
	(mem_Copy(dst, str, string_Length(str) + 1))


// Reverse
#define	string_Reverse(str)	\
	(mem_Reverse(str, 0, string_Length(str)))


// Concat
#define	string_Concat(dst, src)	\
	(mem_Copy((byte*)dst + string_Length(dst) + 1, src, string_Length(src) + 1))


// IndexOfChar
int	string_IndexOfChar(string str, char chr)
{
	for(char* sptr = str; *sptr; sptr++)
	{
		if(*sptr == chr)
		{ return (int)(sptr - str); }
	}
	return -1;
}

#define	string_FindChar		string_IndexOf


// IndexOfChars
int	string_IndexOfChars(string str, char* chr)
{
	for(char* sptr = str; *sptr; sptr++)
	{
		if(string_IndexOfChar(chr, *sptr) >= 0)
		{ return (int)(sptr - str); }
	}
	return -1;
}

#define	string_FindChars		string_IndexOfChars


// IndexOf
int	string_IndexOf(string str, string srch_str)
{
	char *sptr, *sptr2, *pptr;
	for(sptr = str; *sptr; sptr++)
	{
		if(*sptr != *srch_str) continue;
		for(sptr2 = sptr + 1, pptr = srch_str + 1; (*pptr) && (*sptr2 == *pptr); sptr2++, pptr++);
		if(*pptr == '\0') return (int)(sptr - str);
	}
	return -1;
}

#define	string_Find		string_IndexOf


// RemoveChar
void string_RemoveChar(string str, char chr)
{
	char* sptr;
	for(sptr = str; *str; str++)
	{
		if(*str == chr) continue;
		*sptr = *str;
		sptr++;
	}
	*sptr = '\0';
}


// RemoveChars
void string_RemoveChars(string str, char* chr)
{
	char* sptr;
	for(sptr = str; *str; str++)
	{
		if(string_IndexOfChar(chr, *str) >= 0) continue;
		*sptr = *str;
		sptr++;
	}
	*sptr = '\0';
}


// Remove
void string_Remove(string str, string rmv_str)
{
	char *sptr, *sptr2, *pptr;
	for(sptr = str; *str; str++)
	{
		if(*str != *rmv_str)
		{
			*sptr = *str;
			sptr++;
			continue;
		}
		for(sptr2 = str + 1, pptr = rmv_str + 1; (*pptr) && (*sptr2 == *pptr); sptr2++, pptr++);
		if(*pptr == '\0') str = sptr2 - 1;
		else
		{
			*sptr = *str;
			sptr++;
		}
	}
	*sptr = '\0';
}


#endif
