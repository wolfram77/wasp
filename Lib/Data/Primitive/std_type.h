/*
----------------------------------------------------------------------------------------
	type: Type definition and conversion module
	File: std_type.h

	Copyright � 2013 Subhajit Sahu. All rights reserved.

	This file is part of WASP. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WASP is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WASP is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WASP.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/


#ifndef	_std_type_h_
#define	_std_type_h_


#include <string.h>
#include "std_macro.h"




// Function:
// BinaryToHex(*dst, sz, *src, off, len, opt)
// [BinaryToHex / BinToHex]
// 
// Read hexadecimal string (dst) of maximum specified size (sz) of
// the soure binary data (src + off) of specified length (len). The
// options (opt) specify how the conversion is to be performed, and
// it takes as input a set of flags. If source base address is not
// specified, this library's internal buffer is assumed as the source
// base address.
// 
// Parameters:
// dst:	      the destination string where hex string will be stored
// sz:        the maximum possible size of the hex string (buffer size)
// src:	      the base address of source binary data
// off:	      offset to the binary data to be converted (src + off)
// len:	      length of data to be converted
// opt:	      conversion options (TYPE_add_space, TYPE_add_char, TYPE_big_endian)
// 
// Returns:
// nothing
// 
#define type_HexCharToBin(ch)		(((ch) <= '9')? (ch)-'0' : (ch)-'7')

#define type_BinToHexChar(bn)		(((bn) <= 9)? (bn)+'0' : (bn)+'7' )

#define	type_no_space			0

#define type_add_space			1

#define	type_has_space			1

#define	type_no_char			0

#define type_add_char			2

#define	type_has_char			2

#define	type_little_endian		0

#define type_big_endian			4

string type_BinaryToHexF(string dst, int sz, byte* src, int len, byte opt)
{
	string dend = dst + (sz - 1);
	src += (opt & type_big_endian)? 0 : (len - 1);
	int stp = (opt & type_big_endian)? 1 : -1;
	for(int i=0; i<len; i++, src+=stp)
	{
		*dst = type_BinToHexChar(*src >> 4); dst++; if(dst >= dend) break;
		*dst = type_BinToHexChar(*src & 0xF); dst++; if(dst >= dend) break;
		if(opt & type_add_char) { *dst = (*src < 32 || *src > 127)? '.' : *src; dst++; if(dst >= dend) break;}
		if(opt & type_add_space) { *dst = ' '; dst++; if(dst >= dend) break;}
	}
	*dst = '\0';
	return dst;
}

#define	type_BinaryToHex(dst, sz, src, off, len, opt)	\
	type_BinaryToHexF((string)(dst), (int)(sz), (byte*)(src) + (off), (int)(len), (byte)(opt))

#define	type_BinToHex	type_BinaryToHex


// Function:
// HexToBinary(*dst, off, len, *src, opt)
// [HexToBinary / HexToBin]
// 
// Writes binary data from the source hex string (src) to the destination
// address (dst + off) of specified length len. The options (opt) specify
// how the conversion is to be performed, and it takes as input a set of
// flags. If destination base address is not specified, this library's
// internal buffer is assumed as the destination base address.
// 
// Parameters:
// dst:	      the base address of destination
// off:	      the destination offset where the binary data will be stored (dst + off)
// len:       length of data at destination
// src:	      the hex string to be converted
// opt:	      conversion options (TYPE_has_space, TYPE_has_char, TYPE_big_endian)
//
// Returns:
// the converted data (dst)
// 
void type_HexToBinaryD(byte* dst, int len, string src, byte opt)
{
	char* psrc = src + strlen(src) - 1;
	dst += (opt & type_big_endian)? (len - 1) : 0;
	int i ,stp = (opt & type_big_endian)? -1 : 1;
	for(i=0; i<len; i++, dst+=stp)
	{
		if(opt & type_has_space) psrc--;
		if(opt & type_has_char) psrc--;
		*dst = (psrc < src)? 0 : type_HexCharToBin(*psrc); psrc--;
		*dst |= (psrc < src)? 0 : type_HexCharToBin(*psrc) << 4; psrc--;
	}
}

#define	type_HexToBinary(dst, off, len, src, opt)	\
	type_HexToBinaryD((byte*)(dst) + (off), (int)(len), (string)(src), (byte)(opt))

#define	type_HexToBin	type_HexToBinary


// Function:
// ParseBinary(*str, *val)
// [ParseBinary / ParseBin]
// 
// Parses a binary number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseBinaryF(string str, int64* val)
{
	int64 bin = 0;
	for(; *str; str++)
	{
		if((*str != '0') && (*str != '1')) return -1;
		bin = (bin << 1) | (*str - '0');
	}
	*val = bin;
	return 0;
}

#define	type_ParseBinaryM(str, val)	\
	type_ParseBinaryF((string)(str), (int64*)(val))

#define	type_ParseBinaryO(str, off, val)	\
	type_ParseBinaryM((byte*)(str) + (off), val)

#define	type_ParseBinary(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseBinaryO, type_ParseBinaryM)(__VA_ARGS__))

#define	type_ParseBin	type_ParseBinary


// Function:
// ParseOctal(*str, *val)
// [ParseOctal / ParseOct]
// 
// Parses an octal number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseOctalF(string str, int64* val)
{
	int64 oct = 0;
	for(; *str; str++)
	{
		if((*str < '0') || (*str >= '7')) return -1;
		oct = (oct << 3) | (*str - '0');
	}
	*val = oct;
	return 0;
}

#define	type_ParseOctalM(str, val)	\
	type_ParseOctalF((string)(str), (int64*)(val))

#define	type_ParseOctalO(str, off, val)	\
	type_ParseOctalM((byte*)(str) + (off), val)

#define	type_ParseOctal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseOctalO, type_ParseOctalM)(__VA_ARGS__))

#define	type_ParseOct	type_ParseOctal


// Function:
// ParseHexadecimal(*str, *val)
// [ParseHexadecimal / ParseHex]
// 
// Parses a hexadecimal number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseHexadecimalF(string str, int64* val)
{
	int64 hex = 0;
	for(; *str; str++)
	{
		if((*str >= '0') && (*str <= '9')) hex = (hex << 4) | (*str - '0');
		else if((*str >= 'A') && (*str <= 'F')) hex = (hex << 4) | (*str - 'A' + 10);
		else if((*str >= 'a') && (*str <= 'f')) hex = (hex << 4) | (*str - 'a' + 10);
		else return -1;
	}
	*val = hex;
	return 0;
}

#define	type_ParseHexadecimalM(str, val)	\
	type_ParseHexadecimalF((string)(str), (int64*)(val))

#define	type_ParseHexadecimalO(str, off, val)	\
	type_ParseHexadecimalM((byte*)(str) + (off), val)

#define	type_ParseHexadecimal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseHexadecimalO, type_ParseHexadecimalM)(__VA_ARGS__))

#define	type_ParseHex	type_ParseHexadecimal


// Function:
// ParseDecimal(*str, *val)
// [ParseDecimal / ParseDec]
// 
// Parses a decimal number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseDecimalF(string str, int64* val)
{
	int64 dec = 0;
	for(; *str; str++)
	{
		if((*str < '0') || (*str >= '7')) return -1;
		dec = (dec * 10) + (*str - '0');
	}
	*val = dec;
	return 0;
}

#define	type_ParseDecimalM(str, val)	\
	type_ParseDecimalF((string)(str), (int64*)(val))

#define	type_ParseDecimalO(str, off, val)	\
	type_ParseDecimalM((byte*)(str) + (off), val)

#define	type_ParseDecimal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseDecimalO, type_ParseDecimalM)(__VA_ARGS__))

#define	type_ParseDec	type_ParseDecimal


// Function:
// ParseFloat(*str, *val)
// [ParseFloat]
// 
// Parses a floating-point number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFloatF(string str, float* val)
{
	int ret = sscanf_s(str, "%f", val);
	return (ret == 1)? 0 : -1;
}

#define	type_ParseFloatM(str, val)	\
	type_ParseFloatF((string)(str), (float*)(val))

#define	type_ParseFloatO(str, off, val)	\
	type_ParseFloatM((byte*)(str) + (off), val)

#define	type_ParseFloat(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFloatO, type_ParseFloatM)(__VA_ARGS__))


// Function:
// ParseDouble(*str, *val)
// [ParseDouble]
// 
// Parses a double precision floating-point number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseDoubleF(string str, double* val)
{
	int ret = sscanf_s(str, "%lf", val);
	return (ret == 1)? 0 : -1;
}

#define	type_ParseDoubleM(str, val)	\
	type_ParseDoubleF((string)(str), (double*)(val))

#define	type_ParseDoubleO(str, off, val)	\
	type_ParseDoubleM((byte*)(str) + (off), val)

#define	type_ParseDouble(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseDoubleO, type_ParseDoubleM)(__VA_ARGS__))


// Function:
// ParseString(*str, *val)
// [ParseString / ParseStr]
// 
// Parses a string (as a number) in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseStringF(string str, int64* val)
{
	num i = 0;
	int64 strn = 0;
	for(; *str; str++, i++)
	{
		if(i >= 8) return -1;
		strn = (strn << 8) | *str;
	}
	*val = strn;
	return 0;
}

#define	type_ParseStringM(str, val)	\
	type_ParseStringF((string)(str), (int64*)(val))

#define	type_ParseStringO(str, off, val)	\
	type_ParseStringM((byte*)(str) + (off), val)

#define	type_ParseString(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseStringO, type_ParseStringM)(__VA_ARGS__))

#define	type_ParseStr	type_ParseString


// Function:
// ParseFullBinary(*str, *val)
// [ParseFullBinary / ParseFullBin]
// 
// Parses a full binary number in a string in format
// (0b????) or (????b)
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullBinaryF(string str, int64* val)
{
	num ret = -1;
	bool is_neg = false;
	if(*str == '-') is_neg = true;
	if(*str == '+' || *str == '-') str++;
	if((*str == '0') && ((str[1] == 'b') || (str[1] == 'B'))) ret = type_ParseBin(str, 2, val);
	else
	{
		int lsti = strlen(str) - 1;
		char lst = str[lsti];
		if((lst == 'b') || (lst == 'B'))
		{
			str[lsti] = '\0';
			ret = type_ParseBin(str, val);
			str[lsti] = lst;
		}
	}
	if(ret >= 0 && is_neg) *val = -(*val);
	return ret;
}

#define	type_ParseFullBinaryM(str, val)	\
	type_ParseFullBinaryF((string)(str), (int64*)(val))

#define	type_ParseFullBinaryO(str, off, val)	\
	type_ParseFullBinaryM((byte*)(str) + (off), val)

#define	type_ParseFullBinary(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullBinaryO, type_ParseFullBinaryM)(__VA_ARGS__))

#define	type_ParseFullBin	type_ParseFullBinary


// Function:
// ParseFullOctal(*str, *val)
// [ParseFullOctal / ParseFullOct]
// 
// Parses a full octal number in a string in format
// (0o????) or (????o)
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullOctalF(string str, int64* val)
{
	num ret = -1;
	bool is_neg = false;
	if(*str == '-') is_neg = true;
	if(*str == '+' || *str == '-') str++;
	if((*str == '0') && ((str[1] == 'o') || (str[1] == 'O'))) ret = type_ParseOct(str, 2, val);
	else
	{
		int lsti = strlen(str) - 1;
		char lst = str[lsti];
		if((lst == 'o') || (lst == 'O'))
		{
			str[lsti] = '\0';
			ret = type_ParseOct(str, val);
			str[lsti] = lst;
		}
	}
	if(ret >= 0 && is_neg) *val = -(*val);
	return ret;
}

#define	type_ParseFullOctalM(str, val)	\
	type_ParseFullOctalF((string)(str), (int64*)(val))

#define	type_ParseFullOctalO(str, off, val)	\
	type_ParseFullOctalM((byte*)(str) + (off), val)

#define	type_ParseFullOctal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullOctalO, type_ParseFullOctalM)(__VA_ARGS__))

#define	type_ParseFullOct	type_ParseFullOctal


// Function:
// ParseFullHexadecimal(*str, *val)
// [ParseFullHexadecimal / ParseFullHex]
// 
// Parses a full hexadecimal number in a string in format
// (0x????) or (????x) or (0h????) or (????h)
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullHexadecimalF(string str, int64* val)
{
	num ret = -1;
	bool is_neg = false;
	if(*str == '-') is_neg = true;
	if(*str == '+' || *str == '-') str++;
	if((*str == '0') && ((str[1] == 'x') || (str[1] == 'X') || (str[1] == 'h') || (str[1] == 'H'))) ret = type_ParseHex(str, 2, val);
	else
	{
		int lsti = strlen(str) - 1;
		char lst = str[lsti];
		if((lst == 'x') || (lst == 'X') || (lst == 'h') || (lst == 'H'))
		{
			str[lsti] = '\0';
			ret = type_ParseHex(str, val);
			str[lsti] = lst;
		}
	}
	if(ret >= 0 && is_neg) *val = -(*val);
	return ret;
}

#define	type_ParseFullHexadecimalM(str, val)	\
	type_ParseFullHexadecimalF((string)(str), (int64*)(val))

#define	type_ParseFullHexadecimalO(str, off, val)	\
	type_ParseFullHexadecimalM((byte*)(str) + (off), val)

#define	type_ParseFullHexadecimal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullHexadecimalO, type_ParseFullHexadecimalM)(__VA_ARGS__))

#define	type_ParseFullHex	type_ParseFullHexadecimal


// Function:
// ParseFullDecimal(*str, *val)
// [ParseFullDecimal / ParseFullDec]
// 
// Parses a full decimal number in a string in format
// (????) or (0d????) or (????d)
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullDecimalF(string str, int64* val)
{
	num ret = -1;
	bool is_neg = false;
	if(*str == '-') is_neg = true;
	if(*str == '+' || *str == '-') str++;
	if((*str == '0') && ((str[1] == 'd') || (str[1] == 'D'))) ret = type_ParseDec(str, 2, val);
	else
	{
		int lsti = strlen(str) - 1;
		char lst = str[lsti];
		if((lst == 'd') || (lst == 'D'))
		{
			str[lsti] = '\0';
			ret = type_ParseDec(str, val);
			str[lsti] = lst;
		}
		else ret = type_ParseDec(str, val);
	}
	if(ret >= 0 && is_neg) *val = -(*val);
	return ret;
}

#define	type_ParseFullDecimalM(str, val)	\
	type_ParseFullDecimalF((string)(str), (int64*)(val))

#define	type_ParseFullDecimalO(str, off, val)	\
	type_ParseFullDecimalM((byte*)(str) + (off), val)

#define	type_ParseFullDecimal(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullDecimalO, type_ParseFullDecimalM)(__VA_ARGS__))

#define	type_ParseFullDec	type_ParseFullDecimal


// Function:
// ParseFullFloat(*str, *val)
// [ParseFullFloat]
// 
// Parses a full floating-point number
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullFloatF(string str, int64* val)
{
	num ret = -1;
	int lsti = strlen(str) - 1;
	char lst = str[lsti];
	if((lst == 'f') || (lst == 'F')) ret = type_ParseFloat(str, val);
	return ret;
}

#define	type_ParseFullFloatM(str, val)	\
	type_ParseFullFloatF((string)(str), (int64*)(val))

#define	type_ParseFullFloatO(str, off, val)	\
	type_ParseFullFloatM((byte*)(str) + (off), val)

#define	type_ParseFullFloat(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullFloatO, type_ParseFullFloatM)(__VA_ARGS__))


// Function:
// ParseFullDouble(*str, *val)
// [ParseFullDouble]
// 
// Parses a full double precision floating-point number
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullDoubleF(string str, int64* val)
{
	num ret = -1;
	int lsti = strlen(str) - 1;
	char lst = str[lsti];
	if((lst != 'f') && (lst != 'F')) ret = type_ParseDouble(str, val);
	return ret;
}

#define	type_ParseFullDoubleM(str, val)	\
	type_ParseFullDoubleF((string)(str), (int64*)(val))

#define	type_ParseFullDoubleO(str, off, val)	\
	type_ParseFullDoubleM((byte*)(str) + (off), val)

#define	type_ParseFullDouble(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullDoubleO, type_ParseFullDoubleM)(__VA_ARGS__))


// Function:
// ParseFullString(*str, *val)
// [ParseFullString / ParseFullStr]
// 
// Parses a full string number in a string in format
// ('????') or ("????")
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
// 
num type_ParseFullStringF(string str, int64* val)
{
	num ret = -1;
	bool is_neg = false;
	if(*str == '-') is_neg = true;
	if(*str == '+' || *str == '-') str++;
	int lsti = strlen(str) - 1;
	char lst = str[lsti];
	if(((*str == '\'') && (lst == '\'')) || ((*str == '\"') && (lst == '\"')))
	{
		str[lsti] = '\0';
		ret = type_ParseStr(str, 1, val);
		str[lsti] = lst;
	}
	if(ret >= 0 && is_neg) *val = -(*val);
	return ret;
}

#define	type_ParseFullStringM(str, val)	\
	type_ParseFullStringF((string)(str), (int64*)(val))

#define	type_ParseFullStringO(str, off, val)	\
	type_ParseFullStringM((byte*)(str) + (off), val)

#define	type_ParseFullString(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseFullStringO, type_ParseFullStringM)(__VA_ARGS__))

#define	type_ParseFullStr	type_ParseFullString


// Function:
// ParseNumber(*str, *val)
// [ParseNumber / ParseNum]
// 
// Parses a number in a string
// 
// Parameters:
// *str:	string from which to parse
// *val:	pointer to value variable
// 
// Returns:
// num_type:	the type of number parsed
// 
#define	type_unknown		-1
#define	type_binary			1
#define	type_octal			2
#define	type_decimal		3
#define	type_hexadecimal	4
#define	type_float			5
#define	type_double			6
#define	type_string			7
#define	type_bin	type_binary
#define	type_oct	type_octal
#define	type_hex	type_hexadecimal
#define	type_str	type_string

num type_ParseNumberF(string str, int64* val)
{
	num ret = type_ParseFullBin(str, val);
	if(ret < 0) ret = type_ParseFullOct(str, val);
	if(ret < 0) ret = type_ParseFullHex(str, val);
	if(ret < 0) ret = type_ParseFullDec(str, val);
	if(ret < 0) ret = type_ParseFullFloat(str, val);
	if(ret < 0) ret = type_ParseFullDouble(str, val);
	if(ret < 0) ret = type_ParseFullStr(str, val);
	return ret;
}

#define	type_ParseNumberM(str, val)	\
	type_ParseNumberF((string)(str), (int64*)(val))

#define	type_ParseNumberO(str, off, val)	\
	type_ParseNumberM((byte*)(str) + (off), val)

#define	type_ParseNumber(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, type_ParseNumberO, type_ParseNumberM)(__VA_ARGS__))

#define	type_ParseNum			type_ParseNumber
#define	type_ParseFullNumber	type_ParseNumber
#define	type_ParseFullNum		type_ParseNumber


#endif
