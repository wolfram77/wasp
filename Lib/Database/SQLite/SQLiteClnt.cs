﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Data;
using System.Data.SQLite;
using System.Xml;
using Wasp.Data.Collection;

namespace Wasp.Database
{
	public class SQLiteClnt
	{
		protected string addr;
		protected SQLiteConnection conn;
		protected int operations;
		public DbEventFnGroup EventFn;
		public object Type;
		public SQLiteHostInfo HostInfo;
		public string Addr
		{
			get
			{ return addr; }
		}
		public SQLiteConnection Conn
		{
			get
			{ return conn; }
		}
		public bool Connected
		{
			get
			{ return (conn != null) && (conn.State == ConnectionState.Open); }
		}
		public int Operations
		{
			get
			{ return operations; }
		}


		// Create a database client
		public SQLiteClnt(string addr)
		{
			this.addr = addr;
		}
		public SQLiteClnt(SQLiteConnection conn)
		{
			addr = conn.ConnectionString;
			this.conn = conn;
		}

		// Connect
		protected void connect()
		{
			if ( !Connected )
			{
				if ( conn != null ) conn.Close();
				conn = new SQLiteConnection(addr);
				conn.Open();
			}
			if ( HostInfo != null )
			{
				HostInfo.Client.Remove(this);
				HostInfo.Client.Add(this);
			}
		}
		protected void task_Connect(object obj)
		{
			try
			{
				connect();
				if ( EventFn.Connected != null )
					EventFn.Connected(this, new DbEventArgs(DbEvent.Connected, this));
			}
			catch ( Exception err )
			{
				if ( EventFn.ConnectionFailed != null )
					EventFn.ConnectionFailed(this, new DbEventArgs(DbEvent.ConnectionFailed, this, err));
			}
		}
		public void Connect(bool async = false)
		{
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_Connect));
			else connect();
		}

		// Execute NonQuery
		protected int executeNonQuery(SQLiteCommand cmd)
		{
			cmd.Connection = conn;
			int rows = cmd.ExecuteNonQuery();
			operations++;
			if ( HostInfo != null )
			{
				lock ( HostInfo.OperationLock )
				{ HostInfo.Operations++; }
			}
			return rows;
		}
		protected void task_ExecuteNonQuery(object obj)
		{
			try
			{
				int rows = executeNonQuery((SQLiteCommand) obj);
				if ( EventFn.NonQueryExecuted != null )
					EventFn.NonQueryExecuted(this, new DbEventArgs(DbEvent.NonQueryExecuted, rows));
			}
			catch ( Exception err )
			{
				if ( EventFn.ExecutionFailed != null )
					EventFn.ExecutionFailed(this, new DbEventArgs(DbEvent.ExecutionFailed, obj, err));
			}
		}
		public int ExecuteNonQuery(SQLiteCommand cmd, bool async = false)
		{
			int rows = 0;
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_ExecuteNonQuery), cmd);
			else rows = executeNonQuery(cmd);
			return rows;
		}

		// Execute Scalar
		protected object executeScalar(SQLiteCommand cmd)
		{
			cmd.Connection = conn;
			object val = cmd.ExecuteScalar();
			operations++;
			if ( HostInfo != null )
			{
				lock ( HostInfo.OperationLock )
				{ HostInfo.Operations++; }
			}
			return val;
		}
		protected void task_ExecuteScalar(object obj)
		{
			try
			{
				object val = executeScalar((SQLiteCommand) obj);
				if ( EventFn.ScalarExecuted != null )
					EventFn.ScalarExecuted(this, new DbEventArgs(DbEvent.ScalarExecuted, val));
			}
			catch ( Exception err )
			{
				if ( EventFn.ExecutionFailed != null )
					EventFn.ExecutionFailed(this, new DbEventArgs(DbEvent.ExecutionFailed, obj, err));
			}
		}
		public object ExecuteScalar(SQLiteCommand cmd, bool async = false)
		{
			object val = null;
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_ExecuteScalar), cmd);
			else val = executeScalar(cmd);
			return val;
		}

		// Execute Reader
		protected SQLiteDataReader executeReader(SQLiteCommand cmd)
		{
			cmd.Connection = conn;
			SQLiteDataReader vals = cmd.ExecuteReader();
			operations++;
			if ( HostInfo != null )
			{
				lock ( HostInfo.OperationLock )
				{ HostInfo.Operations++; }
			}
			return vals;
		}
		protected void task_ExecuteReader(object obj)
		{
			try
			{
				SQLiteDataReader vals = executeReader((SQLiteCommand) obj);
				if ( EventFn.ReaderExecuted != null )
					EventFn.ReaderExecuted(this, new DbEventArgs(DbEvent.ReaderExecuted, vals));
			}
			catch ( Exception err )
			{
				if ( EventFn.ExecutionFailed != null )
					EventFn.ExecutionFailed(this, new DbEventArgs(DbEvent.ExecutionFailed, obj, err));
			}
		}
		public SQLiteDataReader ExecuteReader(SQLiteCommand cmd, bool async = false)
		{
			SQLiteDataReader vals = null;
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_ExecuteReader), cmd);
			else vals = executeReader(cmd);
			return vals;
		}

		// Disconnect
		protected void disconnect()
		{
			if ( conn != null )
			{
				try { conn.Close(); }
				catch ( Exception ) { }
				conn = null;
			}
			if ( HostInfo != null && HostInfo.AutoRemove)
				HostInfo.Client.Remove(this);
		}
		protected void task_Disconnect(object obj)
		{
			disconnect();
			if ( EventFn.Disconnected != null )
				EventFn.Disconnected(this, new DbEventArgs(DbEvent.Disconnected, this));
		}
		public void Disconnect(bool async = false)
		{
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_Disconnect));
			else disconnect();
		}

		// Close
		protected void close()
		{
			disconnect();
			addr = null;
			operations = 0;
			EventFn = null;
			Type = null;
			HostInfo = null;
		}
		protected void task_Close(object obj)
		{
			DbEventFn closed = EventFn.Closed;
			close();
			if ( closed != null )
				closed(this, new DbEventArgs(DbEvent.Closed, this));
		}
		public void Close(bool async = false)
		{
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_Close));
			else close();
		}
	}
}
