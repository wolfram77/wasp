﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SQLite;

namespace AppServer.Lib.Database
{
	public class Database
	{
		private static string FileName = "Data/Wasp1.db";
		private static string ConnString = "Datasource = ....";
		private static SQLiteConnection Conn;

		public static void Init()
		{
			if ( !File.Exists(FileName) ) SQLiteConnection.CreateFile(FileName);
			Conn = new SQLiteConnection(ConnString);
			Conn.Open();
		}
	}
}
