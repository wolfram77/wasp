﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Wasp.Data.Collection;

namespace Wasp.Database
{
	public static class MySqlCmd
	{
		// Set Parameters
		public static void SetParameters(MySqlCommand cmd, KeyVals prms)
		{
			KeyVals.Enumerator enm = prms.GetEnumerator();
			while (enm.MoveNext())
			{ cmd.Parameters.AddWithValue((string)enm.Current.Key, enm.Current.Value); }
			enm.Dispose();
		}
		public static void SetParameters(MySqlCommand cmd, params object[] prms)
		{
			for (int i = 0; i < prms.Length; i += 2)
			{ cmd.Parameters.AddWithValue((string)prms[i], prms[i + 1]); }
		}
	}
}
