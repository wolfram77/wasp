﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Wasp.Database
{
	public enum DbTaskType
	{
		Connect,
		Disconnect,
		Close,
		ExecuteNonQuery,
		ExecuteScalar,
		ExecuteReader,
		ExecuteXmlReader
	}

	public class DbTask
	{
		public DbTaskType Type;
		public object Cmd;

		// Create a Database task
		public DbTask(DbTaskType type)
		{
			Type = type;
		}
		public DbTask(DbTaskType type, object cmd)
		{
			Type = type;
			Cmd = cmd;
		}
	}
}
