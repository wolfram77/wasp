﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Wasp.Data.Collection;

namespace Wasp.Network
{
	public class TcpHost : NetHost
	{
		protected ParList<TcpClnt> client;
		public TcpHostInfo Info;
		public TcpClnt this[int indx]
		{
			get
			{ return client.ElementAt(indx); }
		}
		public int Clients
		{
			get { return client.Count; }
		}
		protected const int maxPendingConns = 16;
		protected const int noPktSleepTime = 16;
		protected static TimeSpan clientCheckTime = new TimeSpan(0, 2, 0);
		protected static TimeSpan clientRecheckTime = new TimeSpan(0, 2, 0);

		// Creates a TCP Host
		public TcpHost(IPEndPoint addr)
			: base(addr)
		{
			if ( addr != null )
			{
				sckt = new Socket(addr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
				sckt.ExclusiveAddressUse = true;
				sckt.Bind(addr);
				sckt.Listen(maxPendingConns);
			}
			// initialize
			client = new ParList<TcpClnt>(true);
			Info = new TcpHostInfo();
			Info.Host = this;
			Info.Client = client;
			Info.Rcvd = rcvd;
			Info.PktLock = new object();
			Info.ClientAccess = true;
			Info.AutoRemove = true;
			Info.AutoClose = true;
			// launch threads
			if ( addr != null ) ThreadPool.QueueUserWorkItem(new WaitCallback(thread_Accept));
			ThreadPool.QueueUserWorkItem(new WaitCallback(thread_ConnCheck));
		}
		public TcpHost(IPAddress addr, int port)
			: this(new IPEndPoint(addr, port))
		{
		}
		public TcpHost()
			: this(null)
		{
		}

		// Thread: Accept incoming connections (Server mode only)
		protected void thread_Accept(object obj)
		{
			try
			{
				while ( true )
				{
					// accept any new incoming connection
					TcpClnt clnt = new TcpClnt(sckt.Accept());
					clnt.HostInfo = Info;
					clnt.EventFn = EventFn;
					client.Add(clnt);
					if ( EventFn.NewClient != null )
						EventFn.NewClient(this, new NetEventArgs(NetEvent.NewClient, clnt));
				}
			}
			catch ( Exception ) { }
		}

		// Thread: Client Connection Checker
		protected void thread_ConnCheck(object obj)
		{
			int i = 0;
			while ( true )
			{
				// wait for some time before checking (each connection)
				Thread.Sleep(clientCheckTime);
				TcpClnt clnt = client.NextElement(ref i);
				// if client found, and is not connected, then
				if ( clnt != null && !clnt.Connected )
				{
					// remove the client from client list
					if ( EventFn.ConnectionLost != null )
						EventFn.ConnectionLost(this, new NetEventArgs(NetEvent.ConnectionLost, clnt));
					if ( Info.AutoRemove ) client.Remove(clnt);
					if ( Info.AutoClose ) clnt.Close();
				}
				// wait for recheck time once every client list traversal
				if ( i == 0 ) Thread.Sleep(clientRecheckTime);
			}
		}

		// Get Client
		public TcpClnt GetClient(int indx)
		{
			return client.ElementAt(indx);
		}

		// Add a new Client
		public TcpClnt AddClient(TcpClnt clnt, bool async = false)
		{
			if ( clnt.Addr == null ) return null;
			clnt.HostInfo = Info;
			clnt.EventFn = EventFn;
			if ( !clnt.Connected ) clnt.Connect(async);
			return clnt;
		}

		// Remove a Client
		public void RemoveClient(TcpClnt clnt, bool async = false)
		{
			clnt.Disconnect(async);
			client.Remove(clnt);
			if ( Info.AutoClose ) clnt.Close(async);
		}

		// Recieve Data (in indirect access mode)
		public NetPacket Receive(TcpClnt clnt)
		{
			return clnt.Receive();
		}

		// Send Data
		public override void Send(object clnt, NetPacket pkt, bool async = false)
		{
			((TcpClnt) clnt).Send(pkt, async);
		}
		public override void Send(NetPacket pkt, bool async = false)
		{
			Send(pkt.Addr, pkt, async);
		}

		// Send data to a type of clients
		public void SendToType(object type, NetPacket pkt, bool async = false)
		{
			foreach ( TcpClnt clnt in client )
			{
				if ( type == null || clnt.Type.Equals(type) )
				{ clnt.Send(pkt, async); }
			}
		}

		// Close the Host
		protected new void close()
		{
			base.close();
			Info.AutoRemove = false;
			Info.AutoClose = false;
			foreach ( TcpClnt clnt in client )
				clnt.Close();
			client.Close();
			client = null;
		}
		protected void task_Close(object obj)
		{
			NetEventFn closed = EventFn.Closed;
			close();
			if ( closed != null )
				closed(this, new NetEventArgs(NetEvent.Closed, this));
		}
		public override void Close(bool async = false)
		{
			if ( async ) ThreadPool.QueueUserWorkItem(new WaitCallback(task_Close));
			else close();
		}
	}
}
