﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wasp.Network
{
	public enum NetTaskType
	{
		Connect,
		Disconnect,
		Close,
		CheckReceive,
		Recieve,
		Send
	}

	public class NetTask
	{
		public object Sender;
		public NetTaskType Type;
		public object Data;

		// Create a Network task
		public NetTask(object sender, NetTaskType type, object data = null)
		{
			Sender = sender;
			Type = type;
			Data = data;
		}
	}
}
