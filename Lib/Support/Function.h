#ifndef	_Compile_Function_h_
#define	_Compile_Function_h_


// TypeOf
#if compiler_Name == compiler_Name_GCC
#define TypeOf(expr)	\
	typeof(expr)
#elif compiler_Name == compiler_Name_VisualC
#define	TypeOf(expr)	\
	decltype(expr)
#endif


// Asm
#define Asm(lines)	\
	__asm__ __volatile__(lines)


// AsmLn
#define AsmLn(text)	\
	text "\n\t"


// Attrib
#define Attrib(type)	\
	__attribute__((type))


// NoInline
#define NoInline	\
	Attrib(noinline)


// ForceInline
#define AlwaysInline	\
	Attrib(always_inline)


// Const
#define Const	\
	Attrib(const)


// Pure
#define Pure	\
	Attrib(pure)


// Interrupt
#define Interrupt	\
	Attrib(interrupt)


// Naked
#define Naked	\
	Attrib(naked)


// NoReturn
#define NoReturn	\
	Attrib(noreturn)


// NoThrow
#define NoThrow	\
	Attrib(nothrow)


// Section
#define Section(name)	\
	Attrib(section(name))


// Signal
#define Signal	\
	Attrib(signal)


// Unused
#define Unused	\
	Attrib(unused)


// Signal
#define Used	\
	Attrib(used)


// Packed
#define Packed	\
	Attrib(packed)


// Align
#define Align(size)	\
	Attrib(align(size))


// Deprecated
#define Deprecated	\
	Attrib(deprecated)


#endif
