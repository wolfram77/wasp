#ifndef	_Memory_Stack_h_
#define	_Memory_Stack_h_


// Register
#ifndef SP
#define SP		reg_DefineIO16(0x3D)
#endif
#ifndef SPL
#define SPL		reg_DefineIO8(0x3D)
#define SPH		reg_DefineIO8(0x3E)
#endif


// Init
inline void stack_Init()
{
	SP = ram_Size - 2;
}


#endif
