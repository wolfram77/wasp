#ifndef	_Memory_RAM_Dynamic_h_
#define	_Memory_RAM_Dynamic_h_


// MemRegion
#if platform_Name == platform_Name_Win32
typedef	HANDLE	MemRegion;
#else
typedef	uint	MemRegion;
#endif


// NewRegion
#if platform_Name == platform_Name_Win32
#define	mem_NewRegionIM(initsz, maxsz)	\
HeapCreate((DWORD)0, (SIZE_T)(initsz), (SIZE_T)(maxsz))

#define	mem_NewRegionM(maxsz)	\
mem_NewRegionIM(0, maxsz)

#define	mem_NewRegion(...)	\
macro_Fn(macro_Fn2(__VA_ARGS__, mem_NewRegionIM, mem_NewRegionM)(__VA_ARGS__))
#else
#define	mem_NewRegion(...)	null
#endif


// DefaultRegion
#if platform_Name == platform_Name_Win32
#define	mem_DefaultRegion()	\
GetProcessHeap()
#else
#define	mem_DefaultRegion()		null
#endif


// Delete Region
#if platform_Name == platform_Name_Win32
#define	mem_DeleteRegion(rgn)	\
HeapDestroy((HANDLE)(rgn))
#else
#define	mem_DeleteRegion(rgn)	null
#endif


// Alloc
#if platform_Name == platform_Name_Win32
#define	mem_AllocSO(rgn, sz, options)	\
HeapAlloc((HANDLE)(rgn), (DWORD)(options), (SIZE_T)(sz))

#define	mem_AllocS(rgn, sz)	\
mem_AllocSO(rgn, sz, 0)

#define	mem_AllocN(sz)	\
mem_AllocS(mem_DefaultRegion(), sz)

#define	mem_Alloc(...)	\
macro_Fn(macro_Fn3(__VA_ARGS__, mem_AllocSO, mem_AllocS, mem_AllocN)(__VA_ARGS__))
#else
#define	mem_Alloc(sz)	\
alloca((size_t)(sz))
#endif

#define	mem_initialize_to_zero	HEAP_ZERO_MEMORY
#define	mem_init_to_zero		mem_initialize_to_zero


// ReAlloc
#if platform_Name == platform_Name_Win32
#define	mem_ReAllocSO(rgn, ptr, sz, opt)	\
HeapReAlloc((HANDLE)(rgn), (DWORD)(opt), (LPVOID)(ptr), (SIZE_T)(sz))

#define	mem_ReAllocS(rgn, ptr, sz)	\
mem_ReAllocSO(rgn, ptr, sz, 0)

#define	mem_ReAllocN(ptr, sz)	\
mem_ReAllocS(mem_DefaultRegion(), ptr, sz)

#define	mem_ReAlloc(...)	\
macro_Fn(macro_Fn4(__VA_ARGS__, mem_ReAllocSO, mem_ReAllocS)(__VA_ARGS__))
#else
#define	mem_ReAlloc(...)	null
#endif


// Free
#if platform_Name == platform_Name_Win32
#define	mem_FreeR(rgn, ptr)	\
HeapFree((HANDLE)(rgn), (DWORD)0, (LPVOID)(ptr))

#define	mem_FreeN(ptr)	\
mem_FreeR(mem_DefRgn(), ptr)

#define	mem_Free(...)	\
macro_Fn(macro_Fn2(__VA_ARGS__, mem_FreeR, mem_FreeN)(__VA_ARGS__))
#else
#define	mem_Free(...)	null
#endif


#endif
