/*
----------------------------------------------------------------------------------------
	stream:  Data-block stream module for WASP with options for addressable
			 destination and busy indication for source memory
	File: std_stream.h

	Copyright � 2013 Subhajit Sahu. All rights reserved.

    This file is part of WASP. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

    WASP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WASP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WASP.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	stream is a data-block stream module for WASP with options for addressable destination and busy indication for source memory. For more information,
	please visit: https://github.com/wolfram77/WASP.
*/



#ifndef	_std_stream_h_
#define	_std_stream_h_



// Requisite headers
#include "std_macro.h"
#include "std_type.h"



// Select abbreviation level
// 
// The default abbreviation level is 7 i.e., members of this
// module can be accessed as stm<function_name> directly.
#ifndef	stream_abbr
#define	stream_abbr		7
#endif



// Stream Mold Making
// [Make / Mk]
// 
// Stream molds (structures) can be created with desired sizes. The Make() can be used to create such
// molds. The molds can then be used to create objects as stream_<size><type> <object>;
// 
#define	stream_Make(sz)	\
typedef struct _stream##sz	\
{	\
	uint16	type;		\
	num		front;		\
	num		count;		\
	num		size;		\
	byte*	srcF;		\
	num		lenF;		\
	byte*	src[sz];	\
	num		len[sz];	\
}stream##sz

#define	streamA_Make(sz)	\
typedef struct _streamA##sz	\
{	\
	uint16	type;		\
	num		front;		\
	num		count;		\
	num		size;		\
	byte*	srcF;		\
	num		lenF;		\
	byte*	dstF;		\
	byte*	src[sz];	\
	num		len[sz];	\
	byte*	dst[sz];	\
}streamA##sz

#define	streamB_Make(sz)	\
typedef struct _streamB##sz	\
{	\
	uint16	type;		\
	num		front;		\
	num		count;		\
	num		size;		\
	byte*	srcF;		\
	num		lenF;		\
	byte*	src[sz];	\
	num		len[sz];	\
	byte*	busy[sz];	\
}streamB##sz

#define	streamAB_Make(sz)	\
typedef struct _streamAB##sz	\
{	\
	uint16	type;		\
	num		front;		\
	num		count;		\
	num		size;		\
	byte*	srcF;		\
	num		lenF;		\
	byte*	dstF;		\
	byte*	src[sz];	\
	num		len[sz];	\
	byte*	dst[sz];	\
	byte*	busy[sz];	\
}streamAB##sz

#define	stream_Mk		stream_Make
#define	streamA_Mk		streamA_Make
#define streamB_Mk		streamB_Make
#define streamAB_Mk		streamAB_Make

// Stream Header format
// [header / hdr]
typedef struct _stream_header
{
	uint16	type;
	num		front;
	num		count;
	num		size;
	byte*	srcF;
	num		lenF;
}stream_header;

typedef struct _streamA_header
{
	uint16	type;
	num		front;
	num		count;
	num		size;
	byte*	srcF;
	num		lenF;
	byte*	dstF;
}streamA_header;

typedef struct _streamB_header
{
	uint16	type;
	num		front;
	num		count;
	num		size;
	byte*	srcF;
	num		lenF;
}streamB_header;

typedef struct _streamAB_header
{
	uint16	type;
	num		front;
	num		count;
	num		size;
	byte*	srcF;
	num		lenF;
	byte*	dstF;
}streamAB_header;

typedef stream_header	stream_hdr;
typedef streamA_header	streamA_hdr;
typedef streamB_header	streamB_hdr;
typedef streamAB_header	streamAB_hdr;

typedef struct _stream_block
{
	byte*	src;
	num		len;
}stream_block;

typedef struct _streamA_block
{
	byte*	src;
	num		len;
	byte*	dst;
}streamA_block;

typedef struct _streamB_block
{
	byte*	src;
	num		len;
	byte*	busy;
}streamB_block;

typedef struct _streamAB_block
{
	byte*	src;
	num		len;
	byte*	dst;
	byte*	busy;
}streamAB_block;

typedef stream_block	stream_blk;
typedef	streamA_block	streamA_blk;
typedef streamB_block	streamB_blk;
typedef streamAB_block	streamAB_blk;

stream_Make(2);
stream_Make(4);
stream_Make(8);
stream_Make(16);
stream_Make(32);
stream_Make(64);
stream_Make(128);
stream_Make(256);

streamA_Make(2);
streamA_Make(4);
streamA_Make(8);
streamA_Make(16);
streamA_Make(32);
streamA_Make(64);
streamA_Make(128);
streamA_Make(256);

streamB_Make(2);
streamB_Make(4);
streamB_Make(8);
streamB_Make(16);
streamB_Make(32);
streamB_Make(64);
streamB_Make(128);
streamB_Make(256);

streamAB_Make(2);
streamAB_Make(4);
streamAB_Make(8);
streamAB_Make(16);
streamAB_Make(32);
streamAB_Make(64);
streamAB_Make(128);
streamAB_Make(256);

#ifndef	stream_Def
#define	stream_Def		stream4
#endif

#ifndef	streamA_Def
#define	streamA_Def		streamA4
#endif

#ifndef	streamB_Def
#define	streamB_Def		streamB4
#endif

#ifndef	streamAB_Def
#define	streamAB_Def	streamAB4
#endif

typedef stream_Def		stream;
typedef	streamA_Def		streamA;
typedef	streamB_Def		streamB;
typedef	streamAB_Def	streamAB;

#if stream_abbr >= 3
#define	stm_Make	stream_Make
#define	stm_Mk		stream_Make
#define	stmA_Make	streamA_Make
#define	stmA_Mk		streamA_Make
#define	stmB_Make	streamB_Make
#define	stmB_Mk		streamB_Make
#define	stmAB_Make	streamAB_Make
#define	stmAB_Mk	streamAB_Make
#endif

#if stream_abbr >= 5
#define	streamMake	stream_Make
#define	streamMk	stream_Make
#endif

#if stream_abbr >= 7
#define	stmMake		stream_Make
#define	stmMk		stream_Make
#endif

#if	stream_abbr >= 10
#define	Make	stream_Make
#define	Mk		stream_Make
#endif



// Function:
// Initialize(strm)
// [Initialize / Init]
// 
// Initializes a stream before use.
// 
// Parameters:
// strm:	the stream to initialize
// 
// Returns:
// nothing
//
#define stream_Initialize(strm)	\
	macro_Begin	\
	(strm).type = 0;	\
	(strm).front = 0;	\
	(strm).count = 0;	\
	(strm).lenF = 0;	\
	(strm).size = ((sizeof(strm) - sizeof(stream_hdr)) / sizeof(stream_blk)) - 1;	\
	macro_End

#define	streamA_Initialize(strm)	\
	macro_Begin	\
	(strm).type = 1;	\
	(strm).front = 0;	\
	(strm).count = 0;	\
	(strm).lenF = 0;	\
	(strm).size = ((sizeof(strm) - sizeof(streamA_hdr)) / sizeof(streamA_blk)) - 1;	\
	macro_End

#define	streamB_Initialize(strm)	\
	macro_Begin	\
	(strm).type = 2;	\
	(strm).front = 0;	\
	(strm).count = 0;	\
	(strm).lenF = 0;	\
	(strm).size = ((sizeof(strm) - sizeof(streamB_hdr)) / sizeof(streamB_blk)) - 1;	\
	macro_End

#define	streamAB_Initialize(strm)	\
	macro_Begin	\
	(strm).type = 3;	\
	(strm).front = 0;	\
	(strm).count = 0;	\
	(strm).lenF = 0;	\
	(strm).size = ((sizeof(strm) - sizeof(streamAB_hdr)) / sizeof(streamAB_blk)) - 1;	\
	macro_End

#define	stream_Init		stream_Initialize
#define streamA_Init	streamA_Initialize
#define streamB_Init	streamB_Initialize
#define streamAB_Init	streamAB_Initialize

#if stream_abbr >= 3
#define	stm_Initialize		stream_Initialize
#define	stm_Init			stream_Initialize
#define	stmA_Initialize		streamA_Initialize
#define	stmA_Init			streamA_Initialize
#define	stmB_Initialize		streamB_Initialize
#define	stmB_Init			streamB_Initialize
#define	stmAB_Initialize	streamAB_Initialize
#define	stmAB_Init			streamAB_Initialize
#endif

#if stream_abbr >= 5
#define	streamInitialize	stream_Initialize
#define	streamInit			stream_Initialize
#endif

#if stream_abbr >= 7
#define	stmInitialize	stream_Initialize
#define	stmInit			stream_Initialize
#endif

#if	stream_abbr >= 10
#define	Initialize	stream_Initialize
#define	Init		stream_Initialize
#endif



// Function:
// Clear(strm)
// [Clear / Clr]
// 
// Clears a stream of all data.
// 
// Parameters:
// strm:	the stream to clear
// 
// Returns:
// nothing
//
#define stream_Clear(strm)	\
	((strm).count = 0)

#define streamA_Clear	stream_Clear
#define streamB_Clear	stream_Clear
#define streamAB_Clear	stream_Clear

#define	stream_Clr		stream_Clear
#define streamA_Clr		streamA_Clear
#define streamB_Clr		streamB_Clear
#define streamAB_Clr	streamAB_Clear

#if stream_abbr >= 3
#define	stm_Clear		stream_Clear
#define	stm_Clr			stream_Clear
#define	stmA_Clear		streamA_Clear
#define	stmA_Clr		streamA_Clear
#define	stmB_Clear		streamB_Clear
#define	stmB_Clr		streamB_Clear
#define	stmAB_Clear		streamAB_Clear
#define	stmAB_Clr		streamAB_Clear
#endif

#if stream_abbr >= 5
#define	streamClear		stream_Clear
#define	streamClr		stream_Clear
#endif

#if stream_abbr >= 7
#define	stmClear	stream_Clear
#define	stmClr		stream_Clear
#endif

#if	stream_abbr >= 10
#define	Clear	stream_Clear
#define	Clr		stream_Clear
#endif



// Function:
// GetAvailable(strm)
// [GetAvailable / GetAvail]
// 
// Gives the number of cells available in a stream. Can be used to check if
// sufficient data is available before reading.
// 
// Parameters:
// strm:	the stream whose available cells are to be known
// 
// Returns:
// cells:	number of available cells in stream
//
#define	stream_GetAvailable(strm)	\
	((strm).count)

#define streamA_GetAvailable	stream_GetAvailable
#define streamB_GetAvailable	stream_GetAvailable
#define streamAB_GetAvailable	stream_GetAvailable

#define	stream_GetAvail		stream_GetAvailable
#define streamA_GetAvail	streamA_GetAvailable
#define streamB_GetAvail	streamB_GetAvailable
#define streamAB_GetAvail	streamAB_GetAvailable

#if stream_abbr >= 3
#define	stm_GetAvailable	stream_GetAvailable
#define	stm_GetAvail		stream_GetAvailable
#define	stmA_GetAvailable	streamA_GetAvailable
#define	stmA_GetAvail		streamA_GetAvailable
#define	stmB_GetAvailable	streamB_GetAvailable
#define	stmB_GetAvail		streamB_GetAvailable
#define	stmAB_GetAvailable	streamAB_GetAvailable
#define	stmAB_GetAvail		streamAB_GetAvailable
#endif

#if stream_abbr >= 5
#define	streamGetAvailable	stream_GetAvailable
#define	streamGetAvail		stream_GetAvailable
#endif

#if stream_abbr >= 7
#define	stmGetAvailable		stream_GetAvailable
#define	stmGetAvail			stream_GetAvailable
#endif

#if	stream_abbr >= 10
#define	GetAvailable	stream_GetAvailable
#define	GetAvail		stream_GetAvailable
#endif


 
// Function:
// GetFree(strm)
// [GetFree]
// 
// Gives the number of free cells available in a stream. Can be used to
// check if sufficient free cells are available before writing.
// 
// Parameters:
// strm:	the stream whose number of free cells are to be known
// 
// Returns:
// cells:	number of free cells in stream
//
#define	stream_GetFree(strm)	\
	((strm).size - (strm).count)

#define streamA_GetFree		stream_GetFree
#define streamB_GetFree		stream_GetFree
#define streamAB_GetFree	stream_GetFree

#if stream_abbr >= 3
#define	stm_GetFree		stream_GetFree
#define	stmA_GetFree	streamA_GetFree
#define	stmB_GetFree	streamB_GetFree
#define	stmAB_GetFree	streamAB_GetFree
#endif

#if stream_abbr >= 5
#define	streamGetFree	stream_GetFree
#endif

#if stream_abbr >= 7
#define	stmGetFree		stream_GetFree
#endif

#if	stream_abbr >= 10
#define	GetFree		stream_GetFree
#endif



// Function:
// Write(strm, *src, len)
// <A>Write(strm, *src, len, *dst)
// <B>Write(strm, *src, len, *busy)
// <AB>Write(strm, *src, len, *dst, *busy)
// [Write / Wrt]
// 
// Writes a data block to the stream. The data buffer should not be modified
// as long as busy is indicated.
// 
// Parameters:
// status:	status variable for getting the status
// strm:	the stream to which data buffer is to be written
// *src:	source data buffer to be written to the stream
// len:		number of bytes to be written to stream from source
// *dst:	destination address (on an addressable device)
// *busy:	pointer to a "busy" byte indicating that source data buffer is being used
// 
// Returns:
// status: 0 for success, -1 / 0xFF for failure
//
#define stream_Write(strm, psrc, plen)	\
	macro_Begin	\
	num status;	\
	if(stream_GetFree(strm))	\
	{	\
		status = ((strm).front + (strm).count) & ((strm).size - 1);	\
		(strm).src[status] = (byte*)(psrc);	\
		(strm).len[status] = (num)(plen);	\
		(strm).count++;	\
		status = 0;	\
	}	\
	else status = (num)-1;	\
	status;	\
	macro_End

#define streamA_Write(strm, psrc, plen, pdst)	\
	macro_Begin	\
	num status;	\
	if(streamA_GetFree(strm))	\
	{	\
		status = ((strm).front + (strm).count) & ((strm).size - 1);	\
		(strm).src[status] = (byte*)(psrc);	\
		(strm).len[status] = (num)(plen);	\
		(strm).dst[status] = (byte*)(pdst);	\
		(strm).count++;	\
		status = 0;	\
	}	\
	else status = (num)-1;	\
	status;	\
	macro_End

#define streamB_Write(strm, psrc, plen, pbusy)	\
	macro_Begin	\
	num status;	\
	if(streamB_GetFree(strm))	\
	{	\
		status = ((strm).front + (strm).count) & ((strm).size - 1);	\
		(strm).src[status] = (byte*)(psrc);	\
		(strm).len[status] = (num)(plen);	\
		(strm).busy[status] = (byte*)(pbusy);	\
		(strm).count++;	\
		*((byte*)(pbusy)) = 1;	\
		status = 0;	\
	}	\
	else status = (num)-1;	\
	status;	\
	macro_End

#define streamAB_Write(strm, psrc, plen, pdst, pbusy)	\
	macro_Begin	\
	num status;	\
	if(streamAB_GetFree(strm))	\
	{	\
		status = ((strm).front + (strm).count) & ((strm).size - 1);	\
		(strm).src[status] = (byte*)(psrc);	\
		(strm).len[status] = (num)(plen);	\
		(strm).dst[status] = (byte*)(pdst);	\
		(strm).busy[status] = (byte*)(pbusy);	\
		(strm).count++;	\
		*((byte*)(pbusy)) = 1;	\
		status = 0;	\
	}	\
	else status = (num)-1;	\
	status;	\
	macro_End

#define	stream_Wrt		stream_Write
#define streamA_Wrt		streamA_Write
#define streamB_Wrt		streamB_Write
#define streamAB_Wrt	streamAB_Write

#if stream_abbr >= 3
#define	stm_Write		stream_Write
#define	stm_Wrt			stream_Write
#define	stmA_Write		streamA_Write
#define	stmA_Wrt		streamA_Write
#define	stmB_Write		streamB_Write
#define	stmB_Wrt		streamB_Write
#define	stmAB_Write		streamAB_Write
#define	stmAB_Wrt		streamAB_Write
#endif

#if stream_abbr >= 5
#define	streamWrite		stream_Write
#define	streamWrt		stream_Write
#endif

#if stream_abbr >= 7
#define	stmWrite	stream_Write
#define	stmWrt		stream_Write
#endif

#if	stream_abbr >= 10
#define	Write	stream_Write
#define	Wrt		stream_Write
#endif



// Function:
// Pop(strm)
// [Pop]
// 
// Pops a block of data from the stream (unbusy the memory block).
// 
// Parameters:
// status:	variable for getting status
// strm:	the stream from which a block of data is to be popped
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failure
//
#define stream_Pop(strm)	\
	macro_Begin	\
	num	status;	\
	if(stream_GetAvail(strm))	\
	{	\
		(strm).front = ((strm).front + 1) & ((strm).size - 1);	\
		(strm).count--;		\
		(strm).lenF = 0;	\
		status = 0;			\
	}	\
	else status = (num)-1;	\
	status;	\
	macro_End

#define streamA_Pop		stream_Pop

#define streamB_Pop(strm)	\
	macro_Begin	\
	num	status;	\
	if(stream_GetAvail(strm))	\
	{	\
		*((strm).busy[(strm).front]) = 0;	\
		(strm).front = ((strm).front + 1) & ((strm).size - 1);	\
		(strm).count--;		\
		(strm).lenF = 0;	\
		status = 0;			\
	}	\
	else status = (num)-1;	\
	status;	\
	macro_End

#define streamAB_Pop	streamB_Pop

#if stream_abbr >= 3
#define	stm_Pop		stream_Pop
#define	stmA_Pop	streamA_Pop
#define	stmB_Pop	streamB_Pop
#define	stmAB_Pop	streamAB_Pop
#endif

#if stream_abbr >= 5
#define	streamPop	stream_Pop
#endif

#if stream_abbr >= 7
#define	stmPop		stream_Pop
#endif

#if	stream_abbr >= 10
#define	Pop		stream_Pop
#endif



// Function:
// Read(src, len, strm)
// <A>Read(src, len, dst, strm)
// <B>Read(src, len, busy, strm)
// <AB>Read(src, len, dst, busy, strm)
// [Read / Rd]
//
// Reads a chunk of data from the stream. Returns pointer to the data block.
// After reading is complete, unbusy must be done manually.
//
// Parameters:
// strm:	the stream from which a data block is to be read
// dst:		destination address for an addressable end (device)
// len:		length of the data block to be read
//
// Returns:
// src:		source address of data block (null for no data)
//
#define stream_Read(psrc, plen, strm)	\
	macro_Begin	\
	num	status;	\
	if(stream_GetAvail(strm))	\
	{	\
		if((strm).lenF == 0)	\
		{	\
			(strm).srcF = (strm).src[(strm).front];	\
			(strm).lenF = (strm).len[(strm).front];	\
		}	\
		psrc = (strm).srcF;	\
		plen = (strm).lenF;	\
		(strm).front = ((strm).front + 1) & ((strm).size - 1);	\
		(strm).count--;		\
		(strm).lenF = 0;	\
		status = 0;	\
	}	\
	else status = (num)-1;	\
	status;	\
	macro_End

#define streamA_Read(psrc, plen, pdst, strm)	\
	macro_Begin	\
	num	status;	\
	if(stream_GetAvail(strm))	\
	{	\
		if((strm).lenF == 0)	\
		{	\
			(strm).srcF = (strm).src[(strm).front];	\
			(strm).lenF = (strm).len[(strm).front];	\
			(strm).dstF = (strm).dst[(strm).front];	\
		}	\
		psrc = (strm).srcF;	\
		plen = (strm).lenF;	\
		pdst = (strm).dstF;	\
		(strm).front = ((strm).front + 1) & ((strm).size - 1);	\
		(strm).count--;		\
		(strm).lenF = 0;	\
		status = 0;	\
	}	\
	else status = (num)-1;	\
	status;	\
	macro_End

#define streamB_Read(psrc, plen, pbusy, strm)	\
	macro_Begin	\
	num	status;	\
	if(stream_GetAvail(strm))	\
	{	\
		if((strm).lenF == 0)	\
		{	\
			(strm).srcF = (strm).src[(strm).front];	\
			(strm).lenF = (strm).len[(strm).front];	\
		}	\
		psrc = (strm).srcF;	\
		plen = (strm).lenF;	\
		pbusy = (strm).busy[(strm).front];	\
		(strm).front = ((strm).front + 1) & ((strm).size - 1);	\
		(strm).count--;		\
		(strm).lenF = 0;	\
		status = 0;	\
	}	\
	else status = (num)-1;	\
	status;	\
	macro_End

#define streamAB_Read(psrc, plen, pdst, pbusy, strm)	\
	macro_Begin	\
	num	status;	\
	if(stream_GetAvail(strm))	\
	{	\
		if((strm).lenF == 0)	\
		{	\
			(strm).srcF = (strm).src[(strm).front];	\
			(strm).lenF = (strm).len[(strm).front];	\
			(strm).dstF = (strm).dst[(strm).front];	\
		}	\
		psrc = (strm).srcF;	\
		plen = (strm).lenF;	\
		pdst = (strm).dstF;	\
		pbusy = (strm).busy[(strm).front];	\
		(strm).front = ((strm).front + 1) & ((strm).size - 1);	\
		(strm).count--;		\
		(strm).lenF = 0;	\
		status = 0;	\
	}	\
	else status = (num)-1;	\
	status;	\
	macro_End

#define	stream_Rd	stream_Read
#define stream_


#if stream_abbr >= 3
#define	stm_Read	stream_Read
#define	stm_Rd		stream_Read
#endif

#if stream_abbr >= 5
#define	streamRead	stream_Read
#define	streamRd	stream_Read
#endif

#if stream_abbr >= 7
#define	stmRead		stream_Read
#define	stmRd		stream_Read
#endif

#if	stream_abbr >= 10
#define	Read	stream_Read
#define	Rd		stream_Read
#endif



// Function:
// ReadByte(*strm, *dst)
// ReadByte(*strm)
// [ReadByte / RdByte]
// 
// Reads a byte from a stream. Use this function only if data is available.
// This function "pops" a byte from stream.
// 
// Parameters:
// *strm:	the stream from which a byte is to be read
// *dst:	destination address on an addressable end (device)
// 
// Returns:
// val:		byte value, which is valid only if stream has some data
//


byte stream_ReadByteF(stream* strm, byte** dst)
{
	num front;
	byte val;
	if(stream_GetAvail(strm))
	{
		front = strm->front;
		val = *(strm->chunk[front].src);
		if(dst) *dst = strm->chunk[front].dst;
		(strm->chunk[front].dst)++;
		(strm->chunk[front].src)++;
		if(--(strm->chunk[front].len) == 0)
		{
			*(strm->chunk[front].busy) = 0;
			if(++front == strm->size) front = 0;
			strm->front = front;
			strm->count--;
		}
		return val;
	}
	return (byte)-1;
}

#define stream_ReadByteA(strm, dst)	\
	stream_ReadByteF((stream*)(strm), (byte**)(dst))

#define	stream_ReadByteNA(strm)	\
	stream_ReadByteF((stream*)(strm), null)

#define	stream_ReadByte(...)	\
macro_Fn(macro_Fn2(__VA_ARGS__, stream_ReadByteA, stream_ReadByteNA)(__VA_ARGS__))

#define	stream_RdByte	stream_ReadByte

#if stream_abbr >= 3
#define	stm_ReadByte	stream_ReadByte
#define	stm_RdByte		stream_ReadByte
#endif

#if stream_abbr >= 5
#define	streamReadByte	stream_ReadByte
#define	streamRdByte	stream_ReadByte
#endif

#if stream_abbr >= 7
#define	stmReadByte		stream_ReadByte
#define	stmRdByte		stream_ReadByte
#endif

#if	stream_abbr >= 10
#define	ReadByte	stream_ReadByte
#define	RdByte		stream_ReadByte
#endif



#endif
