/*
----------------------------------------------------------------------------------------
	tcppkt: Implements TCP Packet
	File: wasp_tcppkt.h

    This file is part of WASP. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

    WASP is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WASP is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WASP.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	tcppkt is an an implementation of TCP packet for WASP. For more information,
	please visit: https://github.com/wolfram77/WASP.
*/



#ifndef	_std_list_h_
#define	_std_list_h_



// Requisite headers
#include "std_macro.h"
#include "std_type.h"
#include "std_math.h"



// Select abbreviation level
// 
// The default abbreviation level is 1 i.e., members of this
// library can be accessed as lst<function_name>.
#ifndef	list_abbr
#define	list_abbr	7
#endif



// List Mold Making
// [Make / Mk]
// 
// List molds (structures) can be created with desired sizes and datatypes. The Make() can be used to
// create such molds. The molds can then be used to create objects as list_<size><type> <object>; Size
// of the defined list must always be a power of 2.
// 
#define	list_Make(typ, sz)	\
typedef struct _list_##sz##typ	\
{	\
	uint16	type;		\
	num		count;		\
	num		size;		\
	typ		val[sz];	\
}list_##sz##typ

#define	list_Mk		list_Make

// List Header format
// [header / hdr]
typedef struct _list_header
{
	uint16	type;
	num		count;
	num		size;
}list_header;

typedef	list_header		list_hdr;

#if list_abbr >= 3
#define	lst_Make	list_Make
#define	lst_Mk		list_Make
#endif

#if list_abbr >= 5
#define	listMake	list_Make
#define	listMk		list_Make
#endif

#if list_abbr >= 7
#define	lstMake		list_Make
#define	lstMk		list_Make
#endif

#if list_abbr >= 10
#define	Make	list_Make
#define	Mk		list_Make
#endif

list_Make(byte, 2);
list_Make(byte, 4);
list_Make(byte, 8);
list_Make(byte, 16);
list_Make(byte, 32);
list_Make(byte, 64);
list_Make(byte, 128);
list_Make(byte, 256);

#ifndef list_Def
#define	list_Def	list_32byte
#endif

typedef list_Def	list;



// Function:
// Initialize(lst)
// [Initialize / Init]
// 
// Initializes a list before use.
// 
// Parameters:
// lst:		the list to initialize
// 
// Returns:
// nothing
//
#define	list_Initialize(lst)	\
	macro_Begin	\
		(lst).type = sizeof((lst).val[0]);	\
		(lst).count = 0;	\
		(lst).size = (sizeof(lst) - sizeof(list_hdr)) / sizeof((lst).val[0]);	\
	macro_End

#define	list_Init	list_Initialize

#if list_abbr >= 3
#define	lst_Initialize		list_Initialize
#define	lst_Init			list_Initialize
#endif

#if list_abbr >= 5
#define	listInitialize	list_Initialize
#define	listInit		list_Initialize
#endif

#if list_abbr >= 7
#define	lstInitialize	list_Initialize
#define	lstInit			list_Initialize
#endif

#if	list_abbr >= 10
#define	Initialize	list_Initialize
#define	Init		list_Initialize
#endif



// Function:
// Clear(lst)
// [Clear / Clr]
// 
// Clears a list of all data.
// 
// Parameters:
// lst:		the list to clear
// 
// Returns:
// nothing
//
#define	list_Clear(lst)	\
		((lst).count = 0)

#define	list_Clr	list_Clear

#if list_abbr >= 3
#define	lst_Clear	list_Clear
#define	lst_Clr		list_Clear
#endif

#if list_abbr >= 5
#define	listClear	list_Clear
#define	listClr		list_Clear
#endif

#if list_abbr >= 7
#define	lstClear	list_Clear
#define	lstClr		list_Clear
#endif

#if	list_abbr >= 10
#define	Clear	list_Clear
#define	Clr		list_Clear
#endif



// Function:
// GetAvailable(lst)
// [GetAvailable / GetAvail]
// 
// Gives the number of elements available in a list.
// 
// Parameters:
// lst:		the list whose available pairs are to be known
// 
// Returns:
// avail:	number of available elements in list
//
#define	list_GetAvailable(lst)	\
	((lst).count)

#define	list_GetAvail	list_GetAvailable

#if list_abbr >= 3
#define	lst_GetAvailable	list_GetAvailable
#define	lst_GetAvail		list_GetAvailable
#endif

#if list_abbr >= 5
#define	listGetAvailable	list_GetAvailable
#define	listGetAvail		list_GetAvailable
#endif

#if list_abbr >= 7
#define	lstGetAvailable		list_GetAvailable
#define	lstGetAvail			list_GetAvailable
#endif

#if	list_abbr >= 10
#define	GetAvailable	list_GetAvailable
#define	GetAvail		list_GetAvailable
#endif



// Function:
// GetFree(lst)
// [GetFree]
// 
// Gives the number of free cells available in a list.
// 
// Parameters:
// lst:		the list whose number of free cells are to be known
// 
// Returns:
// free:	number of free cells in list
//
#define	list_GetFree(lst)	\
	((lst).size - (lst).count)

#if list_abbr >= 3
#define	lst_GetFree		list_GetFree
#endif

#if list_abbr >= 5
#define	listGetFree		list_GetFree
#endif

#if list_abbr >= 7
#define	lstGetFree		list_GetFree
#endif

#if	list_abbr >= 10
#define	GetFree			list_GetFree
#endif



// Function:
// IndexOf(lst, elem)
// [IndexOf / IndxOf]
// 
// Get the index of an element from the list.
// 
// Parameters:
// lst:		the list from which index of element is required
// elem:	the element, whose index in the list is required
// 
// Returns:
// indx:	index of element (-1 / 0xFF for not found)
//
#define list_IndexOf(lst, elem)	\
	macro_Begin	\
	num indx;	\
	for(indx = 0; indx < (lst).count; indx++)	\
	if((lst).val[indx] == (elem)) break;	\
	indx = (indx != (lst).count)? indx : (num)-1;	\
	indx;	\
	macro_End

#define	list_IndxOf		list_IndexOf

#if list_abbr >= 3
#define	lst_IndexOf		list_IndexOf
#define	lst_IndxOf		list_IndexOf
#endif

#if list_abbr >= 5
#define	listIndexOf		list_IndexOf
#define	listIndxOf		list_IndexOf
#endif

#if list_abbr >= 7
#define	lstIndexOf	list_IndexOf
#define	lstIndxOf	list_IndexOf
#endif

#if	list_abbr >= 10
#define	IndexOf		list_IndexOf
#define	IndxOf		list_IndexOf
#endif



// Function:
// Add(lst, elem)
// [Add]
// 
// Add an element to the list. If sufficient space is not
// available, then the element will not be added.
// 
// Parameters:
// lst:		the list to which element is to be added
// elem:	element to be added
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failed
//
#define list_Add(lst, elem)	\
	((list_GetFree(lst))? (((lst).val[(lst).count++] = (elem)) & 0) : (num)-1)

#if list_abbr >= 3
#define	lst_Add		list_Add
#endif

#if list_abbr >= 5
#define	listAdd		list_Add
#endif

#if list_abbr >= 7
#define	lstAdd		list_Add
#endif

#if	list_abbr >= 10
#define	Add		list_Add
#endif



// Function:
// RemoveAt(lst, indx)
// [RemoveAt / RmvAt]
// 
// Removes an element at a specified index of list.
// 
// Parameters:
// lst:		the list from which an element is to be removed
// indx:	index of the element to be removed
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failed
//
#define list_RemoveAt(lst, indx)	\
	(((indx) < (lst).count)? (((lst).val[indx] = (lst).val[((lst).count--) - 1]) & 0) : (num)-1)

#define	list_RmvAt		list_RemoveAt

#if list_abbr >= 3
#define	lst_RemoveAt	list_RemoveAt
#define	lst_RmvAt		list_RemoveAt
#endif

#if list_abbr >= 5
#define	listRemoveAt	list_RemoveAt
#define	listRmvAt		list_RemoveAt
#endif

#if list_abbr >= 7
#define	lstRemoveAt		list_RemoveAt
#define	lstRmvAt		list_RemoveAt
#endif

#if	list_abbr >= 10
#define	RemoveAt	list_RemoveAt
#define	RmvAt		list_RemoveAt
#endif



// Function:
// Remove(lst, elem)
// [Remove / Rmv]
// 
// Removes an element from the list.
// 
// Parameters:
// status:	status variable for returning
// lst:		the list from which element is to be removed
// elem:	the element to be removed
// 
// Returns:
// status:	0 for success, -1 / 0xFF for failed
//
#define list_RemoveV(lst, elem)	\
	macro_Begin	\
	num status;	\
	status = list_IndexOf(lst, elem);	\
	if(status != (num)-1) status = list_RemoveAt(lst, status);	\
	status;	\
	macro_End

#define	list_Remove(...)	\
	macro_Fn(macro_Fn3(__VA_ARGS__, list_RemoveR, list_RemoveV)(__VA_ARGS__))

#define	list_Rmv	list_Remove

#if list_abbr >= 3
#define	lst_Remove	list_Remove
#define	lst_Rmv		list_Remove
#endif

#if list_abbr >= 5
#define	listRemove	list_Remove
#define	listRmv		list_Remove
#endif

#if list_abbr >= 7
#define	lstRemove	list_Remove
#define	lstRmv		list_Remove
#endif

#if	list_abbr >= 10
#define	Remove		list_Remove
#define	Rmv			list_Remove
#endif



#endif
